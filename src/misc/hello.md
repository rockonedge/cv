Hi Mr. Lobey, 

I was writing to you primarily because I was concurred with you on the trend of electrification, not just in the auto industry. 




I am thrilled to see the recent trend of electrification in the auto industry.

I believe I am seeing a living history that, after about 100 years, electrical engineering has finally approaching its peak by taking on one of the largest industrial sectors ever. 

And I want to be part of it. 

Rooted in electrical engineering firmly, I've seen how its been evolving through advances in electronics(which, in essence, is about decoupling of information from electrical *energy*) and software(which, in essence, is flexible management of electronics in large scale). 

I've gone through and played a core role in the transformation from hydraulic to electrification for lower energy consumption and finer control while working for Husky Injection Molding System. 

Along with my previous experience in Schneider Electric and STMicroelectronics, I could be a truly valuable asset, should there be an opportunity to be part of the electrification revolution.

It happens I am looking for opportunities currently.

I'm wondering if you'd like to take a look at my profile and see if there's a match between your team requirement and my background.

Looking forward to hearing from you.

Best regards, 
Tom

===================================================

Hi Mr. Kroepfl,

I am looking for some opportunities for senior technical management in a global company like Orbotech. 

I'm wondering if there's any opening in your team that my background could possibly fit in.

I'd appreciate it very much if you could take a look at my most up-to-date CV online below:

https://ttan.netlify.com/cv
https://ttan.netlify.com/projects-en

https://ttan.netlify.com/pdf/cv-en.pdf (printable)

Looking forward to hearing from you.

Thanks,
Tom  

=============================================================

> Hi Mr. Wyatt, 

I am looking for some opportunities on engineering management in a global company like Viasystems. 

I am wondering if you would like to read my CV, or better yet, have a face to face talk. 

I live in Pudong, so it'll be convenient if so desired. 

Thanks, Tom

=============================================================

> Hi Mr.  Devantier

I am looking for some opportunities on R&D management in a global company . 

I am wondering if  you have a team to lead or interested in starting up a team here in Shanghai. If so, I'd like to introduce myself for  that kind of endeavor. 

Le me know  if you would like to read my CV, or better yet, have a talk over the phone.

Thanks, Tom

=============================================================


Hi Mr. Zimmerman,

I am specifically looking at heading a R&D team designing electronic systems combining software and hardware.

While I was looking for opportunities in medical devices domain. I was very impressed by what Dräger is doing, but I could not found any job information.

I reached out to you, because I believed you are in charge, and in the power of initiating such R&D teams/activities locally if there's none yet and you see opportunities there.

I can be a valuable asset to your team to build up local technical strength if there's a match.

By the way, you could find my most updated cv and feature projects here:

https://ttan.netlify.com/cv-en-zh
https://ttan.netlify.com/projects-en

Looking forward to hearing from you.

Tom


=================
To whom it may concern,

I came across your company on linkedin and was very much attracted to your culture, which I firmly believed in and cultivated one while working for Husky.

While my past experience had little to do with the auto industry, it was all software related, for electronic/electrical systems in particular.

I understand that you are looking for an engineer, I am leveraging this application channel to expose myself to you in case you are open to other options, such as a manager position to build a bigger team here in Shanghai/China. 

Thanks for reading & looking forward to hearing from you.

Best regards,
Tom
====================================

To whom it may concern,

I am applying for the Senior Software Engineering Manager position in Optiver.

While my past experience had little to do with the trading/HFT industry, it was all software related. After all, a computer is an electrical device,and I often see software as an offspring of electrical engineering(Father) and math(Mother).

I was nodding all the way reading your job description, and this below cited part is exactly I started out and succeeded in Husky a decade ago, among others:

"You will have extensive technical knowledge and know how to build and manage small teams (10+) of exceptionally talented and passionate technologists including Developers and Infrastructure engineers."  

If you, like me, would like to think out of the box and offer an interview for better understanding,
I'd appriciate it very much, and I bet it's worth it.

Looking forward to hearing from you.

Best regards,
Tom 