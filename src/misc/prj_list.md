
赫斯基
=====
     
- 上海工厂启动
    - 引进了拿大总部工厂的生产支持系统 （Polaris）及流程
    - 招聘，培训了首批生产支持 （装配和测试）工程团队
- 建立了第一个全球研发工程团队， 全部由硕士研究生以上学历组成。
- 领导开发用于所有赫斯基注塑机的 Polaris 软件平台， 适用于 Hylectric， HyPET， HyPET HPP， HyCAP， HyperSync， Barrier等机型， 包括
    - Polaris HMI（人机界面）， 
        - 维护性开发基于 COM/VB6 平台HMI
        - 为新一代的注塑机重新架构和实现了基于 .NET/C# 平台的HMI
        - 十多年来，保持上述平台按季度/半年定期发布
    - Polaris PLC，基于 TwinCAT/CODESYS 技术的 PLC 编程以实现注射成型的实时控制相关算法，例如
        - 伺服驱动(Servo)
        - 机器人控制等。
    - Polaris Production System， 此系统是内部各部门 （研发， 生产，售后支持服务等）都协同公作的公共 SDLC（Software Development Life Cycle） 平台
- 领导 HyperSync 注塑机系列工业 4.0 相关特性，获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。        
- 领导开发统计过程控制（SPC）及 统计质量控制 （SQC） 模块
- 领导开发多语言翻译管理系统 （Translation Website） 的开发，简化软件开发中的全球化支持（22+语言）
- 领导开发基于 RuBee（IEEE 1902.1） 无线通讯技术的嵌入式模具识别（Mold ID） 解决方案
- 发起扁鹊（Bianque）项目，融合 Azure IoT Hub， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术。
- 领导[收购整合 Moldflow 制造部门][#BUYMF]的法国与美国团队资产， 并后续支持既有客户
    - 基于 Windows Server 采用 C++/Delphi/Java 开发的工厂过程监控系统系统Celltrack，Shotscope
    - 基于 Linux/Fedora 采用 C++/QT/MySQL 开发的热流道温度控制器 Matrix
- 领导开发 Shotscope NX， 一套基于 web 技术的过程和生产监控自动化实时解决方案。
- 引入 SCRUM/Test-Driven Development（TDD） 敏捷（Agile）开发流程，把源代码控制从 VSS 迁移到 TFS。        
- 领导上海团队成员参与了中国区，越南，墨西哥等地的全球 PLT 支持。 
    - PLT 是一个临时由研发部门设计人员或技术专家组建（因为此时生产和售前/后部门还不具备相关产品知识）， 为尚未正式推出的新机型早期客户提供现场安装调试服务的团队。是 Husky 推出新机型的传统程序。    
- 业余时间开发了看板网站应用 Whiteboard，基于 Python/Django， C#/ASP.NET/RESTful Service/WebAPI 和 JavaScript/Dojo Toolkit，Apache Cordova， 综合 Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 UNC 映射盘等数据源信息
    - 便利了团队领导随时随地在线查看任何项目和状态
    - 在诸如
        - 每日站立会议（daily standup meeting）
        - 部门周会（Weekly team meeting）
        - 项目里程碑会议（milestone meeting）
    等日常或临时会议中简化了与会成员背景介绍时间，迅速进入主题。   
 
    
意法半导体
=========

- 主导开发了基于 uPSD ( Intel8032 内核)的 BIOS Memory (M50xx FWH/LPC 接口系列)参考设计；
- 基于 uPSD (Intel8032内核)的Serial Flash (M25/45xx SPI接口代码和数据存储系列)参考设计；
- 基于 PC 和 ST7 (Motorola内核),  USB接口的 Flash 编程烧录器( Serial Flash Programmer )；
    - 基于以前VB6的原型, 完全以C++/汇编从 PC 端到 MCU 端重新设计的US接口的Serial Flash Programmer, 被ST时期的法国同事商业化, 成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列, 并邀请我协助开发至SF600

- 移植 uCLinux 于 STR71x。
- 移植 uC/OS 于 STR71x。
- 一套项目管理软件(约5万行 C++ 代码), 用于跟踪同步上海, 意大利, 法国, 捷克之间的项目进度。

施耐德电气
=========

- 完成了自动应急电源切换系统的原型设计 (前后3套), 填补市场上只有组合方案, 没有集成产品的空白。 此系统用于医院手术室, 重要的政府设施等需要连续供电, 并配有备用电源的机构, 在紧急情况下自动切换电源
- 协同设计了基于MCU和PID算法的恒温试验控制箱, 用于电路断路器/接触器/PLC 高温老化测试, 提供恒定温度高达 75°
- 为实验室开发了一套基于关系数据库的国产化试验管理软件, 用于实验室数据分析, 采用了VB6/C++以 及 MATLAB
- 小型空气断路器 ASIC 电路设计的改进
    - VC65
    - DPN Vigi
- 从波兰工厂引进开关电源的前期研究报告, 包括
    - 设计了各类性能, 可靠性实验, 用以与本地品牌对比
    - 逆向工程本地品牌, 完成成本分
- Compact NS断路器认证实验
- 工业用LED光报警灯设计


哈工大车辆电器研究所
=================

- 电子镇流器自动化测试系统
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 《低压电器》发表相关论文“铁路客车电子镇流器逆变器综合试验台的研制”
    
- 低压电器自动化测试系统
    - 经黑龙江省科委专家组技术评审鉴定“居于国内领先”。
    - 相关论文发表于第八届国际电器可靠性会议。
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 2002 年 12 月获黑龙江省教育厅科技进步一等奖
    - 2003 年 5 月获黑龙江省科技厅科技进步二等奖