---
- at: sparetime
  category: projects
  duration:
    end: '2017'
    start: '2017'
  keywords:
  - Python
  - HTML
  - Boostrap
  - JQuery
  language: en
  name: Markdown-CV
  summary: ''
  title: Markdown-CV
- at: sparetime
  category: projects
  duration:
    end: '2011'
    start: '2011'
  keywords:
  - C++
  - Image
  - Graphics
  language: en
  name: 2D-Water-Effect-in-WTL
  summary: ''
  title: 2D Water Effect in WTL
- at: sparetime
  category: projects
  duration:
    end: '2004'
    start: '2004'
  keywords: []
  language: en
  name: A-Literal-Converter-for-Integers
  summary: ''
  title: A Literal Converter for Integers
- at: sparetime
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: TwinCAT-TSM-File-Viewer
  summary: ''
  title: TwinCAT TSM File Viewer
- at: sparetime
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: Multi-Ping
  summary: ''
  title: Multi-Ping
- at: sparetime
  category: projects
  duration:
    end: '2012'
    start: '2012'
  keywords:
  - C++
  - TCP
  - socket，Boost.ASIO
  - Boost.spirit
  language: en
  name: HostLinkpp-An-Implementation-of-HostLink-inter-machine-Protocol-
  summary: ''
  title: 'HostLinkpp - An Implementation of HostLink inter-machine Protocol '
- at: sparetime
  category: projects
  duration:
    end: '2016'
    start: '2012'
  keywords: []
  language: en
  name: Console3
  summary: ''
  title: Console3
- at: sparetime
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: Beckhoff-TwinCAT-ADS-Protocol
  summary: ''
  title: Beckhoff TwinCAT ADS Protocol
---
# Spare-time Projects

## Markdown-CV
**2017**
**Python, HTML, Boostrap, JQuery**

A set of scripts and templates to generate html and PDF files from resume written in pure text, i.e. [Markdown][#MD], leveraging [Bootstrap][#BOOTSTRAP] themes and other open source code such as [marked js][#MARKEDJS].

- open sourced on [GitHub][#MDCV]
- Written in Python, HTML, CSS, JavaScript, [Bootswatch][#BOOTSWATCH]/[Bootstrap 4.0 alpha][#BOOTSTRAP]

- [link][#MDCV]

**This very resume is generated with this tool**

[#MDCV]: https://github.com/rockonedege/markdown-cv
[#BOOTSTRAP]: http://v4-alpha.getbootstrap.com/
[#BOOTSWATCH]: http://bootswatch.com/
[#MD]: http://daringfireball.net/projects/markdown/
[#MARKEDJS]: https://github.com/chjj/marked

## 2D Water Effect in WTL
2011
C++, Image, Graphics

A hobby project while learning about image processing and graphics rendering.  
- Open sourced with an article on [The Codeproject][#2DWTL].
- Written in C++/WTL

- [#2DWTL]

[#2DWTL]: https://www.codeproject.com/Articles/188236/D-Water-Effect-in-WTL


## A Literal Converter for Integers
2004

A small utility for myself to help process the data file from the oscilloscope, which I used to record data going through Serial Peripheral Interface(SPI) ports while verifying the flash memory chips.

- Open sourced with an article on [The Codeproject][#LITERAL]
- Written in C++

- [link][#LITERAL]

[#LITERAL]: https://www.codeproject.com/Articles/10379/A-Literal-Converter-for-Integers


## TwinCAT TSM File Viewer
**2016**

A binary opaque format, named tsm, is used in TwinCAT 2 for configurations, which is where hardware *meets* software in the TwinCAT-based control system, specifically it maps software *variables* with Hardware I/O *signals*

- loads variables from software logic written in PLC/CNC/C++ etc.
- scans hardware I/O devices such as cards, boxes, terminals
- scheduling: exchanges values to-and-from in a timely manner(i.e. as tasks).

Reading directly from the tsm file is the most reliable way at runtime to extract the information for extension.

Husky's HMI has been structured in a way that's closely coupled to a local TwinCAT runtime,  which has hindered its evolution in two ways
- engineering efficiency
  - even simple non-PLC HMI change requires to fire up the whole simulation environment in complicated VMWare image, which, in turn, makes it hard to bring in new bloods to the team, as the learning curve for such simple tasks is is very unusually steep and daunting for new engineers

- innovations for industry 4.0
  To Quote Microsoft, this world is heading toward **cloud first** and **mobile first**. A universal set of UI that enables people to monitor, or operate with properly configured access rights is a must to be industry 4.0 compatible.

- product roadmap
  while it's been long desired to unify the platforms for the Machines and HotRunner controllers business to strengthen the one-Husky brand, duplicate development and features have to be paid and the customers have to operator on two separate HMI/Touch screens to get their job done.

I figured decyphering the tsm file and hosting it through a web service would be an ideal decoupling solution
- minimal risk
 - leaving the PLC algorithms intact largely.
- maximum extensibility of HMI
   - with modern UI frameworks, the HMI can be develop only once to fit all client platforms. declarative style UI can be developed any platform, mobile or desktop, deployable to local, on-premises servers or on cloud.
   - dependency on the PLC can be easily simulated with a simple JSON file in most cases.

I personally completed the proof of concept with some open source tool
- [websocketd](http://websocketd.com/)
- [ADS, Beckhoff protocol to communicate with TwinCAT devices](https://github.com/rockonedege/ADS)
 and in-house coding based on TwinCAT APIs.

C++, Python, Javascript and some Go were used in the prototyping.

I did not have a chance to present the concepts and the work I've done before leaving Husky, Though planned to.

## Multi-Ping
**2016**

While integrating Altanium controllers into Horizon machines, one of the features was to detect the connection of a controller, and fetch its APIPA address, which became a challenge due to a maximum of delay was allowed.  

The built-in .NET classes did not cut as the it resulted in **10s at best and 40s on average**, and cost about 700MB memory or higher till the host HMI apllication crashes.

Guessing that it might be the wrapping of .NET over the raw system sockets, I implemented the functionality with C++/Boost.Asio and leveraged asynchrous techniques, and was able to complete the detection within **2s** using around **5MB** memory.

The algorithm was then wrapped 
- through C++/CLI as a .Net Assembly for consumption of Polaris HMI
- as a trouble-shooting  utility apllication for testers on the shopfloor and service people in the field.  

## HostLinkpp - An Implementation of HostLink inter-machine Protocol 
**2012**
**C++, TCP, socket?Boost.ASIO, Boost.spirit**

![](./pics/hostlinkpp-ubuntu.png)

A C++ Implementation of Husky Hostlink, a proprietary raw sockets based binary protocol accessing Husky injection machines, e.g.

- issuing commands  
- reading statuses and cycle data

It has been adopted by some major customers to integrate Husky systems into their OPC/ERP systems, who requested more commands(e.g reading injection cycle breakdown data) to be supported, hence the project.

I personally wrote the whole code, based on the original MFC based code written in around 2000, as an illustration and teaching material of standard C++ for efficient, cross-platform system related code.

- added more advanced commands to fetch extended cycle data available in new generations of machines  
- fixed bugs specific to 64-bit, which was not considered when the original code was written
- replaced ad-hoc command  construction&parsing with Boost.Spirit and Boost.Qi
- replaced MFC based sockets communicated with Boost.ASIO
- supported platforms beyond Windows to Linux(tested Ubuntu)
- supported Python binding with Boost.Python
- supported .NET binding with C++/CLI

This project leverage quite a few  [Boost][#BOOST] libraries such as [Boost.Asio][#ASIO], [Boost.Spirit][#SPIRIT]， [Boost.PP][#PP]，[Boost.Python][#PYTHON] and advanced modern C++ techniques such as metaprogramming.

Built with CMake.

The code was not open sourced because the Hostlink protocol is proprietary.

[#BOOST]: http://www.boost.org/
[#ASIO]: http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html
[#SPIRIT]: http://boost-spirit.com/home/
[#PP]: http://www.boost.org/doc/libs/1_63_0/libs/preprocessor/doc/index.html
[#PYTHON]: http://www.boost.org/doc/libs/1_63_0/libs/python/doc/html/index.html


## Console3
**2012 - 2016**

A fork of [Console 2][#CONSOLE2]

- fixed bugs
- replaced rendering GDI from to [Direct2D][#]/[DirectWrite][#DW] 
- added Husky-specific feature with integration to Husky tools

This tool was widely used by the Shanghai team.

[#CONSOLE2]: http://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx
[#D2D]: https://msdn.microsoft.com/zh-cn/library/dd370990(v=vs.85).aspx
[#DW]: https://msdn.microsoft.com/zh-cn/library/windows/desktop/dd368038(v=vs.85).aspx


## Beckhoff TwinCAT ADS Protocol
**2016**

A fork of the open source implementation of Beckhoff ADS protocol

- added support of TwinCAT 2

- [link][#ADS]

[#ADS]: https://github.com/rockonedege/ADS



