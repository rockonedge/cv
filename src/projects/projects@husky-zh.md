---
- at: husky
  category: projects
  duration:
    end: '2006'
    start: '2005'
  keywords:
  - 团队筹建，　项目管理
  language: zh
  name: 上海注塑机系统工厂筹建
  summary: ''
  title: 上海注塑机系统工厂筹建
- at: husky
  category: projects
  duration:
    end: '2006'
    start: '2006'
  keywords:
  - 团队筹建
  language: zh
  name: 全球研发工程团队筹建
  summary: ''
  title: 全球研发工程团队筹建
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2006'
  keywords:
  - 项目管理
  language: zh
  name: Polaris-HMI-人机界面-开发
  summary: ''
  title: Polaris HMI（人机界面） 开发
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2006'
  keywords:
  - 项目管理
  language: zh
  name: Polaris-PLC开发
  summary: ''
  title: Polaris PLC开发
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2006'
  keywords:
  - 项目管理
  language: zh
  name: Polaris-生产管理系统开发
  summary: ''
  title: Polaris 生产管理系统开发
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords:
  - 项目管理
  language: zh
  name: HyperSync-工业-4-0-特性开发
  summary: ''
  title: HyperSync 工业 4.0 特性开发
- at: husky
  category: projects
  duration:
    end: '2011'
    start: '2011'
  keywords:
  - C++
  - 代码编写
  language: zh
  name: 统计过程控制-SPC-及-统计质量控制-SQC-模块-开发
  summary: ''
  title: 统计过程控制（SPC）及 统计质量控制 （SQC） 模块 开发
- at: husky
  category: projects
  duration:
    end: '2007'
    start: '2007'
  keywords:
  - C#
  - 服务器开发
  language: zh
  name: 多语言翻译管理系统-Translation-Website-开发-
  summary: ''
  title: 多语言翻译管理系统 （Translation Website） 开发，
- at: husky
  category: projects
  duration:
    end: '2010'
    start: '2009'
  keywords:
  - 无线通讯
  - 网络通讯，嵌入式，　硬件烧录
  language: zh
  name: 嵌入式模具自识别-Mold-ID-解决方案
  summary: ''
  title: 嵌入式模具自识别（Mold ID） 解决方案
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2014'
  keywords:
  - 云计算
  - 大数据，机器智能，IoT
  - 机器智能
  language: zh
  name: 扁鹊-Bianque-生产管理系统
  summary: ''
  title: 扁鹊（Bianque）生产管理系统
- at: husky
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords:
  - 公司兼并，技术融合
  language: zh
  name: Celltrack-Shotscope-收购整合
  summary: ''
  title: Celltrack，Shotscope 收购整合
- at: husky
  category: projects
  duration:
    end: '2008'
    start: '2006'
  keywords: []
  language: zh
  name: 敏捷-Agile-开发流程
  summary: ''
  title: 敏捷（Agile）开发流程
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords:
  - 客户支持，项目管理，团队领导
  language: zh
  name: 全球-PLT-支持
  summary: ''
  title: 全球 PLT 支持
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords: []
  language: zh
  name: 项目进度看板网站应用-Whiteboard
  summary: ''
  title: 项目进度看板网站应用 Whiteboard
- at: husky
  category: projects
  duration:
    end: '2017.01'
    start: '2015.11'
  keywords: []
  language: zh
  name: 上海注塑机生产工厂组建
  summary: ''
  title: 上海注塑机生产工厂组建
- at: husky
  category: projects
  duration:
    end: '2016.12'
    start: '2005.12'
  keywords: []
  language: zh
  name: Polaris-注塑机实时控制系统
  summary: ''
  title: Polaris 注塑机实时控制系统
- at: husky
  category: projects
  duration:
    end: '2008.01'
    start: '2006.06'
  keywords:
  - 桌面软件设计
  - 第3方软件集成
  language: zh
  name: Polaris-HMI-NET-架构与实现
  summary: ''
  title: Polaris HMI .NET 架构与实现
- at: husky
  category: projects
  duration:
    end: '2015.09'
    start: '2008.06'
  keywords:
  - SCADA
  - 网络
  - WCF
  language: zh
  name: Shotscope-NX
  summary: 一套基于 web 技术的过程和生产监控自动化实时解决方案。
  title: Shotscope NX
- at: husky
  category: projects
  duration:
    end: '2015.09'
    start: '2014.06'
  keywords: []
  language: zh
  name: 嵌入式模具无线自动识别-Wireless-Mold-ID-管理系统
  summary: ''
  title: 嵌入式模具无线自动识别（Wireless Mold ID）管理系统
- at: husky
  category: projects
  duration:
    end: '2007.04'
    start: '2006.12'
  keywords:
  - 桌面软件设计
  - 硬件信息实时采集
  language: zh
  name: HyMET-HeatLogger
  summary: ''
  title: HyMET HeatLogger
- at: husky
  category: projects
  duration:
    end: '2006.07'
    start: '2006.05'
  keywords:
  - 软件设计
  - COM/ATL
  language: zh
  name: HkResource-多国语言支持数据库
  summary: ''
  title: HkResource 多国语言支持数据库
- at: husky
  category: projects
  duration:
    end: '2007.07'
    start: '2006.11'
  keywords:
  - 服务器软件设计
  - 网站设计
  language: zh
  name: 多语言翻译在线管理系统
  summary: ''
  title: 多语言翻译在线管理系统
- at: husky
  category: projects
  duration:
    end: '2016.10'
    start: '2016.02'
  keywords:
  - 软件设计
  - 系统集成
  - 硬件采集
  language: zh
  name: HyperSync-Altanium-工业4-0集成
  summary: ''
  title: HyperSync-Altanium 工业4.0集成
- at: husky
  category: projects
  duration:
    end: '2016.08'
    start: '2016.04'
  keywords:
  - 软件设计
  - 系统集成
  - 网络通讯
  language: zh
  name: Altanium-远程桌面
  summary: 基于 RDP 协议的嵌入式远程桌面, 实现对　Altanium　温度控制器的远程控制
  title: Altanium 远程桌面
- at: husky
  category: projects
  duration:
    end: '2016.12'
    start: '2013.04'
  keywords:
  - 软件设计
  - 系统集成
  - 网络通讯
  language: zh
  name: 扁鹊
  summary: ''
  title: 扁鹊
---
# 赫斯基注塑系统有限公司

## 筹建（上海）全球研发工程部
2006 - 2007 
团队与项目管理, 团队筹建

建立加拿大总部以外唯一一个全球研发工程团队， 全部由硕士研究生以上学历组成。

## 注塑机人机界面 (HMI) 开发
2006 - 2016
项目管理, 软件架构, 软件编码
领导开发用于所有赫斯基注塑机的 Polaris HMI 基础软件平台及各种衍生版本，涵盖所有系列注塑机系统。

- 维护性开发基于 COM/VB6 平台HMI
- 重新架构和实现了新一代 HMI,基于 .NET/C# 平台的
- 十多年来，保持上述平台按 SCRUM 开发季度/半年定期滚动发布

## 注塑机PLC 开发
2006 - 2016
团队与项目管理, 运动控制， CNC, 软件开发 
实时控制相关算法。 包括伺服驱动(Servo),机器人控制等运动控制，以及温度控制， 第三方设备通讯等。基于 IEC 61131 标准，及 TwinCAT/CODESYS 运行环境。

## 注塑机工厂 DevOps 生产管理系统开发
2006 - 2016
团队与项目管理, 软件架构，软件开发
Polaris Production System， 此系统赫斯基注塑机控制系统软件开发，模拟，测试和发布的 DevOps 平台。也是车间内部各部门 （研发， 生产，售后支持服务等）的协同工作平台。

基于COM/Visual Basic, Ruby, C#/.NET等技术。

## HyperSync/Altanium 工业 4.0 特性开发
2016 - 2016
团队与项目管理, 软件架构, 代码编写
基于 TwinCAT 实时工业互联网技术。获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。

## 统计过程/质量控制（SPC & SQC）模块
2011 - 2011
代码编写, 数据科学, C++, DirectX, 图形编程

实时采集注塑周期各项参数，进行 统计过程控制（SPC）及 统计质量控制 （SQC） 计算，6-sigma 生产过程控制要求，并提供多样图表绘制。

## 在线 HMI 多语言翻译管理系统 
2016 - 2016
项目管理,软件架构,服务器软件,网站设计,C#, SQL, ASP.NET 

一体化软件界面多语言开发管理系统，支持22+语言。支持数据库查询，外部翻译请求，审批，上下文场景管理，多语言资源文件生成管理等。基于C#, ASP.NET, SQL Server开发。

该项目由上海开发，部署，日常维护支持全球业务。

该网站的开发使得原来的人工过程自动化, 
- 申请新字符串翻译从原来大约 2 星期等待 减少到 0 阻塞实时异步, 并能
- 提供更多上下文信息用于提高翻译准确性。
还提供了其他查询管理高级新功能。
 
为了支持多达 22+ 种语言 HMI 显示, 开发人员在开发控制系统软件时涉及到任何字符串均需, 事先向专职人员申请并获得特定 ID 替代原字符串用于开法中, 以支持用户使用时界面语言动态翻译。


## 兼并 Moldflow 制造部门 MES/SCADA 产品
2008
管理, 公司兼并,  知识产权转移, 资产接收, 技术融合

领导[收购整合 Moldflow 制造部门](http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit)的法国与美国团队资产， 并后续支持既有客户
如欧莱雅，利乐包装等。产品包括开发的工厂过程监控系统系统Celltrack，Shotscope 及热流道温度控制器 Matrix。

## Celltrack MES/SCADA系统维护&开发 
2008
管理,  公司兼并, 知识产权转移, 资产接收, 技术融合, JAVA

在 8 周以内，从原公司法国团队接收基于 Windows Server 采用 Java 的工厂生产过程监控系统， 原始代码编写于 1990 年代初。有稳定欧洲市场客户。

## Shotscope MES/SCADA系统维护&开发
2008
管理,  公司兼并, 知识产权转移, 资产接收, 技术融合,   C++, Delphi, Firmware

在 8 周以内，从原公司法国团队接收基于  Windows Server 采用 C++/Delphi 开发的工厂生产过程监控系统， 原始代码编写于 1990 年代初。有稳定北美市场客户。
还包括嵌入式数据采集监控系统，及 Firmware 。

## 热流道实时温度控制系统 Matrix
2008
管理,  公司兼并, 知识产权转移, 资产接收, 技术融合,  C++, QT, MySQL
从原公司美国团队接收，并持续开发新功能， 包括嵌入式电子硬件及基于 Linux/Fedora 采用 C++/QT/MySQL 开发软件 HMI.


## 引入确立敏捷（Agile）开发流程
2006-2008
管理, Agile, SCRUM, TDD 

布道引入 SCRUM/Test-Driven Development（TDD） 敏捷（Agile）开发模式， 建立了现代软件生命周期(Software Life Cycle)的开发流程， 实践标准， 以及配套工具。 把以 VSS 源代码控制为中心的__瀑布式__传统模式, 升级到 __需求，文档和源代码__并举的__测试驱动(TDD)敏捷开发__模式。期间多次赴加拿大参与建立流程及相关工具评估(比如 IBM Rational Rose vs. Team Foundation Server(TFS))。


## 新注塑机产品预上市 (Product Launch Trip)
2011 - 2016
管理,新产品上市,全球客户现场支持

作为研发传统，每当新产品研发结束，型号正式成型上市之前，都会有研发部门自组团队，在早期测试客户现场安装，调试，交付，并收集相关过程注意事项，编写相关文档。以备正式推出后培训生产，和技术服务部门。

从 2011 年起，领导上海团队参与客户全球工厂新型注塑系统安装，调试及交付，遍及__中国，越南，墨西哥，澳大利亚__等全球各地。客户包括可口可乐， 利乐包装，康师傅，哇哈哈， 养生堂等。 


## 项目与团队管理 Dashboard 应用 
2011 - 2016
管理, 软件架构, 软件编码, 项目管理, 敏捷开发
基于 micro service 项目管理网站。

基于 Python/Django， C#/ASP.NET/RESTful Service/WebAPI 和 JavaScript/Dojo Toolkit，Apache Cordova，NodeJS 综合 Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 网络映射盘等数据源信息
    - 便利了团队及项目管理人员随时随地在线查看任何项目和状态
    - 在敏捷开发实践中
        - 每日站立会议（daily standup meeting）
        - 部门周会（Weekly team meeting）
        - 项目里程碑会议（milestone meeting）
    等日常或临时会议中简化了与会成员背景介绍时间，迅速进入主题。

![](./pics/wb-overview.png) 
![](./pics/wb-projects.png) 

## 上海注塑机生产工厂组建
**2015.11- 2017.01**
团队筹建,项目管理

适逢赫斯基战略性扩大中国投入, 决定在此建立注塑机生产工厂， 并把亚太地区总部从香港迁至上海。虽然注塑行业对我而言是全新的领域。

加入公司立刻赴加拿大总部工作，并在第一年里， 频繁往返于两地，一半时间在加拿大，一半时间在上海, 一边接受高强度的相关的产品， 工具， 及工作流程培训， 一边组建培训团队，建立车间生产，组装及测试的软件流程。

- 移植了庞大的生产软件系统(Polaris Production System)
    - 包括 500G+ 生产数据， 因数据太大，多次网络传输失败后， 我不得不带上硬盘大年三十从加拿大飞回上海。
    - 此系统严重加拿大工厂的本地 IT 环境， 不得不与两地 IT 部门紧密合作，并解决许多技术与非技术问题，在上海重建相似环境， 并与总部数据库连接同步，实现全球生产数据与流程的统一化管理
- 招聘组建了第一批软件，电气控制系统支持团队
    - 很多时候在不得不从加拿大通过电话面试


最终成功为上海工厂建立生产支持系统（Polaris）及流程，与加拿大，卢森堡工厂数据对接同步。招聘，培训了首批生产支持 （装配和测试）工程团队

由于在此项目的贡献，获得杰出贡献奖， 由时任全球机器事业部副总裁（Global VP of Machines）签署颁发。

## Polaris 注塑机实时控制系统
**2005.12- 2016.12**
软件编码, 项目管理, 敏捷开发

Polaris注塑机实时控制系统是一个基于工业控制计算机的注塑机操作系统（Injection Operating System）及与之配套的装机发布系统（Production Deployment System）。

操作系统（Injection Operating System）通过控制基于 Profibus，SERCOS，EtherCAT 等接口的电气系统，实现原料加热，注射，机器人控制等注塑成型周期过程的运动控制和自动化。此系统包括

- 人机界面( HMI ) 
    - 提供友好的人机界面，故障诊断，状态监控，参数调整等上层功能；
- 软 PLC(soft PLC) 组成:
    - 实现PID温度控制，各种电机运动控制算法和实时性等底层控制。

每季度发行一次基本版本升级，包含bug修复，新功能，以及与最新的机械和电气设计相对应的更动，并作为定制功能的最新平台。

装机发布系统（Production Deployment System）根据用户需求及每一台机器的机械与电气配置，选择相应的软件模块，把操作系统（Injection Operating System）定制安装到注塑机上。

项目职责：

- 在上海工厂的建立过程中，主导建立了控制软件的本地装机发布系统（Production Deployment System），使生产车间组装后装机测试成为可能，从而为2006年生产线顺利投产提供前提条件。
- 参与培训了第一批生产线控制软件安装测试工程师（Controls Test Engineer Team）。
- 领导维护本地装机发布系统（Production System）并支持车间生产，直至2007年建立专门的生产支持团队(Production Team)。
- 持续领导并参与此系统相关工作的一切开发，维护，季度升级工作等等，涉及

- C++, C#, VB, IEC61131 PLC 程序设计的开发维护
- Profibus, SERCOS, EtherCAT 接口硬件调试故障诊断
- 第三方辅助系统集成， 比如机器人(Robots)等。


## Polaris HMI .NET 架构与实现
**2006.06 - 2008.01**
**桌面软件设计, 第3方软件集成**
 
由于现有控制系统 HMI 已经使用多年

- 经过多年的修改维护，已经演变得性能降低(比如40秒软件启动时间)，众多临时特性的叠加也使用户操作复杂， 系统庞大而难以维护；
- 作为公司第一代迁移自 PLC 的 PC/softPLC 架构控制系统, 软件构架上不成熟而难以扩展；
- 大量代码基于 Visual Basic 6, 难于添加现代 UI 体验及功能
- 软件工程思想和开发工具的发展使得重构原有系统变得可能而且代价低廉。

该项目组织大量人员梳理现有功能, 基于.NET/C#, 重新架构和编写 HMI 的 UI 及 service 部分。

本人职责包括

- 阅读原系统代码及设计文档，分析，提取，重组设计需求，找出性能瓶颈
- 参与设计确认软件架构
- 技术培训讲解并领导上海团队确保 TDD/SCRUM 开发过程实施, 确保开发进度和代码质量。
- 编写部分代码
- 招聘工程师充实项目组

该应用部署于运行 Windows Embedded XP 的工控机( Industrial PC ), 技术涉及Windows Form, WPF, C#, C++/CLI, SQL Server等。


## Shotscope NX 车间生产管理系统
**2008.06 - 2015.09**
**SCADA, 网络,WCF**
一套基于 web 技术的过程和生产监控自动化实时解决方案。
 
整合现有 SmartLink, Shotscope, CellTrack 过程与生产监控系统软件和硬件，实现生产制造企业数字化管理，包括

- 基于 Window Server 的网站服务器软件，
- 基于工控机的数据采集器（Data Collector）控制软件
    - 适用于 Non-Husky 注塑机客户采集生产过程状态等数据

该系统

- 上端连接**企业上层 IT 系统**等第三方系统（ 如 ERP ）
- 下端连接机器运行状态（实时周期参数），能源消耗，物料分配与消耗， 工班安排等**实时生产车间数据** 
    - 对于 Husky 品牌注塑机，从其控制软件内建相关模块，以私有软件协议，获得专属详尽的生产细节数据，比如
        - Husky Polaris 注塑机实时控制系统 
        - Husky Altanium 热流道控制系统 
              
    - 对于 非 Husky 品牌第三方注塑机， 通过
        - 标准工业**软件协议**，如 [OPC][#OPC], 从第三方机器控制系统获取数据
        - Husky 专用数据采集器以 Side-by-Side 方式**硬件采集**数据

开发内容包括

- 服务器软件开发
- Polaris Polaris 注塑机实时控制系统相关 HMI 及数据接口开发 
- Data Collector 专属定制及工业标准(SPI, OPC, USB, RS422 等) 数据协议开发

利用 WCF, Silverlight, SQL Server 等C#/.NET技术系统，使其与其前身系统相比

- 更友好直观的操作界面与数据呈现， 报表生成能力， 
- 更广泛，分布式的监控能力。
    - 单车间
    - 全球跨地域车间
- 更强大的数据存储与处理能力

该产品按计划于 2009.6北美塑胶展（NPE Show 2009）前完成初步版本，作为主展产品之一，[在展会上取得巨大成功，并获得用户热烈欢迎][#NPE].

初期开发阶段项目职责：

- 领导协调上海团队与加拿大，卢森堡同事及印度外包公司开发的分工
- 与销售部门同事协作，转化用户需求（User Story）为可实现的技术特性(Feature)
- 参与制定阶段目标，划分可独立实现的单项任务(Work Item)，
- 制定以2星期为周期的迭代开发计划，并确保其实施，每一个Milestone能按计划达到
- 确保测试驱动开发（TDD）等开发模式和工程标准的实施
- 当项目方向不明确或出现争议时，做出决策，并推动项目前进


从产品推出后， 根据市场需求，上海团队一直专人负责升级开发中， ，直至 2015 年产品转由 After-Market Service Engineering 支持，期间内容包括

- 性能优化
- 客户专有生产流程模板
- 客户专用软，硬件接口支持

更多关于该产品的功能综述， [请参考其主页][#SSNX].

[#NPE]: http://www.husky.ca/abouthusky/news/content-20090608.html
[#SSNX]: http://www.husky.co/EN-US/Shotscope-NX.aspx
[#OPC]: http://baike.baidu.com/item/opc/3875


## 嵌入式模具无线自动识别 IoT 解决方案
**2010.06 - 2015.09**
无线通讯, 网络通讯, 嵌入式, 硬件烧录

基于 [RuBee(IEEE 1902.1)][#RUBEE] 无线通讯技术的工业 IoT 解决方案，实现模具的自动识别，参数加载及寿命磨损管理。, 实现赫斯基领先全球的快速换模技术。 内容包括：

- 服务器数据管理
    - Windows Server 网络服务器程序开发
    - 编写 Mold ID Server 门户网站, 做为全球唯一数据中心，存储所有生产，售出的模具参数信息。
    - 在设计阶段供模具工程师录入，修改模具信息
    - 供生产工程师，服务工程师在线下载每套模具信息供生产现场或客户现场升级注塑机/模具配置
- Polaris HMI 集成
    - Windows 桌面程序
    - 从 Polaris HMI 中无线检测 Rubee Tags 并从中读取模具参数功能
- Tag 烧写工具维护工具
    - Windows 桌面程序
    - 模具信息以 [RuBee(IEEE 1902.1)][#RUBEE] 协议无线烧写进 Tag
    - 在有限的 256 字节内压缩存储众多模具信息
    - 连上公司内部网路 (Intranet)，可以通过全球烧写, 比如上海的工程师可以远程对加拿大工厂的Tag 烧写。

该项目在我的领导下由上海开发完成，服务器部署于上海，由上海团队维护支持全球业务。

开发内容涉及 SharePoint, Silverlight, WinForm 以及 Socket 网络通讯。

[#RUBEE]: http://www.baike.com/wiki/rubee

## HyMET HeatLogger
**2006.12- 2007.04**
**桌面软件设计, 硬件信息实时采集**
一位资深工程师特意写信给我
> 这是我在 Husky 见过的最漂亮的软件工具

![](./pics/heatlogger-main.png) 
监测, 记录 HyMET 合金注塑机上各热电偶监测到的热流道( Hot Runner )温度, 供

- 设计工程师验证设计, 
- 服务工程师解决客户现场故障。

技术要求包括：

- 同时采集多达100通道数, 采集周期在 1s-180s 范围内可调。
- 因为程序安装于全球客户生产现场, 由 Husky 服务工程师每 3 个月至 6 个月取回数据供设计工程师分析, 
    - 程序必须占用尽可能少的系统资源 
    - 程序不能被客户意外终止 
    - 程序能自动监测注塑机运行/停止状态, 智能停止或重新启动记录, 并保证记录数据不能因为意外掉电而丢失。
    - 预测记录数据大小, 以保证在无人监控时导致记录空间不足。
 
该项目全部以 C++ 编写, 

- 利用 TWinCAT ADS API 实时采集硬件信息, 
- 利用开源库 [The POCO C++ Libraries][#POCO] 管理配置信息和数据持久化, 
- 利用 [WTL][#WTL]/ATL 架构主体程序及用户界面, 呈现实时信息, 用户权限认证管理。

设计领导解决方案及架构, 并亲自编写除 TWinCAT ADS API 之外全部代码, 作为范例讲授 Windows/C++ GUI, 高性能（低CPU占有率，低内存使用率）后台监控应用程序设计相关知识。
并在 [POCO C++ Libraries][#POCO] 的使用过程中, 改进其 XML 处理模块, 回馈社区, 位列 [Contributers][#CONTRIB]。

该应用部署于运行 Windows Embedded XP 的工控机( Industrial PC )。



[#WTL]: https://en.wikipedia.org/wiki/Windows_Template_Library
[#CONTRIB]: https://pocoproject.org/community/contributors.html
[#POCO]: https://pocoproject.org/

## 多语言翻译管理数据库(HkResource) 
**2006.05 - 2006.07**
**软件设计, COM/ATL**
设计新数据库文件格式，为 HMI 提供 22+ 种语言支持服务。采用 ATL/C++ 实现 COM 服务器。

当 Polaris HMI 支持的界面语言从一种语言（英文）增长到 22 种以上，原有语言字符串数据库设计不再适应

- 基于单个文件的设计，典型大小 70+ MB
    - 文件经常损坏， 尤其当 HMI 意外崩溃（比如系统掉电）
    - 读写性能太差
    - 不能裁剪选择只用到的语言，大部分客户只用到 2 种语言（英语及本国语言）

保持其接口不变的情况下重新

- 设计新数据库文件格式
- 实现其 COM 服务器，采用 ATL/C++
- 从用内存映射文件(Memory Mapped File)方式操作文件

克服了以上所有缺点， 而 Polaris HMI 不需要为此更改一行代码。


## HyperSync-Altanium 工业4.0集成
**2016.02 - 2016.10**
**软件设计, 系统集成,硬件采集**

HyperSync™为同步化系统，模具、机器、热流道以及辅助设备协同工作。除具备突出工业4.0级别的智能性及连通性外，机器及模具流程同步化的增强可以较低的总产品成本实现较快的注塑周期，同时不会影响产品的质量


Husky 的注塑机（Injection Molding Machin）与热流道控制器 (Hotrunner Controller)作为两个独立运营的部门，虽然各自居于市场领先， 当客户同时购买两者产品时，彼此视对方为普通第三方设备，并不能启用对方独有的高阶功能/性能。

二者紧密高效的集成，成为年度新机型 Horizon 的核心功能之一， 意在

-  把各种功能的分别连接方式（如 Digital I/O, RS422 等），简化整合为一根 以太连接线， 从而
    - **在硬件上**，简化电气连接， 及生产组装车间工人，客户现场服务工程师接线的复杂性和调试难度
    - **在软件上**，具有向前扩展性， 便于增加新功能

此项目涉及面繁复庞大，**需要在数百万，行数十个工程的 C# 和 PLC 代码中，阅读，理解并修改添加相关功能**。设计内容包括

- 整理所有拟支持的 Altanium 产品线包括Delta 3, Matrix 2 等的全部功能，重新设计基于 Ethernet 的命令集，
    - 温度控制
    - 伺服电机控制
    - 版本控制
    - 对端识别，及专属功能锁定与开放
- 在 Altanium Controller 端添加支持
- 在 Polaris HMI/PLC 端添加支持
- 验证 Beckhoff/TwinCAT RT-Ethernet 实时
- 对部分重要客户提供兼容性升级，在原有硬件连接(RS232/USB)基础上实现新功能


此项目方案及软件实现完全由上海团队调研设计完成， 付出众多， 比如 

- 方案的初期一直争议怀疑意见中进行
    - TwinCAT RT-Ethernet 传输，尤其对于伺服电机控制指令传输未经实际验证
    - 在同一电缆线种，实时信号与非实时信号相互干扰程度未经实际验证


- 12小时时差下，持续数月密集的电话，视频英文设计讨论会议
- 部分重要功能在没有硬件情况下完全靠软件模拟开发测试
- 多次通宵达旦，远程连接到加拿大车间的原型注塑机上调试
- 多次往返于上海与加拿大，调试验证
- 亲赴墨西哥原型机客户（利乐包装）现场组装调试机器




在此核心功能的支撑下，该机型如期在 2016 年秋天德国举行 [K-Show][#KSHOW] 首秀成功，并在 2017 获得 [Ringier 2017 Innovation Award][#RINGIER] 创新奖。


[#RINGIER]: http://www.husky.co/News.aspx?id=6442451127
[#KSHOW]: http://www.husky.co/News.aspx?id=6442451075


## Altanium 远程桌面
**2016.04 - 2016.08**
**软件设计, 系统集成,网络通讯**
基于 RDP 协议的嵌入式远程桌面, 实现对　Altanium　温度控制器的远程控制

当前注塑机（IMM）与 Altanium 控制器是两套相互独立运行的工控机（IPC）系统，各自拥有自己的采集与控制硬件以及 HMI 系统，仅在重要信号上抄送对方一份。操作工必须在两套设备的 HMI 显示屏 上来回操作。

此项目是 HyperSync/Altanium 以太网 One-Cable 集成的互补项目。意在直接从 IMM 的显示屏上直接复制 Altanium 的 HMI 控制界面，实现 One-Display 操作。

- 前者重点在于**实时硬件信号**的相互通讯，实现运动控制的有序性同步， 以**工业以太网( Industrial Ethernet )**协议传输
- 后者的重点在于**非实时软件偏好设置**，以标准 TCP/IP 协议传输

技术内容包括

- VNC 与 RDP 远程协议的方案选择与原型设计
- 设计/验证 RDP 通讯对带宽的占用，及对同电缆内实时信号的影响
- Polaris HMI 系统架构下集成 RDP 协议及界面
- Altanium 的 [APIPA][#APIPA] 自动接入检测，识别
- Altanium 控制器自动解锁屏处理
    - Altanium 采用无键盘纯触摸屏设计
        - 其在 被 RDP 协议远程连接后，会锁屏， 导致操作员不能进入


- 在工业强度下性能的应急响应实时性与可靠性设计与检验
[#APIPA]: http://baike.baidu.com/item/APIPA?sefr=cr


## 扁鹊 DevOps 云生产管理系统
**2013.04 - 2016.12**
**软件架构, 软件设计, 云计算, 大数据, 机器智能, IoT, 数据科学, MQTT**
立足 Azure 云技术（比如 IoT Hub）， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术的新一代 DevOps 系统。

本项目由我以上古名医命名，借指其对现有 Polaris Production System 的“起死回生”的治疗。其凝聚了自加入Husky 以来，我对其开发流程，生产支持流程的观察和解决之道。

Polaris Production System 自 2000 左右开始使用以来，众多的功能添加使其及庞大复杂，经常错误，以致在加拿大有工作 20 年经验的工程师几乎以每天解释各种错误及其绕过之道专职。在上海，其学习难度和莫名其妙的错误模式阻碍了团队的迅速扩大。

其开发环境与发布环境杂揉，多年增量式添加更改配置，延续到目前 VMWare 虚拟机已达已经 70GB 以上的，极少数人能从头配置其环境。

- 其中的完全依赖于加拿大，上海，卢森堡的本地网络映射盘数百Ｇ数据文件，时常因为文件不及同步，丢失，重命名等导致崩溃，网络速度。

- 众多的虚拟机既导致工作站导致性能下降， 共享盘空间浪费，其不停拷贝，也导致网络拥挤，引起 IT 部门的不满。

- 其复杂的工具集，界面，和莫名错误让需求相对单一的车间生产支持工程师不知所措。


扁鹊精分开发和发布的环境需求，立足当前虚拟化技术的前沿与 DevOps 的相关理念，改变其本地应用程序架构，基于网络服务（Web Services）的架构，

- 针对开发环境需求
  
  利用 Vagrant 管理虚拟机使
  
  - 开发轻量化

    基于公共 Vagrant Box， 不必囤积大量虚拟机镜像

  - 共享简单化 

    不必通过拷贝 70+GB 的文件实现开发/调试环境共享

  - 版本化

    容易回退或前进到某特定版本，或比较其差异

- 针对软件发布环境需求

  采用后端前端 Single Page Application (SPA)　网站应用 后端 REST API Service
  - 可以从新浏览器窗口， 发起新的编译请求
  - 进度状态消息实时 email 通知
  - 可以在 Intranet 覆盖的任何地方发起请求和下载结果，而不必守在电脑前监控过程等待结果 
  - 当部署到云端 （Microsoft Azure）上后，服务工程师可以全球远程下载补丁


扁鹊由若干子项目组成，主要

- Lancet（柳叶刀）

    主要以 Python 语言实现的 REST Service 总称， 包括重新实现部分原 Polaris Production System 的功能。

- Acupuncture（针灸）

    以 Django/Dojo Toolkit/Web Socket 为框架实现的单页网站应用(Single Page Application), 作为生产支持和现场服务工程师生成软件包的主要 GUI 门户。

- Transformer（变形金刚）

   管理预配置好的 VMWare 虚拟机群， 

   - 包装， 并行化执行 Polaris Production System 命令， 使编译生成机器时间从原先 典型 40 分钟， 缩减至 5-10 分钟
   - 合理分配请求，提高虚拟机资源的利用率

- HySearch

   基于 MongoDB / Django 的搜索网站， 通过关键字或主要类别，搜索 软件模块或现有机器，以参考比较。

- **部分截图(来自早期 PPT 准备素材)**
    
- **Bianque 早期概念图**
![](./pics/bq-arch.png)

- **HySearch 搜索门户视图**
![](./pics/bq-search.png)

- **Acupuncture 早期视图**
![](./pics/bq-acupuncture-1.jpg)
![](./pics/bq-acupuncture-2.png)

- **Acupuncture 早期视图（移动设备）**
![](./pics/bq-mobile.jpg)

- **Transformers 早期调试窗口**
![](./pics/bq-transformer.jpg)