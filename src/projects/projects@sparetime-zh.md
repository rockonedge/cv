---
- at: sparetime
  category: projects
  duration:
    end: '2013.03'
    start: '2017.01'
  keywords:
  - HTML5 设计
  - 脚本编写
  - 效率工具
  language: zh
  name: Markdown-CV
  summary: ''
  title: Markdown CV
- at: sparetime
  category: projects
  duration:
    end: '2011'
    start: '2011'
  keywords:
  - 图像处理
  language: zh
  name: 2D-Water-Effect-in-WTL
  summary: ''
  title: 2D Water Effect in WTL
- at: sparetime
  category: projects
  duration:
    end: '2005'
    start: '2005'
  keywords:
  - C++
  language: zh
  name: A-Literal-Converter-for-Integers
  summary: ''
  title: A Literal Converter for Integers
- at: sparetime
  category: projects
  duration:
    end: '2016.12'
    start: '2016.06'
  keywords:
  - 桌面/服务器/云服务器/软件设计
  - 网络通讯
  - IoT
  language: zh
  name: TwinCAT-TSM-查看器
  summary: ''
  title: TwinCAT TSM 查看器
- at: sparetime
  category: projects
  duration:
    end: '2013.04'
    start: '2012.08'
  keywords:
  - 桌面软件设计
  - 网络通讯
  - Windows
  - Linux
  language: zh
  name: HostLink-TCP-注塑机通讯协议
  summary: ''
  title: HostLink TCP 注塑机通讯协议
- at: sparetime
  category: projects
  duration:
    end: '2016'
    start: '2012'
  keywords: []
  language: zh
  name: Console3
  summary: ''
  title: Console3
- at: sparetime
  category: projects
  duration:
    end: '2016.12'
    start: '2016.07'
  keywords:
  - 软件设计
  language: zh
  name: Beckhoff-TwinCAT-ADS-通讯协议
  summary: ''
  title: Beckhoff TwinCAT ADS 通讯协议
- at: sparetime
  category: projects
  duration:
    end: '2016.07'
    start: '2016.07'
  keywords:
  - 软件设计
  - 网络程序设计
  language: zh
  name: Multi-Ping
  summary: ''
  title: Multi-Ping
---
# 业余时间

## Markdown CV
**2017.01 - 2013.03**
**HTML5 设计, 脚本编写, 效率工具** 

**本简历由此工具辅助完成**

在整理编写自己简历时，决定采用奥卡姆剃刀原则， 以 Markdown 格式纯文本书写

- 不依赖于 Window/Word或其他系统和工具环境依赖, 便于随时随地更新
- 分离内容与呈现形式，便于输出多种格式
  - PDF 以便于纸质打印输出
  - HTML 以便于电脑屏幕上链接跳转，浏览
  - TXT/Markdown 便于版本控制

采用了 Python, HTML, CSS, JavaScript, Bootstrap 4.0 alpha 及 [Bootstrap][#BOOTSTRAP] 主题。

相关模板及脚本开源于 [GitHub][#MDCV] 供免费下载。

- [link][#MDCV]

[#MDCV]: https://github.com/rockonedege/markdown-cv
[#BOOTSTRAP]: http://getbootstrap.com/

## 2D Water Effect in WTL
2011
图像处理

该代码源于工作中**图像识别处理**, 以及 Windows 图形编程(GDI+, Direct2D ) 学习研究时，兴趣所致。

一段时间需要频繁处理从示波器中导出的芯片通信信号, 并需要于是写了此小工具用于数值的二进制，十进制，十六进制之间相互转换，

- 采用 C++/WTL, 并用英文解释了代码的设计和使用方法。 
- 发表于开源网站 [The Codeproject][#2DWTL]

- [link][#2DWTL]

[#2DWTL]: https://www.codeproject.com/Articles/188236/D-Water-Effect-in-WTL


## A Literal Converter for Integers
2005
开源, C++

该代码源于工作中一段时间需要频繁处理从示波器中导出的芯片通信信号, 并需要于是写了此小工具用于数值的二进制，十进制，十六进制之间相互转换，

- 采用 C++, 并用英文解释了代码的设计和使用方法。 
- 发表于开源网站 [The Codeproject][#LITERAL]

- [link][#LITERAL]

[#LITERAL]: https://www.codeproject.com/Articles/10379/A-Literal-Converter-for-Integers


## TwinCAT TSM 查看器
**2016.06 - 2016.12**
**桌面/服务器/云服务器/软件设计, 网络通讯, IoT** 

TSM 文件是 倍福公司( Beckhoff ) 自动化设备管理与 PLC 运行环境软件 TwinCAT 2 的配置文件，该文件采用不透明的二进制格式，包含

- 所有**硬件**比如板卡及其配置信息
- 所有**软件**比如 PLC， 数控（NC） 等程序及相关**实时任务**配置信息
- 硬件采集与控制端口与对应软件变量的**映射**信息

一台典型的 Husky 注塑机系统拥有 6000+ 的节点映射信息，导致无论

- 开发阶段，
- 车间组装测试阶段， 
- 还是客户现场服务

调试，排错，定位都非常低效。

此工具采用最新 IT 技术, 实现以下功能

- 把所有二进制信息提取成文本格式，比如 JSON, XML 等
- 以 HTML5/JavaScript/CSS 的单页应用(SPA - Single Page Application) 实现了
  - 可视化节点信息
  - 快速搜索查询节点
  - 关系图（Dependency Graph）可视化
- REST 服务，供远程读取该注塑机的所有配置信息
  - ** 此功能使得设计工程师可以从办公室协助车间生产测试工程师调试机器， 而不用来回跑，节省大量时间 **
- Microsoft Azure IoT Hub 支持
  - ** 此功能使得设计工程师可以从 Husky 办公室协助全球客户现场工程师调试，节省大量时间金钱 **

此项目主要采用 C++/CMake, Python, JavaScript，采用开源库 [Vis.js][#VIS]等。

[#VIS]: http://visjs.org/

## HostLink TCP 注塑机通讯协议
**2012.08 - 2013.04**
**桌面软件设计, 网络通讯,Windows, Linux**

![](./pics/hostlinkpp-ubuntu.png)

Hostlink 是原定义于 90 年代中期， Husky 专有的数据通讯协议， 基于原始 Socket（Raw Sockets） 定义众多命令，用于远程与 Husky 注塑机交互，包括

- 实时读取注塑周期各种参数(injection cycle data)
- 设置各种注塑参数

客户通过购买此协议模块，可以把 Husky 注塑机集成到其 生产执行系统( MES )与 ERP 系统中。

在为某大型客户修复该模块程序错误（Bugs）时， 我发现从长远计，原代码架构与系统依赖不久将淘汰，遂决定

- 在业余时间面向未来重新实现，
- 并作为小组相关技术培训的范例。 

开发内容包括

- 以标准 C++ (Standard C++) 重新实现该协议
    - 摒弃了以前依赖于 MFC Sockets 相关类的命令实现 
    - 在 Windows 之外，支持 Linux
    - 支持 64 bit 操作系统， 修复微妙的错误(Bugs)
- 通过 C++/CLI 支持 .NET 绑定， 供 C# / PowerShell / IronPython 等语言调用
- 通过 Boost.Python 支持 Python 调用 
- 增加新的命令字及数据格式, 以传递新的 HPP 机型拥有更多注塑周期实时信息

本代码大量使用使用 [Boost][#BOOST] 库，如 [Boost.Asio][#ASIO], [Boost.Spirit][#SPIRIT]， [Boost.PP][#PP]，[Boost.Python][#PYTHON] 等，以及模板元编程（metaprogramming）等高级现代 C++ 技术及库。

构建工具采用 CMake.
因为 Hostlink 协议不是开放协议，此代码未能开源。

[#BOOST]: http://www.boost.org/
[#ASIO]: http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html
[#SPIRIT]: http://boost-spirit.com/home/
[#PP]: http://www.boost.org/doc/libs/1_63_0/libs/preprocessor/doc/index.html
[#PYTHON]: http://www.boost.org/doc/libs/1_63_0/libs/python/doc/html/index.html


## Console3
**2012-2016**
C++, Direct2D, DirectWrite

该项目基于开源工具[Console 2][#CONSOLE2]定制

- 修复众多错误
- 重新以 [Direct2D][#]/[DirectWrite][#DW] 替代原基于 GDI 的 UI 层，以提高性能和视觉效果
- 添加便利性功能，比如 集成常用 Husky 开发工具与系统变量设置等

该工具在 Husky 上海研发团队广泛长期使用。


[#CONSOLE2]: http://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx
[#D2D]: https://msdn.microsoft.com/zh-cn/library/dd370990(v=vs.85).aspx
[#DW]: https://msdn.microsoft.com/zh-cn/library/windows/desktop/dd368038(v=vs.85).aspx


## Beckhoff TwinCAT ADS 通讯协议
**2016.07 - 2016.12**
**软件设计** 

基于开源的 Beckhoff ADS 协议跨平台实现， 加入了 TwinCAT 2 的支持。

- [link][#ADS]
[#ADS]: https://github.com/rockonedege/ADS

## Multi-Ping
**2016.07**
**软件设计, 网络程序设计** 

在集成 Altanium 热流道控制器的项目中，一项设计目标是控制器被接入注塑机的瞬间，

- 自动探测检测到， 并获迅速或得其 APIPA 地址

使用 ping 应用程序 和 .NET库不能达到实时性要求（耗时 **10s - 40+s**），并且导致大量系统资源（比如内存 **700MB**）消耗, 以致程序崩溃。

我分析其原因可能是过多的 .NET 库封装开销所致， 以 C++/Boost.Asio 从 socket 层异步实现相似功能, 达到 **2s** 完成检测，内存消耗 **5MB** 左右。

随后以此算法封装成
- .NET 组件供 Polaris HMI 集成调用
- 独立应用程序，供车间测试工程师，现场服务工程师调试( Troubleshooting )用
