---
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2005'
  keywords: []
  language: en
  name: List-of-Achievements
  summary: ''
  title: List of Achievements
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2005'
  keywords: []
  language: en
  name: Polaris-Injection-Control-System
  summary: ''
  title: Polaris Injection Control System
- at: husky
  category: projects
  duration:
    end: '2006'
    start: '2006'
  keywords: []
  language: en
  name: Polaris-HkLabel
  summary: ''
  title: Polaris HkLabel
- at: husky
  category: projects
  duration:
    end: '2006.05'
    start: '2006.05'
  keywords: []
  language: en
  name: Polaris-HkResource-Refactor
  summary: ''
  title: Polaris HkResource Refactor
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2013'
  keywords: []
  language: en
  name: Bianque
  summary: ''
  title: Bianque
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: Hypersync-Altanium-Industrie-4-0-Features
  summary: ''
  title: Hypersync-Altanium Industrie 4.0 Features
- at: husky
  category: projects
  duration:
    end: '2007'
    start: '2007'
  keywords: []
  language: en
  name: HyMET-Heatlogger
  summary: ''
  title: HyMET Heatlogger
- at: husky
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords: []
  language: en
  name: Shotscope-Celltrack-Matrix
  summary: ''
  title: Shotscope, Celltrack & Matrix
- at: husky
  category: projects
  duration:
    end: '2015'
    start: '2008'
  keywords: []
  language: en
  name: Shotscope-NX
  summary: ''
  title: Shotscope NX
- at: husky
  category: projects
  duration:
    end: '2006'
    start: '2005'
  keywords: []
  language: en
  name: Shanghai-Factory-Startup
  summary: ''
  title: Shanghai Factory Startup
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords: []
  language: en
  name: Whiteboard
  summary: ''
  title: Whiteboard
- at: husky
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords: []
  language: en
  name: Product-Launch-Team-PLT-Trips
  summary: ''
  title: Product Launch Team(PLT) Trips
---
# Husky

## List of Achievements
**2005 - 2016**

  - Instrumental during the startup of Shanghai Machines plant, from the scratch

    - Replicated **Polaris Production System** from Bolton to Shanghai 
    - Helped starting up the **production engineering team** 

  - Started up a first offshore **global development engineering team**, all with Master of Science/Engineering degrees, and had successfully managed it for 11+ years.
  - Led the development on controls platform for all Husky injection machines, consisting of 
    -  2 Polaris HMI platforms
      - COM/VB6 HMI platform for legacy generations of injection machines
      - re-Architecture and re-implementation of a .NET/C# HMI platform for later generations of injection machines
      - value-added modules, e.g.
        - development of Statistical Quality Control (SQC) and Statistical Process Control (SPC) module
      - hostlink, a proprietary communication protocol to talk to Husky machines remotely 
    - Polaris PLC 
      - **PLC programming for real-time control** with TWinCAT/CODESYS technologies, such as
        - servo drives
        - robotics etc.
    - Polaris production system.
      - daily used by engineers across departments(DevEng, Production and Service etc.) , such as
        - Translation System, supporting 22+ languages for Husky software
        - Mold ID System, managing RuBee(IEEE 1902.1) based tags mounted onto Husky made molds
        - ...
    - **regular releases**(quarterly/semi-annually) of the aforementioned platforms for over a decade
  - Initiated **Bianque Production System**, a web-based replacement for Polaris Production System, leveraging Cloud-era technologies such as Microsoft Azure IoT Hub, REST, microservice, and mobile apps.
  - Led the development of Matrix, a Hot Runner Temperature Controller based on Linux/Fedora using QT/C++ and MySQL.
  - Design and implemented in c++ Husky Hostlink, a proprietary raw TCP sockets based protocol remoting to Husky injection machines in realtime
  - Instrumental in the takeover of software&hardware assets from French&US teams during [Husky buying MoldFlow manufacturing division](http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit) and subsequent customer retention.
  - Led the implementation of [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), an automated, real-time, web-based software&hardware hybrid solution for process and production monitoring.[
  - Led Industry 4.0 features introduced in the HyperSync machines, and won _Ringier Technology Innovation Award_](https://omnexus.specialchem.com/news/industry-news/husky-ringier-technology-innovation-award-000186265) .

  - Led the participation in the worldwide Product Launch Team(PLT), an ad-hoc top technical expert team to set up to-be-launched machines in the field for early adopter.

  - Fostered Agile methodologies such as SCRUM/Test-Driven Development(TDD) 
  - Helped the migration source control process from VSS to TFS.

  - Innovated on effective global teams collaboration&management process
    - developed the **Whiteboard**  website in spare time to synthesize info from various data source including 
      - Team Foundation Server
      - Visual SourceSafe
      - SharePoint
      - ERP/Oracle and 
      - UNC/mapped network drives

      leveraging 
      - MongoDB
      - Python/Django
      - Javascript/Dojo Toolkit
      - C#/ASP.NET/REST API
      - TFS/ ShairePoint SDKs etc.
      - Apache Cordova
      
      having saved time in routine meetings such as
        - daily standup meetings
        - weekly team meetings
        - milestone meetings etc. by quickly putting everyone on the same page
      - enabled team leaders to query project statuses within clicks.

![](./pics/wb-overview.png) 
![](./pics/wb-projects.png) 

## Polaris Injection Control System
**2005 - 2016**

### **Overview**

Polaris is an umbrella name covering all development activities surrounding the general software platform. All software coming out of DevEng, unless specifically named(e.g. Shotscope NX), is regarded as part of Polaris by default.

Polaris to Husky is Windows to Microsoft, in that it's being developed, thus evolving, all the time and released regularly.  Any mechanical, electrical and software design change, or technology advances in the industry would pretty much likely result in changes in Polaris one way or another.

Roughly, Polaris consists of 

- Polaris HMI
- Polaris PLC 
- Integration of 3rd auxiliaries
- Polaris Production System

Polaris is an IPC-based software solution for controlling all Husky-made injection molding machines, written in C++, C#, VB6, PLC (IEC 61131) and Ruby among others.

All Polaris HMI&PLC features are wrapped into reusable and composable _modules_ like LEGOs, Polaris Production System 

- manages the 6K+ and growing modules and 
- generateds deployable packages(11K+), 

one for each machine, be it physically real or simulation for development purposes), so that each machine is equipped with a unique software package of HMI and PLC combined.  

**Suffice it to say, a big part of my time, as well as my team's in Husky, was spent directly or directly on this platform.**

It was Shanghai team's daily routine, under my leadership, to 

- pick tasks from the same pool and 
- follow the same schedules 
 
as peer development teams back in the HQ, with contributions to to the same code base with
- new features
- feature improvements
- bug fixes etc.

### Polaris HMI
**2006 - 2016**

A GUI/touch-screens enabled application running on Windows XP/7 Embedded with rich features enabling operators with an ultimate control 

- from cycle breakdowns to production jobs schedules. 

The UI is **configured** during package generation with the Production System per customer orders.

2 versions are maintained:

-  Legacy version

  - started around 2000
  - default for all product lines roughly between circa 2000 and 2007
    - being slow and corrupt-prone for being resource demanding as a result of accumulative abuse of system resources over the years.
  - coded in VB6 using COM technology, and source controlled by SourceSafe
  - running on Windows XP Embedded 


- Current version

  - started around 2007
  - default for all product lines starting from around 2008
  - coded in C# using .NET technology and modern Tools
    - A new architecture leveraging C#/.NET and with lessons(a lot of them!) from the previous version
      - fully OOP
      - decoupled layers for devices, domains and UI logics, with message-based communication
      - less dependent on TwinCAT opaque file formats
    - using new Application lifecycle management (ALM) tool and methodology
      - Microsoft Team Foundation System(TFS)
      - TDD/SCRUM process
  - running on Windows 7 Embedded

  I was wholly involved since the inception of the architecture of the .NET version, such as 

  - early concepts selection
    - e.g. meeting at Canada the representatives from Microsoft, IBM and McCabe and voting for the final choice of TFS
  - leading the year-long re-implementation of every VB6 screen in C#/.NET in Shanghai

### Polaris PLC

A TwinCAT based real-time program for 

- servo motors 
- sensors 
- actuators 

across the 

- injection
- clamping and 
- robotics systems, sending status to and receiving commands from the HMI.

Algorithms were written in IEC61131/ST Language and stored as modules per functionality, merged with the help of the Production system during machine packages generation.


### Integration of 3rd auxiliaries

It's more often than not that 3rd-party auxiliary equipments were requested to be integrated into Husky's HMI to 

- complete a whole production cycle/process
- facilitate the operators with a single one screen.

The task usually involved 

- data transfer through standard or proprietary(e.g. RS-485, digital I/O etc.) protocols 
- embedding screens into the Polaris HMI UI

examples included:

- ABB robots, 2006
- Resin Dryers from [Plastic Systems](http://plasticsystems.com/) 
- dehumidifier
- conveyers etc.

## Polaris HkLabel
**2006**

**This was the my first code check-in check-in at Husky.**

This project was to develop a replacement of the built-in Visual Basic Label control to 

- fix the truncation of text display due to extended text length caused by translation(e.g. from English to German) 
  - should show trailing ellipsis and pop up the full text when finger-clicked
  - should change the display when it's too long
- fix the overuse of GDI handles [hitting the 16K limit][#GDI] allowed by Windows XP.

Developed in C++ as a COM component. 

A companion utility was developed too to replace 1K+ occurrences of usages across the whole source code tree (hundreds of Kloc) at one go.

[#GDI]: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724291(v=vs.85).aspx


### Polaris Production System
**2006 - 2016**

The Production System assembles 

- from 6k+ modules a package of the Platform System 
- specific to the machine configuration ordered by customers, and 
- delivers it to a target machine on the shopfloor for testing and shipping
- through network .

 A loosely-coupled collection of utilities, including CLI, GUI and web sites, for

- **creation and versioning of tens of thousands of modules** of HMI and PLC features
   - source code fragments
   - pre-built binaries
   - customizable templates etc.
  - assembly of deployable **platform installation packages and deviation/retrofit patches** per injection molding machine serial number
  - bridging development, production and service engineers for service issues
  - improving engineering time performance

For its nature, dealing with the Product System is a part of everyone daily work, and fighting with it to make it work or workaround it stayed a main theme.


### Polaris Mold ID Server
**2009**

Latest Husky injection molding systems are equipped with the mold identification(Mold ID) feature, enabled by [Rubee](https://en.wikipedia.org/wiki/RuBee) tags attached to mold tooling plates.

When a mold is installed, parameters are read from the tags over the air, 
- enabling mold identification and quick alignment if matches, or
- reject installation if not

to avoid accidental damages.

My team built 

- a web portal 
  - global data center for all mold toolings, sold or under development
  - for daily cross-function collaboration.
    - mold designers to specify parameters
    - production engineers to program tags
    - service engineers to refurbish tags
- Integration into Polaris HMI
  - wireless detection of Rubee tags and parameter fetching
- Tag Programming/refurbishing Tool   
  - writing of mold tooling parameters through RuBee(IEEE 1902.1)
  - compress data into the 256-byte memory space
  - enabling engineers to program over the intranet, e.g. writing a Shanghai tag from Bolton 

SharePoint, Silverlight, WinForm and  Socket programming were involved. 

I oversaw the whole project as it was completely developed in Shanghai and deployed in Shanghai's IT servers for global access. The maintenance and response to requests raised through IT Help Desk are handle solely by the Shanghai team.


## Polaris HkResource Refactor
**2006.05**

With the growing number of supported languages from a mere 1, i.e. Engish, to over 22, the original single-file based solution suffered from

- too large file size up to 70+ MB
    - prone to corruption due to HMI crash or power loss
    - poor read/write performance
    - all-or-nothing rigidity, while only 2 languages(English and local) are requested in most cases.

By keeping its client interfaces intact,  I personally

- re-designed the storage schema
- re-implemented the COM server with ATL/C++
- leveraged memory-mapped file to access the files

thus getting rid of all the aforementioned shortcomings, without a single line of change in the HMI code.

### Polaris Translation System
**2006**

The Polaris HMI supports 22+ languages. For any phrase to be added to the screens, it's required to determine whether new translation is needed early on, and if so, among others

- applications should be filed for approval from team leaders for payment to a 3rd-part translation company 
- contexts of phrase usage, e.g. screenshots, should be provided to assist translation
- only selected languages should be packed and deployed onto the machine

A fresh college graduate, under my coach, developed a website using ASP.NET and SQL Server to have the goal achieved.

## Bianque
**2013 - 2016**

The Polaris Production System dated back to circa 2000 and evolved into a daunting complexity with a very steep learning curve over the years, and a daily drain of engineering efficiency, To name a few,

- fail frequently
  
  unmanaged and undocumented heavy dependency on assumed fixed paths which are not

- poor errors 
  
  pop-up dialogues in the middle of automated processes, which invalidated the automation itself.

- convoluted system configurations

  70-100+GB decade-old VMWare image one can hardly create from the scratch.

- overuse of VMWare image file, a.k.a under use of virtualization technologies

  every engineer has **a copy for each task**. It's normal to have 2-3 such images running at the same time to get work done.

Named after the ancient Chinese doctor, Bianque was meant to fix it with a it-just-works solution with modern DevOps and virtualization technologies by distinguishin the requirements between

- Development Engineering, which creates software packages

  introduced  Vagrant for virtual eenvironment management  

- Production Engineering, which consumes software packages 

  replaced the legacy VB6 desktop application with a single page application(SPA) site backed up with REST web services

  - cloud(Microsoft Azure) based
    - removing the waste of gigantic duplicate VMWare image files(which IT complains all the time) with efficient cloud solutions
    - accessible from multiple platform
    - accessible from anywhere to benefit field service
    - easy upgrades
  - service-oriented with APIs
    - extensible for specific needs

Bianque largely consisted of

- Lancet

    A collection of REST Service mostly code in Python, including re-implementation of part of Polaris Production System functionalites。

 - Acupuncture

    A single page application leveraging Django/Dojo Toolkit/Web Socket, the front portal for 
    - production engineers on the shopfloor
    - service engineers in the field

- Transformer

    A manager of pre-configured VMware images
    
    - dispatch requests among images to maximized resource ultilization
    - concurrently execution of the Polaris Production System commands to have reduce from 40m to 5m-10m typically
    - exposing Polaris Production System commands as REST services 

-   HySearch

    A MongoDB/Django based portal as a convenience tool for search modules and machine packages through keywords.

### Peeks of some screenshots

- **early concept of Bianque**
  ![](./pics/bq-arch.png)

- **HySearch Engine**
  ![](./pics/bq-search.png)

- **Acupuncture**
  ![](./pics/bq-acupuncture-1.jpg)
  ![](./pics/bq-acupuncture-2.png)

- **Acupuncture on mobile devices**
  ![](./pics/bq-mobile.jpg)

- **early debug view of Transformers**
  ![](./pics/bq-transformer.jpg)

## Hypersync-Altanium Industrie 4.0 Features
**2016**

[Altanium controller](http://www.husky.co/EN-US/Altanium-Controllers-Overview.aspx) has stayed an independent business from the machine systems since its purchase from Moldflow, based in US.

It was decided from the Horizon(a.k.a HyperSync) product line, the Altanium controller would be an integrated part of the injection system for branding promotion and cost benefits:

- _one single touch-screen panel_ and integrated HMI instead of one for each.
- _premium Altanium feature/performance_ when ordered together, starting from HyperSync.

which means a design of a new unified & proprietary interface

- hardware

  one ethernet cable instead of various digital I/O, RS422 and other industrial interfaces

- software
  - design of a protocol 
    - unifying all in-use commands for temperature and servo control commands
    - extensibility of future functionalities
    - identification of Husky equipments from either side
      - protection of Husky IP
      - protection of malice penetration from the network
  - remote desktop viewing
    - an VNC/RDP-like solution for an operator to control the Altanium controller from the machine

This was completely done by my team in shanghai

- started with a few slides of ppt from the Product Manager, mentioning _Altanium Integration_,
- collected requirements and consensus from all teams at stake and 
- Concepting
- Design execution and validation

It was an unforgettable experience for all the challenges we had to cope with. 

- The one and only Prototype machine built in Bolton
  - late nights of remote connection&debugging on Bolton's shopfloor till 4:00AM
  - several tight validation trips to and from Bolton
    - compete with other design teams for the availability of the prototype machine
- all teams except for software design are in North American
- had to borrow servo and motors from [Baumueller](http://www.baumueller.de/en) at the [ChinaPlas](http://www.chinaplasonline.com/CPS15/Home/lang-eng/Information.aspx)
- had to figure the Altanium side technologies by ourselves through code reading

Due to departmental priorities conflicts between US and Canada teams, we stayed in the middle of one party too demanding and one party too uninterested.

The features were successfully shipped to the first customer TetraPak at Mexicali, Mexico. One designer from my team was part of the PLT team to startup the system .

**It was quite an experience and I was proud that my team made it.**

## HyMET Heatlogger
**2007**

![](./pics/heatlogger-main.png)

Lack of experience alloys injection, designers wished to monitor the heat parameters at the barrels and sprues in the long term. It was then decided to develop this tool to be deployed onto every sold HyMET machine, logging all interesting parameters. A husky service guy would visit the customer to copy the data out every 6 month or longer.

The project is to read from up to about 100 temperatures values from thermal couplers and save them into logs.

The critical requirements include:

- no threat to normal machines operations
  - no system resource contention with the Polaris HMI/PLC, i.e. minimum CPU cycles and memory footprints
  - no disk space running out
- non-stoppable by the factory operators
- up to 100 channels
- records at an interval from 1 sec to 180 seconds.
- flexible scheduling based on calendars and shifts
- intelligent start/stop of logging with on/off statuses of injection cycles
- data must be power-failure safe, i.e., no corrupt file due to accidental power loss 
- tight schedule imposed by machine shipping date


I coached an intern to have developed the code to read temperatures through TwinCAT API, and personally  wrote the UI in [WTL](https://en.wikipedia.org/wiki/Windows_Template_Library) and the logging in [POCO](https://pocoproject.org/), I also contributed back the enhancement I made since it's an open source project.

A colleague from the HyMET team wrote to me saying:
> this is the most beautiful software I've ever seen in Husky.

## Shotscope, Celltrack & Matrix
**2008**

When it bought Moldflow manufacturing division, Husky dismissed the original French&US teams and had all the assets(software&hardware) sent to Shanghai.

Some of the code dated backed to early 1990s, and the technologies varied from Delphi, Borland C++, Sybase database, MySQL, Linux

- Shotscope, a production monitoring software
- Celltrack, another production monitoring software
- **Matrix 1**, a linux-based Altanium HotRunner Controller software

My team successfully supported existing European/American customers with sustainment development until the next .net versions of Shotscope and Matrix, i.e. SSNX,  came out.

## Shotscope NX
**2008 - 2015**

The successor to Shotscope and Celltrack, with modern IT technologies. To quote its [homepage](http://www.husky.co/EN-US/Shotscope-NX.aspx):
  >The industry’s most advanced plant-wide process and production monitoring system

A first ever Husky made manufacturing monitoring system aimed to be an integration of 3 SCADA/ERP software products: SmartLink, Shotscope, CellTrack, embracing modern technologies at the time to achieve:

- Friendly and intuitive UI and better data presentation ability
- More distributed supervisory ability
- More data storage and processing power
- Built-in communication with other Husky software such as Polaris IMS Control system and Altanium Hot Runner Controller

The first release was delivered on time and received immediate orders at its debut at NPE show 2009, Chicago.

The majority of implementation was done in Shanghai under my supervision with a 3rd-party test team hired from Toronto for feedbacks and bugs reporting.

My team was a core party from the start and kept improving it until it was feature stable and transfered to After Market Service(AMS) engineering team for customer-specific changes in 2015.

Major Technologies include WCF, Silverlight, SQL Server, written in C#.

## Shanghai Factory Startup
**2005 - 2006**

In the context of Husky's rapid growth in China, it was decided to build **a machines&hot runners manufacturing factory**, hence the need for strong engineering teams to

- **build infrastructures**
- **build up teams** of assembly, testing, production engineering
- **transfer engineering knowledge, disciplines&experience** from Canada to local teams

3 Global Engineering Execution(GEE) teams, including 1 controls focused(which I led) and 2 mechanical focused, were founded to support the mission.

With no prior domain knowledge of injection&molding, and by splitting my time in half between Bolton and Shanghai in the first year, I successfully replicated the production infrastructures concerning controls(Polaris Production System) from the ground up.

This 1+ year long efforts earned me a Special Contribution Award, signed by the then Global Vice President of Machines.

## Whiteboard
**2011-2016**

One of the typical challenges for global teams is the time differences and geographical distances making it hard to put everyone on the same page in meetings. While local teams could debrief at a coffee break or stop by the desk, global teams do not have the luxury. It could be only worse if different projects are interwoven and information is scattered among multiple sources

From expereince of talking to various teams from mechanical, electrical to software and all levels of leaders up to VP, I developed an application to pull information from various sources, including

- Visual Sourcesafe database
- Team Foundation Server
- SharePoint portals
- SQL Server database from IT's attendance&vacation system
- Shared files on mapped network drives

and synthesize them into a dashboard site with links back to the information sources.

An orchestration of technologies/languages are used:

- [Django](https://www.djangoproject.com)
- [Dojo](https://dojotoolkit.org)
- [ASP.NET Web API](https://www.asp.net/web-api)
- [MongoDB](https://www.mongodb.com/)
- Python
- C#
- JavaScript

## Product Launch Team(PLT) Trips
**2011 - 2016**

It's been Husky's tradition to set up a highly knowledgeable and experienced team directly from the Dev Eng, instead of regular service teams, to start up machines for early adopter before the formal launch of a product line.

My team had been backing up these teams since 2011 and sending people directly since 2013 to destinations like Vietnam, Mexico and multiple cities in China.
