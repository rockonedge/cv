# Readme

## Dependency

- joffreykern.markdown-toc
- yzane.markdown-pdf


## buzzwords
信息技术，　智能终端，　云计算，　大数据，　智能硬件, 移动智能终端，　互联网+，　消费互联网，　产业互联网，　智能制造，　虚拟现实，　



## summary 


>- 创建全新全球研发团队，并领导 11+ 年
    - 成功完成众多全球协作开发项目，成绩备受赞誉
    - 善于促进团队合作与成长，富于责任感，感染力


>- 专业，高效的职业素质
    - 工作自觉性高与责任感强，崇尚以身作则
    - 善于多文化环境下沟通合作
    - 富有远见, 敏锐的技术敏感性, 渴望挑战


>- 18+ 年电气工程工业自动化，计算机软件/固件/硬件项目经验
    - 15+ 年供职于跨国公司研发部门
    - 自动化测量，工业实时驱动控制，半导体电子软/硬件系统研发
    - 熟悉电气/电子设计( Schematic & PCB ) 调试,仿真
    - 熟悉 Windows/Linux, 包括桌面，服务器，嵌入式以及 Azure 云计算软件开发
    - 熟练使用 C++，Python，C#，JavaScript, Go, Matlab 等语言
    - 长期紧密支持生产，销售，服务部门


>- 跨国高科技公司成功创业经验
>- 定期捐赠[联合国儿童基金( UNICEF )](http://www.unicef.cn)助学西部儿童



Husky 贡献
- 建厂并盈利，
－　建研发团队１０年
－　收购moldflow
－　最佳团队2011
- TFS/Agile/TDD/Scrum
- Modernize DevOps
- IoT/I4.0