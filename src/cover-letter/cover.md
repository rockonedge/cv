## Axis
To Whom it may concern,

After one year's planned parental leave, I'm eagerly looking for opportunities and I came across this excellent one that I feel so attracted.

My long focused past experience developing system software for all  kinds of hardware systems, ranging from tiny chips to titanic machinery, plus my whole career in global R&D sector of world leading companies, makes me an instant fit to both your technical and cultural requirements.

I am looking forward to a face to face communication. As a Chinese proverb goes, one eye-contact is worth a hundred more hearsay. I'd appreciate it very much if there's one.

Best Regards&Wishes,
Tom


 




## Optivar

To whom it may concern,

I came across your company on linkedin and was very much attracted toyour culture, which I firmly believed in and cultivated one while working for Husky.

While my past experience had little to do with the auto industry, it was all software related, for electronic/electrical systems in particular.

I understand that you are looking for an engineer, I am leveraging this application channel to expose myself to you in case you are open to other options, such as a manager position to build a bigger team here in Shanghai/China. 

Thanks for reading & looking forward to hearing from you.

Best regards,
Tom


## auto


To whom it may concern,

I came across your company on linkedin and was very much attracted toyour culture, which I firmly believed in and cultivated one while working for Husky.

While my past experience had little to do with the auto industry, it was all software related, for electronic/electrical systems in particular.

I understand that you are looking for an engineer, I am leveraging this application channel to expose myself to you in case you are open to other options, such as a manager position to build a bigger team here in Shanghai/China. 

Thanks for reading & looking forward to hearing from you.

Best regards,
Tom

## chnt
张先生，你好！

谢谢你在领英上的回应。 依约以下简述我主动联系，寻求诺雅克工作机会的原因。

首先从知识背景而言， 我对于诺雅克所从事的行业相当“专业”，拥有学士和硕士学位。

从 1998 年至 2003 年 7 年间我一直专注低压电器相关的学习和研发， 包括
    - 哈尔滨工业大学电机与电器专业的学士与硕士学历（当年本专业30多名本科生中， 唯一一位考上本专业研究生）
    - 为哈尔滨铁路局开发电压电器（断路器，接触器，热继电器等）测试装置， 
        - 相关设备在下辖 7 个车辆段推广使用，共销售 10 多套， 达 100+ 万人民币销售额
        - 相关设计通过省级科委评定科技进步奖
        - 相关理论及实验数据发表于国际专业会议， 也是硕士学位论文的基础
    - 在施耐德中国研发中心，从事低压断路器的国产化设计，及相关解决方案工作

接下来的十多年直到 2016 年底，也一直在上游（电子， 即意法半导体）和下游（工业控制， 即赫斯基注塑系统）行业从事技术研发和团队管理工作，延续对电气工程的专注， 近年来尤其关注工业 4.0 与 中国制造 2025 的相关发展。

正泰电器因为其行业地位，大名早有所闻。但因总部不在上海，一直了解有限。

最近看工作机会，偶然发现诺雅克，被其行业高端的市场定位深深吸引，希望能有机会加入担任高级研发职务，带领或筹建团队，从事工业领域研发， 包括
    - 电子/电气硬件系统
    - 云端，桌面，嵌入式/移动设备软件研发
    - 工业4.0/物联网 （IoT）研发
以期
-	在工业 4.0和智能制造方兴未艾的历史性时刻，以广泛的技术积累助力诺雅克站在行业技术前列，
-	以多年成功的国际化工作经验，助力诺雅克的全球研发战略。

随信附中文简历供参考。如能有进一步的面聊机会，不胜感谢。

祝 商祺，
谭智

