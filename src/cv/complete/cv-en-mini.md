---
language: en
category: cv
---
**(+86) 135-248-26926 &middot; [tom.tan@live.com](mailto:tom.tan@live.com) &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; Shanghai, China**
# **Zhi(Tom) Tan**

- **Experience of Global Hi-Tech Venture Startup at Shanghai/Taiwan**
- **Experience of Global R&D Team Startup**
- **Experience of R&D Management(11+ years)**
- **Experience of Software/Firmware/Hardware Development(18+ years)**
- **Generally Interested in all things Science, Technology, Engineering and Math(STEM) Related**
- **A Proud Father of 2 Kids**
- **A Regular [UNICEF](http://www.unicef.cn) Donator**

## **Objective**

A senior/management R&D position in a global company, preferably based in Shanghai

## **Summary**

- 18+ years of R&D-focused experience of
  - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
  - schematic&PCB design and simulation
  - programming with C++/Python/C#/JavaScript/Go/Matlab


- 15+ years of professional career in World's top companies with 
  - industrial automation, electronics and manufacturing
  - abundant cross-team experience supporting production, sales and service teams


- 11+ years of leading a successful global engineering team
  - multiple global engineering projects accomplished
  - leadership acknowledged from within and outside of the team

- Lead by example
- Great self-motivation
- Visionary and open minds
- enthusiastic pursuer of knowledge and challenges

## **Experience**

### **Team Leader/Founder, Global Development Engineering Department**
- **[Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016**
- **Reported to Director of Global Development Engineering(HQ in Canada)**

The founder&top regional leader of a global engineering team with direct reports to the headquarter. Grew team from 0 to 5, and then to 12 headcounts 2 years later due to exceptional team performance.

Worked with Canadian Development Engineering teams daily, through email, audio/video conference calls and mutual visits.

Controls system development for the injection molding sector, mostly software(desktop, firmware and server/cloud) design along with electronic, electrical, mechanical tasks, including HMI, PLC and MES/SCADA in C#, C++, Python Ruby, JavaScript etc. for Windows, Linux and the cloud.

**Major Achievements**

- led the acquisition of [Moldflow][#BUYMF] from French/US teams to Shanghai
- Shipped [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), the industry’s most advanced plant-wide process and production monitoring system
- Rolling-shipped Polaris, the primary software platform(HMI, PLC and Production System in C#, C++, Ruby etc.) for all injection machines for 11 years
- **Special Contributions Award**, Signed by the then Global Vice President of Machines, 2007
- **Top-Ranked Team Leader**, 2011
  - Husky's global employee survey conducted by [Kenexa][#KENEXA]
- **10-Year Service Award**, 2015

### **Senior Application Engineer/Interim Team Leader, Memory Competence Center**
- **[STMicroelectronics][#ST], 9/2003 - 11/2005**
- **Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France)**

A startup member and interim leader for the *Serial Non-Volatile Memory & Embedded System Solutions Team*, explored and built a connection with the R&D Division in France, and maintained effective communication.

Daily work included

- electronic Schematic/PCB layout,
- embedded/desktop software/firmware coding in C/C++/ASM
- English application notes authoring

**Major Achievements**

- initiated a startup [Dediprog Inc.][#DP] extending my work in ST with French colleagues, being the interim CTO 

### **Electronic Design Engineer, China Design Center**
- **[Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003**
- **Reported to Manager of China Design Center(French Expat)**

Worked closely with the lab technicians, providing premium technical support for local joint ventures on industrial control&automation(ICA) products, including

- electrical/electronic localization design
​    - Authored and conducted homologation tests for IEC/GB standards compliance for local joined ventures products
- development of lab equipment / utilities

**Major Achievements**

- Auto-Transfer System(ATS), an emergency backup power switch for critical sectors like hospitals built with PLC and circuit breakers


### **Researcher, HIT Research Institute of Electrical Apparatus**
- **[Harbin Institute of Technology][#HIT], 4/1998 - 3/2002**
- **Reported to Head of Institute / Mentor Professor of My Postgraduate Study**

This was a technical center/lab, founded and headed by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.
​
**Major Achievements**

- The Electronic Ballast Test Platform
- The Low voltage Apparatus Automatic Maintenance Platform

**The above systems, completely my design, generated sales of over 1 million RMB.**

## **Education**

- **Master**, _summa cum laude_
  - **Electrical Engineering**, 09/1999 - 03/2002
  - [Harbin Institute of Technology][#HIT], Harbin

- **Bachelor**, _summa cum laude_
  - **Electrical Engineering**, 09/1995 - 07/1999
  - [Harbin Institute of Technology][#HIT], Harbin


## **Languages**

- Chinese: Native
- English: Fluent

## **Skills**

- Management
  - successful startup of a hi-tech venture in Shanghai/Taiwan 
  - successful startup of global engineering teams in Shanghai
  - team & project management with high retention rate
  - Top-ranked team leader in a global employee survey by [Kenexa][#KENEXA]


- Technologies

  - Industrial Standards

    - OPC(OLE for Process Control)
    - EtherCAT/Realtime Ethernet
    - Sercos
    - Profibus
    - Euromap
    - CAN
    - SPI etc.

  - Hardware
    - Schematic & PCB design with Protel/Altium, 
      - A/D, D/A and I/O and other peripheral circuits
      - MCU(MCS51, ARM, ST7)
      - PC(ISA/PCI) architectures.
    - knowledge of Verilog HDL.
    - Simulation with Matlab, pSPICE
    - Industrial PC, PLC, softPLC(CodeSys/TwinCAT)
    - wide-range current power supply(1A -1200A) design


  - Software
    - Open Source software(OSS) contributor to 
        - [BOOST][#BOOST_REVIEWER], 
        - [POCO][#POCO_CONTRIBUTOR] 
        - [The Code Project][#CP_AUTHOR]
    - Programming Languages
        - frequent use of C++(esp. C++ 11/14/17), Python, C#, JavaScript/HTML/CSS
        - occasional Golang, R, Matlab, ruby, TypeScript etc.
    - Application
        - IC chip drivers
        - realtime motion control(e.g servo motor, robotics)
        - desktop GUI/CLI applications
        - server/web based enterprise LOB(line of business)
        - cloud based mobile/IoT solutions
    - Platforms
        - Windows desktop, Windows Embedded/Windows Mobile, Windows Server
        - Windows Azure, AWS
        - Linux(Ubuntu/Fedora)
        - uC/OS, eCos and uCLinux
    - Tools&Library
        - Microsoft Team Foundation System(TFS)/Microsoft Visual Studio
        - Keil, ARM RealView 
        - SQL Server, MongoDB, SQLite 
        - .NET, ASP.NET, WinForm, WPF, UWP
        - MFC/WTL, QT, STL/BOOST, ACE
        - Dojo Toolkit, AngularJS
        - NodeJS, Django, Flask 
        - Cordova, Xamarian
        - Git, Mercurial(HG), SVN
        - Vagrant, Ansible
    - Methodology
        - Object Oriented Programming(OOP), generic programming/metaprogramming(GP), functional programming 
        - Agile/SCRUM/TDD

## **Additional Information**

- **Spare Time**

  - **Family**, Reading, Running Half-Marathon, Cycling, Travel


- **Web Pages**
  - [CV in English](https://ttan.netlify.com/cv-en)
  - [CV in Chinese](https://ttan.netlify.com/cv-zh)
  - [Feature Projects in English](https://ttan.netlify.com/projects-en)
  - [Feature Projects in Chinese](https://ttan.netlify.com/projects-zh)

[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/
