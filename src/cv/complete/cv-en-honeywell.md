---
language: en
category: cv
---
**[tom.tan@live.com](mailto:tom.tan@live.com) · (+86) 135-248-26926 · [LinkedIn](https://www.linkedin.com/in/zhitan/) · Shanghai, China**


# **Zhi(Tom) Tan**

- **A Seasoned Team Leader of Electrical/Electronics&Software Systems**

## **Objective**

- Engineering Manager at Honeywell Building & Technologies

## **Summary**

A voyager surfing Science, Technology, Engineering and Math(STEM)

- with special interests in any system/device involving electrical/electronic/computer engineering, 
- believing in the trend of industries towards being more digital and more software defined.



- 18+ years of R&D-focused experience of
    - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
    - schematic&PCB design and simulation
    - programming with C++/Python/C#/JavaScript/Go/Matlab 


- 15+ years of professional career in World's top companies with 
  - industrial automation, electronics&manufacturing
  - abundant cross-team experience supporting production, sales&service teams


- 10+ years of leading a successful global engineering team
  - multiple global engineering projects accomplished
  - leadership acknowledged from within and outside of the team

- Lead by example
- Great self-motivation
- Visionary and open minds
- enthusiastic pursuer of knowledge and challenges

## **Experience**

- **Team Leader/Founder, Global Development Engineering Department**
  - [Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016  

  - Reported to Director of Global Development Engineering(HQ in Canada)  

    1. Chose Husky  over the Corporate Technology of Siemens's offer whose offer happened to be a team leader to build a team for **smart meters**,  for greater challenges: 

       - little prior knowledge of injection molding
       - startup of a global engineering team from the scratch

    2.  Built and led a team of 5 people in the 1 year and expanded to 12 thereafter.

       - working hand-in-hand with Development Engineering teams in Canadian HQ daily, through email, audio/video conference calls and mutual visits
       - people management
         - talent acquisition&retention
         - team motivation/training
       - project management
         - collaboration/prioritization of milestones with global/local engineering teams
         - engineering discipline/standards enforcement
         - regional R&D interface to production/service teams et al. 
         - evaluation and evangelism of new technologies and methodologies
       - budgets forecast&control

    3.  mostly real-time software development for injection molding specifically, along with electronic/electrical/mechanical tasks , covering HMI, PLC and MES/SCADA

    -  **Major Achievements**

      - led the acquisition/merge of [Moldflow][#BUYMF] from French/US teams to Shanghai
      - led [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), the industry’s most advanced plant-wide process and production monitoring system
      - led Polaris,  the primary software platform(HMI, PLC and Production System)
      - **Special Contributions Award**, Signed by the then Global Vice President of Machines, 2007
    
      - **Top-Ranked Team Leader**, 2011
          - Husky's global employee survey conducted by [Kenexa][#KENEXA]
    
      - **10-Year Service Award**, 2015

- **Senior Application Engineer/Interim Team Leader, Memory Competence Center**
  - [STMicroelectronics][#ST], 9/2003 - 11/2005  
  - Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France) 

    1.  Being a startup member and interim leader for the Serial Non-Volatile Memory & Embedded System Solutions Team, assumed the responsibility to **explore and build** the connection with the R&D Division back in France, and maintained the effective communication through trustworthy relationship.
    2.  Daily work included
       - electronic Schematic/PCB layout, 
       - embedded/desktop software/firmware coding 
       - application notes authoring 
    3.  Spawned  with French/Shanghai colleagues an startup [Dediprog Inc.][#DP] based on my work

- **Electronic Design Engineer, China Design Center**
  - [Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003  

  - Reported to Manager of China Design Center(French Expat)  

    Worked closely with the lab technicians, providing premium technical support for local joint ventures on industrial control&automation(ICA) products&solutions, including

    - electrical/electronic localization design
      ​    - Authored and conducted homologation tests for IEC/GB standards compliance for local joined ventures products

    - development of lab equipments / utilities      ​

- **Researcher, HIT Research Institute of Electrical Apparatus **
  - [Harbin Institute of Technology][#HIT], 4/1998 - 3/2002  
  - Reported to Head of Institute / Mentor Professor of My Postgraduate Study  

    This was a technical center/lab, founded&led by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.
 ​

    **Most notably, the following systems I developed were sold, having generated sales of over 1 million RMB.**

      - The Electronic Ballast Test Platform

      - The Low voltage Apparatus Automatic Maintenance Platform

    **Both involved collection and comprehensive analysis of the Voltage/Current such as such as THD, Power factors etc. which is also one of the core features of a typical power meter** 


## **Education**

- **Master**, _summa cum laude_
  - **Electrical Engineering**, 09/1999 - 03/2002
  - [Harbin Institute of Technology][#HIT], Harbin


- **Bachelor**, _summa cum laude_
  - **Electrical Engineering**, 09/1995 - 07/1999
  - [Harbin Institute of Technology][#HIT], Harbin


- **Paper**

  - 2 papers published in pursuit of the Master degree
    - Zhai Guofu, Tan Zhi, et al. The development of integral testing equipment for electronic ballasts and inverters used in trains. *Low Voltage Apparatus* 
    - Zhai Guofu, Tan Zhi, et al. Research on a test system of performance&reliability of rolling-stock circuit breakers&contactors. *The 8th International Low Voltage Apparatus Reliability Conference*


- **Awards etc.**
  - Awardee(multiple times), Award of People's Scholarship, 1995 - 1999
  - Member, Students' Union, College of Electrical Engineering and Computer Science, 1996 - 1997
  - Member, Students' Union, Harbin Institute of Technology, 1995 - 1996
  - Volunteer, the 8th International Conference for Northern Cities, Harbin, 1/1998

## **Languages**

- Chinese: Native
- English: Fluent

## **Additional Information**

- **When Not At Work**

  - **Family**
  - Reading
  - Running/Half-Marathon/Cycling
  - Travel


- **Web Pages**

  -  [CV in English](https://ttan.netlify.com/cv-en)(**this document**)
  -  [CV in Chinese](https://ttan.netlify.com/cv-zh)
  -  [Feature Projects in English](https://ttan.netlify.com/projects-en)
  -  [Feature Projects in Chinese](https://ttan.netlify.com/projects-zh)

[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/
