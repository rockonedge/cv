---
language: zh
1: ['谭 智']
2: ['求职目标', '职业经历', '教育', '语言能力', '培训', '其它']
3: ['部门主管/创立者，全球研发工程部 &middot; <small>2005.11 - 2016.12</small>', '高级应用工程师/代理主管，存储器创新应用中心 &middot; <small>2003.9 - 2005.11</small>', '电子设计工程师，中国研发中心 &middot; <small>2002.4 - 2003.8</small>', '科研人员，哈尔滨工业大学车辆电器研究所 &middot; <small>1998.4 - 2002.3</small>']
4: ['主要职责', '主要技术内容', '部分项目', '奖励', '部分项目', '部分项目', '部分项目']
category: cv
---
<small>**(+86)135-248-26926 &middot; [tom.tan@live.com](mailto:tom.tan@live.com) &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **研发管理 &middot; 工业4.0 &middot; 软件 &middot; 固件 &middot; 硬件 &middot; 云计算 &middot; 数据科学 &middot; IoT**</small>

# **谭 智**

>- 11+ 年全球项目和团队管理
>- 15+ 顶尖跨国公司研发经历
>- 19+ 年工业软件，固件，硬件系统开发


>- 跨国创业高科技电子公司( Dediprog Inc.)   
>- 跨国生产工厂建立，并首次盈利中获__杰出贡献嘉奖__                          
>- 创建领导全球研发工程团队 11 年 


>- 职业生涯贯穿工业自动化，半导体电子和机械制造
>- 长期与生产，销售，服务部门紧密合作

>- 自我驱动，渴望挑战
>- 善于多文化环境下沟通合作


>- 自动化测量，工业实时驱动控制，半导体电子软/硬件系统研发
>- 熟悉电气/电子设计(Schematic & PCB) 调试,仿真
>- 熟悉 Windows/Linux, 包括桌面，服务器，嵌入式以及 Azure 云计算软件开发
>- 熟练使用 C++, Python, C#, JavaScript, Go, Matlab 等语言


>- 定期捐赠[联合国儿童基金( UNICEF )](http://www.unicef.cn)助学西部儿童

## **求职目标**

- 研发总监／高级经理
    - 团队创建，管理
    - 工业4.0/工业互联网（IIoT）/物联网 （IoT）研发
    - 云端，桌面，嵌入式/移动设备软件研发


## **职业经历**

### 部门主管/创立者，全球研发工程部 &middot; <small>2005.11 - 2016.12</small>
**Team Leader/Founder，Global Development Engineering Department**

**汇报对象：全球研发工程部总监， 加拿大总部**

**[赫斯基注塑系统（上海）有限公司](http://www.husky.co)**

#### 奖励

- **杰出贡献奖**， 2007 年
    - 由时任全球机器事业部副总裁（VP of Machines）签署， 以奖励对上海工厂成立作出的杰出贡献
- **优秀团队领导（排名全球名列前茅）**，  2011
    - 知名人力资源机构肯耐珂萨（Kenexa）受聘对 Husky 全球员工满意度调查匿名调查报告
- **5 年服务奖**， 2010 年
- **10 年服务奖**， 2015 年

#### 主要职责
11 年来作为部门创始人和本地区最高研发领导，直接向加拿大总部研发总监和VP负责，赢得充分信任授权，享有一切本地团队事务最终决定权。

__因团队在项目进度，质量，国际协作上表现杰出，多次作为模范团队受邀分享国际工程，团队组织和领导经验__。

在第 1 年左右的时间里，助力上海注塑机工厂建成，并提前实现首次100万美元盈利。并因此获得__全球机器制造部门 VP 签发　“杰出贡献奖”__。

负责控制系统（软件，电气）， 单枪匹马从零开始搭建生产系统（ Production System）基础设施软件，　成功组建，生产，装配，测试和工程师团队。

随后 10 多年里，再筹建并领导了加拿大总部以外迄今唯一的研发工程部门，频繁于上海和加拿大两地工作，与加拿大机械设计部门紧密合作，从事控制系统核心开发（软件，电气，电子）。在一年左右, 在总部提议下团队编制从原定 5 人扩至 12人。

每天通过电子邮件，电话视频会议以及人员互访与加拿大总部的研发团队同步协作，以英文为日常工作语言。主导开发的实时系统，遍及全球２４小时运行的 1000 余台注塑系统。行业内第一款工厂监控 SCADA 系统,　年销售额　300 万美元以上。


日常职责包括

1.  组建上海研发团队，管理日常运作，承担全球合作开发。
1.  编制与控制预算开支，团队建设与人才培养等。
1.  与加拿大总部研发团队合作，协调开发计划与进度，设计原则，工程标准与质量。
1.  探索实践跨国研发团队协作（Global Engineering）的高效管理方式和方法。
1.  跟踪评估 IT 新软件开发技术理念和方法，持续优化自动化控制系统。
1.  技术支持市场，销售，客服部门，并根据反馈和建议研发产品功能。
1.  领导探索开发本行业自动化解决方案（控制系统软硬件为主），比如注塑机实时控制系统(MES)，工厂监控（SCADA）系统
1.  领导维护，持续升级 20 多年历史的生产软件系统（Production System），　确保加拿大，卢森堡，美国，上海各地工厂之间，　以及研发，生产，客户现场，　客服支持部门之间的高效　DevOps 流程。

#### 主要技术内容

过去11年里， 率领上海团队启动或参与各类系统研发。 

- 从 2014年左右，开始结合亚马逊 AWS 和微软 Windows Azure 研究实施

    - 基于虚拟化技术 Vagrant 和 Docker 的 DevOps 等数字化转型(Digital Transformation)
    - 云计算 / 工业4.0 / 物联网（IoT）解决方案， 比如 AWS IoT 和 Azure IoT Hub。
    - 数据科学(机器学习)(基于 Python / R)

- 主要产品包括

    - 过去的十年推出的所有**注塑机**，
    - 部分热流道**控制器**，
    - **系统整合**解决方案等

- 主要技术包括

    - 人机交互（HMI）

        包括图形界面，实时/历史数据库，权限管理，SPC/SQC算法等。

    - 机器设备监控，与运动控制(含机器人)

        基于**工业互联网协议 EtherCAT** 和 Beckhoff/TWinCAT 控制器的温度采集，压力感应， 伺服电机驱动，实时运动控制，生产监控（SCADA）开发相关控制驱动软件等。

    - 企业生产监控系统(SCADA/MES)

        专注于注塑行业，监控采集企业单车间或跨地域工厂的所有机器（赫斯基和非赫斯基）运行，实时采集与控制周期参数，以及工班人员，能源消耗，物料分配安排， 报表生成等, 通过各种标准软件协议和硬件设备, 如 OPC(OLE for Process Control) 等，架起机器*生产车间* 与企业上层 IT 系统 如 ERP 等第三方系统的桥梁。

    - 第三方自动化设备（机器人，干燥剂，　传送带等）系统集成

        通过各种工业接口， 如数字IO(GPIO), RS485/RS232，工业以太网E(thernet) 等集成辅助设备， 包括 ABB 机器人，Plastic Systems 树脂干燥机， 除湿器等。

    - 也视情形涉及电气系统调试与机械设计辅助任务。


- 目标平台包括
    Linux 和 Windows 平台的传统应用， 如
    - 嵌入式(Embedded Systems)
    - 移动设备(Mobile Platforms)
    - 桌面(Desktop Workstations)
    - 服务器(Web Servers)
    - Amazon AWS 和 Microsoft Azure 云平台 


- 主要使用语言包括
    - C++
    - C#
    - Visual Basic
    - Python
    - Ruby
    - JavaScript
    - PLC/IEC61131 programming languages 等。

 
- 工业总线接口/协议包括
    - EtherCAT/Realtime Ethernet
    - Profibus
    - Sercos
    - Euromap
    - CAN
    - SPI
    - USB
    - RS232/RS422/RS485 等。


#### 部分项目

此处仅简单列举。[**详情参阅在线描述**](https://ttan.netlify.com/cv/?src=projects-zh)

- 上海工厂启动
    - 引进了拿大总部工厂的生产支持系统 （Polaris）及流程
    - 招聘，培训了首批生产支持 （装配和测试）工程团队
- 建立了第一个全球研发工程团队， 全部由硕士研究生以上学历组成。
- 领导开发用于所有赫斯基注塑机的 Polaris 软件平台， 适用于 Hylectric， HyPET， HyPET HPP， HyCAP， HyperSync， Barrier等机型， 包括
    - Polaris HMI（人机界面）， 
        - 维护性开发基于 COM/VB6 平台HMI
        - 为新一代的注塑机重新架构和实现了基于 .NET/C# 平台的HMI
        - 十多年来，保持上述平台按季度/半年定期发布
    - Polaris PLC，基于 TwinCAT/CODESYS 技术的 PLC 编程以实现注射成型的实时控制相关算法，例如
        - 伺服驱动(Servo)
        - 机器人控制等。
    - Polaris Production System， 此系统是内部各部门 （研发， 生产，售后支持服务等）都协同公作的公共 SDLC（Software Development Life Cycle） 平台
- 领导 HyperSync 注塑机系列工业 4.0 相关特性，获得 [Ringier Technology Innovation Award（荣格技术创新大奖）][#RINGIER]。        
- 领导开发统计过程控制（SPC）及 统计质量控制 （SQC） 模块
- 领导开发多语言翻译管理系统 （Translation Website） 的开发，简化软件开发中的全球化支持（22+语言）
- 领导开发基于 RuBee（IEEE 1902.1） 无线通讯技术的嵌入式模具无线自动识别(Mold ID)工业 IoT 解决方案
- 发起扁鹊（Bianque）项目数字化转型(digital transformation)，融合 Azure IoT Hub， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术的DevOps系统。
- 领导[收购整合 Moldflow 制造部门][#BUYMF]的法国与美国团队资产， 并后续支持既有客户
    - 基于 Windows Server 采用 C++/Delphi/Java 开发的工厂过程监控系统系统Celltrack，Shotscope
    - 基于 Linux/Fedora 采用 C++/QT/MySQL 开发的热流道温度控制器 Matrix
- 领导开发 Shotscope NX， 一套基于 web 技术的过程和生产监控自动化实时解决方案, 该软件作为公司自主开发的第一套SCADA软件，在2009芝加哥北美塑胶展（NPE Show 2009）上首次发布获得用户热烈欢迎, 年销售额逾３００万美元。
    
- 把开发流程模式提升 10 年左右(从90年代中期水平到２００６年时代)，　引入SCRUM/Test-Driven Development（TDD）等敏捷（Agile）开发实践， 把从 VSS 源代码版本控制模式 迁移到 TFS　的全方位软件开发周期管理（源代码，设计文档，测试，发布等）。        
- 领导上海团队成员参与了全新研制产品线中国区，越南，墨西哥等地的现场安装调试，客户包括朝日啤酒，可口可乐，利乐包装等。   
- 业余时间开发了团队与项目管理Dashboard　看板网站 Whiteboard，整合Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 UNC 映射盘等数据源信息，　基于 Python/Django， REST API, Micro Service, Dojo Toolkit 可视化，实时显示项目完成情况，工程师工作强度等，数量级的简化了流程，提高了日常沟通效率。 
  


[#RINGIER]: https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/


### 联合创始人/代理CTO，某电子行业跨国高科技创业公司 &middot; <small>2005.6 - 2009.6</small>
**Co-Founder/Interim CTO，A Global Start-up**

**汇报对象：CTO**

**[Dediprog Inc.](.)**

#### 主要职责

在 1 年左右，成功推出产品并获得 Intel 以色列研发中心，华硕，以及北欧等稳定客户后淡出，以咨询身份延续至２００９年中期，以专注于赫斯基的工作。

该创业应意法半导体 Flash 芯片研发中心工作的法国同事邀请，创业成立　SPI 总线　Flash 芯片程序器烧录套件，　并在上海和台湾分别注册成立公司。


该产品基于本人意法半导体的免费套件原型设计，重新架构技术栈（软件，固件，硬件），以友好易用的界面，丰富的功能和高速的烧录性能迅速打开市场。

该公司至今健康运作，目前年产值上千万。 


### 高级应用工程师/代理主管，存储器创新应用中心 &middot; <small>2003.9 - 2005.11</small>
**Senior Application Engineer/Interim Team Leader，Memory Competence Center**

**汇报对象：部门经理（上海）/研发经理（法国）**

**[意法半导体（上海）有限公司](http://www.st.com)**

#### 主要职责

作为新部门早期成立员工之一，履行团队成立职责，负责接触法国芯片研发中心，迅速获得信任，建立起有效关系，获邀赴法国产品培训，成立了本地 Serial Non-Volatile Memory & Embedded System  Solutions 团队。

作为公司 R&D 力量在亚太地区的延伸，代表 Serial Non-Volatile Memory Division 负责整个亚太地区应用开发，为本地现场应用工程师 （Field Application Engineer）提供技术和方案支持。

- 负责建立与维持与法国 R&D 部门的日常联系，培训安排等，
- 开发重心包括 
    - **Software/Firmware 开发**
    - **原理图与 PCB 电路板设计**

    具体而言

    - 协助 IC 设计师进行样片功能验证，包括信号质量，时序图等。
    - 基于MCU，DSP，ARM7/9 等 CPU 开发嵌入式应用参考设计 （包括软件代码编写与硬件验证平台），演示 EEPROM/FLASH 等的功能和应用场景。
        - 使用 Altium 设计电路原理图与 PCB 布线制作，调试，演示主要功能和应用领域。
        - 编写，发布可移植，高性能 C 语言芯片 Firmware 驱动代码到公司全球网站
    - 用英文撰写，发布应用指南（Application Notes）到公司全球网站
    - 各类部门自用工具软件，脚本编写，采用 C++, C#, Python 等

#### 部分项目

- 基于 uPSD （Intel8032 内核）开发维护 3 个系列串行 Flash 存储芯片的参考设计
    - M50xx FWH/LPC 接口 BIOS 存储芯片
    - M25Pxx  SPI 接口代码存储芯片系列
    - M45PExx SPI 接口数据存储芯片系列

    借助 ST 在相关芯片市场的支配地位， __用 C 语言编写的 M50xx 驱动代码运行于全球数千万台式 PC 的 BIOS 启动过程。M25/45系列代码运行于上亿台各类嵌入式，和移动设备（比如 Nokia ）的内存读写。__

- 基于 PC 和 ST7 （ Motorola 内核）， USB 接口的 Flash 编程烧录器（Serial Flash Programmer）
    - 基于以前 VB6 的未完成原型，完全以C++/汇编从 PC 端到 MCU 端重新设计
    - 被 ST 时期的法国同事商业化，成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列，并邀请我协助维护开发至SF300。 
    - 根据[其官网上的用户手册][#DEDIMANUAL], 时至今日其架构和功能几乎没有什么大的变动。



- 移植嵌入式操作系统到 STR71x
    - uC/OS II
    - uCLinux


- 开发了 Anatidae 项目管理软件跟踪同步上海，意大利，法国，捷克之间的项目进度。
    - 基于 MFC/STL/BOOST，XML 与 ADO 
    - 约5万行 C++ 代码 

[#DP]: http://www.dediprog.com/

[#DEDIMANUAL]: http://www.dediprog.com/save/84.pdf/to/dp_SF%20User%20Manual_EN_V6.7.pdf

### 电子设计工程师，中国研发中心 &middot; <small>2002.4 - 2003.8</small>
**Electronic Design Engineer，China Design Center**

**汇报对象：中国研发中心经理（法国人，由总部研发中心委派）**

**[施耐德电气 （中国）投资有限公司](http://www.schneider-electric.cn/zh/)**

#### 主要职责

作为独资公司的研发力量，以__研发中心实验室__和本地区10多家__合资工厂__为中心，

- 独立牵头完成基于 PLC 的自动应急电源切换系统新产品设计，前后 3 台原型设计和相关成本核算，填补市场产品空白，比现有解决方案（组合不同厂家元件）节约 1/3 成本。
- 协同设计了基于 MCU 和 PID 算法的恒温试验控制箱，用于电路断路器/接触器/PLC 高温老化测试，提供恒定温度高达 75&deg;
- 为实验室开发了一套基于关系数据库的国产化试验管理软件，用于实验室数据分析，采用了 VB6/C++ 以 及 MATLAB
- 改进小型空气断路器 ASIC 电路设计
    - VC65
    - DPN Vigi
- 独立完成从波兰工厂引进开关电源的前期研究报告，包括
    - 设计了各类性能，可靠性实验，用以与本地品牌对比
    - 逆向工程本地品牌，完成成本分析
- Compact NS 断路器认证实验
- 协作完成工业用 LED 光报警灯设计

日常职责包括：

- 本地化产品研发
- 投产产品故障分析等高级技术支持
- 着重于工控自动化（ICA）产品的电子设计改进和创新，包括

    - 从全球其他市场引进产品和国产化设计
    - 中国市场特有的新产品原型设计
    - 为合资工厂提供故障分析高级技术支持， 统计检验
    - 根据 IEC/GB 相关标准对新产品 / 样品进行预试验，并起草新产品的检验/验收试验方案及标准
    - 提交各类所需实验数据， 协助合资工厂向主管机构（比如上海电科所）申请各类本地化准入认证
    - 为实验室设计开发，选购软/硬件设备/工具
    - 对实验室操作人员技术培训

### 科研人员，哈尔滨工业大学车辆电器研究所／实验室 &middot; <small>1998.4 - 2002.3</small>
**Researcher，HIT Research Institute of Electrical Apparatus**

**汇报对象：研究所所长/研究生导师**

**[哈尔滨工业大学](http://www.hit.edu.cn/)**

#### 主要职责


期间独立主导软，硬件研发的自动化系统，被哈尔滨铁路局试用验收后，指定全局推广，　累计销售 10+ 套, 逾百万元人民币。相关理论研究多次发表国家级论文，获得省部级科技奖项。并分别获得__优秀学士，硕士毕业论文__。　

**此段经历奠定了我迄今为止的专注于科学研究与开发（R&D）技术基础和毕生不懈追求。**

从大三(1998年)自荐进入实验室开始，贯穿研究生院学习期间，一直到取得硕士学位毕业离校(2002年)，大约 4 年间，一直参与实验室各类大小电气自动化设备开发及理论研发项目，承担各类大小职责。

哈尔滨工业大学校级研究所（实验室），主要成员包括教师及在读研究生，与哈尔滨铁路局合作，依托全国“铁路电器现代化”战略，研发各类铁路车辆电器自动化研发项目。

我的本科及研究生学位论文均取材于期间负责的有关项目。

参与的研究方向和开发包括

- 电机与电器
    - 交直流电动机，发电机，变压器以及马达驱动 （罗克韦尔自动化（Rockwell）实验室）
    - 低压电器(继电器，接触器和断路器)
- 计算机控制系统
    - 直接数字控制系统 
    - 离散控制系统 
    - 现场总线(Fieldbus)控制系统 
    - 单片机嵌入式系统
    －实时嵌入式的系统（RTOS）和嵌入式应用程序开发
- 实时嵌入式的系统（RTOS）和嵌入式应用程序开发
- 各类电气电子接口电路与总线（串行，并行）协议的研究
- 实时控制及测量方法和电器可靠性(FMEA)
- 工业控制与自动化设备研究与开发
    - 基于工控机（IPC）和单片机（英特尔 MCS-51 系列 MCU）
    - 人机界面编写及数据实时采集，处理和管理软件开发，采用Visual Basic/C++
    - 驱动程序开发，采用C/C++/汇编
    - 硬件系统设的电气原理图及 PCB 设计，采用 Protel
    - 电气/电路仿真，采用 pSPICE, MATLAB/SIMULINK， ANSYS等
    - 机械设计（操作平台，凸轮设计），采用AutoCAD


#### 部分项目

受哈尔滨铁路局委托，先后**独立**成功开发

- **电子镇流器自动化综合检测系统**
    - 并被广泛推广于齐齐哈尔等 7 个下辖个车辆段。
    - 《低压电器》发表相关论文*《铁路客车电子镇流器逆变器综合试验台的研制》*
    

- **低压电器自动化综合检测系统**
    - 并被广泛推广于 7　个下辖个车辆段。
    - 经黑龙江省科委专家组技术评审鉴定“居于国内领先”。
    - 相关论文发表于*第八届国际电器可靠性会议*。
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 黑龙江省教育厅 科技进步一等奖， 2002 年 12 月
    - 黑龙江省科技厅 科技进步二等奖， 2003 年 5 月

**此两系统分别销售 10+ 套, 累计销售逾百万元人民币。**

从前期立项到后期交付，以及人员培训，一力承担了除前一项目的软件开发以外的全部系统架构及硬件设计。

软件设计

- 基于 Windows, 运行于工控机（IPC） 

硬件设计涉及

- ISA I/O，PCI A/D 等计算机板卡设计
- 电源及负载的二进制编码设计与控制
- EMI，谐波干扰处理等以及
- 试验台的机械设计。

## **教育**

- **硕士**
    - 毕业答辩成绩: **优异**
    - 1999 年 9 月 - 2002 年 3 月
    - 电气工程系，计算机与电气工程学院
    - [哈尔滨工业大学](http://www.hit.edu.cn/)


- **学士**
    - 毕业答辩成绩: **优异**
    - 1995 年 9 月 - 1999 年 7 月
    - 电气工程系，计算机与电气工程学院
    - [哈尔滨工业大学](http://www.hit.edu.cn/)


- **著作/论文**

    研究生攻读期间在国家级期刊与国际会议发表2 篇学术论文。

    - 翟国富, 谭智, 陈 伟. 铁路客车电子镇流器逆变器综合试验台的研制. 低压电器
    - 翟国富, 谭智等. 铁路客车断路器接触器性能及可靠性测试系统的研究. 第8届国际电器可靠性会议论文集
    - 翻译导师论文供外刊发表


- **荣誉**
    - 获奖者 （多次），人民奖学金，         1995 年 - 1999 年
    - 委员，电气工程和计算机学院 学生会，    1996 年 - 1997 年
    - 委员，哈尔滨工业大学 学生会，          1995 年 - 1996 年
    - 志愿者，第八届国际北方城市会议，哈尔滨，1998 年 1 月

    - 大学英语四级（CET4），  1996 年 7 月（大学一年级 下学期）
    - 大学英语六级（CET6），  1997 年 1 月（大学二年级 上学期）

    - 全国计算机等级二级，    1998 年 4 月

## **语言能力**

- 汉语︰ 母语
- 英语︰ 流利

## **培训**

- **The 7 Habits of Highly Effective People Training Program**

    - 上海, 2008 年 3 月
    - 由 [Franklin Covey Co.][#FRANKLIN] 培训

- **The 4 Disciplines of Execution: Manager Certification**

    - 上海, 2007 年 3 月
    - 由 [Franklin Covey Co.][#FRANKLIN] 培训

- **Team Leadership development Training Program**

    - Bolton(加拿大), 2006 年 1 月
    - 由 [Workplace Competence International Limited](https://www.linkedin.com/company/674972) 培训

-  **In-Fab Product training on EEPROM/SPI Memory Chips**
    - Rousset(法国), 2004 年 5 月

-  **剑桥商务英语（BEC）︰ 中级 和 高级**
    - 上海, 2002 年 - 2003 年
    - 由 [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan) 培训

- **Creative Thinking and Problem Solving**，
    - 上海, 2002 年 7 月
    - 由 [Key Consulting][#KEYCONSULTING] 培训

- **知识产权专利培训**，
    - 上海, 2002 年 6 月
    - 上海知识产权局

[#FRANKLIN]: http://www.franklincovey.com
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/

## **其它**

- 开源代码与社区参与

    - [Boost C++ Libraries](http://www.boost.org/)

      审阅 [Boost.Chrono][#BOOST_REVIEWER]，此库被收入 最新 C++ 标准库

    - [POCO C++ Libraries](http://pocoproject.org/)

      贡献[代码][#POCO_CONTRIBUTOR]

    - [The CodeProject](http://www.codeproject.com/)

      发表部分[文章及代码][#CP_AUTHOR]

    - [Github](https://github.com/rockonedege) 

      维护部分[开源代码][#GITHUB]

[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#GITHUB]: https://github.com/rockonedege
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html


- **闲暇**
    - 家庭活动
    - 阅读
    - 跑步/半程马拉松
    - 骑行
    - 旅行


- **网页**
  - [个人主页](https://ttan.netlify.com/)
  - [Linkedin](https://www.linkedin.com/in/zhitan/)



