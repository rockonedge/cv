---
language: en
3: ['Electronic Design Engineer, China Design Center']
category: cv
---
### **Electronic Design Engineer, China Design Center**

- **[Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003**
- **Reported to Manager of China Design Center(French Expat)**
  
  As one of the regional corporate R&D staff, working closely with the lab technicians, with a focus on improving and innovating industrial control&automation(ICA) products&solutions, including

  - electrical/electronic products design
    - localization of global products
    - new products targeting Chinese market only
  - premium technical support for joint ventures(JVs)
    - Homologation tests for JVs
  - hardware/software development of lab equipments/utilities

 Achievements included

  - Auto-Transfer System(ATS), an automatic switches on/off between two independent power supplies in no time in case of emergency for sectors like hospitals
    - based on NS Compact circuit breakers and NEZA PLC.
    - from the scratch to 3 full-functional prototypes
    - 1/3 cost saving from existing market solutions
    - submission to Chinese Management Committee(CMC) for final production investment evaluation 
  - Co-design of a thermostat for IEC regulated high-temperature(up to 75&#8451;) aging tests
  - Developed software data analytics utilities for experiments in VB6/C++/MATLAB
  - Localization design of ASICs for circuit breakers
    - VC65
    - DPN Vigi
  - preliminary investigation of local manufacturing of switching power supplies from Poland, including reports on performance/reliability/cost comparison with local brands.
  - Compact NS circuit breakers homologation tests for China Certification.
  - Development of industrial-strength LED alarms
