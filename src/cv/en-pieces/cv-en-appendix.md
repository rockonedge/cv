---
language: en
2: ['Appendix']
category: cv
---
<hr class="page_break" />

## **Appendix**


- ![](./pics/husky-contribution.jpg)
- ![](./pics/husky-leadership.jpg)
- ![](./pics/kenexa-1.jpg)
- ![](./pics/kenexa-2.jpg)
- ![](./pics/mbti.istj-2.jpg)
- ![](./pics/unicef.jpg)
- ![](./pics/shanghai-half-marathon-2008.jpg)

