---
language: en
3: ['Senior Application Engineer/Interim Team Leader, Memory Competence Center']
category: cv
---
### **Senior Application Engineer/Interim Team Leader, Memory Competence Center**

- **[STMicroelectronics][#ST], 9/2003 - 11/2005**
- **Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France)**

  A technical liaison connecting Central R&D IC designers and A/P Field Application Engineering, involving 
  
  - electronic Schematic/PCB layout, 
  - software/firmware coding  
  - application notes authoring 
  
  for embedded/desktop systems as requested/required.

  Being a startup member and interim leader for the Serial Non-Volatile Memory & Embedded System Solutions Team, assumed the responsibility to **explore and build** the connection with the R&D Division back in France, and maintained the effective communication through trustworthy relationship.

  More specifically,

  - Assisted IC designers in France
    - IC chip verification of prototyping samples to ensure all specifications are met according to datasheets, e.g Signal-to-noise ratio(SNR), signal glitches etc.
    - authoring in English application notes for each product family

  - Software/hardware reference designs showcasing application scenarios
    - PCB design with Altium
    - Firmware driver code(in Assembly/C) for
      - M50 Firmware Hub/LPC BIOS Memory chips
      - M25PE/M25P/M45PE SPI code/data flash&EEPROM memory chips
    - Porting popular OSes to STR71x(uCLinux, µC/OS-II)
    - USB based memory chips programming kit(in C/C++/Assembly) for PC/Windows

  - Development of Anatidae, an in-house tracking system to synchronize projects between Shanghai, Czech, Italy and France, using MFC/STL/BOOST.

  The driver code written for M50xx PC BIOS chips were running on hundreds of millions of PCs, and even more devices(servers, workstations and embedded) for M25/M45 serial flash code thanks to ST being the major vendor on the market.

  Being the best tool in class, the aforementioned memory programmer kit was later licensed by my then-colleagues for a startup company named [Dediprog Inc.][#DP], branded as SF100/SF200/SF300 series. I was the _interim CTO_ during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards. 


### **Co-Founder/Interim CTO, A Global Start-up**
- **[Dediprog Inc.][#DP], 6/2005 - 6/2009**
- **Reported to CEO**


Together with colleagues from ST France and Shanghai, and with license from ST, started up a company dedicated to flash memory programming&verification toolkits.

In charge of all things technology, esp. software both on PC and MCU. 

- Built a full-blown memory chip programmer
  - based on the application kit(H/W & F/W) I developed in ST
  - implemented consumer-focused features, e.g.
      - non-ST chips, e.g. Atmel, SST
      - feature for R&D and mass production scenarios
  - Development
      - Desktop: Re-Architecture
          - Overhauled GUI, USB driver and database on PC with C++
      - Embedded: Enhanced F/W ST7 (Assembly for a DTC co-processor)
- Serving key customers, e.g Intel's R&D Engineers at Israel

After the successful startup and healthy running, transfered my role from a partner to consultant and remained so till 2009. 

  