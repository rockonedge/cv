---
language: en
2: ['Languages', 'Skills']
category: cv
---
## **Languages**

- Chinese: Native
- English: Fluent

## **Skills**

- Management
  - successful startup of a hi-tech venture in Shanghai/Taiwan 
  - successful startup of global engineering teams in Shanghai
  - team & project management with high retention rate
  - Top-ranked team leader in a global employee survey by [Kenexa][#KENEXA]


- Technologies

  - Industrial Standards

    - OPC(OLE for Process Control)
    - EtherCAT/Realtime Ethernet
    - Sercos
    - Profibus
    - Euromap
    - CAN
    - SPI etc.

  - Hardware
    - Schematic & PCB design with Protel/Altium, 
      - A/D, D/A and I/O and other peripheral circuits
      - MCU(MCS51, ARM, ST7)
      - PC(ISA/PCI) architectures.
    - knowledge of Verilog HDL.
    - Simulation with Matlab, pSPICE
    - Industrial PC, PLC, softPLC(CodeSys/TwinCAT)
    - wide-range current power supply(1A -1200A) design


  - Software
    - Open Source software(OSS) contributor to 
        - [BOOST][#BOOST_REVIEWER], 
        - [POCO][#POCO_CONTRIBUTOR] 
        - [The Code Project][#CP_AUTHOR]
    - Programming Languages
        - frequent use of C++(esp. C++ 11/14/17), Python, C#, JavaScript/HTML/CSS
        - occasional Golang, R, Matlab, ruby, TypeScript etc.
    - Application
        - IC chip drivers
        - realtime motion control(e.g servo motor, robotics)
        - desktop GUI/CLI applications
        - server/web based enterprise LOB(line of business)
        - cloud based mobile/IoT solutions
    - Platforms
        - Windows desktop, Windows Embedded/Windows Mobile, Windows Server
        - Windows Azure, AWS
        - Linux(Ubuntu/Fedora)
        - uC/OS, eCos and uCLinux
    - Tools&Library
        - Microsoft Team Foundation System(TFS)/Microsoft Visual Studio
        - Keil, ARM RealView 
        - SQL Server, MongoDB, SQLite 
        - .NET, ASP.NET, WinForm, WPF, UWP
        - MFC/WTL, QT, STL/BOOST, ACE
        - Dojo Toolkit, AngularJS
        - NodeJS, Django, Flask 
        - Cordova, Xamarian
        - Git, Mercurial(HG), SVN
        - Vagrant, Ansible
    - Methodology
        - Object Oriented Programming(OOP), generic programming/metaprogramming(GP), functional programming 
        - Agile/SCRUM/TDD
