---
language: en
2: ['Trainings']
category: cv
---
## **Trainings**

- **The 7 Habits of Highly Effective People Training Program**
  - Shanghai, 3/2008
  - [Franklin Covey Co.](http://www.franklincovey.com)


- **The 4 Disciplines of Execution: Manager Certification**
  - Shanghai, 3/2007
  - [Franklin Covey Co.](http://www.franklincovey.com)


- **Team Leadership development Training Program**
  - Bolton(Canada), 01/2006
  - [Workplace Competence International Limited](https://www.linkedin.com/company/674972)


- **In-Fab Product training on EEPROM/Flash Memory Chips**
  - Rousset(France), 05/2004
  - Serial Flash Division, [STMicroelectronics][#ST]

- **Cambridge English: Business (BEC)**, _Vantage_ through _Higher_
  - Shanghai, 2002-2003
  - [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan)


- **Creative Thinking and Problem Solving**,
  - Shanghai, 07/2002
  - [Key Consulting][#KEYCONSULTING]


- **Intellectual Property & Patents**，
  - Shanghai, 06/2002
  - [Shanghai Intellectual Property Administration (SIPA)][#SIPA]

[#SIPA]: http://www.sipa.gov.cn/gb/zscq/zscqjel/index.html