---
language: en
2: ['Additional Information']
category: cv
---
## **Additional Information**

- **Contributions**
  - [Boost C++ Libraries](http://www.boost.org/)

    Reviewer of [Boost.Chrono][#BOOST_REVIEWER], a reference design of std::chrono in the standard C++ library.

  - [POCO C++ Libraries](http://pocoproject.org/)

    Contributed [Code][#POCO_CONTRIBUTOR] 

  - [The CodeProject](http://www.codeproject.com/)

    Authored [a few articals with code][#CP_AUTHOR]
  - [Github](https://github.com/rockonedege)
    Maintained [a few open source code](https://github.com/rockonedege)


- **When Not At Work**

  - **Family**
  - Reading
  - Running/Half-Marathon/Cycling
  - Travel

- **Web Pages**
  - [Personal Site](https://ttan.netlify.com/)
  - [Linkedin Profile](https://www.linkedin.com/in/zhitan/)


[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/

