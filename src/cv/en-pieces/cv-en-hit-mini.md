---
language: en
3: ['Researcher, HIT Research Institute of Electrical Apparatus ']
category: cv
---
### **Researcher, HIT Research Institute of Electrical Apparatus**
- **[Harbin Institute of Technology][#HIT], 4/1998 - 3/2002**
- **Reported to Head of Institute / Mentor Professor of My Postgraduate Study**

This was a technical center/lab, founded and headed by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.
​
**Major Achievements**

- The Electronic Ballast Test Platform
- The Low voltage Apparatus Automatic Maintenance Platform

**The above systems, completely my design, generated sales of over 1 million RMB.**
