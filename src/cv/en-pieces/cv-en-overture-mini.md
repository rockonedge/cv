---
language: en
1: ['Tom Tan']
2: ['Objective']
category: cv
---
**[tom.tan@live.com](mailto:tom.tan@live.com) &middot; (+86) 135-248-26926 &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **R&D Management**



# **Tom Tan**

>- 11+ years of Acknowledged Global Projects&People Leadership
>- 15+ years of Multi-sector R&D Career in World's top companies across Automation, Electronics and Machinery
>- 19+ years of Hands-on Software/Firmware/Hardware Development


>- Started up A Global Venture([Dediprog Inc.](https://www.dediprog.com/US)) 
>- Started up Global Development & Production Engineering Teams
>- [Acknowledged contribution](./pics/husky-contribution.jpg) to launch&profit of a global manufacturing factory  



>- Challenges Pursuer, Visionary and Open-minded
>- Deeply rooted in Science, Technology, Engineering and Math(STEM) Related
>- A Regular [UNICEF](http://www.unicef.cn) Donator


## **Objective**

A Senior Manager/Director position, preferably

- Engineering teams startup&leadership
- Industrial, Scientific, Medical(ISM) or Automotive sectors
- IoT/IIoT/Industry 4.0 innovations 
- A global company Based in Shanghai


