---
language: en
2: ['Additional Information']
category: cv
---
## **Additional Information**

- **Spare Time**
  - **Family**, Reading, Running Half-Marathon, Cycling, Travel


- **Web Pages**
  - [Personal Site](https://ttan.netlify.com/)
  - [Linkedin Profile](https://www.linkedin.com/in/zhitan/)

[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/


