---
language: en
2: ['Experience']
3: ['Team Leader/Founder, Global Development Engineering Department']
category: cv
---
## **Experience**
### **Founding Departmental Leader, Global Development Engineering Department(Shanghai)**

- **[Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016**
- **Reported to Director of Global Development Engineering(HQ in Canada)**

  **Major Achievements**

  - led the acquisition & merge of [Moldflow][#BUYMF] from US and France team(software only), with no loss of legacy customers
  - led [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), , the industry’s most advanced plant-wide process and production monitoring system with a yearly sales of $3M
  - led Polaris(HMI, PLC and Production System) development for 10+ years, shipped with 1000+ machines around the world running 7x24 
  - led the modernization of software development process from mid-1990 technology with Agile/SCRUN/TDD and TFS 

  - **Significant Contribution Award**, 2007
    - Signed by the then Global Vice President of Machines
    - In recognition of contributions during the startup and first profitable year of Husky's Shanghai Machines plant

  - **Top-Ranked Team Leader**, 2011
    - Husky's global employee survey conducted by [Kenexa][#KENEXA]


  - **10-Year Service Award**, 2015

  **Summary**
  
  Helped to have started up the manufacturing plant of injection molding machines at Shanghai so as to break even ahead of schedule with $1M profit in about 1 year, 
  
  During the process, in charge of the controls system, single handed and from the scratch,  built the production software infrastructure. Awarded a certificate of [**Significant Contribution** acknowledged by the Global Vice President of Machines](./pics/husky-contribution.jpg).

  Thereafter, started up the one-and-only offshore development engineering team of 5.

  Grew team size from 5 to 12 after 1 year at the proposal of Engineering VP due to exceptional team performance.

  During the next decade, working hand-in-hand with Dev Eng teams in the HQ daily, through email, audio/video conference calls and mutual visits, in support of Husky's business around the world. 

  By leading a widely an acknowledged model team with high retention, efficient global collaboration and quality deliverables, I was invited to share experience on global collaboration by other teams, such as Global Production.

  **As the founder&top regional leader of the team, ultimately responsible for Shanghai team with direct reports to the headquarter**. 
  
  Daily routines inluded

  - people management
    - talent acquisition&retention
    - team motivation/training etc.

  - project management
    - collaboration/prioritization of milestones with global/local engineering teams
    - engineering discipline/standards enforcement
    - regional R&D interface to production/service teams et al. 

  - evaluation and evangelism of new technologies and methodologies
  - budgets forecast&control

  
  Technically, Various levels of software development from real-time motion control to enterprise solutions, along with electronic/electrical/mechanical tasks had been accomplished under my leadership, covering

  - Human-Machine interfaces(HMI)
  - TWinCAT based motion control(PLC)
  - integration of 3rd auxiliary systems, e.g. ABB robot, Plastic Systems resin dryers, Altanium temperature/servo controllers
  - Manufacturing execution systems (MES) and manufacturing monitoring/management(SCADA) 


 for platforms including

  - Windows Embedded/Server
  - Linux(Fedora, Ubuntu)
  - Mobile(iPhone) 
  - Atmel MCUs
  - Microsoft Azure / Amazon AWS 


  with languages such as

  - C++
  - C#
  - Visual Basic
  - Python
  - Ruby
  - Java
  - JavaScript
  - IEC61131 PLC programming languages etc.

  and Industrial standards/protocols such as 
  
  - OPC(OLE for Process Control)
  - EtherCAT/Realtime Ethernet
  - Sercos
  - Profibus
  - Euromap
  - CAN
  - SPI etc.


  **Starting from 2014, focused on advocating Virtualization/DevOps/Cloud/Big Data/Mobile technologies inside the company, towards Industry 4.0/IIoT solutions, leveraging Iaas/Paas/SaaS offerings from Amazon AWS/ Microsoft Azure, e.g. AWS IoT, Azure IoT Hub etc.**


[**See Feature Projects >>**](https://ttan.netlify.com/cv/?src=projects-en)