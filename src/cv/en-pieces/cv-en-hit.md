---
language: en
3: ['Researcher, HIT Research Institute of Electrical Apparatus ']
category: cv
---
### **Researcher, HIT Research Institute of Electrical Apparatus**

- **[Harbin Institute of Technology][#HIT], 4/1998 - 3/2002**
- **Reported to Head of Institute / Mentor Professor of My Postgraduate Study**

  This was a technical center/lab, founded&led by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.

  My period in the lab started when I was a junior and volunteered to clean up desk(soldering iron etc.) and ended when I graduated, with a Master Degree of Electrical Engineering, from the postgraduate School and left for Shanghai.

  **It could never be emphasized enough what a firm technical foundation as well as long lasting interests in science and technology this 4 years laid for my later career till today.**

  Both my theses for the Bachelor&Master degrees and several published academic papers were inspired from my research and development activities in the lab.

  My interests and yields included

  - Research on industrial control and automation system
    - Electrical machines and Apparatus
      - transformers, generators and (servo) motors(Rockwell Automation brands)
      - relays, contactors and circuit breakers
    - Computer-based control&automation systems
      - Direct Digital Control System
      - Distributed Control System (DCS)
      - Fieldbus Control system (FCS)
    - real-time control & measurement methodology and reliability, e.g. FMEA
    - real-time embedded system (RTOS) and embedded application development
    - Research on electrical/electronic interface/bus protocols(ISA, PCI etc.)
  - Development of control & automation solutions
    - based on industrial PCs(IPC) and/or MCUs(Intel MCS-51 family)
    - HMI development in C++/Visual Basic
    - device driver development in Assembly/C/C++

    - Electrical schematic&PCB design with Protel, and simulation with pSPICE/MATLAB/SIMULINK
    - mechanical design(cam wheel based vibration platforms) with AutoCAD


  **Most notably, 10+ sets of the following sytems I developed were sold, having generated sales of over 1 million RMB.**

  - The Electronic Ballast Test Platform 
  - The Low voltage Apparatus Automatic Maintenance Platform


  **Awards**

  - Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
  - Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
  - An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*.