---
language: en
2: ['Education']
category: cv
---
## **Education**

- **Master**, _summa cum laude_
  - **Electrical Engineering**, 09/1999 - 03/2002
  - [Harbin Institute of Technology][#HIT], Harbin

- **Bachelor**, _summa cum laude_
  - **Electrical Engineering**, 09/1995 - 07/1999
  - [Harbin Institute of Technology][#HIT], Harbin

- **Academic Papers**

    - Zhai Guofu, Tan Zhi, et al. The development of integral testing equipment for electronic ballasts and inverters used in trains. *Low Voltage Apparatus* 
    - Zhai Guofu, Tan Zhi, et al. Research on a test system of performance&reliability of rolling-stock circuit breakers&contactors. *The 8th International Low Voltage Apparatus Reliability Conference*


- **Awards & Affiliations**
  - Awardee of People's Scholarship, 1995 - 1999
  - Standing Committee of Students' Union(College), College of Electrical Engineering and Computer Science, 1996 - 1997
  - Standing Committee of Students' Union(University), Harbin Institute of Technology, 1995 - 1996
  - Volunteer, the 8th International Conference for Northern Cities, Harbin, 1/1998








