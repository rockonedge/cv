---
language: en
2: ['Summary']
category: cv
---
## **Summary**

**Personalities**

- Great self-motivation
- Being visionary and open-minded
- Enthusiastic pursuer of challenges
- Lead-by-example leadership style


**General Interests**

- any system/device involving electrical/electronic/computer engineering, 
- the industrial trend towards an ever more digital and software-defined future.


**Leadership Experience**

- successful startup of a hi-tech venture 
- **11+ years of global engineering team management**
  - quick startup of a high-profile team
  - high talent retention
  - high quality projects & accomplishments
  - widely acknowledged leadership from within and outside of the team


**Technical Experience**

- **18+ R&D-centric years**
  - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
  - programming with C++/Python/C#/JavaScript/Go/Matlab 
  - schematic&PCB design and simulation with Protel/Altium/Matlab/PSpice
  - Industrial PC, PLC and MCU


- **15+ professional years of service in World's top companies**
  - industrial automation, electronics& machinery manufacturing
  - cross-team collaboration with production, pre/after sales&service teams
