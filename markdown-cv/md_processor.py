import os
from datetime import date, datetime

import re
import concurrent.futures

import logging
logger = logging.getLogger('.'.join(['cv', __name__]))

info = logger.info


from toc import TOC, TocFormatter
from json_settings import JsonSettings
from yaml_helper import YamlHelper

from pathlib import Path


license_footer = f'\n-----\n<small>Generated from <a href="http://daringfireball.net/projects/markdown/">Markdown</a> text using my open source tool <a href="https://github.com/rockonedege/markdown-cv">markdown-cv</a>. Last updated at {date.today()}. </small>'

#
# paths of sources
#


class IORoots:
    template = Path(__file__).parent / 'template'
    md = template.parent / 'markdown'

    out_root = Path(__file__).parent / 'release'
    pdf_root = out_root / 'pdf'
    style_root = out_root / 'css'
    md_root = out_root / 'md'
    toc_root = out_root / 'toc'


#
# path of output, make sure it exists
#

for d in [IORoots.out_root,
          IORoots.pdf_root,
          IORoots.style_root,
          IORoots.toc_root,
          IORoots.md_root]:
    d.mkdir(exist_ok=True)


def lstrip_list(lines):
    for i, line in enumerate(lines):
        if line.strip():
            return lines[i:]
    return 0


def append_header(text, yml):
    header, body = YamlHelper.split_md(text)
    header.update(yml)
    return YamlHelper.yaml_to_string(header) + body


def read_all_md():
    def _read_md(p):
        content = Path(p).read_text(encoding='utf-8').strip()
        reh, b = YamlHelper.split_md(content)
        return b.split('\n')

    return {f.name: _read_md(f) for f in IORoots.md.glob('**/*.md')}

def json_dump(data, to):
  import json

#   to = dump /f'{name}.json'
  to.write_text(json.dumps(data, ensure_ascii=False), encoding='utf-8')

  print(f'dumped {to}')

class Context():
    files_md = read_all_md()

    settings = JsonSettings.read()
    rule_md = settings['md-merge-rules']


def generate_extra_md():

    for rule in Context.rule_md:
        contents = []
        for idx, keyword in enumerate(rule['from']['src']):

            to = rule['to']

            if idx > 0 and rule['from']['insert-hr']:
                contents += [
                    f'\n\n<hr class="page_break" id="{to}-{idx}" />\n\n']

            contents += ['\n\n']

            contents += Context.files_md[keyword]

        to_md = to + '.md'

        out = IORoots.md / to_md
        out.write_text(
            '\n'.join(lstrip_list(contents)),
            encoding='utf-8')
        logger.info(f'done generating {out}')

        Context.files_md[to_md] = contents  # refresh the cache

        decorate(rule)


def decorate(rule):
    """ Generate bootswatch base html files.
    - http://bootswatch.com/
    inspiried by
    - https://github.com/arturadib/strapdown
    - http://strapdownjs.com/
    """
    name = rule['to']

    def add_toc(name):

        what = '\n'.join(Context.files_md[name + '.md'])

        if 'pro' in name.lower() and not what[0].lstrip().startswith('- '):
            toc = TOC(what)
            what = toc.with_toc

            toc_md = IORoots.toc_root / (name + '.toc.md')
            toc_md.write_text(TocFormatter.as_md(
                toc.toc_body[0], f'https://ttan.netlify.com/cv?src={name}'), encoding='utf-8')
            info(f'{toc_md} generated.')

            dump = Path(__file__).absolute().parent.parent.parent / f'vue-landing/src/assets/json/{name}-toc.json'
            json_dump({i['title']:i['anchor'] for i in toc.toc_body[0]}, dump)

        return '\n'.join([what, license_footer])

    def get_banner_header():

        template = rule["template"]

        navbar = template['navbar']
        brand = navbar.get('brand', '')
        nav = ' '.join(navbar.get('nav', ''))
        text = navbar.get('text', '')
        lastupdate = f'{date.today()}'

        to = rule['to']
        # home_link = f'<a class="btn-link" href="/"><i class="fas fa-home"></i></a>'
        pdf_link = f'<a class="btn-link" href="./pdf/{to}.pdf">PDF</a>'
        docx_link = f'<a class="btn-link" href="./docx/{to}.docx">WORD</a>'
        slides_link = f'<a class="btn-link" href="https://tslides.netlify.com/cv">PPT</a>'
        links = ' &middot; '.join([slides_link, pdf_link, docx_link])

        navbar_text = links, text, lastupdate
        navbar_text = [e for e in navbar_text if e.strip()]

        yaml_banner = {'banner': dict(
            # http://stackoverflow.com/a/4869782
            title=re.sub('<[^<]+?>', '', brand),
            time=datetime.now(),
            brand=brand,
            nav=nav,
            lastupdate=' &middot; '.join(navbar_text))
        }
        return yaml_banner

    header = YamlHelper.yaml_to_string(get_banner_header())
    md = YamlHelper.strip_yaml(add_toc(name))

    (IORoots.md_root/(name + '.md')).write_text(header + md, encoding='utf-8')


def generate_index():

    items = [f for f in sorted(IORoots.md_root.glob('*.md'))]
    items = [f'- [{f.stem}](?src={f.stem})' for f in items]

    (IORoots.md_root / 'index.md').write_text('\n'.join(items), encoding='utf-8')


def go():
    generate_extra_md()
    generate_index()


if __name__ == "__main__":
    go()
