import re

class TocFormatter():

    @staticmethod
    def as_md_list(ttt):
        # Compute min header level so we can offset output to be flush with
        # left edge
        min_header_level = min(ttt, key=lambda e: e['level'])['level']

        toc_lines =[]
        for r in ttt:
            header_level = r['level']
            spaces = '  ' * (header_level - min_header_level)
            toc_line = f"{spaces}{TOC.LIST_PREFIX} [{r['title']}]({r['anchor']})"
            toc_lines.append(toc_line)
        
        return toc_lines
    @staticmethod
    def as_md(ttt, prefix = False):

        lines = '\n'.join(TocFormatter.as_md_list(ttt))
        return lines.replace('#', f'{prefix}#') if prefix else lines

    @staticmethod
    def as_bs4_columns(ttt):
        """with bs4 column styles"""
        md = TocFormatter.as_md_list(ttt)

        min_header_level = ttt[0]['level']
        break_line = 0
        for i , line in enumerate(ttt[1:]):
            if line['level'] == min_header_level:
                break_line = i + 1
                break

        first_col = '\n'.join(md[:break_line])
        second_col = '\n'.join(md[break_line:])
        return f"""<div class="container">
<div class="row">
<div class="col">
{first_col}
</div>
<div class="col">
<p></p>
{second_col}
</div>
</div>
</div>"""

    @staticmethod
    def as_bs_navbar(ttt):
        #  "<li class='nav-item active'><a class='nav-link' href='#profile-en-1'>Husky</a></li>",
        # dict(level=header_level, title=title, anchor=f'#{a_id}'


        menus = []
        menu_items =[]
        min_header_level = min(ttt, key=lambda e: e['level'])['level']
        for r in [t for t in ttt if t['level'] - min_header_level < 2][::-1]:
            level = r['level']
            anchor = r['anchor']
            title = r['title']

            if level == min_header_level:
                menus.append(f''' <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {title}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        {''.join(menu_items)}
                        </div>
                    </li>''')
                menu_items.clear()
            else:
                menu_items.append(f'<a class="dropdown-item" href="{anchor}">{title}</a>')
        return ''.join(menus)

class TOC():

    LIST_PREFIX = "-"
    # TOC.LIST_PREFIX = "*"

    HEADER_LINE_RE = re.compile(r"^(#+)\s*(.*?)\s*(#+$|$)", re.IGNORECASE)
    HEADER1_UNDERLINE_RE = re.compile("^-+$")
    HEADER2_UNDERLINE_RE = re.compile("^=+$")

    def __init__(self, md_lines):
        '''
        given a list of md format lines name, 
        - generate and prepend TOC  

        based on md_toc 
        # Author: Antonio Maiorano (amaiorano@gmail.com)
        '''
        if isinstance(md_lines, str): 
            self._lines_in = md_lines.split('\n')
        else: # isinstance(md_lines, list):
            self._lines_in = md_lines
        self._toc = []  # list of (header level, title, anchor) tuples
        self._lines_out =[]
        self.run()      

    @staticmethod
    def _toggles_block_quote(line):
        '''Returns true if line toggles block quotes on or off'''
        '''(i.e. finds odd number of ```)'''
        n = line.count("```")
        return n > 0 and line.count("```") % 2 != 0


    def run(self):
        in_block_quote = False
        last_line = ""

        lines_out = []
        i = 0
        for line in self._lines_in:

            if TOC._toggles_block_quote(line):
                in_block_quote = not in_block_quote

            if in_block_quote:
                continue

            found_header = False
            header_level = 0

            m = TOC.HEADER_LINE_RE.match(line)
            if m is not None:
                header_level = len(m.group(1))
                title = m.group(2)
                found_header = True

            if not found_header:
                m = TOC.HEADER1_UNDERLINE_RE.match(line)
                if m is not None:
                    header_level = 1
                    title = last_line.rstrip()
                    found_header = True

            if not found_header:
                m = TOC.HEADER2_UNDERLINE_RE.match(line)
                if m is not None:
                    header_level = 2
                    title = last_line.rstrip()
                    found_header = True

            if found_header:
                i += 1
                a_id = f'header-{header_level}-{i}'
                a_node = f'<a id="{a_id}" class="md-header-anchor"></a>'

                self._toc.append(dict(level=header_level, title=title, anchor=f'#{a_id}'))

                line = re.sub(r'^(#+\s*)', r'\1' + a_node, line)
                line = line.rstrip() + '<a class="back-to-top-link" href="#top">&#x27B6;</a>\n'

            last_line = line
            self._lines_out.append(line)   
    @property
    def toc_body(self):
        return self._toc, self._lines_out

    @property
    def with_toc(self): 
        return '\n'.join(['<div id="md-toc">', TocFormatter.as_bs4_columns(self._toc), '</div>'] + ['<hr id="end-of-toc" />'] + self._lines_out)


