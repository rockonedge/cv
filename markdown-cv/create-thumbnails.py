from pathlib import Path
from PIL import Image

IMAGE_ROOT = Path(__file__).resolve().parent.parent /'src/pics'

def create_thumbnails(root):
  for ext in ['*.png', 
            '*.jpg']:
    print(f'listing {ext}')
    for img in root.rglob(ext):
      print(img)

      if not 'thumb' in img.name:
        im = Image.open(img)
        if ext == '*.png': 

          try:
            # Get the alpha band
            alpha = im.split()[3]

            bg = Image.new("RGB", im.size, (255,255,255))
            bg.paste(im,im)
            im = bg
          except IndexError as e:
            im = im.convert('RGB')

        im.thumbnail((800, 600))
        im.save(img.with_suffix(".thumb.jpg"), "JPEG")


if __name__ == "__main__":

  create_thumbnails(IMAGE_ROOT)




