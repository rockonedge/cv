import requests

mds = ['full-cv-en-zh', 'full-cv-en', 'full-cv-zh-en', 'full-cv-zh', 'index', 'mini-cv-en-zh', 'mini-cv-en', 'mini-cv-zh', 'pics', 'profile-en', 'profile-zh', 'projects-en', 'projects-zh']

root = 'https://ttan.netlify.com/'

urls = [root, f'{root}/cv']
urls += [f'{root}cv?src={md}' for md in mds]

for u in urls:
  r = requests.get(u)
  print(r)