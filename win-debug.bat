@echo off

rmdir /S /Q  F:\cv\sandbox\datafy\temp
rmdir /S /Q   f:\html-templates\boostrap4\md
rmdir /S /Q   f:\html-templates\boostrap4\Release\md

python F:\cv\sandbox\datafy\transform_md.py
python  F:\cv\sandbox\datafy\to_table.py
xcopy /Q /Y /S F:\cv\sandbox\datafy\temp\. F:\html-templates\boostrap4\md\

python F:\html-templates\boostrap4\run.py
