---
banner:
  brand: all
  lastupdate: 2018-05-06
  nav: " <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        Anatidae　项目管理系统\n\
    \                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        嵌入式实时操作系统　uCOS/II　移植\n                     \
    \   </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        \n                        </div>\n\
    \                    </li> <li class=\"nav-item dropdown\">\n                \
    \        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\"\
    \ data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n \
    \                       嵌入式实时操作系统　uCLinux　移植\n                        </a>\n \
    \                       <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       SPI Flash 闪存芯片编程器\n                        </a>\n                    \
    \    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n\
    \                        \n                        </div>\n                  \
    \  </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       SPI FLASH 闪存芯片嵌入式系统应用包\n                        </a>\n               \
    \         <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       PC BIOS Flash 闪存芯片功能验证及演示平台\n                        </a>\n          \
    \              <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Serial Flash Programmer Tool Kit\n                        </a>\n     \
    \                   <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Memory Competence Center(MMCC) Project Tracking System\n             \
    \           </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        \n                        </div>\n\
    \                    </li> <li class=\"nav-item dropdown\">\n                \
    \        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\"\
    \ data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n \
    \                       Driver and Application Demos for Flash Memories\n    \
    \                    </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        Porting Embedded Operating Systems,\n      \
    \                  </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        Multi-Ping\n                        </a>\n \
    \                       <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Beckhoff TwinCAT ADS 通讯协议\n                        </a>\n            \
    \            <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Console3\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        HostLink TCP 注塑机通讯协议\n\
    \                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        TwinCAT TSM 查看器\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       A Literal Converter for Integers\n                        </a>\n     \
    \                   <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       2D Water Effect in WTL\n                        </a>\n               \
    \         <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Markdown CV\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        Beckhoff\
    \ TwinCAT ADS Protocol\n                        </a>\n                       \
    \ <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n \
    \                       \n                        </div>\n                   \
    \ </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Console3\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        HostLinkpp - An Implementation\
    \ of HostLink inter-machine Protocol\n                        </a>\n         \
    \               <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Multi-Ping\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        TwinCAT\
    \ TSM File Viewer\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        A Literal\
    \ Converter for Integers\n                        </a>\n                     \
    \   <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n\
    \                        \n                        </div>\n                  \
    \  </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       2D Water Effect in WTL\n                        </a>\n               \
    \         <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Markdown-CV\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        开关电源国产化\n\
    \                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        恒温加热箱\n                        </a>\n      \
    \                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       工业 LED 指示报警灯\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        空气断路器国产化设计\n\
    \                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        紧急备用电源自动切换系统(ATS)\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Development of industrial-strength LED alarms\n                      \
    \  </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        \n                        </div>\n\
    \                    </li> <li class=\"nav-item dropdown\">\n                \
    \        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\"\
    \ data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n \
    \                       Automatic Power Transfer System\n                    \
    \    </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        \n                        </div>\n\
    \                    </li> <li class=\"nav-item dropdown\">\n                \
    \        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\"\
    \ data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n \
    \                       扁鹊 DevOps 云生产管理系统\n                        </a>\n    \
    \                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Altanium 远程桌面\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        HyperSync-Altanium\
    \ 工业4.0集成\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        多语言翻译管理数据库(HkResource)\n\
    \                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        HyMET HeatLogger\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       嵌入式模具无线自动识别 IoT 解决方案\n                        </a>\n                 \
    \       <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Shotscope NX 车间生产管理系统\n                        </a>\n                \
    \        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Polaris HMI .NET 架构与实现\n                        </a>\n               \
    \         <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Polaris 注塑机实时控制系统\n                        </a>\n                    \
    \    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n\
    \                        \n                        </div>\n                  \
    \  </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       上海注塑机生产工厂组建\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        项目与团队管理\
    \ Dashboard 应用\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        新注塑机产品预上市 (Product Launch\
    \ Trip)\n                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        引入确立敏捷（Agile）开发流程\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       热流道实时温度控制系统 Matrix\n                        </a>\n                   \
    \     <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Shotscope MES/SCADA系统维护&开发\n                        </a>\n           \
    \             <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Celltrack MES/SCADA系统维护&开发\n                        </a>\n           \
    \             <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       兼并 Moldflow 制造部门 MES/SCADA 产品\n                        </a>\n        \
    \                <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       在线 HMI 多语言翻译管理系统\n                        </a>\n                     \
    \   <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n\
    \                        \n                        </div>\n                  \
    \  </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       统计过程/质量控制（SPC & SQC）模块\n                        </a>\n               \
    \         <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       HyperSync/Altanium 工业 4.0 特性开发\n                        </a>\n       \
    \                 <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       注塑机工厂 DevOps 生产管理系统开发\n                        </a>\n                \
    \        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       注塑机PLC 开发\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        注塑机人机界面\
    \ (HMI) 开发\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        筹建（上海）全球研发工程部\n     \
    \                   </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        Product Launch Team(PLT) Trips\n           \
    \             </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        \n                        </div>\n\
    \                    </li> <li class=\"nav-item dropdown\">\n                \
    \        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\"\
    \ data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n \
    \                       Whiteboard\n                        </a>\n           \
    \             <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Shanghai Factory Startup\n                        </a>\n             \
    \           <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Shotscope NX\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        Shotscope,\
    \ Celltrack & Matrix\n                        </a>\n                        <div\
    \ class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \
    \                  \n                        </div>\n                    </li>\
    \ <li class=\"nav-item dropdown\">\n                        <a class=\"nav-link\
    \ dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\"\
    \ aria-haspopup=\"true\" aria-expanded=\"false\">\n                        HyMET\
    \ Heatlogger\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        Hypersync-Altanium Industrie\
    \ 4.0 Features\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        \n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        Bianque\n           \
    \             </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        <a class=\"dropdown-item\"\
    \ href=\"#header-3-17\">Peeks of some screenshots</a>\n                      \
    \  </div>\n                    </li> <li class=\"nav-item dropdown\">\n      \
    \                  <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\"\
    \ data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n \
    \                       Polaris HkResource Refactor\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        <a class=\"dropdown-item\" href=\"#header-3-15\">Polaris\
    \ Translation System</a>\n                        </div>\n                   \
    \ </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Polaris HkLabel\n                        </a>\n                      \
    \  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n\
    \                        <a class=\"dropdown-item\" href=\"#header-3-13\">Polaris\
    \ Mold ID Server</a><a class=\"dropdown-item\" href=\"#header-3-12\">Polaris Production\
    \ System</a>\n                        </div>\n                    </li> <li class=\"\
    nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        Polaris Injection Control\
    \ System\n                        </a>\n                        <div class=\"\
    dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                \
    \        <a class=\"dropdown-item\" href=\"#header-3-10\">Integration of 3rd auxiliaries</a><a\
    \ class=\"dropdown-item\" href=\"#header-3-9\">Polaris PLC</a><a class=\"dropdown-item\"\
    \ href=\"#header-3-8\">Polaris HMI</a><a class=\"dropdown-item\" href=\"#header-3-7\"\
    >**Overview**</a>\n                        </div>\n                    </li> <li\
    \ class=\"nav-item dropdown\">\n                        <a class=\"nav-link dropdown-toggle\"\
    \ href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"\
    true\" aria-expanded=\"false\">\n                        List of Achievements\n\
    \                        </a>\n                        <div class=\"dropdown-menu\"\
    \ aria-labelledby=\"navbarDropdownMenuLink\">\n                        \n    \
    \                    </div>\n                    </li> <li class=\"nav-item dropdown\"\
    >\n                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"\
    navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"\
    false\">\n                        电子镇流器自动化综合测试系统\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       铁道车辆低压电器自动化综合测试系统\n                        </a>\n                    \
    \    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n\
    \                        \n                        </div>\n                  \
    \  </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Auto Test Platform for Electronic Ballast\n                        </a>\n\
    \                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"\
    >\n                        \n                        </div>\n                \
    \    </li> <li class=\"nav-item dropdown\">\n                        <a class=\"\
    nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"\
    dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                 \
    \       Automatic Test Platform for Rolling Stock Apparatus\n                \
    \        </a>\n                        <div class=\"dropdown-menu\" aria-labelledby=\"\
    navbarDropdownMenuLink\">\n                        \n                        </div>\n\
    \                    </li>"
  time: 2018-05-06 00:11:41.376446
  title: F:\cv\db\release\by_filter\all.md
items:
- at: Harbin Institute of Technology
  category: projects
  duration:
    end: '2001.04'
    start: '2000.09'
  keywords: []
  language: en
  name: Automatic-Test-Platform-for-Rolling-Stock-Apparatus
  summary: ''
  title: Automatic Test Platform for Rolling Stock Apparatus
- at: Harbin Institute of Technology
  category: projects
  duration:
    end: '1999.10'
    start: '1999.04'
  keywords: []
  language: en
  name: Auto-Test-Platform-for-Electronic-Ballast
  summary: ''
  title: Auto Test Platform for Electronic Ballast
- at: '哈尔滨工业大学  '
  category: projects
  duration:
    end: '2001.04'
    start: '2000.09'
  keywords:
  - 软件设计
  - 电子电路设计
  - 电气设计
  - 学术研究
  - Matlab
  - C++
  - Visual Basic
  language: zh
  name: 铁道车辆低压电器自动化综合测试系统
  summary: ''
  title: 铁道车辆低压电器自动化综合测试系统
- at: '哈尔滨工业大学  '
  category: projects
  duration:
    end: '1999.10'
    start: '1999.04'
  keywords:
  - 软件设计
  - 电子电路设计
  - 电气设计
  - 学术研究
  - Matlab
  - C++
  - Visual Basic
  language: zh
  name: 电子镇流器自动化综合测试系统
  summary: ''
  title: 电子镇流器自动化综合测试系统
- at: Husky
  category: projects
  duration:
    end: '2016'
    start: '2005'
  keywords: []
  language: en
  name: List-of-Achievements
  summary: ''
  title: List of Achievements
- at: Husky
  category: projects
  duration:
    end: '2016'
    start: '2005'
  keywords: []
  language: en
  name: Polaris-Injection-Control-System
  summary: ''
  title: Polaris Injection Control System
- at: Husky
  category: projects
  duration:
    end: '2006'
    start: '2006'
  keywords: []
  language: en
  name: Polaris-HkLabel
  summary: ''
  title: Polaris HkLabel
- at: Husky
  category: projects
  duration:
    end: '2006.05'
    start: '2006.05'
  keywords: []
  language: en
  name: Polaris-HkResource-Refactor
  summary: ''
  title: Polaris HkResource Refactor
- at: Husky
  category: projects
  duration:
    end: '2016'
    start: '2013'
  keywords: []
  language: en
  name: Bianque
  summary: ''
  title: Bianque
- at: Husky
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: Hypersync-Altanium-Industrie-4-0-Features
  summary: ''
  title: Hypersync-Altanium Industrie 4.0 Features
- at: Husky
  category: projects
  duration:
    end: '2007'
    start: '2007'
  keywords: []
  language: en
  name: HyMET-Heatlogger
  summary: ''
  title: HyMET Heatlogger
- at: Husky
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords: []
  language: en
  name: Shotscope-Celltrack-Matrix
  summary: ''
  title: Shotscope, Celltrack & Matrix
- at: Husky
  category: projects
  duration:
    end: '2015'
    start: '2008'
  keywords: []
  language: en
  name: Shotscope-NX
  summary: ''
  title: Shotscope NX
- at: Husky
  category: projects
  duration:
    end: '2006'
    start: '2005'
  keywords: []
  language: en
  name: Shanghai-Factory-Startup
  summary: ''
  title: Shanghai Factory Startup
- at: Husky
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords: []
  language: en
  name: Whiteboard
  summary: ''
  title: Whiteboard
- at: Husky
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords: []
  language: en
  name: Product-Launch-Team-PLT-Trips
  summary: ''
  title: Product Launch Team(PLT) Trips
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2007'
    start: '2006'
  keywords:
  - 团队与项目管理
  - 团队筹建
  language: zh
  name: 筹建-上海-全球研发工程部
  summary: ''
  title: 筹建（上海）全球研发工程部
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2006'
  keywords:
  - 项目管理
  - 软件架构
  - 软件编码
  language: zh
  name: 注塑机人机界面-HMI-开发
  summary: 领导开发用于所有赫斯基注塑机的 Polaris HMI 基础软件平台及各种衍生版本，涵盖所有系列注塑机系统。
  title: 注塑机人机界面 (HMI) 开发
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2006'
  keywords:
  - 团队与项目管理
  - 运动控制， CNC
  - 软件开发
  language: zh
  name: 注塑机PLC-开发
  summary: 实时控制相关算法。 包括伺服驱动(Servo),机器人控制等运动控制，以及温度控制， 第三方设备通讯等。基于 IEC 61131 标准，及 TwinCAT/CODESYS
    运行环境。
  title: 注塑机PLC 开发
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2006'
  keywords:
  - 团队与项目管理
  - 软件架构，软件开发
  language: zh
  name: 注塑机工厂-DevOps-生产管理系统开发
  summary: Polaris Production System， 此系统赫斯基注塑机控制系统软件开发，模拟，测试和发布的 DevOps 平台。也是车间内部各部门
    （研发， 生产，售后支持服务等）的协同工作平台。
  title: 注塑机工厂 DevOps 生产管理系统开发
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords:
  - 团队与项目管理
  - 软件架构
  - 代码编写
  language: zh
  name: HyperSync-Altanium-工业-4-0-特性开发
  summary: 基于 TwinCAT 实时工业互联网技术。获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。
  title: HyperSync/Altanium 工业 4.0 特性开发
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2011'
    start: '2011'
  keywords:
  - 代码编写
  - 数据科学
  - C++
  - DirectX
  - 图形编程
  language: zh
  name: 统计过程-质量控制-SPC-SQC-模块
  summary: ''
  title: 统计过程/质量控制（SPC & SQC）模块
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords:
  - 项目管理
  - 软件架构
  - 服务器软件
  - 网站设计
  - C#
  - SQL
  - ASP.NET
  language: zh
  name: 在线-HMI-多语言翻译管理系统
  summary: ''
  title: 在线 HMI 多语言翻译管理系统
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords:
  - 管理
  - 公司兼并
  - 知识产权转移
  - 资产接收
  - 技术融合
  language: zh
  name: 兼并-Moldflow-制造部门-MES-SCADA-产品
  summary: ''
  title: 兼并 Moldflow 制造部门 MES/SCADA 产品
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords:
  - 管理
  - 公司兼并
  - 知识产权转移
  - 资产接收
  - 技术融合
  - JAVA
  language: zh
  name: Celltrack-MES-SCADA系统维护-开发
  summary: ''
  title: Celltrack MES/SCADA系统维护&开发
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords:
  - 管理
  - 公司兼并
  - 知识产权转移
  - 资产接收
  - 技术融合
  - C++
  - Delphi
  - Firmware
  language: zh
  name: Shotscope-MES-SCADA系统维护-开发
  summary: ''
  title: Shotscope MES/SCADA系统维护&开发
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2008'
    start: '2008'
  keywords:
  - 管理
  - 公司兼并
  - 知识产权转移
  - 资产接收
  - 技术融合
  - C++
  - QT
  - MySQL
  language: zh
  name: 热流道实时温度控制系统-Matrix
  summary: 从原公司美国团队接收，并持续开发新功能， 包括嵌入式电子硬件及基于 Linux/Fedora 采用 C++/QT/MySQL 开发软件 HMI.
  title: 热流道实时温度控制系统 Matrix
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2008'
    start: '2006'
  keywords:
  - 管理
  - Agile
  - SCRUM
  - TDD
  language: zh
  name: 引入确立敏捷-Agile-开发流程
  summary: ''
  title: 引入确立敏捷（Agile）开发流程
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords:
  - 管理
  - 新产品上市
  - 全球客户现场支持
  language: zh
  name: 新注塑机产品预上市-Product-Launch-Trip-
  summary: ''
  title: 新注塑机产品预上市 (Product Launch Trip)
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016'
    start: '2011'
  keywords:
  - 管理
  - 软件架构
  - 软件编码
  - 项目管理
  - 敏捷开发
  language: zh
  name: 项目与团队管理-Dashboard-应用
  summary: 基于 micro service 项目管理网站。
  title: 项目与团队管理 Dashboard 应用
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2017.01'
    start: '2015.11'
  keywords:
  - 团队筹建
  - 项目管理
  language: zh
  name: 上海注塑机生产工厂组建
  summary: ''
  title: 上海注塑机生产工厂组建
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016.12'
    start: '2005.12'
  keywords:
  - 软件编码
  - 项目管理
  - 敏捷开发
  language: zh
  name: Polaris-注塑机实时控制系统
  summary: ''
  title: Polaris 注塑机实时控制系统
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2008.01'
    start: '2006.06'
  keywords:
  - 桌面软件设计
  - 第3方软件集成
  language: zh
  name: Polaris-HMI-NET-架构与实现
  summary: ''
  title: Polaris HMI .NET 架构与实现
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2015.09'
    start: '2008.06'
  keywords:
  - SCADA
  - 网络
  - WCF
  language: zh
  name: Shotscope-NX-车间生产管理系统
  summary: 一套基于 web 技术的过程和生产监控自动化实时解决方案。
  title: Shotscope NX 车间生产管理系统
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2015.09'
    start: '2010.06'
  keywords:
  - 无线通讯
  - 网络通讯
  - 嵌入式
  - 硬件烧录
  language: zh
  name: 嵌入式模具无线自动识别-IoT-解决方案
  summary: ''
  title: 嵌入式模具无线自动识别 IoT 解决方案
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2007.04'
    start: '2006.12'
  keywords:
  - 桌面软件设计
  - 硬件信息实时采集
  language: zh
  name: HyMET-HeatLogger
  summary: 一位资深工程师特意写信给我
  title: HyMET HeatLogger
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2006.07'
    start: '2006.05'
  keywords:
  - 软件设计
  - COM/ATL
  language: zh
  name: 多语言翻译管理数据库-HkResource-
  summary: 设计新数据库文件格式，为 HMI 提供 22+ 种语言支持服务。采用 ATL/C++ 实现 COM 服务器。
  title: 多语言翻译管理数据库(HkResource)
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016.10'
    start: '2016.02'
  keywords:
  - 软件设计
  - 系统集成
  - 硬件采集
  language: zh
  name: HyperSync-Altanium-工业4-0集成
  summary: ''
  title: HyperSync-Altanium 工业4.0集成
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016.08'
    start: '2016.04'
  keywords:
  - 软件设计
  - 系统集成
  - 网络通讯
  language: zh
  name: Altanium-远程桌面
  summary: 基于 RDP 协议的嵌入式远程桌面, 实现对　Altanium　温度控制器的远程控制
  title: Altanium 远程桌面
- at: 赫斯基注塑系统有限公司
  category: projects
  duration:
    end: '2016.12'
    start: '2013.04'
  keywords:
  - 软件架构
  - 软件设计
  - 云计算
  - 大数据
  - 机器智能
  - IoT
  - 数据科学
  - MQTT
  language: zh
  name: 扁鹊-DevOps-云生产管理系统
  summary: 立足 Azure 云技术（比如 IoT Hub）， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术的新一代
    DevOps 系统。
  title: 扁鹊 DevOps 云生产管理系统
- at: Schneider Electric
  category: projects
  duration:
    end: '2003.06'
    start: '2002.11'
  keywords: []
  language: en
  name: Automatic-Power-Transfer-System
  summary: ''
  title: Automatic Power Transfer System
- at: Schneider Electric
  category: projects
  duration:
    end: '2003.06'
    start: '2002.11'
  keywords: []
  language: en
  name: Development-of-industrial-strength-LED-alarms
  summary: ''
  title: Development of industrial-strength LED alarms
- at: '施耐德电气 '
  category: projects
  duration:
    end: '2003.08'
    start: '2002.11'
  keywords:
  - 电气设计
  - 产品架构
  - PLC 应用
  language: zh
  name: 紧急备用电源自动切换系统-ATS-
  summary: ''
  title: 紧急备用电源自动切换系统(ATS)
- at: '施耐德电气 '
  category: projects
  duration:
    end: '2003.08'
    start: '2002.11'
  keywords:
  - 电子电路设计
  language: zh
  name: 空气断路器国产化设计
  summary: ''
  title: 空气断路器国产化设计
- at: '施耐德电气 '
  category: projects
  duration:
    end: '2002.11'
    start: '2002.08'
  keywords:
  - 电气设计
  - 产品设计前期调研
  language: zh
  name: 工业-LED-指示报警灯
  summary: ''
  title: 工业 LED 指示报警灯
- at: '施耐德电气 '
  category: projects
  duration:
    end: '2003.01'
    start: '2002.11'
  keywords:
  - 电气设计
  - 产品设计
  - 单片机嵌入式应用
  language: zh
  name: 恒温加热箱
  summary: ''
  title: 恒温加热箱
- at: '施耐德电气 '
  category: projects
  duration:
    end: '2003.08'
    start: '2003.05'
  keywords:
  - 电气设计
  - 产品设计前期调研
  language: zh
  name: 开关电源国产化
  summary: ''
  title: 开关电源国产化
- at: Spare-time Projects
  category: projects
  duration:
    end: '2017'
    start: '2017'
  keywords:
  - Python
  - HTML
  - Boostrap
  - JQuery
  language: en
  name: Markdown-CV
  summary: ''
  title: Markdown-CV
- at: Spare-time Projects
  category: projects
  duration:
    end: '2011'
    start: '2011'
  keywords:
  - C++
  - Image
  - Graphics
  language: en
  name: 2D-Water-Effect-in-WTL
  summary: ''
  title: 2D Water Effect in WTL
- at: Spare-time Projects
  category: projects
  duration:
    end: '2004'
    start: '2004'
  keywords: []
  language: en
  name: A-Literal-Converter-for-Integers
  summary: ''
  title: A Literal Converter for Integers
- at: Spare-time Projects
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: TwinCAT-TSM-File-Viewer
  summary: ''
  title: TwinCAT TSM File Viewer
- at: Spare-time Projects
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: Multi-Ping
  summary: ''
  title: Multi-Ping
- at: Spare-time Projects
  category: projects
  duration:
    end: '2012'
    start: '2012'
  keywords:
  - C++
  - TCP
  - socket?Boost.ASIO
  - Boost.spirit
  language: en
  name: HostLinkpp-An-Implementation-of-HostLink-inter-machine-Protocol
  summary: ''
  title: HostLinkpp - An Implementation of HostLink inter-machine Protocol
- at: Spare-time Projects
  category: projects
  duration:
    end: '2016'
    start: '2012'
  keywords: []
  language: en
  name: Console3
  summary: ''
  title: Console3
- at: Spare-time Projects
  category: projects
  duration:
    end: '2016'
    start: '2016'
  keywords: []
  language: en
  name: Beckhoff-TwinCAT-ADS-Protocol
  summary: ''
  title: Beckhoff TwinCAT ADS Protocol
- at: 业余时间
  category: projects
  duration:
    end: '2013.03'
    start: '2017.01'
  keywords:
  - HTML5 设计
  - 脚本编写
  - 效率工具
  language: zh
  name: Markdown-CV
  summary: ''
  title: Markdown CV
- at: 业余时间
  category: projects
  duration:
    end: '2011'
    start: '2011'
  keywords:
  - 图像处理
  language: zh
  name: 2D-Water-Effect-in-WTL
  summary: ''
  title: 2D Water Effect in WTL
- at: 业余时间
  category: projects
  duration:
    end: '2005'
    start: '2005'
  keywords:
  - 开源
  - C++
  language: zh
  name: A-Literal-Converter-for-Integers
  summary: ''
  title: A Literal Converter for Integers
- at: 业余时间
  category: projects
  duration:
    end: '2016.12'
    start: '2016.06'
  keywords:
  - 桌面/服务器/云服务器/软件设计
  - 网络通讯
  - IoT
  language: zh
  name: TwinCAT-TSM-查看器
  summary: ''
  title: TwinCAT TSM 查看器
- at: 业余时间
  category: projects
  duration:
    end: '2013.04'
    start: '2012.08'
  keywords:
  - 桌面软件设计
  - 网络通讯
  - Windows
  - Linux
  language: zh
  name: HostLink-TCP-注塑机通讯协议
  summary: ''
  title: HostLink TCP 注塑机通讯协议
- at: 业余时间
  category: projects
  duration:
    end: '2016'
    start: '2012'
  keywords:
  - C++
  - Direct2D
  - DirectWrite
  language: zh
  name: Console3
  summary: ''
  title: Console3
- at: 业余时间
  category: projects
  duration:
    end: '2016.12'
    start: '2016.07'
  keywords:
  - 软件设计
  language: zh
  name: Beckhoff-TwinCAT-ADS-通讯协议
  summary: ''
  title: Beckhoff TwinCAT ADS 通讯协议
- at: 业余时间
  category: projects
  duration:
    end: '2016.07'
    start: '2016.07'
  keywords:
  - 软件设计
  - 网络程序设计
  language: zh
  name: Multi-Ping
  summary: ''
  title: Multi-Ping
- at: STMicroelectronics
  category: projects
  duration:
    end: '2005.09'
    start: '2005.04'
  keywords: []
  language: en
  name: Porting-Embedded-Operating-Systems-
  summary: ''
  title: Porting Embedded Operating Systems,
- at: STMicroelectronics
  category: projects
  duration:
    end: '2004.10'
    start: '2003.12'
  keywords: []
  language: en
  name: Driver-and-Application-Demos-for-Flash-Memories
  summary: ''
  title: Driver and Application Demos for Flash Memories
- at: STMicroelectronics
  category: projects
  duration:
    end: '2004.06'
    start: '2004.02'
  keywords: []
  language: en
  name: Memory-Competence-Center-MMCC-Project-Tracking-System
  summary: ''
  title: Memory Competence Center(MMCC) Project Tracking System
- at: STMicroelectronics
  category: projects
  duration:
    end: '2003.12'
    start: '2003.09'
  keywords: []
  language: en
  name: Serial-Flash-Programmer-Tool-Kit
  summary: ''
  title: Serial Flash Programmer Tool Kit
- at: '意法半导体 '
  category: projects
  duration:
    end: '2004.03'
    start: '2003.12'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: PC-BIOS-Flash-闪存芯片功能验证及演示平台
  summary: ''
  title: PC BIOS Flash 闪存芯片功能验证及演示平台
- at: '意法半导体 '
  category: projects
  duration:
    end: '2004.10'
    start: '2004.05'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: SPI-FLASH-闪存芯片嵌入式系统应用包
  summary: ''
  title: SPI FLASH 闪存芯片嵌入式系统应用包
- at: '意法半导体 '
  category: projects
  duration:
    end: '2005.11'
    start: '2003.09'
  keywords:
  - 嵌入式软件设计
  - 桌面软件设计
  - 电子电路设计
  - 应用架构
  language: zh
  name: SPI-Flash-闪存芯片编程器
  summary: ''
  title: SPI Flash 闪存芯片编程器
- at: '意法半导体 '
  category: projects
  duration:
    end: '2005.09'
    start: '2005.04'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: 嵌入式实时操作系统-uCLinux-移植
  summary: ''
  title: 嵌入式实时操作系统　uCLinux　移植
- at: '意法半导体 '
  category: projects
  duration:
    end: '2005.09'
    start: '2005.04'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: 嵌入式实时操作系统-uCOS-II-移植
  summary: ''
  title: 嵌入式实时操作系统　uCOS/II　移植
- at: '意法半导体 '
  category: projects
  duration:
    end: '2004.06'
    start: '2004.02'
  keywords:
  - 软件架构
  - 软件设计
  language: zh
  name: Anatidae-项目管理系统
  summary: ''
  title: Anatidae　项目管理系统
---
<div id="md-toc">
<div class="container">
<div class="row">
<div class="col">
- [Automatic Test Platform for Rolling Stock Apparatus](#header-2-1)
</div>
<div class="col">
<p></p>
- [Auto Test Platform for Electronic Ballast](#header-2-2)
- [铁道车辆低压电器自动化综合测试系统](#header-2-3)
- [电子镇流器自动化综合测试系统](#header-2-4)
- [List of Achievements](#header-2-5)
- [Polaris Injection Control System](#header-2-6)
  - [**Overview**](#header-3-7)
  - [Polaris HMI](#header-3-8)
  - [Polaris PLC](#header-3-9)
  - [Integration of 3rd auxiliaries](#header-3-10)
- [Polaris HkLabel](#header-2-11)
  - [Polaris Production System](#header-3-12)
  - [Polaris Mold ID Server](#header-3-13)
- [Polaris HkResource Refactor](#header-2-14)
  - [Polaris Translation System](#header-3-15)
- [Bianque](#header-2-16)
  - [Peeks of some screenshots](#header-3-17)
- [Hypersync-Altanium Industrie 4.0 Features](#header-2-18)
- [HyMET Heatlogger](#header-2-19)
- [Shotscope, Celltrack & Matrix](#header-2-20)
- [Shotscope NX](#header-2-21)
- [Shanghai Factory Startup](#header-2-22)
- [Whiteboard](#header-2-23)
- [Product Launch Team(PLT) Trips](#header-2-24)
- [筹建（上海）全球研发工程部](#header-2-25)
- [注塑机人机界面 (HMI) 开发](#header-2-26)
- [注塑机PLC 开发](#header-2-27)
- [注塑机工厂 DevOps 生产管理系统开发](#header-2-28)
- [HyperSync/Altanium 工业 4.0 特性开发](#header-2-29)
- [统计过程/质量控制（SPC & SQC）模块](#header-2-30)
- [在线 HMI 多语言翻译管理系统](#header-2-31)
- [兼并 Moldflow 制造部门 MES/SCADA 产品](#header-2-32)
- [Celltrack MES/SCADA系统维护&开发](#header-2-33)
- [Shotscope MES/SCADA系统维护&开发](#header-2-34)
- [热流道实时温度控制系统 Matrix](#header-2-35)
- [引入确立敏捷（Agile）开发流程](#header-2-36)
- [新注塑机产品预上市 (Product Launch Trip)](#header-2-37)
- [项目与团队管理 Dashboard 应用](#header-2-38)
- [上海注塑机生产工厂组建](#header-2-39)
- [Polaris 注塑机实时控制系统](#header-2-40)
- [Polaris HMI .NET 架构与实现](#header-2-41)
- [Shotscope NX 车间生产管理系统](#header-2-42)
- [嵌入式模具无线自动识别 IoT 解决方案](#header-2-43)
- [HyMET HeatLogger](#header-2-44)
- [多语言翻译管理数据库(HkResource)](#header-2-45)
- [HyperSync-Altanium 工业4.0集成](#header-2-46)
- [Altanium 远程桌面](#header-2-47)
- [扁鹊 DevOps 云生产管理系统](#header-2-48)
- [Automatic Power Transfer System](#header-2-49)
- [Development of industrial-strength LED alarms](#header-2-50)
- [紧急备用电源自动切换系统(ATS)](#header-2-51)
- [空气断路器国产化设计](#header-2-52)
- [工业 LED 指示报警灯](#header-2-53)
- [恒温加热箱](#header-2-54)
- [开关电源国产化](#header-2-55)
- [Markdown-CV](#header-2-56)
- [2D Water Effect in WTL](#header-2-57)
- [A Literal Converter for Integers](#header-2-58)
- [TwinCAT TSM File Viewer](#header-2-59)
- [Multi-Ping](#header-2-60)
- [HostLinkpp - An Implementation of HostLink inter-machine Protocol](#header-2-61)
- [Console3](#header-2-62)
- [Beckhoff TwinCAT ADS Protocol](#header-2-63)
- [Markdown CV](#header-2-64)
- [2D Water Effect in WTL](#header-2-65)
- [A Literal Converter for Integers](#header-2-66)
- [TwinCAT TSM 查看器](#header-2-67)
- [HostLink TCP 注塑机通讯协议](#header-2-68)
- [Console3](#header-2-69)
- [Beckhoff TwinCAT ADS 通讯协议](#header-2-70)
- [Multi-Ping](#header-2-71)
- [Porting Embedded Operating Systems,](#header-2-72)
- [Driver and Application Demos for Flash Memories](#header-2-73)
- [Memory Competence Center(MMCC) Project Tracking System](#header-2-74)
- [Serial Flash Programmer Tool Kit](#header-2-75)
- [PC BIOS Flash 闪存芯片功能验证及演示平台](#header-2-76)
- [SPI FLASH 闪存芯片嵌入式系统应用包](#header-2-77)
- [SPI Flash 闪存芯片编程器](#header-2-78)
- [嵌入式实时操作系统　uCLinux　移植](#header-2-79)
- [嵌入式实时操作系统　uCOS/II　移植](#header-2-80)
- [Anatidae　项目管理系统](#header-2-81)
</div>
</div>
</div>
</div>
<hr id="end-of-toc" />

## <a id="header-2-1" class="md-header-anchor"></a>Automatic Test Platform for Rolling Stock Apparatus<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2000.09 - 2001.04__ 




An Industrial PC-based (IPC) automation system bid for Harbin Railway Bureau, a regional bureau affiliated to the Ministry of Railways (China).

which I took full charge of and design from scratch, including hardware, software and some mechanical design. 

The objective of the project was to provide an automatic and all-in-one platform to cover the routine maintenance check of all low voltage apparatuses used on the train: 

- circuit breakers
- contactors, and
- thermal relays

The checklist included

- electrical performance, e.g. 
  - contact resistance
  - make/break time
  - over-voltage
  - under-voltage
  - over-current 
  - short circuit etc. 
- mechanical performance 
  - sinusoidal vibration.

- Hardware design 

  - ISA board for digital I/Os
  - PCI board for data sampling and A/D conversion
  - Timer/counter circuit design
  - digital-coded power supply ranging between 1A-1300A or 0-440VAC/220VDC
  - Precise resistant load (resolution: 1/512 ohm)
  - High current sample and switch circuit.


- Software development
  - Motor control &　Hardware driving
  - Precise voltage/current control algorithm
  - Complete and user-friendly performance parameter display
  - Failure report and Lifetime estimation(FMEA)
  - Test data management

Awards&Achievements:

- Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
- Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
- An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*


## <a id="header-2-2" class="md-header-anchor"></a>Auto Test Platform for Electronic Ballast<a class="back-to-top-link" href="#top">&#x27B6;</a>

__1999.04 - 1999.10__ 



Another Industrial PC-based (IPC) system designed for Harbin Railway Bureau. 

Ojective

- up to 8 electronic ballasts (15W, 20W, 30W, 40W) concurrently, 
- detailed performance parameters such as 
  - voltage, 
  - current, 
  - power factor, 
  - harmonic distortions(THD), 
  - temperature rise etc. 
  - real-time voltage and current waveform 
  - FMEA reports
  - historic events database
  - pretty print

- hardware
  - ISA I/O board, A/D board
  - Power supply board (DC 47V-57V, AC 200V – 240V)
  - Sensor board (voltage, current and temperature)
  - Target-switching board

- software
  - Hardware driving
  - Real-time sample and analysis (FFT algorithm)
  - Rendering visual waveform
  - Test data management

This platform greatly sped up the routine check efforts of technicians who were dealing with tens of electronic ballasts daily. The usages were spreaded to another 5 railway bureaus soon after the completion for its satisfactory performance.

A paper was published on a national academic periodical explain the design.

It was all of my work from initial research, hardware design, software design spec and debug, except for the software UI design.

## <a id="header-2-3" class="md-header-anchor"></a>铁道车辆低压电器自动化综合测试系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2000.09 - 2001.04 &middot; 软件设计, 电子电路设计, 电气设计, 学术研究, Matlab, C++, Visual Basic__ 



此系统作为硕士学位论文课题验证设计，基于工业控制计算机,  为铁道车辆用电器（断路器, 接触器, 热继电器）的自动化常规检修平台.

- 经黑龙江省科委专家组技术评审鉴定“__居于国内领先__”。
- 相关论文发表于学术会议__第八届国际电器可靠性会议__。
- 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
- 2002.12 获黑龙江省教育厅__科技进步一等奖__
- 2003.05 获黑龙江省科技厅__科技进步二等奖__
 

此课题应哈尔滨铁路局要求设计, 检修内容包括接触电阻, 闭合/断开时间, 过压/欠压/失压, 过流, 短路等电气性能试验和正弦振动等机械性能试验.

硬件设计包括: 

- 工业控制计算机ISA I/O 板卡设计
- 工业控制计算机PCI A/D 板卡设计
- 计数器/定时器电路设计
- 大电流采样与切换电路
- 数字编码可变电源设计, 提供 1A to 1,300A 电流 及440VAC/220VDC电压
- 高精度阻性负载设计 (分辨率: 1/512 ohm)
- 变频器/电机控制

软件设计（采用Visual C++）包括:

- 精确电压/电流闭环控制算法
- 友好、全面的运行参数显示
- 故障报告，历史数据报表
- 寿命预测(FMEA)
- 测试数据管理，统计分析

此项目也作为硕士学位论文课题验证设计, 承担了全部的项目前期调研, 软/硬件设计及少量机械设计。

此项目深受用户好评, 被广泛推广至齐齐哈尔等7个车辆段销售累计逾百万人民币。
 


## <a id="header-2-4" class="md-header-anchor"></a>电子镇流器自动化综合测试系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__1999.04 - 1999.10 &middot; 软件设计, 电子电路设计, 电气设计, 学术研究, Matlab, C++, Visual Basic__ 



此课题应哈尔滨铁路局要求设计, 作为学士学位论文课题。负责了的调研, 全部硬件设计, 软件设计文档和调试。在国家级学术期刊发表相关论文一篇.
 
此系统能同时测试多达八只电子各种型号镇流器/逆变器 (15W, 20W, 30W, 40W), 提供各种详细性能参数信息, 包括灯电压, 灯电流, 功率因数, 谐波畸变, 镇流器温升等. 实时显示电压, 电流波形。一旦发现故障, 还将提供故障定位与诊断帮助. 建立试品维修历史数据库, 并自动生成报表. 

硬件设计包括: 

- ISA I/O 板, A/D 板 
- 交/直流电源 (DC 47V-57V, AC 200V – 240V) 
- 传感器板 (电压, 电流 和 温度检测 ) 
- 试品切换板 

软件设计包括: 

- 硬件驱动 
- 实时数据采样、分析 (快速傅立叶变换) 
- 波形显示 
- 测试数据管理该系统以其高效准确的性能深受好评, 并被推广至齐齐哈尔等铁路分局. 

## <a id="header-2-5" class="md-header-anchor"></a>List of Achievements<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005 - 2016__ 



  - Instrumental during the startup of Shanghai Machines plant, from the scratch

    - Replicated **Polaris Production System** from Bolton to Shanghai 
    - Helped starting up the **production engineering team** 

  - Started up a first offshore **global development engineering team**, all with Master of Science/Engineering degrees, and had successfully managed it for 11+ years.
  - Led the development on controls platform for all Husky injection machines, consisting of 
    -  2 Polaris HMI platforms
      - COM/VB6 HMI platform for legacy generations of injection machines
      - re-Architecture and re-implementation of a .NET/C# HMI platform for later generations of injection machines
      - value-added modules, e.g.
        - development of Statistical Quality Control (SQC) and Statistical Process Control (SPC) module
      - hostlink, a proprietary communication protocol to talk to Husky machines remotely 
    - Polaris PLC 
      - **PLC programming for real-time control** with TWinCAT/CODESYS technologies, such as
        - servo drives
        - robotics etc.
    - Polaris production system.
      - daily used by engineers across departments(DevEng, Production and Service etc.) , such as
        - Translation System, supporting 22+ languages for Husky software
        - Mold ID System, managing RuBee(IEEE 1902.1) based tags mounted onto Husky made molds
        - ...
    - **regular releases**(quarterly/semi-annually) of the aforementioned platforms for over a decade
  - Initiated **Bianque Production System**, a web-based replacement for Polaris Production System, leveraging Cloud-era technologies such as Microsoft Azure IoT Hub, REST, microservice, and mobile apps.
  - Led the development of Matrix, a Hot Runner Temperature Controller based on Linux/Fedora using QT/C++ and MySQL.
  - Design and implemented in c++ Husky Hostlink, a proprietary raw TCP sockets based protocol remoting to Husky injection machines in realtime
  - Instrumental in the takeover of software&hardware assets from French&US teams during [Husky buying MoldFlow manufacturing division](http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit) and subsequent customer retention.
  - Led the implementation of [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), an automated, real-time, web-based software&hardware hybrid solution for process and production monitoring.[
  - Led Industry 4.0 features introduced in the HyperSync machines, and won _Ringier Technology Innovation Award_](https://omnexus.specialchem.com/news/industry-news/husky-ringier-technology-innovation-award-000186265) .

  - Led the participation in the worldwide Product Launch Team(PLT), an ad-hoc top technical expert team to set up to-be-launched machines in the field for early adopter.

  - Fostered Agile methodologies such as SCRUM/Test-Driven Development(TDD) 
  - Helped the migration source control process from VSS to TFS.

  - Innovated on effective global teams collaboration&management process
    - developed the **Whiteboard**  website in spare time to synthesize info from various data source including 
      - Team Foundation Server
      - Visual SourceSafe
      - SharePoint
      - ERP/Oracle and 
      - UNC/mapped network drives

      leveraging 
      - MongoDB
      - Python/Django
      - Javascript/Dojo Toolkit
      - C#/ASP.NET/REST API
      - TFS/ ShairePoint SDKs etc.
      - Apache Cordova
      
      having saved time in routine meetings such as
        - daily standup meetings
        - weekly team meetings
        - milestone meetings etc. by quickly putting everyone on the same page
      - enabled team leaders to query project statuses within clicks.

![](./pics/wb-overview.png) 
![](./pics/wb-projects.png) 


## <a id="header-2-6" class="md-header-anchor"></a>Polaris Injection Control System<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005 - 2016__ 



### <a id="header-3-7" class="md-header-anchor"></a>**Overview**<a class="back-to-top-link" href="#top">&#x27B6;</a>


Polaris is an umbrella name covering all development activities surrounding the general software platform. All software coming out of DevEng, unless specifically named(e.g. Shotscope NX), is regarded as part of Polaris by default.

Polaris to Husky is Windows to Microsoft, in that it's being developed, thus evolving, all the time and released regularly.  Any mechanical, electrical and software design change, or technology advances in the industry would pretty much likely result in changes in Polaris one way or another.

Roughly, Polaris consists of 

- Polaris HMI
- Polaris PLC 
- Integration of 3rd auxiliaries
- Polaris Production System

Polaris is an IPC-based software solution for controlling all Husky-made injection molding machines, written in C++, C#, VB6, PLC (IEC 61131) and Ruby among others.

All Polaris HMI&PLC features are wrapped into reusable and composable _modules_ like LEGOs, Polaris Production System 

- manages the 6K+ and growing modules and 
- generateds deployable packages(11K+), 

one for each machine, be it physically real or simulation for development purposes), so that each machine is equipped with a unique software package of HMI and PLC combined.  

**Suffice it to say, a big part of my time, as well as my team's in Husky, was spent directly or directly on this platform.**

It was Shanghai team's daily routine, under my leadership, to 

- pick tasks from the same pool and 
- follow the same schedules 
 
as peer development teams back in the HQ, with contributions to to the same code base with
- new features
- feature improvements
- bug fixes etc.

### <a id="header-3-8" class="md-header-anchor"></a>Polaris HMI<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006 - 2016**

A GUI/touch-screens enabled application running on Windows XP/7 Embedded with rich features enabling operators with an ultimate control 

- from cycle breakdowns to production jobs schedules. 

The UI is **configured** during package generation with the Production System per customer orders.

2 versions are maintained:

-  Legacy version

  - started around 2000
  - default for all product lines roughly between circa 2000 and 2007
    - being slow and corrupt-prone for being resource demanding as a result of accumulative abuse of system resources over the years.
  - coded in VB6 using COM technology, and source controlled by SourceSafe
  - running on Windows XP Embedded 


- Current version

  - started around 2007
  - default for all product lines starting from around 2008
  - coded in C# using .NET technology and modern Tools
    - A new architecture leveraging C#/.NET and with lessons(a lot of them!) from the previous version
      - fully OOP
      - decoupled layers for devices, domains and UI logics, with message-based communication
      - less dependent on TwinCAT opaque file formats
    - using new Application lifecycle management (ALM) tool and methodology
      - Microsoft Team Foundation System(TFS)
      - TDD/SCRUM process
  - running on Windows 7 Embedded

  I was wholly involved since the inception of the architecture of the .NET version, such as 

  - early concepts selection
    - e.g. meeting at Canada the representatives from Microsoft, IBM and McCabe and voting for the final choice of TFS
  - leading the year-long re-implementation of every VB6 screen in C#/.NET in Shanghai

### <a id="header-3-9" class="md-header-anchor"></a>Polaris PLC<a class="back-to-top-link" href="#top">&#x27B6;</a>


A TwinCAT based real-time program for 

- servo motors 
- sensors 
- actuators 

across the 

- injection
- clamping and 
- robotics systems, sending status to and receiving commands from the HMI.

Algorithms were written in IEC61131/ST Language and stored as modules per functionality, merged with the help of the Production system during machine packages generation.


### <a id="header-3-10" class="md-header-anchor"></a>Integration of 3rd auxiliaries<a class="back-to-top-link" href="#top">&#x27B6;</a>


It's more often than not that 3rd-party auxiliary equipments were requested to be integrated into Husky's HMI to 

- complete a whole production cycle/process
- facilitate the operators with a single one screen.

The task usually involved 

- data transfer through standard or proprietary(e.g. RS-485, digital I/O etc.) protocols 
- embedding screens into the Polaris HMI UI

examples included:

- ABB robots, 2006
- Resin Dryers from [Plastic Systems](http://plasticsystems.com/) 
- dehumidifier
- conveyers etc.


## <a id="header-2-11" class="md-header-anchor"></a>Polaris HkLabel<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006__ 



**This was the my first code check-in check-in at Husky.**

This project was to develop a replacement of the built-in Visual Basic Label control to 

- fix the truncation of text display due to extended text length caused by translation(e.g. from English to German) 
  - should show trailing ellipsis and pop up the full text when finger-clicked
  - should change the display when it's too long
- fix the overuse of GDI handles [hitting the 16K limit][#GDI] allowed by Windows XP.

Developed in C++ as a COM component. 

A companion utility was developed too to replace 1K+ occurrences of usages across the whole source code tree (hundreds of Kloc) at one go.

[#GDI]: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724291(v=vs.85).aspx


### <a id="header-3-12" class="md-header-anchor"></a>Polaris Production System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006 - 2016**

The Production System assembles 

- from 6k+ modules a package of the Platform System 
- specific to the machine configuration ordered by customers, and 
- delivers it to a target machine on the shopfloor for testing and shipping
- through network .

 A loosely-coupled collection of utilities, including CLI, GUI and web sites, for

- **creation and versioning of tens of thousands of modules** of HMI and PLC features
   - source code fragments
   - pre-built binaries
   - customizable templates etc.
  - assembly of deployable **platform installation packages and deviation/retrofit patches** per injection molding machine serial number
  - bridging development, production and service engineers for service issues
  - improving engineering time performance

For its nature, dealing with the Product System is a part of everyone daily work, and fighting with it to make it work or workaround it stayed a main theme.


### <a id="header-3-13" class="md-header-anchor"></a>Polaris Mold ID Server<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2009**

Latest Husky injection molding systems are equipped with the mold identification(Mold ID) feature, enabled by [Rubee](https://en.wikipedia.org/wiki/RuBee) tags attached to mold tooling plates.

When a mold is installed, parameters are read from the tags over the air, 
- enabling mold identification and quick alignment if matches, or
- reject installation if not

to avoid accidental damages.

My team built 

- a web portal 
  - global data center for all mold toolings, sold or under development
  - for daily cross-function collaboration.
    - mold designers to specify parameters
    - production engineers to program tags
    - service engineers to refurbish tags
- Integration into Polaris HMI
  - wireless detection of Rubee tags and parameter fetching
- Tag Programming/refurbishing Tool   
  - writing of mold tooling parameters through RuBee(IEEE 1902.1)
  - compress data into the 256-byte memory space
  - enabling engineers to program over the intranet, e.g. writing a Shanghai tag from Bolton 

SharePoint, Silverlight, WinForm and  Socket programming were involved. 

I oversaw the whole project as it was completely developed in Shanghai and deployed in Shanghai's IT servers for global access. The maintenance and response to requests raised through IT Help Desk are handle solely by the Shanghai team.



## <a id="header-2-14" class="md-header-anchor"></a>Polaris HkResource Refactor<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006.05__ 



With the growing number of supported languages from a mere 1, i.e. Engish, to over 22, the original single-file based solution suffered from

- too large file size up to 70+ MB
    - prone to corruption due to HMI crash or power loss
    - poor read/write performance
    - all-or-nothing rigidity, while only 2 languages(English and local) are requested in most cases.

By keeping its client interfaces intact,  I personally

- re-designed the storage schema
- re-implemented the COM server with ATL/C++
- leveraged memory-mapped file to access the files

thus getting rid of all the aforementioned shortcomings, without a single line of change in the HMI code.

### <a id="header-3-15" class="md-header-anchor"></a>Polaris Translation System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006**

The Polaris HMI supports 22+ languages. For any phrase to be added to the screens, it's required to determine whether new translation is needed early on, and if so, among others

- applications should be filed for approval from team leaders for payment to a 3rd-part translation company 
- contexts of phrase usage, e.g. screenshots, should be provided to assist translation
- only selected languages should be packed and deployed onto the machine

A fresh college graduate, under my coach, developed a website using ASP.NET and SQL Server to have the goal achieved.


## <a id="header-2-16" class="md-header-anchor"></a>Bianque<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2013 - 2016__ 



The Polaris Production System dated back to circa 2000 and evolved into a daunting complexity with a very steep learning curve over the years, and a daily drain of engineering efficiency, To name a few,

- fail frequently
  
  unmanaged and undocumented heavy dependency on assumed fixed paths which are not

- poor errors 
  
  pop-up dialogues in the middle of automated processes, which invalidated the automation itself.

- convoluted system configurations

  70-100+GB decade-old VMWare image one can hardly create from the scratch.

- overuse of VMWare image file, a.k.a under use of virtualization technologies

  every engineer has **a copy for each task**. It's normal to have 2-3 such images running at the same time to get work done.

Named after the ancient Chinese doctor, Bianque was meant to fix it with a it-just-works solution with modern DevOps and virtualization technologies by distinguishin the requirements between

- Development Engineering, which creates software packages

  introduced  Vagrant for virtual eenvironment management  

- Production Engineering, which consumes software packages 

  replaced the legacy VB6 desktop application with a single page application(SPA) site backed up with REST web services

  - cloud(Microsoft Azure) based
    - removing the waste of gigantic duplicate VMWare image files(which IT complains all the time) with efficient cloud solutions
    - accessible from multiple platform
    - accessible from anywhere to benefit field service
    - easy upgrades
  - service-oriented with APIs
    - extensible for specific needs

Bianque largely consisted of

- Lancet

    A collection of REST Service mostly code in Python, including re-implementation of part of Polaris Production System functionalites。

 - Acupuncture

    A single page application leveraging Django/Dojo Toolkit/Web Socket, the front portal for 
    - production engineers on the shopfloor
    - service engineers in the field

- Transformer

    A manager of pre-configured VMware images
    
    - dispatch requests among images to maximized resource ultilization
    - concurrently execution of the Polaris Production System commands to have reduce from 40m to 5m-10m typically
    - exposing Polaris Production System commands as REST services 

-   HySearch

    A MongoDB/Django based portal as a convenience tool for search modules and machine packages through keywords.

### <a id="header-3-17" class="md-header-anchor"></a>Peeks of some screenshots<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **early concept of Bianque**
  ![](./pics/bq-arch.png)

- **HySearch Engine**
  ![](./pics/bq-search.png)

- **Acupuncture**
  ![](./pics/bq-acupuncture-1.jpg)
  ![](./pics/bq-acupuncture-2.png)

- **Acupuncture on mobile devices**
  ![](./pics/bq-mobile.jpg)

- **early debug view of Transformers**
  ![](./pics/bq-transformer.jpg)


## <a id="header-2-18" class="md-header-anchor"></a>Hypersync-Altanium Industrie 4.0 Features<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016__ 



[Altanium controller](http://www.husky.co/EN-US/Altanium-Controllers-Overview.aspx) has stayed an independent business from the machine systems since its purchase from Moldflow, based in US.

It was decided from the Horizon(a.k.a HyperSync) product line, the Altanium controller would be an integrated part of the injection system for branding promotion and cost benefits:

- _one single touch-screen panel_ and integrated HMI instead of one for each.
- _premium Altanium feature/performance_ when ordered together, starting from HyperSync.

which means a design of a new unified & proprietary interface

- hardware

  one ethernet cable instead of various digital I/O, RS422 and other industrial interfaces

- software
  - design of a protocol 
    - unifying all in-use commands for temperature and servo control commands
    - extensibility of future functionalities
    - identification of Husky equipments from either side
      - protection of Husky IP
      - protection of malice penetration from the network
  - remote desktop viewing
    - an VNC/RDP-like solution for an operator to control the Altanium controller from the machine

This was completely done by my team in shanghai

- started with a few slides of ppt from the Product Manager, mentioning _Altanium Integration_,
- collected requirements and consensus from all teams at stake and 
- Concepting
- Design execution and validation

It was an unforgettable experience for all the challenges we had to cope with. 

- The one and only Prototype machine built in Bolton
  - late nights of remote connection&debugging on Bolton's shopfloor till 4:00AM
  - several tight validation trips to and from Bolton
    - compete with other design teams for the availability of the prototype machine
- all teams except for software design are in North American
- had to borrow servo and motors from [Baumueller](http://www.baumueller.de/en) at the [ChinaPlas](http://www.chinaplasonline.com/CPS15/Home/lang-eng/Information.aspx)
- had to figure the Altanium side technologies by ourselves through code reading

Due to departmental priorities conflicts between US and Canada teams, we stayed in the middle of one party too demanding and one party too uninterested.

The features were successfully shipped to the first customer TetraPak at Mexicali, Mexico. One designer from my team was part of the PLT team to startup the system .

**It was quite an experience and I was proud that my team made it.**


## <a id="header-2-19" class="md-header-anchor"></a>HyMET Heatlogger<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2007__ 



![](./pics/heatlogger-main.png)

Lack of experience alloys injection, designers wished to monitor the heat parameters at the barrels and sprues in the long term. It was then decided to develop this tool to be deployed onto every sold HyMET machine, logging all interesting parameters. A husky service guy would visit the customer to copy the data out every 6 month or longer.

The project is to read from up to about 100 temperatures values from thermal couplers and save them into logs.

The critical requirements include:

- no threat to normal machines operations
  - no system resource contention with the Polaris HMI/PLC, i.e. minimum CPU cycles and memory footprints
  - no disk space running out
- non-stoppable by the factory operators
- up to 100 channels
- records at an interval from 1 sec to 180 seconds.
- flexible scheduling based on calendars and shifts
- intelligent start/stop of logging with on/off statuses of injection cycles
- data must be power-failure safe, i.e., no corrupt file due to accidental power loss 
- tight schedule imposed by machine shipping date


I coached an intern to have developed the code to read temperatures through TwinCAT API, and personally  wrote the UI in [WTL](https://en.wikipedia.org/wiki/Windows_Template_Library) and the logging in [POCO](https://pocoproject.org/), I also contributed back the enhancement I made since it's an open source project.

A colleague from the HyMET team wrote to me saying:
> this is the most beautiful software I've ever seen in Husky.


## <a id="header-2-20" class="md-header-anchor"></a>Shotscope, Celltrack & Matrix<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008__ 



When it bought Moldflow manufacturing division, Husky dismissed the original French&US teams and had all the assets(software&hardware) sent to Shanghai.

Some of the code dated backed to early 1990s, and the technologies varied from Delphi, Borland C++, Sybase database, MySQL, Linux

- Shotscope, a production monitoring software
- Celltrack, another production monitoring software
- **Matrix 1**, a linux-based Altanium HotRunner Controller software

My team successfully supported existing European/American customers with sustainment development until the next .net versions of Shotscope and Matrix, i.e. SSNX,  came out.


## <a id="header-2-21" class="md-header-anchor"></a>Shotscope NX<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008 - 2015__ 



The successor to Shotscope and Celltrack, with modern IT technologies. To quote its [homepage](http://www.husky.co/EN-US/Shotscope-NX.aspx):
  >The industry’s most advanced plant-wide process and production monitoring system

A first ever Husky made manufacturing monitoring system aimed to be an integration of 3 SCADA/ERP software products: SmartLink, Shotscope, CellTrack, embracing modern technologies at the time to achieve:

- Friendly and intuitive UI and better data presentation ability
- More distributed supervisory ability
- More data storage and processing power
- Built-in communication with other Husky software such as Polaris IMS Control system and Altanium Hot Runner Controller

The first release was delivered on time and received immediate orders at its debut at NPE show 2009, Chicago.

The majority of implementation was done in Shanghai under my supervision with a 3rd-party test team hired from Toronto for feedbacks and bugs reporting.

My team was a core party from the start and kept improving it until it was feature stable and transfered to After Market Service(AMS) engineering team for customer-specific changes in 2015.

Major Technologies include WCF, Silverlight, SQL Server, written in C#.


## <a id="header-2-22" class="md-header-anchor"></a>Shanghai Factory Startup<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005 - 2006__ 



In the context of Husky's rapid growth in China, it was decided to build **a machines&hot runners manufacturing factory**, hence the need for strong engineering teams to

- **build infrastructures**
- **build up teams** of assembly, testing, production engineering
- **transfer engineering knowledge, disciplines&experience** from Canada to local teams

3 Global Engineering Execution(GEE) teams, including 1 controls focused(which I led) and 2 mechanical focused, were founded to support the mission.

With no prior domain knowledge of injection&molding, and by splitting my time in half between Bolton and Shanghai in the first year, I successfully replicated the production infrastructures concerning controls(Polaris Production System) from the ground up.

This 1+ year long efforts earned me a Special Contribution Award, signed by the then Global Vice President of Machines.


## <a id="header-2-23" class="md-header-anchor"></a>Whiteboard<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 - 2016__ 



One of the typical challenges for global teams is the time differences and geographical distances making it hard to put everyone on the same page in meetings. While local teams could debrief at a coffee break or stop by the desk, global teams do not have the luxury. It could be only worse if different projects are interwoven and information is scattered among multiple sources

From expereince of talking to various teams from mechanical, electrical to software and all levels of leaders up to VP, I developed an application to pull information from various sources, including

- Visual Sourcesafe database
- Team Foundation Server
- SharePoint portals
- SQL Server database from IT's attendance&vacation system
- Shared files on mapped network drives

and synthesize them into a dashboard site with links back to the information sources.

An orchestration of technologies/languages are used:

- [Django](https://www.djangoproject.com)
- [Dojo](https://dojotoolkit.org)
- [ASP.NET Web API](https://www.asp.net/web-api)
- [MongoDB](https://www.mongodb.com/)
- Python
- C#
- JavaScript


## <a id="header-2-24" class="md-header-anchor"></a>Product Launch Team(PLT) Trips<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 - 2016__ 



It's been Husky's tradition to set up a highly knowledgeable and experienced team directly from the Dev Eng, instead of regular service teams, to start up machines for early adopter before the formal launch of a product line.

My team had been backing up these teams since 2011 and sending people directly since 2013 to destinations like Vietnam, Mexico and multiple cities in China.

## <a id="header-2-25" class="md-header-anchor"></a>筹建（上海）全球研发工程部<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006 - 2007 &middot; 团队与项目管理, 团队筹建__ 



建立加拿大总部以外唯一一个全球研发工程团队， 全部由硕士研究生以上学历组成。


## <a id="header-2-26" class="md-header-anchor"></a>注塑机人机界面 (HMI) 开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006 - 2016 &middot; 项目管理, 软件架构, 软件编码__ 

__领导开发用于所有赫斯基注塑机的 Polaris HMI 基础软件平台及各种衍生版本，涵盖所有系列注塑机系统。__ 


- 维护性开发基于 COM/VB6 平台HMI
- 重新架构和实现了新一代 HMI,基于 .NET/C# 平台的
- 十多年来，保持上述平台按 SCRUM 开发季度/半年定期滚动发布


## <a id="header-2-27" class="md-header-anchor"></a>注塑机PLC 开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006 - 2016 &middot; 团队与项目管理, 运动控制， CNC, 软件开发__ 

__实时控制相关算法。 包括伺服驱动(Servo),机器人控制等运动控制，以及温度控制， 第三方设备通讯等。基于 IEC 61131 标准，及 TwinCAT/CODESYS 运行环境。__ 




## <a id="header-2-28" class="md-header-anchor"></a>注塑机工厂 DevOps 生产管理系统开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006 - 2016 &middot; 团队与项目管理, 软件架构，软件开发__ 

__Polaris Production System， 此系统赫斯基注塑机控制系统软件开发，模拟，测试和发布的 DevOps 平台。也是车间内部各部门 （研发， 生产，售后支持服务等）的协同工作平台。__ 


基于COM/Visual Basic, Ruby, C#/.NET等技术。


## <a id="header-2-29" class="md-header-anchor"></a>HyperSync/Altanium 工业 4.0 特性开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016 &middot; 团队与项目管理, 软件架构, 代码编写__ 

__基于 TwinCAT 实时工业互联网技术。获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。__ 




## <a id="header-2-30" class="md-header-anchor"></a>统计过程/质量控制（SPC & SQC）模块<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 &middot; 代码编写, 数据科学, C++, DirectX, 图形编程__ 



实时采集注塑周期各项参数，进行 统计过程控制（SPC）及 统计质量控制 （SQC） 计算，6-sigma 生产过程控制要求，并提供多样图表绘制。


## <a id="header-2-31" class="md-header-anchor"></a>在线 HMI 多语言翻译管理系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016 &middot; 项目管理, 软件架构, 服务器软件, 网站设计, C#, SQL, ASP.NET__ 



一体化软件界面多语言开发管理系统，支持22+语言。支持数据库查询，外部翻译请求，审批，上下文场景管理，多语言资源文件生成管理等。基于C#, ASP.NET, SQL Server开发。

该项目由上海开发，部署，日常维护支持全球业务。

该网站的开发使得原来的人工过程自动化, 
- 申请新字符串翻译从原来大约 2 星期等待 减少到 0 阻塞实时异步, 并能
- 提供更多上下文信息用于提高翻译准确性。
还提供了其他查询管理高级新功能。
 
为了支持多达 22+ 种语言 HMI 显示, 开发人员在开发控制系统软件时涉及到任何字符串均需, 事先向专职人员申请并获得特定 ID 替代原字符串用于开法中, 以支持用户使用时界面语言动态翻译。



## <a id="header-2-32" class="md-header-anchor"></a>兼并 Moldflow 制造部门 MES/SCADA 产品<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008 &middot; 管理, 公司兼并, 知识产权转移, 资产接收, 技术融合__ 



领导[收购整合 Moldflow 制造部门](http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit)的法国与美国团队资产， 并后续支持既有客户
如欧莱雅，利乐包装等。产品包括开发的工厂过程监控系统系统Celltrack，Shotscope 及热流道温度控制器 Matrix。


## <a id="header-2-33" class="md-header-anchor"></a>Celltrack MES/SCADA系统维护&开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008 &middot; 管理, 公司兼并, 知识产权转移, 资产接收, 技术融合, JAVA__ 



在 8 周以内，从原公司法国团队接收基于 Windows Server 采用 Java 的工厂生产过程监控系统， 原始代码编写于 1990 年代初。有稳定欧洲市场客户。


## <a id="header-2-34" class="md-header-anchor"></a>Shotscope MES/SCADA系统维护&开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008 &middot; 管理, 公司兼并, 知识产权转移, 资产接收, 技术融合, C++, Delphi, Firmware__ 



在 8 周以内，从原公司法国团队接收基于  Windows Server 采用 C++/Delphi 开发的工厂生产过程监控系统， 原始代码编写于 1990 年代初。有稳定北美市场客户。
还包括嵌入式数据采集监控系统，及 Firmware 。


## <a id="header-2-35" class="md-header-anchor"></a>热流道实时温度控制系统 Matrix<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008 &middot; 管理, 公司兼并, 知识产权转移, 资产接收, 技术融合, C++, QT, MySQL__ 

__从原公司美国团队接收，并持续开发新功能， 包括嵌入式电子硬件及基于 Linux/Fedora 采用 C++/QT/MySQL 开发软件 HMI.__ 




## <a id="header-2-36" class="md-header-anchor"></a>引入确立敏捷（Agile）开发流程<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006 - 2008 &middot; 管理, Agile, SCRUM, TDD__ 



布道引入 SCRUM/Test-Driven Development（TDD） 敏捷（Agile）开发模式， 建立了现代软件生命周期(Software Life Cycle)的开发流程， 实践标准， 以及配套工具。 把以 VSS 源代码控制为中心的__瀑布式__传统模式, 升级到 __需求，文档和源代码__并举的__测试驱动(TDD)敏捷开发__模式。期间多次赴加拿大参与建立流程及相关工具评估(比如 IBM Rational Rose vs. Team Foundation Server(TFS))。



## <a id="header-2-37" class="md-header-anchor"></a>新注塑机产品预上市 (Product Launch Trip)<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 - 2016 &middot; 管理, 新产品上市, 全球客户现场支持__ 



作为研发传统，每当新产品研发结束，型号正式成型上市之前，都会有研发部门自组团队，在早期测试客户现场安装，调试，交付，并收集相关过程注意事项，编写相关文档。以备正式推出后培训生产，和技术服务部门。

从 2011 年起，领导上海团队参与客户全球工厂新型注塑系统安装，调试及交付，遍及__中国，越南，墨西哥，澳大利亚__等全球各地。客户包括可口可乐， 利乐包装，康师傅，哇哈哈， 养生堂等。 



## <a id="header-2-38" class="md-header-anchor"></a>项目与团队管理 Dashboard 应用<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 - 2016 &middot; 管理, 软件架构, 软件编码, 项目管理, 敏捷开发__ 

__基于 micro service 项目管理网站。__ 


基于 Python/Django， C#/ASP.NET/RESTful Service/WebAPI 和 JavaScript/Dojo Toolkit，Apache Cordova，NodeJS 综合 Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 网络映射盘等数据源信息
    - 便利了团队及项目管理人员随时随地在线查看任何项目和状态
    - 在敏捷开发实践中
        - 每日站立会议（daily standup meeting）
        - 部门周会（Weekly team meeting）
        - 项目里程碑会议（milestone meeting）
    等日常或临时会议中简化了与会成员背景介绍时间，迅速进入主题。

![](./pics/wb-overview.png) 
![](./pics/wb-projects.png) 


## <a id="header-2-39" class="md-header-anchor"></a>上海注塑机生产工厂组建<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2015.11 - 2017.01 &middot; 团队筹建, 项目管理__ 



适逢赫斯基战略性扩大中国投入, 决定在此建立注塑机生产工厂， 并把亚太地区总部从香港迁至上海。虽然注塑行业对我而言是全新的领域。

加入公司立刻赴加拿大总部工作，并在第一年里， 频繁往返于两地，一半时间在加拿大，一半时间在上海, 一边接受高强度的相关的产品， 工具， 及工作流程培训， 一边组建培训团队，建立车间生产，组装及测试的软件流程。

- 移植了庞大的生产软件系统(Polaris Production System)
    - 包括 500G+ 生产数据， 因数据太大，多次网络传输失败后， 我不得不带上硬盘大年三十从加拿大飞回上海。
    - 此系统严重加拿大工厂的本地 IT 环境， 不得不与两地 IT 部门紧密合作，并解决许多技术与非技术问题，在上海重建相似环境， 并与总部数据库连接同步，实现全球生产数据与流程的统一化管理
- 招聘组建了第一批软件，电气控制系统支持团队
    - 很多时候在不得不从加拿大通过电话面试


最终成功为上海工厂建立生产支持系统（Polaris）及流程，与加拿大，卢森堡工厂数据对接同步。招聘，培训了首批生产支持 （装配和测试）工程团队

由于在此项目的贡献，获得杰出贡献奖， 由时任全球机器事业部副总裁（Global VP of Machines）签署颁发。


## <a id="header-2-40" class="md-header-anchor"></a>Polaris 注塑机实时控制系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005.12 - 2016.12 &middot; 软件编码, 项目管理, 敏捷开发__ 



Polaris注塑机实时控制系统是一个基于工业控制计算机的注塑机操作系统（Injection Operating System）及与之配套的装机发布系统（Production Deployment System）。

操作系统（Injection Operating System）通过控制基于 Profibus，SERCOS，EtherCAT 等接口的电气系统，实现原料加热，注射，机器人控制等注塑成型周期过程的运动控制和自动化。此系统包括

- 人机界面( HMI ) 
    - 提供友好的人机界面，故障诊断，状态监控，参数调整等上层功能；
- 软 PLC(soft PLC) 组成:
    - 实现PID温度控制，各种电机运动控制算法和实时性等底层控制。

每季度发行一次基本版本升级，包含bug修复，新功能，以及与最新的机械和电气设计相对应的更动，并作为定制功能的最新平台。

装机发布系统（Production Deployment System）根据用户需求及每一台机器的机械与电气配置，选择相应的软件模块，把操作系统（Injection Operating System）定制安装到注塑机上。

项目职责：

- 在上海工厂的建立过程中，主导建立了控制软件的本地装机发布系统（Production Deployment System），使生产车间组装后装机测试成为可能，从而为2006年生产线顺利投产提供前提条件。
- 参与培训了第一批生产线控制软件安装测试工程师（Controls Test Engineer Team）。
- 领导维护本地装机发布系统（Production System）并支持车间生产，直至2007年建立专门的生产支持团队(Production Team)。
- 持续领导并参与此系统相关工作的一切开发，维护，季度升级工作等等，涉及

- C++, C#, VB, IEC61131 PLC 程序设计的开发维护
- Profibus, SERCOS, EtherCAT 接口硬件调试故障诊断
- 第三方辅助系统集成， 比如机器人(Robots)等。



## <a id="header-2-41" class="md-header-anchor"></a>Polaris HMI .NET 架构与实现<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006.06 - 2008.01 &middot; 桌面软件设计, 第3方软件集成__ 



由于现有控制系统 HMI 已经使用多年

- 经过多年的修改维护，已经演变得性能降低(比如40秒软件启动时间)，众多临时特性的叠加也使用户操作复杂， 系统庞大而难以维护；
- 作为公司第一代迁移自 PLC 的 PC/softPLC 架构控制系统, 软件构架上不成熟而难以扩展；
- 大量代码基于 Visual Basic 6, 难于添加现代 UI 体验及功能
- 软件工程思想和开发工具的发展使得重构原有系统变得可能而且代价低廉。

该项目组织大量人员梳理现有功能, 基于.NET/C#, 重新架构和编写 HMI 的 UI 及 service 部分。

本人职责包括

- 阅读原系统代码及设计文档，分析，提取，重组设计需求，找出性能瓶颈
- 参与设计确认软件架构
- 技术培训讲解并领导上海团队确保 TDD/SCRUM 开发过程实施, 确保开发进度和代码质量。
- 编写部分代码
- 招聘工程师充实项目组

该应用部署于运行 Windows Embedded XP 的工控机( Industrial PC ), 技术涉及Windows Form, WPF, C#, C++/CLI, SQL Server等。



## <a id="header-2-42" class="md-header-anchor"></a>Shotscope NX 车间生产管理系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2008.06 - 2015.09 &middot; SCADA, 网络, WCF__ 

__一套基于 web 技术的过程和生产监控自动化实时解决方案。__ 


整合现有 SmartLink, Shotscope, CellTrack 过程与生产监控系统软件和硬件，实现生产制造企业数字化管理，包括

- 基于 Window Server 的网站服务器软件，
- 基于工控机的数据采集器（Data Collector）控制软件
    - 适用于 Non-Husky 注塑机客户采集生产过程状态等数据

该系统

- 上端连接**企业上层 IT 系统**等第三方系统（ 如 ERP ）
- 下端连接机器运行状态（实时周期参数），能源消耗，物料分配与消耗， 工班安排等**实时生产车间数据** 
    - 对于 Husky 品牌注塑机，从其控制软件内建相关模块，以私有软件协议，获得专属详尽的生产细节数据，比如
        - Husky Polaris 注塑机实时控制系统 
        - Husky Altanium 热流道控制系统 
              
    - 对于 非 Husky 品牌第三方注塑机， 通过
        - 标准工业**软件协议**，如 [OPC][#OPC], 从第三方机器控制系统获取数据
        - Husky 专用数据采集器以 Side-by-Side 方式**硬件采集**数据

开发内容包括

- 服务器软件开发
- Polaris Polaris 注塑机实时控制系统相关 HMI 及数据接口开发 
- Data Collector 专属定制及工业标准(SPI, OPC, USB, RS422 等) 数据协议开发

利用 WCF, Silverlight, SQL Server 等C#/.NET技术系统，使其与其前身系统相比

- 更友好直观的操作界面与数据呈现， 报表生成能力， 
- 更广泛，分布式的监控能力。
    - 单车间
    - 全球跨地域车间
- 更强大的数据存储与处理能力

该产品按计划于 2009.6北美塑胶展（NPE Show 2009）前完成初步版本，作为主展产品之一，[在展会上取得巨大成功，并获得用户热烈欢迎][#NPE].

初期开发阶段项目职责：

- 领导协调上海团队与加拿大，卢森堡同事及印度外包公司开发的分工
- 与销售部门同事协作，转化用户需求（User Story）为可实现的技术特性(Feature)
- 参与制定阶段目标，划分可独立实现的单项任务(Work Item)，
- 制定以2星期为周期的迭代开发计划，并确保其实施，每一个Milestone能按计划达到
- 确保测试驱动开发（TDD）等开发模式和工程标准的实施
- 当项目方向不明确或出现争议时，做出决策，并推动项目前进


从产品推出后， 根据市场需求，上海团队一直专人负责升级开发中， ，直至 2015 年产品转由 After-Market Service Engineering 支持，期间内容包括

- 性能优化
- 客户专有生产流程模板
- 客户专用软，硬件接口支持

更多关于该产品的功能综述， [请参考其主页][#SSNX].

[#NPE]: http://www.husky.ca/abouthusky/news/content-20090608.html
[#SSNX]: http://www.husky.co/EN-US/Shotscope-NX.aspx
[#OPC]: http://baike.baidu.com/item/opc/3875



## <a id="header-2-43" class="md-header-anchor"></a>嵌入式模具无线自动识别 IoT 解决方案<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2010.06 - 2015.09 &middot; 无线通讯, 网络通讯, 嵌入式, 硬件烧录__ 



基于 [RuBee(IEEE 1902.1)][#RUBEE] 无线通讯技术的工业 IoT 解决方案，实现模具的自动识别，参数加载及寿命磨损管理。, 实现赫斯基领先全球的快速换模技术。 内容包括：

- 服务器数据管理
    - Windows Server 网络服务器程序开发
    - 编写 Mold ID Server 门户网站, 做为全球唯一数据中心，存储所有生产，售出的模具参数信息。
    - 在设计阶段供模具工程师录入，修改模具信息
    - 供生产工程师，服务工程师在线下载每套模具信息供生产现场或客户现场升级注塑机/模具配置
- Polaris HMI 集成
    - Windows 桌面程序
    - 从 Polaris HMI 中无线检测 Rubee Tags 并从中读取模具参数功能
- Tag 烧写工具维护工具
    - Windows 桌面程序
    - 模具信息以 [RuBee(IEEE 1902.1)][#RUBEE] 协议无线烧写进 Tag
    - 在有限的 256 字节内压缩存储众多模具信息
    - 连上公司内部网路 (Intranet)，可以通过全球烧写, 比如上海的工程师可以远程对加拿大工厂的Tag 烧写。

该项目在我的领导下由上海开发完成，服务器部署于上海，由上海团队维护支持全球业务。

开发内容涉及 SharePoint, Silverlight, WinForm 以及 Socket 网络通讯。

[#RUBEE]: http://www.baike.com/wiki/rubee


## <a id="header-2-44" class="md-header-anchor"></a>HyMET HeatLogger<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006.12 - 2007.04 &middot; 桌面软件设计, 硬件信息实时采集__ 

__一位资深工程师特意写信给我__ 


![](./pics/heatlogger-main.png) 
监测, 记录 HyMET 合金注塑机上各热电偶监测到的热流道( Hot Runner )温度, 供

- 设计工程师验证设计, 
- 服务工程师解决客户现场故障。

技术要求包括：

- 同时采集多达100通道数, 采集周期在 1s-180s 范围内可调。
- 因为程序安装于全球客户生产现场, 由 Husky 服务工程师每 3 个月至 6 个月取回数据供设计工程师分析, 
    - 程序必须占用尽可能少的系统资源 
    - 程序不能被客户意外终止 
    - 程序能自动监测注塑机运行/停止状态, 智能停止或重新启动记录, 并保证记录数据不能因为意外掉电而丢失。
    - 预测记录数据大小, 以保证在无人监控时导致记录空间不足。
 
该项目全部以 C++ 编写, 

- 利用 TWinCAT ADS API 实时采集硬件信息, 
- 利用开源库 [The POCO C++ Libraries][#POCO] 管理配置信息和数据持久化, 
- 利用 [WTL][#WTL]/ATL 架构主体程序及用户界面, 呈现实时信息, 用户权限认证管理。

设计领导解决方案及架构, 并亲自编写除 TWinCAT ADS API 之外全部代码, 作为范例讲授 Windows/C++ GUI, 高性能（低CPU占有率，低内存使用率）后台监控应用程序设计相关知识。
并在 [POCO C++ Libraries][#POCO] 的使用过程中, 改进其 XML 处理模块, 回馈社区, 位列 [Contributers][#CONTRIB]。

该应用部署于运行 Windows Embedded XP 的工控机( Industrial PC )。



[#WTL]: https://en.wikipedia.org/wiki/Windows_Template_Library
[#CONTRIB]: https://pocoproject.org/community/contributors.html
[#POCO]: https://pocoproject.org/


## <a id="header-2-45" class="md-header-anchor"></a>多语言翻译管理数据库(HkResource)<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2006.05 - 2006.07 &middot; 软件设计, COM/ATL__ 

__设计新数据库文件格式，为 HMI 提供 22+ 种语言支持服务。采用 ATL/C++ 实现 COM 服务器。__ 


当 Polaris HMI 支持的界面语言从一种语言（英文）增长到 22 种以上，原有语言字符串数据库设计不再适应

- 基于单个文件的设计，典型大小 70+ MB
    - 文件经常损坏， 尤其当 HMI 意外崩溃（比如系统掉电）
    - 读写性能太差
    - 不能裁剪选择只用到的语言，大部分客户只用到 2 种语言（英语及本国语言）

保持其接口不变的情况下重新

- 设计新数据库文件格式
- 实现其 COM 服务器，采用 ATL/C++
- 从用内存映射文件(Memory Mapped File)方式操作文件

克服了以上所有缺点， 而 Polaris HMI 不需要为此更改一行代码。



## <a id="header-2-46" class="md-header-anchor"></a>HyperSync-Altanium 工业4.0集成<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016.02 - 2016.10 &middot; 软件设计, 系统集成, 硬件采集__ 



HyperSync™为同步化系统，模具、机器、热流道以及辅助设备协同工作。除具备突出工业4.0级别的智能性及连通性外，机器及模具流程同步化的增强可以较低的总产品成本实现较快的注塑周期，同时不会影响产品的质量


Husky 的注塑机（Injection Molding Machin）与热流道控制器 (Hotrunner Controller)作为两个独立运营的部门，虽然各自居于市场领先， 当客户同时购买两者产品时，彼此视对方为普通第三方设备，并不能启用对方独有的高阶功能/性能。

二者紧密高效的集成，成为年度新机型 Horizon 的核心功能之一， 意在

-  把各种功能的分别连接方式（如 Digital I/O, RS422 等），简化整合为一根 以太连接线， 从而
    - **在硬件上**，简化电气连接， 及生产组装车间工人，客户现场服务工程师接线的复杂性和调试难度
    - **在软件上**，具有向前扩展性， 便于增加新功能

此项目涉及面繁复庞大，**需要在数百万，行数十个工程的 C# 和 PLC 代码中，阅读，理解并修改添加相关功能**。设计内容包括

- 整理所有拟支持的 Altanium 产品线包括Delta 3, Matrix 2 等的全部功能，重新设计基于 Ethernet 的命令集，
    - 温度控制
    - 伺服电机控制
    - 版本控制
    - 对端识别，及专属功能锁定与开放
- 在 Altanium Controller 端添加支持
- 在 Polaris HMI/PLC 端添加支持
- 验证 Beckhoff/TwinCAT RT-Ethernet 实时
- 对部分重要客户提供兼容性升级，在原有硬件连接(RS232/USB)基础上实现新功能


此项目方案及软件实现完全由上海团队调研设计完成， 付出众多， 比如 

- 方案的初期一直争议怀疑意见中进行
    - TwinCAT RT-Ethernet 传输，尤其对于伺服电机控制指令传输未经实际验证
    - 在同一电缆线种，实时信号与非实时信号相互干扰程度未经实际验证


- 12小时时差下，持续数月密集的电话，视频英文设计讨论会议
- 部分重要功能在没有硬件情况下完全靠软件模拟开发测试
- 多次通宵达旦，远程连接到加拿大车间的原型注塑机上调试
- 多次往返于上海与加拿大，调试验证
- 亲赴墨西哥原型机客户（利乐包装）现场组装调试机器




在此核心功能的支撑下，该机型如期在 2016 年秋天德国举行 [K-Show][#KSHOW] 首秀成功，并在 2017 获得 [Ringier 2017 Innovation Award][#RINGIER] 创新奖。


[#RINGIER]: http://www.husky.co/News.aspx?id=6442451127
[#KSHOW]: http://www.husky.co/News.aspx?id=6442451075



## <a id="header-2-47" class="md-header-anchor"></a>Altanium 远程桌面<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016.04 - 2016.08 &middot; 软件设计, 系统集成, 网络通讯__ 

__基于 RDP 协议的嵌入式远程桌面, 实现对　Altanium　温度控制器的远程控制__ 


当前注塑机（IMM）与 Altanium 控制器是两套相互独立运行的工控机（IPC）系统，各自拥有自己的采集与控制硬件以及 HMI 系统，仅在重要信号上抄送对方一份。操作工必须在两套设备的 HMI 显示屏 上来回操作。

此项目是 HyperSync/Altanium 以太网 One-Cable 集成的互补项目。意在直接从 IMM 的显示屏上直接复制 Altanium 的 HMI 控制界面，实现 One-Display 操作。

- 前者重点在于**实时硬件信号**的相互通讯，实现运动控制的有序性同步， 以**工业以太网( Industrial Ethernet )**协议传输
- 后者的重点在于**非实时软件偏好设置**，以标准 TCP/IP 协议传输

技术内容包括

- VNC 与 RDP 远程协议的方案选择与原型设计
- 设计/验证 RDP 通讯对带宽的占用，及对同电缆内实时信号的影响
- Polaris HMI 系统架构下集成 RDP 协议及界面
- Altanium 的 [APIPA][#APIPA] 自动接入检测，识别
- Altanium 控制器自动解锁屏处理
    - Altanium 采用无键盘纯触摸屏设计
        - 其在 被 RDP 协议远程连接后，会锁屏， 导致操作员不能进入


- 在工业强度下性能的应急响应实时性与可靠性设计与检验
[#APIPA]: http://baike.baidu.com/item/APIPA?sefr=cr



## <a id="header-2-48" class="md-header-anchor"></a>扁鹊 DevOps 云生产管理系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2013.04 - 2016.12 &middot; 软件架构, 软件设计, 云计算, 大数据, 机器智能, IoT, 数据科学, MQTT__ 

__立足 Azure 云技术（比如 IoT Hub）， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术的新一代 DevOps 系统。__ 


本项目由我以上古名医命名，借指其对现有 Polaris Production System 的“起死回生”的治疗。其凝聚了自加入Husky 以来，我对其开发流程，生产支持流程的观察和解决之道。

Polaris Production System 自 2000 左右开始使用以来，众多的功能添加使其及庞大复杂，经常错误，以致在加拿大有工作 20 年经验的工程师几乎以每天解释各种错误及其绕过之道专职。在上海，其学习难度和莫名其妙的错误模式阻碍了团队的迅速扩大。

其开发环境与发布环境杂揉，多年增量式添加更改配置，延续到目前 VMWare 虚拟机已达已经 70GB 以上的，极少数人能从头配置其环境。

- 其中的完全依赖于加拿大，上海，卢森堡的本地网络映射盘数百Ｇ数据文件，时常因为文件不及同步，丢失，重命名等导致崩溃，网络速度。

- 众多的虚拟机既导致工作站导致性能下降， 共享盘空间浪费，其不停拷贝，也导致网络拥挤，引起 IT 部门的不满。

- 其复杂的工具集，界面，和莫名错误让需求相对单一的车间生产支持工程师不知所措。


扁鹊精分开发和发布的环境需求，立足当前虚拟化技术的前沿与 DevOps 的相关理念，改变其本地应用程序架构，基于网络服务（Web Services）的架构，

- 针对开发环境需求
  
  利用 Vagrant 管理虚拟机使
  
  - 开发轻量化

    基于公共 Vagrant Box， 不必囤积大量虚拟机镜像

  - 共享简单化 

    不必通过拷贝 70+GB 的文件实现开发/调试环境共享

  - 版本化

    容易回退或前进到某特定版本，或比较其差异

- 针对软件发布环境需求

  采用后端前端 Single Page Application (SPA)　网站应用 后端 REST API Service
  - 可以从新浏览器窗口， 发起新的编译请求
  - 进度状态消息实时 email 通知
  - 可以在 Intranet 覆盖的任何地方发起请求和下载结果，而不必守在电脑前监控过程等待结果 
  - 当部署到云端 （Microsoft Azure）上后，服务工程师可以全球远程下载补丁


扁鹊由若干子项目组成，主要

- Lancet（柳叶刀）

    主要以 Python 语言实现的 REST Service 总称， 包括重新实现部分原 Polaris Production System 的功能。

- Acupuncture（针灸）

    以 Django/Dojo Toolkit/Web Socket 为框架实现的单页网站应用(Single Page Application), 作为生产支持和现场服务工程师生成软件包的主要 GUI 门户。

- Transformer（变形金刚）

   管理预配置好的 VMWare 虚拟机群， 

   - 包装， 并行化执行 Polaris Production System 命令， 使编译生成机器时间从原先 典型 40 分钟， 缩减至 5-10 分钟
   - 合理分配请求，提高虚拟机资源的利用率

- HySearch

   基于 MongoDB / Django 的搜索网站， 通过关键字或主要类别，搜索 软件模块或现有机器，以参考比较。

- **部分截图(来自早期 PPT 准备素材)**
    
- **Bianque 早期概念图**
![](./pics/bq-arch.png)

- **HySearch 搜索门户视图**
![](./pics/bq-search.png)

- **Acupuncture 早期视图**
![](./pics/bq-acupuncture-1.jpg)
![](./pics/bq-acupuncture-2.png)

- **Acupuncture 早期视图（移动设备）**
![](./pics/bq-mobile.jpg)

- **Transformers 早期调试窗口**

## <a id="header-2-49" class="md-header-anchor"></a>Automatic Power Transfer System<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2002.11 - 2003.06__ 



 a set of critical switching unit for dual power supplies ensuring reliably & continual power supply where power faults may lead to critical results (e.g. surgical rooms in hospitals).

 There was no out-of-box product on the market, except for ad-hoc solutions built up with components from vendors of customers choice.

The goal was to develop a complete auto-transfer system aimed for

- more intelligence
- friendlier HMI
- higher reliability
- lower cost

All objectives achieved using all Schneider products

- NEZA PLC for intelligence and user-friendliness
- popular NS circuit breakers for reliability
- safety achieved with interlocks
  - software/electrical interlock by PLC
  - built-in mechanical interlock by NS

Led a team from multiple departments (Design center, Marketing, Field Application) and pushed from concepts to fully-functional prototype, includes analysis reports on
- cost (only 1/3 of solution on the market)
- technical risks
- qualification plan

It was submitted for Chinese Management Committee(CMC) for final review of production investment by the time I left.


## <a id="header-2-50" class="md-header-anchor"></a>Development of industrial-strength LED alarms<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2002.11 - 2003.06__ 



research to in preparation for launch Schneider branded alarms for factory shopfloors.
- investiagate LED technologies.
- collect LED samples from select vendors  
- devise tests for IEC conformance tests regarding lumen, product life etc.

## <a id="header-2-51" class="md-header-anchor"></a>紧急备用电源自动切换系统(ATS)<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2002.11 - 2003.08 &middot; 电气设计, 产品架构, PLC 应用__ 



开发新一代auto-transfer system, 以提供更智能, 更友好的人机界面, 更高可靠性和更低廉的成本.此系统基于 NEZA PLC, 配合NS系列塑壳式断路器使用, 为医院等场所提供可靠的主备用电源管理, 以持续电源供应. 

本项目由研发中心，合资工厂，NEZA PLC 事业部合作，意在

- 填补自动电源切换装置空白
- 提供新型 哪吒(NEZA)系列 PLC 的应用案例

本人领导并推动此项目, 

- 负责全部原型设计与改进，先后设计出 3 台样品
- 拟定并完成了各项 IEC/GB 规定相关试验
- 完成了投资/收益分析, 技术可行性分析及设计方案。

此项设计节省了 1/3 的成本。 


## <a id="header-2-52" class="md-header-anchor"></a>空气断路器国产化设计<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2002.11 - 2003.08 &middot; 电子电路设计__ 



为降低成本，缩小与本地生产商的价格差距，受合资工厂委托，对引进产品 Vigi 系列小型空气断路器进行本地化设计更改， 包括

- 分析国外原 PCB/ASIC 的设计原理图， 针对国内标准及供应商的差异修改设计，并设计实验验证设计
- 逆向工程国内市场竞争产品，研究其原理， 成本， 可靠性等， 并通过设计实验获取相关参数

此项目与另一同部门工程师合作完成， 责任不分主次。



## <a id="header-2-53" class="md-header-anchor"></a>工业 LED 指示报警灯<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2002.08 - 2002.11 &middot; 电气设计, 产品设计前期调研__ 



为可能进入工业 LED 指示/报警灯市场，受合资工厂委托进行调研， 包括

- LED 光源技术原理
- LED 光源技术指标及检测手段，拜访包括复旦大学电光源研究所， 美能达光学仪器等研究所和企业交流潜在合作机会与意向
- 收集 LED 生产厂家样品， 在研发中心实验室根据国标（GB）和 IEC 相关标准，设计实验，获取原始数据。

此项目与另一同部门工程师合作完成， 责任不分主次。



## <a id="header-2-54" class="md-header-anchor"></a>恒温加热箱<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2002.11 - 2003.01 &middot; 电气设计, 产品设计, 单片机嵌入式应用__ 



此项目意在提供高达70 &#8451;的恒温实验环境, 节省昂贵的外购设备费用。 包括

- 单片机 PID 控制调试
- 封闭箱体机械设计，组装

此项目与另一同部门工程师合作完成， 责任不分主次。


## <a id="header-2-55" class="md-header-anchor"></a>开关电源国产化<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2003.05 - 2003.08 &middot; 电气设计, 产品设计前期调研__ 



此项目研究引进原产波兰工厂开关直流电源的可行性， 包括

- 设计实验验证原产波兰工厂样品国家标准合规性，及收集各项性能指标。
- 收集调研国内市场同类产品的性能参数
- 出具可行性报告及建议 

此项目独立完成。

## <a id="header-2-56" class="md-header-anchor"></a>Markdown-CV<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2017 &middot; Python, HTML, Boostrap, JQuery__ 



A set of scripts and templates to generate html and PDF files from resume written in pure text, i.e. [Markdown][#MD], leveraging [Bootstrap][#BOOTSTRAP] themes and other open source code such as [marked js][#MARKEDJS].

- open sourced on [GitHub][#MDCV]
- Written in Python, HTML, CSS, JavaScript, [Bootswatch][#BOOTSWATCH]/[Bootstrap 4.0 alpha][#BOOTSTRAP]

- [link][#MDCV]

**This very resume is generated with this tool**

[#MDCV]: https://github.com/rockonedege/markdown-cv
[#BOOTSTRAP]: http://v4-alpha.getbootstrap.com/
[#BOOTSWATCH]: http://bootswatch.com/
[#MD]: http://daringfireball.net/projects/markdown/
[#MARKEDJS]: https://github.com/chjj/marked


## <a id="header-2-57" class="md-header-anchor"></a>2D Water Effect in WTL<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 &middot; C++, Image, Graphics__ 



A hobby project while learning about image processing and graphics rendering.  
- Open sourced with an article on [The Codeproject][#2DWTL].
- Written in C++/WTL

- [#2DWTL]

[#2DWTL]: https://www.codeproject.com/Articles/188236/D-Water-Effect-in-WTL



## <a id="header-2-58" class="md-header-anchor"></a>A Literal Converter for Integers<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2004__ 



A small utility for myself to help process the data file from the oscilloscope, which I used to record data going through Serial Peripheral Interface(SPI) ports while verifying the flash memory chips.

- Open sourced with an article on [The Codeproject][#LITERAL]
- Written in C++

- [link][#LITERAL]

[#LITERAL]: https://www.codeproject.com/Articles/10379/A-Literal-Converter-for-Integers



## <a id="header-2-59" class="md-header-anchor"></a>TwinCAT TSM File Viewer<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016__ 



A binary opaque format, named tsm, is used in TwinCAT 2 for configurations, which is where hardware *meets* software in the TwinCAT-based control system, specifically it maps software *variables* with Hardware I/O *signals*

- loads variables from software logic written in PLC/CNC/C++ etc.
- scans hardware I/O devices such as cards, boxes, terminals
- scheduling: exchanges values to-and-from in a timely manner(i.e. as tasks).

Reading directly from the tsm file is the most reliable way at runtime to extract the information for extension.

Husky's HMI has been structured in a way that's closely coupled to a local TwinCAT runtime,  which has hindered its evolution in two ways
- engineering efficiency
  - even simple non-PLC HMI change requires to fire up the whole simulation environment in complicated VMWare image, which, in turn, makes it hard to bring in new bloods to the team, as the learning curve for such simple tasks is is very unusually steep and daunting for new engineers

- innovations for industry 4.0
  To Quote Microsoft, this world is heading toward **cloud first** and **mobile first**. A universal set of UI that enables people to monitor, or operate with properly configured access rights is a must to be industry 4.0 compatible.

- product roadmap
  while it's been long desired to unify the platforms for the Machines and HotRunner controllers business to strengthen the one-Husky brand, duplicate development and features have to be paid and the customers have to operator on two separate HMI/Touch screens to get their job done.

I figured decyphering the tsm file and hosting it through a web service would be an ideal decoupling solution
- minimal risk
 - leaving the PLC algorithms intact largely.
- maximum extensibility of HMI
   - with modern UI frameworks, the HMI can be develop only once to fit all client platforms. declarative style UI can be developed any platform, mobile or desktop, deployable to local, on-premises servers or on cloud.
   - dependency on the PLC can be easily simulated with a simple JSON file in most cases.

I personally completed the proof of concept with some open source tool
- [websocketd](http://websocketd.com/)
- [ADS, Beckhoff protocol to communicate with TwinCAT devices](https://github.com/rockonedege/ADS)
 and in-house coding based on TwinCAT APIs.

C++, Python, Javascript and some Go were used in the prototyping.

I did not have a chance to present the concepts and the work I've done before leaving Husky, Though planned to.


## <a id="header-2-60" class="md-header-anchor"></a>Multi-Ping<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016__ 



While integrating Altanium controllers into Horizon machines, one of the features was to detect the connection of a controller, and fetch its APIPA address, which became a challenge due to a maximum of delay was allowed.  

The built-in .NET classes did not cut as the it resulted in **10s at best and 40s on average**, and cost about 700MB memory or higher till the host HMI apllication crashes.

Guessing that it might be the wrapping of .NET over the raw system sockets, I implemented the functionality with C++/Boost.Asio and leveraged asynchrous techniques, and was able to complete the detection within **2s** using around **5MB** memory.

The algorithm was then wrapped 
- through C++/CLI as a .Net Assembly for consumption of Polaris HMI
- as a trouble-shooting  utility apllication for testers on the shopfloor and service people in the field.  


## <a id="header-2-61" class="md-header-anchor"></a>HostLinkpp - An Implementation of HostLink inter-machine Protocol<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2012 &middot; C++, TCP, socket?Boost.ASIO, Boost.spirit__ 



![](./pics/hostlinkpp-ubuntu.png)

A C++ Implementation of Husky Hostlink, a proprietary raw sockets based binary protocol accessing Husky injection machines, e.g.

- issuing commands  
- reading statuses and cycle data

It has been adopted by some major customers to integrate Husky systems into their OPC/ERP systems, who requested more commands(e.g reading injection cycle breakdown data) to be supported, hence the project.

I personally wrote the whole code, based on the original MFC based code written in around 2000, as an illustration and teaching material of standard C++ for efficient, cross-platform system related code.

- added more advanced commands to fetch extended cycle data available in new generations of machines  
- fixed bugs specific to 64-bit, which was not considered when the original code was written
- replaced ad-hoc command  construction&parsing with Boost.Spirit and Boost.Qi
- replaced MFC based sockets communicated with Boost.ASIO
- supported platforms beyond Windows to Linux(tested Ubuntu)
- supported Python binding with Boost.Python
- supported .NET binding with C++/CLI

This project leverage quite a few  [Boost][#BOOST] libraries such as [Boost.Asio][#ASIO], [Boost.Spirit][#SPIRIT]， [Boost.PP][#PP]，[Boost.Python][#PYTHON] and advanced modern C++ techniques such as metaprogramming.

Built with CMake.

The code was not open sourced because the Hostlink protocol is proprietary.

[#BOOST]: http://www.boost.org/
[#ASIO]: http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html
[#SPIRIT]: http://boost-spirit.com/home/
[#PP]: http://www.boost.org/doc/libs/1_63_0/libs/preprocessor/doc/index.html
[#PYTHON]: http://www.boost.org/doc/libs/1_63_0/libs/python/doc/html/index.html



## <a id="header-2-62" class="md-header-anchor"></a>Console3<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2012 - 2016__ 



A fork of [Console 2][#CONSOLE2]

- fixed bugs
- replaced rendering GDI from to [Direct2D][#]/[DirectWrite][#DW] 
- added Husky-specific feature with integration to Husky tools

This tool was widely used by the Shanghai team.

[#CONSOLE2]: http://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx
[#D2D]: https://msdn.microsoft.com/zh-cn/library/dd370990(v=vs.85).aspx
[#DW]: https://msdn.microsoft.com/zh-cn/library/windows/desktop/dd368038(v=vs.85).aspx



## <a id="header-2-63" class="md-header-anchor"></a>Beckhoff TwinCAT ADS Protocol<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016__ 



A fork of the open source implementation of Beckhoff ADS protocol

- added support of TwinCAT 2

- [link][#ADS]

[#ADS]: https://github.com/rockonedege/ADS




## <a id="header-2-64" class="md-header-anchor"></a>Markdown CV<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2017.01 - 2013.03 &middot; HTML5 设计, 脚本编写, 效率工具__ 



**本简历由此工具辅助完成**

在整理编写自己简历时，决定采用奥卡姆剃刀原则， 以 Markdown 格式纯文本书写

- 不依赖于 Window/Word或其他系统和工具环境依赖, 便于随时随地更新
- 分离内容与呈现形式，便于输出多种格式
  - PDF 以便于纸质打印输出
  - HTML 以便于电脑屏幕上链接跳转，浏览
  - TXT/Markdown 便于版本控制

采用了 Python, HTML, CSS, JavaScript, Bootstrap 4.0 alpha 及 [Bootstrap][#BOOTSTRAP] 主题。

相关模板及脚本开源于 [GitHub][#MDCV] 供免费下载。

- [link][#MDCV]

[#MDCV]: https://github.com/rockonedege/markdown-cv
[#BOOTSTRAP]: http://getbootstrap.com/


## <a id="header-2-65" class="md-header-anchor"></a>2D Water Effect in WTL<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2011 &middot; 图像处理__ 



该代码源于工作中**图像识别处理**, 以及 Windows 图形编程(GDI+, Direct2D ) 学习研究时，兴趣所致。

一段时间需要频繁处理从示波器中导出的芯片通信信号, 并需要于是写了此小工具用于数值的二进制，十进制，十六进制之间相互转换，

- 采用 C++/WTL, 并用英文解释了代码的设计和使用方法。 
- 发表于开源网站 [The Codeproject][#2DWTL]

- [link][#2DWTL]

[#2DWTL]: https://www.codeproject.com/Articles/188236/D-Water-Effect-in-WTL



## <a id="header-2-66" class="md-header-anchor"></a>A Literal Converter for Integers<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005 &middot; 开源, C++__ 



该代码源于工作中一段时间需要频繁处理从示波器中导出的芯片通信信号, 并需要于是写了此小工具用于数值的二进制，十进制，十六进制之间相互转换，

- 采用 C++, 并用英文解释了代码的设计和使用方法。 
- 发表于开源网站 [The Codeproject][#LITERAL]

- [link][#LITERAL]

[#LITERAL]: https://www.codeproject.com/Articles/10379/A-Literal-Converter-for-Integers



## <a id="header-2-67" class="md-header-anchor"></a>TwinCAT TSM 查看器<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016.06 - 2016.12 &middot; 桌面/服务器/云服务器/软件设计, 网络通讯, IoT__ 



TSM 文件是 倍福公司( Beckhoff ) 自动化设备管理与 PLC 运行环境软件 TwinCAT 2 的配置文件，该文件采用不透明的二进制格式，包含

- 所有**硬件**比如板卡及其配置信息
- 所有**软件**比如 PLC， 数控（NC） 等程序及相关**实时任务**配置信息
- 硬件采集与控制端口与对应软件变量的**映射**信息

一台典型的 Husky 注塑机系统拥有 6000+ 的节点映射信息，导致无论

- 开发阶段，
- 车间组装测试阶段， 
- 还是客户现场服务

调试，排错，定位都非常低效。

此工具采用最新 IT 技术, 实现以下功能

- 把所有二进制信息提取成文本格式，比如 JSON, XML 等
- 以 HTML5/JavaScript/CSS 的单页应用(SPA - Single Page Application) 实现了
  - 可视化节点信息
  - 快速搜索查询节点
  - 关系图（Dependency Graph）可视化
- REST 服务，供远程读取该注塑机的所有配置信息
  - ** 此功能使得设计工程师可以从办公室协助车间生产测试工程师调试机器， 而不用来回跑，节省大量时间 **
- Microsoft Azure IoT Hub 支持
  - ** 此功能使得设计工程师可以从 Husky 办公室协助全球客户现场工程师调试，节省大量时间金钱 **

此项目主要采用 C++/CMake, Python, JavaScript，采用开源库 [Vis.js][#VIS]等。

[#VIS]: http://visjs.org/


## <a id="header-2-68" class="md-header-anchor"></a>HostLink TCP 注塑机通讯协议<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2012.08 - 2013.04 &middot; 桌面软件设计, 网络通讯, Windows, Linux__ 



![](./pics/hostlinkpp-ubuntu.png)

Hostlink 是原定义于 90 年代中期， Husky 专有的数据通讯协议， 基于原始 Socket（Raw Sockets） 定义众多命令，用于远程与 Husky 注塑机交互，包括

- 实时读取注塑周期各种参数(injection cycle data)
- 设置各种注塑参数

客户通过购买此协议模块，可以把 Husky 注塑机集成到其 生产执行系统( MES )与 ERP 系统中。

在为某大型客户修复该模块程序错误（Bugs）时， 我发现从长远计，原代码架构与系统依赖不久将淘汰，遂决定

- 在业余时间面向未来重新实现，
- 并作为小组相关技术培训的范例。 

开发内容包括

- 以标准 C++ (Standard C++) 重新实现该协议
    - 摒弃了以前依赖于 MFC Sockets 相关类的命令实现 
    - 在 Windows 之外，支持 Linux
    - 支持 64 bit 操作系统， 修复微妙的错误(Bugs)
- 通过 C++/CLI 支持 .NET 绑定， 供 C# / PowerShell / IronPython 等语言调用
- 通过 Boost.Python 支持 Python 调用 
- 增加新的命令字及数据格式, 以传递新的 HPP 机型拥有更多注塑周期实时信息

本代码大量使用使用 [Boost][#BOOST] 库，如 [Boost.Asio][#ASIO], [Boost.Spirit][#SPIRIT]， [Boost.PP][#PP]，[Boost.Python][#PYTHON] 等，以及模板元编程（metaprogramming）等高级现代 C++ 技术及库。

构建工具采用 CMake.
因为 Hostlink 协议不是开放协议，此代码未能开源。

[#BOOST]: http://www.boost.org/
[#ASIO]: http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html
[#SPIRIT]: http://boost-spirit.com/home/
[#PP]: http://www.boost.org/doc/libs/1_63_0/libs/preprocessor/doc/index.html
[#PYTHON]: http://www.boost.org/doc/libs/1_63_0/libs/python/doc/html/index.html



## <a id="header-2-69" class="md-header-anchor"></a>Console3<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2012 - 2016 &middot; C++, Direct2D, DirectWrite__ 



该项目基于开源工具[Console 2][#CONSOLE2]定制

- 修复众多错误
- 重新以 [Direct2D][#]/[DirectWrite][#DW] 替代原基于 GDI 的 UI 层，以提高性能和视觉效果
- 添加便利性功能，比如 集成常用 Husky 开发工具与系统变量设置等

该工具在 Husky 上海研发团队广泛长期使用。


[#CONSOLE2]: http://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx
[#D2D]: https://msdn.microsoft.com/zh-cn/library/dd370990(v=vs.85).aspx
[#DW]: https://msdn.microsoft.com/zh-cn/library/windows/desktop/dd368038(v=vs.85).aspx



## <a id="header-2-70" class="md-header-anchor"></a>Beckhoff TwinCAT ADS 通讯协议<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016.07 - 2016.12 &middot; 软件设计__ 



基于开源的 Beckhoff ADS 协议跨平台实现， 加入了 TwinCAT 2 的支持。

- [link][#ADS]
[#ADS]: https://github.com/rockonedege/ADS


## <a id="header-2-71" class="md-header-anchor"></a>Multi-Ping<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2016.07 &middot; 软件设计, 网络程序设计__ 



在集成 Altanium 热流道控制器的项目中，一项设计目标是控制器被接入注塑机的瞬间，

- 自动探测检测到， 并获迅速或得其 APIPA 地址

使用 ping 应用程序 和 .NET库不能达到实时性要求（耗时 **10s - 40+s**），并且导致大量系统资源（比如内存 **700MB**）消耗, 以致程序崩溃。

我分析其原因可能是过多的 .NET 库封装开销所致， 以 C++/Boost.Asio 从 socket 层异步实现相似功能, 达到 **2s** 完成检测，内存消耗 **5MB** 左右。

随后以此算法封装成
- .NET 组件供 Polaris HMI 集成调用
- 独立应用程序，供车间测试工程师，现场服务工程师调试( Troubleshooting )用

## <a id="header-2-72" class="md-header-anchor"></a>Porting Embedded Operating Systems,<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005.04 - 2005.09__ 



Based on ARM7 ST7 CPU, meant to showcase the usages, read/write performance specifically, of ST's M45/M25 SPI flashes in embedded apparatuses.

Ported uCLinux and uc/OS-II.

Wrote code in Assembly and C with RealView Development Suite/Keil.


## <a id="header-2-73" class="md-header-anchor"></a>Driver and Application Demos for Flash Memories<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2003.12 - 2004.10__ 



Designed an embedded system for demo & verification of SPI interfaced Flash/EEPROM chips.

- used ST's own MCU chip
- designed PCBs with Altium
- wrote code in C language
  - portable and reusable driver code in C language
    - published on ST's website for download
    - SPI flash & EEPROM chips, e.g. M25Pxx code flash memories/ M45PE data flash memories
    - Firmware Hub(FWH) and Low Pin Count(LPC) chips, e.g M50xx flash memory 

  - showcased driver code with an application demos rotating quotes from _Albert Camus_ on a LCD display, of which my favorite one was

  > Don't walk in front of me;
  > I may not follow.
  > Don't walk behind me;
  > I may not lead.
  > Just walk beside me and be my friend.

- authored&published corresponding application notes, in English, explaining the rationale and usage of above application



## <a id="header-2-74" class="md-header-anchor"></a>Memory Competence Center(MMCC) Project Tracking System<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2004.02 - 2004.06__ 



A Project tracking tool to synchronize projects

- between MMCCs at Shanghai and Prague, Czech Republic.
- between MMCC Shanghai and Divisions in France/Italy.

- written in C++, MFC/STL/BOOST.
- ADO + Access as for local database
- XML import/export remote synchronization



## <a id="header-2-75" class="md-header-anchor"></a>Serial Flash Programmer Tool Kit<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2003.09 - 2003.12__ 



In line with Company’s effort to provide more accessible e-tools for the mass market, this kit was to facilitate the  usage of ST memory products, supporting SPI, I2C Initially. Specifically,

- testing chips with all supported page/sector read, program and erase commands
- free in-production usage of chip programming in small volumes(e.g. R&D designers at Intel Israel)

It consists of two parts:

- PC-based GUI software
  - Windows 9x, 2K, XP
- a PCB board running a ST7 based embedded system
  - connection to PC through the parallel port (LTP) or USB.

I took over from and re-designed the whole system reference a legacy half-done tool written in VB6, including

- a full functional GUI in MFC/C++
- firmware code on the ST7 board
  - enabled the chip-specific USB buffering techniques
    - coded against the Data Transfer Co-processor(DTC), a 2nd processor handling USB data transfer in parallel to the main CPU
      - with its specific Assembly instruction
      - within 128-byte code space

Being the best tool in class, it was later licensed by my then-colleagues from ST to have started up a company named [Dediprog Inc.]( http://www.dediprog.com/), branded as SF100/SF200/SF300 series. I was the interim CTO during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards. 
According to [this manual](http://www.dediprog.com/save/84.pdf/to/dp_SF%20User%20Manual_EN_V6.7.pdf), this features stays largely the same as what I left.  

## <a id="header-2-76" class="md-header-anchor"></a>PC BIOS Flash 闪存芯片功能验证及演示平台<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2003.12 - 2004.03 &middot; 嵌入式软件设计, 电子电路设计__ 



为新推出的 M50 系列 PC BIOS 专用 Firmware Hub/Low Pin Count Flash 总线存储芯片, 提供功能验证平台, 及用户开发演示代码。包括: 

- 功能验证和代码演示的硬件平台; 
- 分成 2 层的驱动代码（C语言）. 
    - 顶层（协议层）: 提供 Firmware Hub/Low Pin Count 协议的封装, 以标准 C 编写, 独立于硬件设计. 
    - 底层（物理层）: 提供硬件平台相关的地址, 数据, 控制相关基本操作代码, 与框架.
 
与芯片设计工程师合作, 全部负责软、硬件设计


## <a id="header-2-77" class="md-header-anchor"></a>SPI FLASH 闪存芯片嵌入式系统应用包<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2004.05 - 2004.10 &middot; 嵌入式软件设计, 电子电路设计__ 



该应用包源于日常工作需求

- 验证 IC 样片
- 演示应用场景
- 提供免费可下载 C 语言通用驱动代码库, 用于读写寄存器, 传输数据

本人负责方案设计以及全部硬件与软件设计

- 采用 [Protel/Altium][#ALTIUM] 设计了采用 uPSD3300 MCU 的嵌入式应用开发板硬件
    - 该应用也因此同时作为为即将推出的SoC(System On Chip)产品 uPSD3300提供开发包,
- 编写调试了通过 SPI 接口访问 M25/M45P(E) 系列程序/数据存储芯片的通用驱动库
- 编写了典型应用
    - 演示字符集存储, 可用于公共场所公告牌显示
    - 音频存储等应用, 可用于 MP3 播放器设计。 
- 编写英文应用指南( Application Notes ), 详细解释全部设计细节

全部软代码, 及应用指南( Application Notes )发布在 ST 官网上共供用户免费下载。

[#ALTIUM]: http://www.altium.com/


## <a id="header-2-78" class="md-header-anchor"></a>SPI Flash 闪存芯片编程器<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2003.09 - 2005.11 &middot; 嵌入式软件设计, 桌面软件设计, 电子电路设计, 应用架构__ 



基于以前原有 VB6 的半成品原型，在理解需求和现有技术架构后， 完全重构重新设计。 包括

- 上位机 PC 端界面
    - GUI 图形界面，便于直观操作。
    - CLI 命令行界面，便于批处理与第 3 方集成
- USB 通讯
    - Windows 端 USB 驱动程序
    - MCU 端 USB 驱动程序
- SPI/I&sup2;C 通讯协议及 Flash 芯片命令的组织和解析

该套件 PC 端以 C++/Python 编写, MCU 端采用 ST7 以 C 及汇编语言编写。
该套件被 ST 时期的法国同事商业化，成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列。在其创业初期，以代理 CTO 身份帮助维护开发维护产品。 **在后来的闲聊中，得知  [Dediprog][#DP] 后续维护开发的工程师对其软件架构高度称赞**， 从 2005 年最初版本至今 10 多年来，支持的芯片种类大幅增加 ， 该架构一直维持无大改动。



## <a id="header-2-79" class="md-header-anchor"></a>嵌入式实时操作系统　uCLinux　移植<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005.04 - 2005.09 &middot; 嵌入式软件设计, 电子电路设计__ 



该项目用于展示如何从 M25Pxx code Flash芯片启动操作系统(Embedded OS), 采用了基于 ARM 内核的 ST MCU 处理器。

采用 C 语言及汇编语言（Assembly）。


## <a id="header-2-80" class="md-header-anchor"></a>嵌入式实时操作系统　uCOS/II　移植<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2005.04 - 2005.09 &middot; 嵌入式软件设计, 电子电路设计__ 



该项目用于展示如何从 M25Pxx code Flash芯片启动操作系统(Embedded OS), 采用了基于 ARM 内核的 ST MCU 处理器。

采用 C 语言及汇编语言（Assembly）。


## <a id="header-2-81" class="md-header-anchor"></a>Anatidae　项目管理系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

__2004.02 - 2004.06 &middot; 软件架构, 软件设计__ 



日常项目跟踪软件, 用以同步上海　MMCC　内部, 以及与法国 Serial Flash Division/意大利 NAND Flash Division 之间的项目进度。
采用C++语言编写, 用到　MFC, STL, BOOST。采用 ADO + Access 构建本地数据库, 以 XML 同步异地数据。
 