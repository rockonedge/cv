from pathlib import Path



class Roots:
  CV = Path(__file__).absolute().parent.parent.parent 
  DB = CV / 'db'
  DB_RELEASE = DB / 'release'
  VUE_LANDING = CV / 'vue-landing'


  SRC_MD_FOLDER = CV / 'src'
  BY_FILTER_FOLDER = DB_RELEASE / 'by_filter'
