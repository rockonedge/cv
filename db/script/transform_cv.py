from pathlib import Path
import json
import re

from roots import Roots

import sys
sys.path.append(str(Roots.CV))

import logging

logger = logging.getLogger('cv')

info = logger.info
debug = logger.debug



info(f'cv root: {Roots.CV}')
root_zh_cv = Roots.CV / r'src/cv/zh-pieces'


for f in root_zh_cv.glob('*.md'):
  info(f)
  
# ff = json.loads(Path('/home/ttan/repo/private/cv/sandbox/datafy/dump/-cv.json').read_text(encoding='utf-8'))
# info(ff)

def copyfile(frm, to):
    import shutil

    shutil.copyfile(str(frm), str(to))
    info(f'{frm}  -> {to}')


def copy_to_vue_landing(name):

    # for debug
    copyfile(Roots.DB_RELEASE / f'{name}.json', 
    Roots.VUE_LANDING / f'src/assets/json/{name}.json')
      

def strip_list(l):

  begin = 0
  while not l[begin].strip():
    begin +=1

  end = -1
  while not l[end].strip():
    end -=1
  
  return l[begin:end + 1]


def to_s(l):
  return strip_list(l)
  return '\n'.join(l).strip()

def json_dump(data, name, to_root = Roots.DB_RELEASE):
  import json

  to = to_root /f'{name}.json'
  to.write_text(json.dumps(data, ensure_ascii=False), encoding='utf-8')

  info(f'dumped {to}')

def remove_em(s):
  return s.replace('*', '').strip()

def clean(s):
  import re
  return re.sub('[ a-zA-Z<>/&;*]', '', s)


def parse_experience(k, lines):
  def parse_k(k):
      k = k.replace('### ', '')
      title, duration = k.split(' ', 1)
      title, department = title.split('，', 1)
      start, end = duration.split('-', 1)

      return dict(
        title=title, 
        department = department,
        start = clean(start),
        end = clean(end))
  e = parse_k(k)



  def parse_body(s):
    from collections import defaultdict
    body = defaultdict(list)

    title = ''
    for line in strip_list(s):
      if line.startswith('#### '):
        if '主要职责' in line: 
          title = 'responsibility'
        elif  '主要技术内容' in line: 
          title = 'technology'
        elif '部分项' in line: 
          title = 'projects'
        elif '奖励' in line: 
          title = 'awards'          
        else: 
          title = line
      else:
        body[title].append(line)

    return body  

  def parse_v(v):
    info(v[0])
    title, department = v[0].split('，', 1)
    report_to = v[2].split('：', 1)[-1]
    employer = remove_em(v[4].strip())
    e_name, e_link = employer[1:-1].split('](', 1)

    return  dict(
      body = parse_body(v[5:]), 
      report_to=remove_em(report_to),
      employer=dict(name=e_name, link=e_link),
      title_en=remove_em(title), 
      department_en = remove_em(department))

  return dict(parse_v(lines), **e)

def to_sections(content):

  content = re.sub(r'^---$.+^---$', '',  content, flags=re.MULTILINE|re.DOTALL).strip()
  to_sections = {}
  title = ''
  lines = content.split('\n')
  for line in lines[1:]:
    line = line.rstrip()
    if re.search(r'^#{1,3} ', line):
      title = line.replace('*', '')
      to_sections[title] = []
    elif to_sections:
      to_sections[title].append(line)
    else:
      info(f'skipped {line}')
      # to_sections['headline'] = line

  to_sections['headline'] = lines[0]

  return to_sections

def run(md):
  r = {}
  ee = []
  cv = to_sections(md.read_text(encoding='utf-8'))
  for k,v in cv.items():
    # info(f"if k.startswith('{k}')")
    if k.startswith('### '):
      ee.append(parse_experience(k, v))
      json_dump(ee, 'employment')  

      copy_to_vue_landing('employment')

    if k.startswith('# 谭 智'):
      r['summary'] = to_s(v)
    if k.startswith('## 求职目标'):
      r['objective'] = to_s(v)
    if k.startswith('## 教育'):
      r['education'] = to_s(v)
    if k.startswith('## 语言能力'):
      r['language'] = to_s(v)
    if k.startswith('## 培训'):
      r['training'] = to_s(v)
    if k.startswith('## 其它'):
      r['other'] = to_s(v)
    if k.startswith('headline'):
      r['headline'] = v
  r['employment'] = ee
  json_dump(r, md.name)  
  json_dump(r, 'cv')  

  copy_to_vue_landing('cv')

def main():

  run(root_zh_cv / 'cv-zh-mini.md')
  run(root_zh_cv / 'cv-zh.md')

if __name__ == '__main__':
  main()