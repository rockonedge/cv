from pathlib import Path
import json
import re

from roots import Roots

import sys
sys.path.append(str(Roots.CV))

import logging
import enable_logging

logger = logging.getLogger('cv')

info = logger.info
debug = logger.debug

from collections import defaultdict


Roots.CV = Path(__file__).absolute().parent.parent.parent # i.e. /cv
root_en_cv = Roots.CV / r'temp/plain/release/md/'

def strip_list(l):

  begin = 0
  while not l[begin].strip():
    begin +=1

  end = -1
  while not l[end].strip():
    end -=1
  
  return l[begin:end + 1]


def to_s(l):
  return strip_list(l)
  return '\n'.join(l).strip()

def json_dump(data, name, to_root = Roots.DB_RELEASE):
  import json

  to = to_root /f'{name}.json'
  to.write_text(json.dumps(data, ensure_ascii=False), encoding='utf-8')

  info(f'dumped {to}')


def remove_em(s):
  return s.replace('*', '').strip()

def clean(s):
  import re
  return re.sub('[ a-zA-Z<>&;*]', '', s)




def parse_experience(k, lines):
  def parse_k(k):
    print(k)
    k = k.replace('### ', '')
    print(k)
    title, department = k.split(',', 1)

    return dict(title=title, department = department)

  e = parse_k(k)

  def parse_v(v):
    first_line = 0
    while not v[first_line].strip():
      first_line += 1

    report_to = v[first_line + 1].replace('Reported to ','')
    print(v[first_line])
    employer, duration = v[first_line].split(',', 1)
    start, end = duration.split('-', 1)
    e_name, e_link = remove_em(employer)[1:-1].split('][', 1)

    return  dict(
      body = to_s(v[4:]),       
      start = clean(start),
      end = clean(end),
      report_to=remove_em(report_to),
      employer=dict(name=e_name, link=e_link),
      )

  return dict(parse_v(lines), **e)

def to_sections(content):

  content = re.sub(r'^---$.+^---$', '',  content, flags=re.MULTILINE|re.DOTALL).strip()
  to_sections = {}
  title = ''
  lines = content.split('\n')
  for line in lines[1:]:
    if re.search(r'^#{1,3} ', line):
      title = line.replace('*', '')
      to_sections[title] = []
    elif to_sections:
      to_sections[title].append(line)
    else:
      print(f'skipped {line}')
      # to_sections['headline'] = line

  to_sections['headline'] = lines[0]
  return to_sections

def run(md):
  r = {}
  ee = []
  cv = to_sections(md.read_text(encoding='utf-8'))
  json_dump(cv, 'en-cv')  
  
  employment = defaultdict(list) 


  for k,v in cv.items():
    # print(f"if k.startswith('{k}')")
    if k.startswith('### '):
      ee.append(parse_experience(k, v))
      json_dump(ee, 'en-parse_experience')  
      employment['employment'].append(ee)
    if k.startswith('## Summary'):
      r['summary'] = handle_summary(v)
    if k.startswith('## Objective'):
      r['objective'] = to_s(v)
    if k.startswith('## Education'):
      # r['education'] = to_s(v)
      r['education'] = handle_education(v) #to_s(v)
    if k.startswith('## Language'):
      r['languages'] = [s.replace('- ', '') for s in v if s]
    if k.startswith('## Skills'):
      r['skills'] = handle_skills(v) #to_s(v)
    if k.startswith('## Trainings'):
      r['training'] = to_s(v)
    if k.startswith('## Additional Information'):
      r['other'] = to_s(v)
    if k.startswith('## Appendix'):
      r['appendix'] = to_s(v)
    if k.startswith('headline'):
      r['headline'] = handle_headline(v)
  r['employment'] = ee
  json_dump(r, md.name)  


def handle_education(v):
  curricula = []

  i = 0
  for i in [1, 5]:
    degree = v[i + 0].replace('**', '').replace('- ', '').replace('_', '')
    major, duration = v[i + 1].replace('- ', '').replace('**', '').split(',')
    university = v[i+2].replace('- ', '')
    
    curricula.append(dict(
      degree=degree.strip(), 
      major=major.strip(), 
      university= university.strip(), 
      duration=duration.strip()
        ))

  remove_prefix = lambda x: x.replace('  - ', '').strip()
  papers = [remove_prefix(x) for x in v[12:14]]
  awards = [remove_prefix(x) for x in v[17:18]]
  social = [remove_prefix(x) for x in v[18:21]]


  return dict(curricula=curricula,
          papers=papers, 
          awards=awards, 
          social=social)


def handle_skills(v):
  skills = defaultdict(list)
  for line in v:
    if line.startswith('- '):
      key = line[2:]
    else:
      if line.strip():
        skills[key].append(line.strip()[2:].replace('**',''))   
  
  return skills

def handle_summary(v):
  summary = defaultdict(list)
  for line in v:
    # print(line)
    if line.startswith('**'):
      key = line[2:-2]
    else:
      if line.strip():
        summary[key].append(line.strip()[2:].replace('**',''))
  return summary



def handle_headline(s):
  s = s.replace('*', '').replace('&middot;', '|')
  s = s.split('|')

  return [i.strip() for i in s]
def main():

  run(root_en_cv / 'full-cv-en.md')
  run(root_en_cv / 'mini-cv-en.md')
  generate_md()

def keywords_join(lines):
  return ' &middot; '.join(lines)

def lines_join(lines):
  return '\n'.join([f'- {line}' for line in lines])

def generate_md():
  import json

  for to in [
  # 'mini-cv-en.md.json',
  'full-cv-en.md.json']:

    s = Path(Roots.DB_RELEASE / to).read_text(encoding='utf-8')
    en_cv = json.loads(s)

    headline = f'{keywords_join(en_cv["headline"])}'
    def summary():
      s = []
      for k,v in en_cv["summary"].items():
        s.append(f'- {k}')
        for vv in v:
          s.append(f'  - {vv}')
      return '\n'.join(s)

    languages = lines_join(en_cv['languages'])
    md = '\n\n'.join([headline, summary(), languages])
    out = Path(Roots.DB_RELEASE / f'gen-{to}.md')
    out.write_text(md, encoding='utf-8')


if __name__ == '__main__':
  enable_logging.init()
  
  main()