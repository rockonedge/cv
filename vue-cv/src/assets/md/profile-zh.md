---
banner:
  brand: '<a class=''navbar-brand'' href=''#top''>简历</a> <a class=''navbar-brand''
    href=''#profile-zh-1''>项目详情</a> '
  lastupdate: <a class="btn-link" href="https://tslides.netlify.com/cv">SLIDES</a>
    &middot; <a class="btn-link" href="./pdf/profile-zh.pdf">PDF</a> &middot; 2018-01-11
  nav: <li class='nav-item'><a class='nav-link' href='#profile-zh-1'>赫斯基</a></li>
    <li class='nav-item'><a class='nav-link' href='#profile-zh-2'>意法半导体</a></li> <li
    class='nav-item'><a class='nav-link' href='#profile-zh-3'>施耐德电气</a></li> <li class='nav-item'><a
    class='nav-link' href='#profile-zh-4'>哈尔滨工业大学</a></li> <li class='nav-item'><a
    class='nav-link' href='#profile-zh-5'>业余时间</a></li>
  time: 2018-01-11 17:56:05.046189
  title: '简历 项目详情 '

---
<div id="md-toc">
- [**谭 智**](#header-1-1)
  - [**求职目标**](#header-2-2)
  - [**职业经历**](#header-2-3)
    - [部门主管/创立者，全球研发工程部 &middot; <small>2005.11 - 2016.12</small>](#header-3-4)
      - [主要职责](#header-4-5)
      - [主要技术内容](#header-4-6)
      - [部分项目](#header-4-7)
      - [奖励](#header-4-8)
    - [高级应用工程师/代理主管，存储器创新应用中心 &middot; <small>2003.9 - 2005.11</small>](#header-3-9)
      - [部分项目](#header-4-10)
    - [电子设计工程师，中国研发中心 &middot; <small>2002.4 - 2003.8</small>](#header-3-11)
      - [部分项目](#header-4-12)
    - [科研人员，哈尔滨工业大学车辆电器研究所 &middot; <small>1998.4 - 2002.3</small>](#header-3-13)
      - [部分项目](#header-4-14)
  - [**教育**](#header-2-15)
  - [**语言能力**](#header-2-16)
  - [**培训**](#header-2-17)
  - [**其它**](#header-2-18)
- [category: cv](#header-1-19)
  - [**附录**](#header-2-20)
- [赫斯基注塑系统有限公司](#header-1-21)
  - [筹建上海注塑机系统生产工厂](#header-2-22)
  - [筹建全球研发工程团队](#header-2-23)
  - [人机界面 (HMI) 开发](#header-2-24)
  - [PLC 开发](#header-2-25)
  - [注塑机工厂生产管理系统开发](#header-2-26)
  - [HyperSync 工业 4.0 特性开发](#header-2-27)
  - [统计过程控制（SPC）及 统计质量控制 （SQC） 模块](#header-2-28)
  - [多语言翻译管理系统 (Website)](#header-2-29)
  - [收购 Moldflow 制造部门产品整合](#header-2-30)
  - [Celltrack 工厂过程监控系统](#header-2-31)
  - [Shotscope 工厂过程监控系统](#header-2-32)
  - [热流道温度控制器 Matrix](#header-2-33)
  - [敏捷（Agile）开发流程](#header-2-34)
  - [全球新型注塑系统推出(Product Launch Trip)支持](#header-2-35)
  - [项目进度看板网站应用 Whiteboard](#header-2-36)
  - [上海注塑机生产工厂组建](#header-2-37)
  - [Polaris 注塑机实时控制系统](#header-2-38)
  - [Polaris HMI .NET 架构与实现](#header-2-39)
  - [Shotscope NX](#header-2-40)
  - [嵌入式模具无线自动识别 IoT 解决方案](#header-2-41)
  - [HyMET HeatLogger](#header-2-42)
  - [多语言翻译管理数据库(HkResource)](#header-2-43)
  - [HyperSync-Altanium 工业4.0集成](#header-2-44)
  - [Altanium 远程桌面](#header-2-45)
  - [扁鹊（Bianque）云生产管理系统](#header-2-46)
- [意法半导体](#header-1-47)
  - [BIOS Flash 闪存芯片功能验证及演示平台](#header-2-48)
  - [嵌入式系统 SPI FLASH 闪存芯片应用包](#header-2-49)
  - [SPI Flash 闪存芯片编程器](#header-2-50)
  - [嵌入式实时操作系统　uCLinux　移植](#header-2-51)
  - [嵌入式实时操作系统　uCOS/II　移植](#header-2-52)
  - [Anatidae　项目管理系统](#header-2-53)
- [施耐德电气](#header-1-54)
  - [紧急备用电源自动切换系统(ATS)](#header-2-55)
  - [空气断路器国产化设计](#header-2-56)
  - [工业 LED 指示报警灯](#header-2-57)
  - [恒温加热箱](#header-2-58)
  - [开关电源国产化](#header-2-59)
- [哈尔滨工业大学](#header-1-60)
  - [铁道车辆用电器综合测试系统](#header-2-61)
  - [电子镇流器综合测试系统](#header-2-62)
- [业余时间](#header-1-63)
  - [Markdown CV](#header-2-64)
  - [2D Water Effect in WTL](#header-2-65)
  - [A Literal Converter for Integers](#header-2-66)
  - [TwinCAT TSM 查看器](#header-2-67)
  - [HostLink TCP 注塑机通讯协议](#header-2-68)
  - [Console3](#header-2-69)
  - [Beckhoff TwinCAT ADS 通讯协议](#header-2-70)
  - [Multi-Ping](#header-2-71)
</div>
<hr id="end-of-toc" />







<small>**(+86)135-248-26926 &middot; [tom.tan@live.com](mailto:tom.tan@live.com) &middot; (+86) 135-248-26926 &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **研发管理 &middot; 工业软件 &middot; 固件 &middot; 硬件 &middot; 云计算 &middot; 数据科学 &middot; IoT**</small>

# <a id="header-1-1" class="md-header-anchor"></a>**谭 智**<a class="back-to-top-link" href="#top">&#x27B6;</a>


>- 创建全新全球研发团队，并领导 11+ 年
    - 成功完成众多全球协作开发项目，成绩备受赞誉
    - 善于促进团队合作与成长，富于责任感，感染力


>- 专业，高效的职业素质
    - 工作自觉性高与责任感强，崇尚以身作则
    - 善于多文化环境下沟通合作
    - 富有远见, 敏锐的技术敏感性, 渴望挑战


>- 18+ 年电气工程工业自动化，计算机软件/固件/硬件项目经验
    - 15+ 年供职于跨国公司研发部门
    - 自动化测量，工业实时驱动控制，半导体电子软/硬件系统研发
    - 熟悉电气/电子设计( Schematic & PCB ) 调试,仿真
    - 熟悉 Windows/Linux, 包括桌面，服务器，嵌入式以及 Azure 云计算软件开发
    - 熟练使用 C++，Python，C#，JavaScript, Go, Matlab 等语言
    - 长期紧密支持生产，销售，服务部门


>- 跨国高科技公司成功创业经验
>- [联合国儿童基金( UNICEF )](http://www.unicef.cn)定期捐赠者

## <a id="header-2-2" class="md-header-anchor"></a>**求职目标**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- 研发总监／高级经理
    - 团队创建，管理
    - 工业4.0/工业互联网（IIoT）/物联网 （IoT）研发
    - 云端，桌面，嵌入式/移动设备软件研发


## <a id="header-2-3" class="md-header-anchor"></a>**职业经历**<a class="back-to-top-link" href="#top">&#x27B6;</a>


### <a id="header-3-4" class="md-header-anchor"></a>部门主管/创立者，全球研发工程部 &middot; <small>2005.11 - 2016.12</small><a class="back-to-top-link" href="#top">&#x27B6;</a>

**Team Leader/Founder，Global Development Engineering Department**

**汇报对象：全球研发工程部总监， 加拿大总部**

**[赫斯基注塑系统（上海）有限公司][#HUSKY]**


帮助建立了上海注塑机工厂， 在一年左右的时间里从零开始成功组建，生产， 装配， 测试所需的软件基础设施和工程师团队， 保证了在上海工厂的制造能力。随后，建立了赫斯基唯一的海外研发工程团队。通过电子邮件，电话视频会议以及人员互访与加拿大总部的研发团队同步协作，对全球提供技术支持达 10 余年。

#### <a id="header-4-5" class="md-header-anchor"></a>主要职责<a class="back-to-top-link" href="#top">&#x27B6;</a>


作为创始人和本地区最高部门领导， 直接向总部研发总监和VP负责，受到充分信任授权，享有一切本地团队事务最终决定权， 日常职责包括

-  组建上海研发团队，承担全球合作开发。
-  管理上海研发团队日常运作，编制与控制预算开支，团队建设与人才培养等。
-  与全球其他各工程开发团队主管共同编制，协调开发计划与进度，设计原则，工程标准与质量。
-  领导团队开发与维护注塑机控制系统(MES)，工厂监控（SCADA）系统等
-  为生产制造部门开发，维护生产系统软件（Production System）以维持加拿大，卢森堡，美国，上海各地工厂的日常生产运行。
-  与市场，销售，客服部门合作为其提供技术支持，并根据反馈和建议研发或改进工业控制软件。
-  评估和引进新软件开发技术理念和方法，优化自动化控制系统。
-  探索实践跨国团队协作（Global Engineering）的高效管理方式和方法。

**因团队在项目质量，团队协作上表现杰出，多次应邀分享跨国工程团队组织和领导经验。**

#### <a id="header-4-6" class="md-header-anchor"></a>主要技术内容<a class="back-to-top-link" href="#top">&#x27B6;</a>


过去11年里， 率领上海团队启动或参与各类系统研发。 

- 从 2014年左右，开始结合亚马逊 AWS 和微软 Windows Azure 研究实施

    - 云计算 / 工业4.0 / 物联网（IoT）解决方案， 比如 AWS IoT 和 Azure IoT Hub。
    - 数据挖掘 与机器学习(基于 Python / R)
    - 虚拟化

- 主要产品包括

    - 过去的十年推出的所有**注塑机**，
    - 部分热流道**控制器**，
    - **系统整合**解决方案等

- 主要技术包括

    - 人机交互（HMI）

        包括图形界面，实时/历史数据库，权限管理，SPC/SQC算法等。

    - 机器设备监控，与运动控制(含机器人)

        基于**工业互联网协议 EtherCAT** 和 Beckhoff/TWinCAT 控制器的温度采集，压力感应， 伺服电机驱动，实时运动控制，生产监控（SCADA）开发相关控制驱动软件等。

    - 企业生产监控系统(SCADA/MES)

        专注于注塑行业，监控采集企业单车间或跨地域工厂的所有机器（赫斯基和非赫斯基）运行，实时采集与控制周期参数，以及工班人员，能源消耗，物料分配安排， 报表生成等, 通过各种标准软件协议和硬件设备, 如 OPC(OLE for Process Control) 等，架起机器*生产车间* 与企业上层 IT 系统 如 ERP 等第三方系统的桥梁。

    - 第三方系统集成

        通过各种工业接口， 如Digital I/O, RS422/RS232， Ethernet 等集成辅助设备， 比如， ABB 机器人（如 ABB），Plastic Systems 树脂干燥机， 除湿器等。

    - 也视情形涉及电气系统调试与机械设计辅助任务。


- 目标平台包括
    Linux 和 Windows 平台的传统应用， 如
    - 嵌入式(Embedded Systems)
    - 移动设备(Mobile Platforms)
    - 桌面(Desktop Workstations)
    - 服务器(Web Servers)
    - Amazon AWS 和 Microsoft Azure 云平台 


- 主要使用语言包括
    - C++
    - C#
    - Visual Basic
    - Python
    - Ruby
    - JavaScript
    - PLC/IEC61131 programming languages 等。

 
- 工业总线接口/协议包括
    - EtherCAT/Realtime Ethernet
    - Profibus
    - Sercos
    - Euromap
    - CAN
    - SPI
    - USB
    - RS232/RS422/RS485 等。


#### <a id="header-4-7" class="md-header-anchor"></a>部分项目<a class="back-to-top-link" href="#top">&#x27B6;</a>


此处仅简单列举。[**详情参阅在线描述**](https://ttan.netlify.com/projects-zh)

- 上海工厂启动
    - 引进了拿大总部工厂的生产支持系统 （Polaris）及流程
    - 招聘，培训了首批生产支持 （装配和测试）工程团队
- 建立了第一个全球研发工程团队， 全部由硕士研究生以上学历组成。
- 领导开发用于所有赫斯基注塑机的 Polaris 软件平台， 适用于 Hylectric， HyPET， HyPET HPP， HyCAP， HyperSync， Barrier等机型， 包括
    - Polaris HMI（人机界面）， 
        - 维护性开发基于 COM/VB6 平台HMI
        - 为新一代的注塑机重新架构和实现了基于 .NET/C# 平台的HMI
        - 十多年来，保持上述平台按季度/半年定期发布
    - Polaris PLC，基于 TwinCAT/CODESYS 技术的 PLC 编程以实现注射成型的实时控制相关算法，例如
        - 伺服驱动(Servo)
        - 机器人控制等。
    - Polaris Production System， 此系统是内部各部门 （研发， 生产，售后支持服务等）都协同公作的公共 SDLC（Software Development Life Cycle） 平台
- 领导 HyperSync 注塑机系列工业 4.0 相关特性，获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。        
- 领导开发统计过程控制（SPC）及 统计质量控制 （SQC） 模块
- 领导开发多语言翻译管理系统 （Translation Website） 的开发，简化软件开发中的全球化支持（22+语言）
- 领导开发基于 RuBee（IEEE 1902.1） 无线通讯技术的嵌入式模具无线自动识别(Mold ID)工业 IoT 解决方案
- 发起扁鹊（Bianque）项目，融合 Azure IoT Hub， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术。
- 领导[收购整合 Moldflow 制造部门][#BUYMF]的法国与美国团队资产， 并后续支持既有客户
    - 基于 Windows Server 采用 C++/Delphi/Java 开发的工厂过程监控系统系统Celltrack，Shotscope
    - 基于 Linux/Fedora 采用 C++/QT/MySQL 开发的热流道温度控制器 Matrix
- 领导开发 Shotscope NX， 一套基于 web 技术的过程和生产监控自动化实时解决方案。
- 引入 SCRUM/Test-Driven Development（TDD） 敏捷（Agile）开发流程，把源代码控制从 VSS 迁移到 TFS。        
- 领导上海团队成员参与了中国区，越南，墨西哥等地的全球 PLT 支持。 
    - PLT 是一个临时由研发部门设计人员或技术专家组建（因为此时生产和售前/后部门还不具备相关产品知识）， 为尚未正式推出的新机型早期客户提供现场安装调试服务的团队。是 Husky 推出新机型的传统程序。    
- 业余时间开发了看板网站应用 Whiteboard，基于 Python/Django， C#/ASP.NET/RESTful Service/WebAPI 和 JavaScript/Dojo Toolkit，Apache Cordova， 综合 Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 UNC 映射盘等数据源信息
    - 便利了团队领导随时随地在线查看任何项目和状态
    - 在诸如
        - 每日站立会议（daily standup meeting）
        - 部门周会（Weekly team meeting）
        - 项目里程碑会议（milestone meeting）
    等日常或临时会议中简化了与会成员背景介绍时间，迅速进入主题。   

#### <a id="header-4-8" class="md-header-anchor"></a>奖励<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **杰出贡献奖**， 2007 年
    - 由时任全球机器事业部副总裁（VP of Machines）签署， 以奖励对上海工厂成立作出的杰出贡献
- **优秀团队领导（排名全球名列前茅）**，  2011 年
    - 知名人力资源机构肯耐珂萨（Kenexa）受聘对 Husky 全球员工满意度调查匿名调查报告
- **5 年服务奖**， 2010 年
- **10 年服务奖**， 2015 年




### <a id="header-3-9" class="md-header-anchor"></a>高级应用工程师/代理主管，存储器创新应用中心 &middot; <small>2003.9 - 2005.11</small><a class="back-to-top-link" href="#top">&#x27B6;</a>

**Senior Application Engineer/Interim Team Leader，Memory Competence Center**

**汇报对象：部门经理（上海）/研发经理（法国）**

**[意法半导体（上海）有限公司][#ST]**


作为公司 R&D 力量在亚太地区的延伸，代表 Serial Non-Volatile Memory Division 负责整个亚太地区应用开发，为本地现场应用工程师 （Field Application Engineer）提供技术和方案支持。

作为此新成立部门最早期员工之一，代理主管职责，协助成立并领导本地 Serial Non-Volatile Memory & Embedded System  Solutions Team，

- 负责建立与维持与法国 R&D 部门的日常联系，培训安排等，
- 开发重心包括 
    - **Software/Firmware 开发**
    - **原理图与 PCB 电路板设计**

    具体而言

    - 协助 IC 设计师进行样片功能验证，包括信号质量，时序图等。
    - 基于MCU，DSP，ARM7/9 等 CPU 开发嵌入式应用参考设计 （包括软件代码编写与硬件验证平台），演示 EEPROM/FLASH 等的功能和应用场景。
        - 电路原理图设计与 PCB 布线制作，调试，演示主要功能和应用领域。
        - 编写，发布可移植，高性能 C 语言芯片 Firmware 驱动代码到公司全球网站
    - 用英文撰写，发布应用指南（Application Notes）到公司全球网站
    - 各类部门自用工具软件，脚本编写，采用 C++, C#, Python 等

#### <a id="header-4-10" class="md-header-anchor"></a>部分项目<a class="back-to-top-link" href="#top">&#x27B6;</a>


- 基于 uPSD （Intel8032 内核）开发维护 3 个系列串行 Flash 存储芯片的参考设计
    - M50xx FWH/LPC 接口 BIOS 存储芯片
    - M25Pxx  SPI 接口代码存储芯片系列
    - M45PExx SPI 接口数据存储芯片系列


- 基于 PC 和 ST7 （ Motorola 内核）， USB 接口的 Flash 编程烧录器（Serial Flash Programmer）
    - 基于以前 VB6 的未完成原型，完全以C++/汇编从 PC 端到 MCU 端重新设计
    - 被 ST 时期的法国同事商业化，成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列，并邀请我协助维护开发至SF300。 
    - 根据[其官网上的用户手册][#DEDIMANUAL], 时至今日其架构和功能几乎没有什么大的变动。



- 移植嵌入式操作系统到 STR71x
    - uC/OS II
    - uCLinux


- 开发了 Anatidae 项目管理软件跟踪同步上海，意大利，法国，捷克之间的项目进度。
    - 基于 MFC/STL/BOOST，XML 与 ADO 
    - 约5万行 C++ 代码 

[#DEDIMANUAL]: http://www.dediprog.com/save/84.pdf/to/dp_SF%20User%20Manual_EN_V6.7.pdf
        
### <a id="header-3-11" class="md-header-anchor"></a>电子设计工程师，中国研发中心 &middot; <small>2002.4 - 2003.8</small><a class="back-to-top-link" href="#top">&#x27B6;</a>

**Electronic Design Engineer，China Design Center**

**汇报对象：中国研发中心经理（法国人，由总部研发中心委派）**

**[施耐德电气 （中国）投资有限公司][#SECI]**


作为独资公司的研发力量，依托研发中心实验室和本地区10多家合资工厂，进行
- 本地化产品研发
- 投产产品故障分析等高级技术支持
- 着重于工控自动化（ICA）产品的电子设计改进和创新，包括

    - 从全球其他市场引进产品和国产化设计
    - 中国市场特有的新产品原型设计
    - 为合资工厂提供故障分析高级技术支持
    - 根据 IEC/GB 相关标准对新产品 / 样品进行预试验，并起草新产品的检验/验收试验方案及标准
    - 提交各类所需实验数据， 协助合资工厂向主管机构（比如上海电科所）申请各类本地化准入认证
    - 为实验室设计开发，选购软/硬件设备/工具
    - 对实验室操作人员技术培训

#### <a id="header-4-12" class="md-header-anchor"></a>部分项目<a class="back-to-top-link" href="#top">&#x27B6;</a>


- 设计了自动应急电源切换系统的前后 3 套原型，填补市场上只有组合方案，没有集成产品的空白。 此系统用于医院手术室，重要的政府设施在紧急情况下主，备用电源之间自动切换
- 协同设计了基于 MCU 和 PID 算法的恒温试验控制箱，用于电路断路器/接触器/PLC 高温老化测试，提供恒定温度高达 75&deg;
- 为实验室开发了一套基于关系数据库的国产化试验管理软件，用于实验室数据分析，采用了 VB6/C++ 以 及 MATLAB
- 改进小型空气断路器 ASIC 电路设计
    - VC65
    - DPN Vigi
- 从波兰工厂引进开关电源的前期研究报告，包括
    - 设计了各类性能，可靠性实验，用以与本地品牌对比
    - 逆向工程本地品牌，完成成本分析
- Compact NS 断路器认证实验
- 工业用 LED 光报警灯设计

### <a id="header-3-13" class="md-header-anchor"></a>科研人员，哈尔滨工业大学车辆电器研究所 &middot; <small>1998.4 - 2002.3</small><a class="back-to-top-link" href="#top">&#x27B6;</a>

**Researcher，HIT Research Institute of Electrical Apparatus  **

**汇报对象：研究所所长/研究生导师**

**[哈尔滨工业大学][#HIT]**

这是由我的研究生导师成立并领导的校级研究所（实验室），与哈尔滨铁路局合作，依托全国“铁路电器现代化”战略，承接各类铁路车辆电器自动化研发项目。

我从大三自荐进入实验室开始，以及研究生院学习期间，一直到取得硕士学位毕业离校，大约 4 年间，一直参与实验室各类大小项目，承担各类职责。

**此段经历奠定了我迄今为止的专注于研究与开发（R&D）技术基础和不懈追求。**

我的本科及研究生学位论文均取材于期间负责的有关项目。

参与的研究方向和开发包括

- 电机与电器
    - 交直流电动机，发电机，变压器以及马达驱动 （Rockwell自动化旗下品牌）
    - 继电器，接触器和断路器
- 计算机控制系统
    - 直接数字控制系统 
    - 离散控制系统 
    - 现场总线控制系统 
- 实时控制及测量方法和电器可靠性(FMEA)
- 实时嵌入式的系统（RTOS）和嵌入式应用程序开发
- 各类电气电子接口电路与总线协议的研究
- 工业控制与自动化设备研究与开发
    - 基于工控机（IPC）和单片机（英特尔 MCS-51 系列 MCU）
    - 人机界面编写及数据实时采集，处理和管理软件开发，采用Visual Basic/C++
    - 驱动程序开发，采用C/C++/汇编
    - 硬件系统设的电气原理图及 PCB 设计，采用 Protel
    - 电气/电路仿真，采用 pSPICE, MATLAB/SIMULINK等
    - 机械设计（操作平台，凸轮设计），采用AutoCAD


#### <a id="header-4-14" class="md-header-anchor"></a>部分项目<a class="back-to-top-link" href="#top">&#x27B6;</a>


受哈尔滨铁路局委托，先后**独立**成功开发

- **电子镇流器自动化测试系统**
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 《低压电器》发表相关论文*《铁路客车电子镇流器逆变器综合试验台的研制》*
    

- **低压电器自动化测试系统**
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 经黑龙江省科委专家组技术评审鉴定“居于国内领先”。
    - 相关论文发表于*第八届国际电器可靠性会议*。
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 黑龙江省教育厅 科技进步一等奖， 2002 年 12 月
    - 黑龙江省科技厅 科技进步二等奖， 2003 年 5 月

**此两系统分别销售 10+ 套, 累计销售逾百万元人民币。**

从前期立项到后期交付，以及人员培训，一力承担了除前一项目的软件开发以外的全部系统架构及硬件设计。

软件设计

- 基于 Windows, 运行于工控机（IPC） 

硬件设计涉及

- ISA I/O，PCI A/D 等计算机板卡设计
- 电源及负载的二进制编码设计与控制
- EMI，谐波干扰处理等以及
- 试验台的机械设计。

## <a id="header-2-15" class="md-header-anchor"></a>**教育**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **硕士**
    - 毕业答辩成绩: **优异**
    - 1999 年 9 月 - 2002 年 3 月
    - 电气工程系，计算机与电气工程学院
    - [哈尔滨工业大学][#HIT]


- **学士**
    - 毕业答辩成绩: **优异**
    - 1995 年 9 月 - 1999 年 7 月
    - 电气工程系，计算机与电气工程学院
    - [哈尔滨工业大学][#HIT]


- **著作/论文**

    研究生攻读期间在国家级期刊与国际会议发表2 篇学术论文。

    - 翟国富, 谭智, 陈 伟. 铁路客车电子镇流器逆变器综合试验台的研制. 低压电器
    - 翟国富, 谭智等. 铁路客车断路器接触器性能及可靠性测试系统的研究. 第8届国际电器可靠性会议论文集
    - 翻译导师论文供外刊发表


- **荣誉**
    - 获奖者 （多次），人民奖学金，         1995 年 - 1999 年
    - 成员，电气工程和计算机学院 学生会，    1996 年 - 1997 年
    - 成员，哈尔滨工业大学 学生会，          1995 年 - 1996 年
    - 志愿者，第八届国际北方城市会议，哈尔滨，1998 年 1 月
    
    -  大学英语四级（CET4），  1996 年 7 月（大学一年级 下学期）
    -  大学英语六级（CET6），  1997 年 1 月（大学二年级 上学期）

    -  全国计算机等级二级，    1998 年 4 月

## <a id="header-2-16" class="md-header-anchor"></a>**语言能力**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- 汉语︰ 母语
- 英语︰ 流利

## <a id="header-2-17" class="md-header-anchor"></a>**培训**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **The 7 Habits of Highly Effective People Training Program**

    - 3 天，2008 年 3 月
    - 上海
    - 赫斯基注塑系统赞助
    - 由 [Franklin Covey Co.][#FRANKLIN] 培训


- **The 4 Disciplines of Execution: Manager Certification**

    - 3 天，2007 年 3 月
    - 上海
    - 赫斯基注塑系统赞助
    - 由 [Franklin Covey Co.][#FRANKLIN] 培训


- **Team Leadership development Training Program**

    - 一周，2006 年 1 月
    - 博尔顿 （Bolton），加拿大
    - 赫斯基注塑系统赞助
    - 由 [Workplace Competence International Limited](https://www.linkedin.com/company/674972) 培训


-  **Product training on Serial Non-volatile Memory Chips**
    - 2 周， 2004 年 5 月
    - Rousset，法国
    - 意法半导体公司赞助
    - 由[意法半导体公司][#ST]培训
    - 覆盖串行 EEPROM，SPI 串行闪存，BIOS Flash，非接触式存储器


-  **剑桥商务英语（BEC）︰ 中级 和 高级**
    - 每周六，2002 年 - 2003 年
    - 上海
    - 施耐德电气 （中国）投资有限公司赞助
    - 由 [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan) 培训


- **Creative Thinking and Problem Solving**，
    - 2 天，2002 年 7 月
    - 上海
    - 施耐德电气中国投资有限公司赞助
    - 由 [Key Consulting][#KEYCONSULTING] 培训


- **知识产权专利培训**，
    - 1 天，2002 年 6 月
    - 上海
    - 施耐德电气中国投资有限公司赞助
    - 由上海知识产权局某退休主任培训

[#FRANKLIN]: http://www.franklincovey.com
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/

## <a id="header-2-18" class="md-header-anchor"></a>**其它**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- 开源代码与社区参与

    - [Boost C++ Libraries](http://www.boost.org/)

      审阅 [Boost.Chrono][#BOOST_REVIEWER]，此库被收入 最新 C++ 标准库

    - [POCO C++ Libraries](http://pocoproject.org/)

      贡献[代码][#POCO_CONTRIBUTOR]

    - [The CodeProject](http://www.codeproject.com/)

      发表部分[文章及代码][#CP_AUTHOR]

    - [Github](https://github.com/rockonedege) 

      维护部分[开源代码][#GITHUB]

[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#GITHUB]: https://github.com/rockonedege

- **闲暇**
    - 家庭活动
    - 阅读
    - 跑步/半程马拉松
    - 骑行
    - 旅行


- **个人主页**

    -  [中文简历](https://ttan.netlify.com/cv-zh)(**本文档**)
    -  [中文项目经历](https://ttan.netlify.com/projects-zh)
    -  [英文简历](https://ttan.netlify.com/cv-en)
    -  [英文项目经历](https://ttan.netlify.com/projects-en)




[#HIT]: http://www.hit.edu.cn/
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#DP]: http://www.dediprog.com/


<hr class="page_break" id="full-cv-zh-1" />





﻿---
language: en
2: ['附录']
category: cv
---<a class="back-to-top-link" href="#top">&#x27B6;</a>

<hr class="page_break" />

## <a id="header-2-20" class="md-header-anchor"></a>**附录**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- ![](./pics/husky-contribution.jpg)
- ![](./pics/husky-leadership.jpg)
- ![](./pics/kenexa-1.jpg)
- ![](./pics/kenexa-2.jpg)
- ![](./pics/mbti.istj-2.jpg)
- ![](./pics/unicef.jpg)
- ![](./pics/shanghai-half-marathon-2008.jpg)


<hr class="page_break" id="profile-zh-1" />






# <a id="header-1-21" class="md-header-anchor"></a>赫斯基注塑系统有限公司<a class="back-to-top-link" href="#top">&#x27B6;</a>



## <a id="header-2-22" class="md-header-anchor"></a>筹建上海注塑机系统生产工厂<a class="back-to-top-link" href="#top">&#x27B6;</a>

2005-2006
团队筹建,团队管理,项目管理

为上海工厂建立生产支持系统（Polaris）及流程，与加拿大，卢森堡工厂数据对接同步。招聘，培训了首批生产支持 （装配和测试）工程团队

## <a id="header-2-23" class="md-header-anchor"></a>筹建全球研发工程团队<a class="back-to-top-link" href="#top">&#x27B6;</a>

2006
团队筹建,团队管理,项目管理

建立加拿大总部以外唯一一个全球研发工程团队， 全部由硕士研究生以上学历组成。

## <a id="header-2-24" class="md-header-anchor"></a>人机界面 (HMI) 开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

2006 - 2016
项目管理, 软件架构
领导开发用于所有赫斯基注塑机的 Polaris HMI 基础软件平台及各种衍生版本，适用于所有系列注塑机系统。

- 维护性开发基于 COM/VB6 平台HMI
- 重新架构和实现了新一代 HMI,基于 .NET/C# 平台的
- 十多年来，保持上述平台按季度/半年定期发布

## <a id="header-2-25" class="md-header-anchor"></a>PLC 开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

2006 - 2016
项目管理，软件开发
实时控制相关算法。 包括伺服驱动(Servo),机器人控制等运动控制，以及温度控制， 第三方设备通讯等。基于 IEC 61131 标准，及 TwinCAT/CODESYS 运行环境。

## <a id="header-2-26" class="md-header-anchor"></a>注塑机工厂生产管理系统开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

2006 - 2016
项目管理, 软件架构，软件开发
Polaris Production System， 此系统赫斯基注塑机控制系统软件开发，模拟，测试和发布的 DevOps 平台。也是车间内部各部门 （研发， 生产，售后支持服务等）的协同工作平台。

基于COM/Visual Basic, Ruby, C#/.NET等技术。

## <a id="header-2-27" class="md-header-anchor"></a>HyperSync 工业 4.0 特性开发<a class="back-to-top-link" href="#top">&#x27B6;</a>

2016
项目管理，软件架构，代码编写
基于 TwinCAT 实时工业互联网技术。获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。

## <a id="header-2-28" class="md-header-anchor"></a>统计过程控制（SPC）及 统计质量控制 （SQC） 模块<a class="back-to-top-link" href="#top">&#x27B6;</a>

2011
C++, 代码编写

## <a id="header-2-29" class="md-header-anchor"></a>多语言翻译管理系统 (Website)<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006.11- 2007.07**
**服务器软件设计, 网站设计**

一体化软件界面多语言开发管理系统，支持22+语言。支持数据库查询，外部翻译请求，审批，上下文场景管理，多语言资源文件生成管理等。基于C#, ASP.NET, SQL Server开发。

该项目由上海开发，部署，日常维护支持全球业务。

该网站的开发使得原来的人工过程自动化, 
- 申请新字符串翻译从原来大约 2 星期等待 减少到 0 阻塞实时异步, 并能
- 提供更多上下文信息用于提高翻译准确性。
还提供了其他查询管理高级新功能。
 
为了支持多达 22+ 种语言 HMI 显示, 开发人员在开发控制系统软件时涉及到任何字符串均需, 事先向专职人员申请并获得特定 ID 替代原字符串用于开法中, 以支持用户使用时界面语言动态翻译。



## <a id="header-2-30" class="md-header-anchor"></a>收购 Moldflow 制造部门产品整合<a class="back-to-top-link" href="#top">&#x27B6;</a>

2008
公司合并，技术融合

领导[收购整合 Moldflow 制造部门](http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit)的法国与美国团队资产， 并后续支持既有客户
如欧莱雅，利乐包装等。产品包括开发的工厂过程监控系统系统Celltrack，Shotscope 及热流道温度控制器 Matrix。

## <a id="header-2-31" class="md-header-anchor"></a>Celltrack 工厂过程监控系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

2008
公司合并，技术接受
基于 Windows Server 采用 Java。


 开发的工厂过程监控系统系统Celltrack，Shotscope
    - 基于 Linux/Fedora 采用 C++/QT/MySQL 开发的

## <a id="header-2-32" class="md-header-anchor"></a>Shotscope 工厂过程监控系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

2008
公司合并，技术融合
基于 Windows Server 采用 C++/Delphi 开发.

## <a id="header-2-33" class="md-header-anchor"></a>热流道温度控制器 Matrix<a class="back-to-top-link" href="#top">&#x27B6;</a>

2008
公司合并，技术融合
包括嵌入式电子硬件及基于 Linux/Fedora 采用 C++/QT/MySQL 开发软件 HMI.


## <a id="header-2-34" class="md-header-anchor"></a>敏捷（Agile）开发流程<a class="back-to-top-link" href="#top">&#x27B6;</a>

2006-2008

引入 SCRUM/Test-Driven Development（TDD） 敏捷（Agile）开发流程及工具，例如把源代码控制从 VSS 迁移到 TFS。   


## <a id="header-2-35" class="md-header-anchor"></a>全球新型注塑系统推出(Product Launch Trip)支持<a class="back-to-top-link" href="#top">&#x27B6;</a>

2011 - 2016
客户支持，项目管理，团队领导
领导上海团队参与客户全球工厂新型注塑系统安装，调试及交付，遍及中国，越南，墨西哥，澳大利亚等全球各地。客户包括可口可乐， 利乐包装，康师傅，哇哈哈， 养生堂等。 


## <a id="header-2-36" class="md-header-anchor"></a>项目进度看板网站应用 Whiteboard<a class="back-to-top-link" href="#top">&#x27B6;</a>

2011-2016
软件开发, 项目管理, 敏捷开发
基于 micro service 项目管理网站。


基于 Python/Django， C#/ASP.NET/RESTful Service/WebAPI 和 JavaScript/Dojo Toolkit，Apache Cordova，NodeJS 综合 Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 网络映射盘等数据源信息
    - 便利了团队及项目管理人员随时随地在线查看任何项目和状态
    - 在敏捷开发实践中
        - 每日站立会议（daily standup meeting）
        - 部门周会（Weekly team meeting）
        - 项目里程碑会议（milestone meeting）
    等日常或临时会议中简化了与会成员背景介绍时间，迅速进入主题。

## <a id="header-2-37" class="md-header-anchor"></a>上海注塑机生产工厂组建<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2015.11- 2017.01**

适逢赫斯基战略性扩大中国投入, 决定在此建立注塑机生产工厂， 并把亚太地区总部从香港迁至上海。虽然注塑行业对我而言是全新的领域。
加入公司立刻赴加拿大总部工作，并在第一年里， 频繁往返于两地，一半时间在加拿大，一半时间在上海, 一边接受高强度的相关的产品， 工具， 及工作流程培训， 一边组建培训团队，建立车间生产，组装及测试的软件流程。 

- 移植了庞大的生产软件系统(Polaris Production System)
    - 包括 500G+ 生产数据， 因数据太大，多次网络传输失败后， 我不得不带上硬盘大年三十从加拿大飞回上海。
    - 此系统严重加拿大工厂的本地 IT 环境， 不得不与两地 IT 部门紧密合作，并解决许多技术与非技术问题，在上海重建相似环境， 并与总部数据库连接同步，实现全球生产数据与流程的统一化管理
- 招聘组建了第一批软件，电气控制系统支持团队
    - 很多时候在不得不从加拿大通过电话面试

由于在此项目的贡献，获得杰出贡献奖， 由时任全球机器事业部副总裁（Global VP of Machines）签署颁发。

## <a id="header-2-38" class="md-header-anchor"></a>Polaris 注塑机实时控制系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005.12- 2016.12**

Polaris注塑机实时控制系统是一个基于工业控制计算机的注塑机操作系统（Injection Operating System）及与之配套的装机发布系统（Production Deployment System）。

操作系统（Injection Operating System）通过控制基于 Profibus，SERCOS，EtherCAT 等接口的电气系统，实现原料加热，注射，机器人控制等注塑成型周期过程的运动控制和自动化。此系统包括

- 人机界面( HMI ) 
    - 提供友好的人机界面，故障诊断，状态监控，参数调整等上层功能；
- 软 PLC(soft PLC) 组成:
    - 实现PID温度控制，各种电机运动控制算法和实时性等底层控制。

每季度发行一次基本版本升级，包含bug修复，新功能，以及与最新的机械和电气设计相对应的更动，并作为定制功能的最新平台。

装机发布系统（Production Deployment System）根据用户需求及每一台机器的机械与电气配置，选择相应的软件模块，把操作系统（Injection Operating System）定制安装到注塑机上。

项目职责：

- 在上海工厂的建立过程中，主导建立了控制软件的本地装机发布系统（Production Deployment System），使生产车间组装后装机测试成为可能，从而为2006年生产线顺利投产提供前提条件。
- 参与培训了第一批生产线控制软件安装测试工程师（Controls Test Engineer Team）。
- 领导维护本地装机发布系统（Production System）并支持车间生产，直至2007年建立专门的生产支持团队(Production Team)。
- 持续领导并参与此系统相关工作的一切开发，维护，季度升级工作等等，涉及

- C++, C#, VB, IEC61131 PLC 程序设计的开发维护
- Profibus, SERCOS, EtherCAT 接口硬件调试故障诊断
- 第三方辅助系统集成， 比如机器人(Robots)等。


## <a id="header-2-39" class="md-header-anchor"></a>Polaris HMI .NET 架构与实现<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006.06 - 2008.01**
**桌面软件设计, 第3方软件集成**
 
由于现有控制系统 HMI 已经使用多年

- 经过多年的修改维护，已经演变得性能降低(比如40秒软件启动时间)，众多临时特性的叠加也使用户操作复杂， 系统庞大而难以维护；
- 作为公司第一代迁移自 PLC 的 PC/softPLC 架构控制系统, 软件构架上不成熟而难以扩展；
- 大量代码基于 Visual Basic 6, 难于添加现代 UI 体验及功能
- 软件工程思想和开发工具的发展使得重构原有系统变得可能而且代价低廉。

该项目组织大量人员梳理现有功能, 基于.NET/C#, 重新架构和编写 HMI 的 UI 及 service 部分。

本人职责包括

- 阅读原系统代码及设计文档，分析，提取，重组设计需求，找出性能瓶颈
- 参与设计确认软件架构
- 技术培训讲解并领导上海团队确保 TDD/SCRUM 开发过程实施, 确保开发进度和代码质量。
- 编写部分代码
- 招聘工程师充实项目组

该应用部署于运行 Windows Embedded XP 的工控机( Industrial PC ), 技术涉及Windows Form, WPF, C#, C++/CLI, SQL Server等。


## <a id="header-2-40" class="md-header-anchor"></a>Shotscope NX<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2008.06 - 2015.09**
**SCADA, 网络,WCF**
一套基于 web 技术的过程和生产监控自动化实时解决方案。
 
整合现有 SmartLink, Shotscope, CellTrack 过程与生产监控系统软件和硬件，实现生产制造企业数字化管理，包括

- 基于 Window Server 的网站服务器软件，
- 基于工控机的数据采集器（Data Collector）控制软件
    - 适用于 Non-Husky 注塑机客户采集生产过程状态等数据

该系统

- 上端连接**企业上层 IT 系统**等第三方系统（ 如 ERP ）
- 下端连接机器运行状态（实时周期参数），能源消耗，物料分配与消耗， 工班安排等**实时生产车间数据** 
    - 对于 Husky 品牌注塑机，从其控制软件内建相关模块，以私有软件协议，获得专属详尽的生产细节数据，比如
        - Husky Polaris 注塑机实时控制系统 
        - Husky Altanium 热流道控制系统 
              
    - 对于 非 Husky 品牌第三方注塑机， 通过
        - 标准工业**软件协议**，如 [OPC][#OPC], 从第三方机器控制系统获取数据
        - Husky 专用数据采集器以 Side-by-Side 方式**硬件采集**数据

开发内容包括

- 服务器软件开发
- Polaris Polaris 注塑机实时控制系统相关 HMI 及数据接口开发 
- Data Collector 专属定制及工业标准(SPI, OPC, USB, RS422 等) 数据协议开发

利用 WCF, Silverlight, SQL Server 等C#/.NET技术系统，使其与其前身系统相比

- 更友好直观的操作界面与数据呈现， 报表生成能力， 
- 更广泛，分布式的监控能力。
    - 单车间
    - 全球跨地域车间
- 更强大的数据存储与处理能力

该产品按计划于 2009.6北美塑胶展（NPE Show 2009）前完成初步版本，作为主展产品之一，[在展会上取得巨大成功，并获得用户热烈欢迎][#NPE].

初期开发阶段项目职责：

- 领导协调上海团队与加拿大，卢森堡同事及印度外包公司开发的分工
- 与销售部门同事协作，转化用户需求（User Story）为可实现的技术特性(Feature)
- 参与制定阶段目标，划分可独立实现的单项任务(Work Item)，
- 制定以2星期为周期的迭代开发计划，并确保其实施，每一个Milestone能按计划达到
- 确保测试驱动开发（TDD）等开发模式和工程标准的实施
- 当项目方向不明确或出现争议时，做出决策，并推动项目前进


从产品推出后， 根据市场需求，上海团队一直专人负责升级开发中， ，直至 2015 年产品转由 After-Market Service Engineering 支持，期间内容包括

- 性能优化
- 客户专有生产流程模板
- 客户专用软，硬件接口支持

更多关于该产品的功能综述， [请参考其主页][#SSNX].

[#NPE]: http://www.husky.ca/abouthusky/news/content-20090608.html
[#SSNX]: http://www.husky.co/EN-US/Shotscope-NX.aspx
[#OPC]: http://baike.baidu.com/item/opc/3875


## <a id="header-2-41" class="md-header-anchor"></a>嵌入式模具无线自动识别 IoT 解决方案<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2010.06 - 2015.09**
无线通讯, 网络通讯，嵌入式，　硬件烧录

基于 [RuBee(IEEE 1902.1)][#RUBEE] 无线通讯技术的工业 IoT 解决方案，实现模具的自动识别，参数加载及寿命磨损管理。, 实现赫斯基领先全球的快速换模技术。 内容包括：

- 服务器数据管理
    - Windows Server 网络服务器程序开发
    - 编写 Mold ID Server 门户网站, 做为全球唯一数据中心，存储所有生产，售出的模具参数信息。
    - 在设计阶段供模具工程师录入，修改模具信息
    - 供生产工程师，服务工程师在线下载每套模具信息供生产现场或客户现场升级注塑机/模具配置
- Polaris HMI 集成
    - Windows 桌面程序
    - 从 Polaris HMI 中无线检测 Rubee Tags 并从中读取模具参数功能
- Tag 烧写工具维护工具
    - Windows 桌面程序
    - 模具信息以 [RuBee(IEEE 1902.1)][#RUBEE] 协议无线烧写进 Tag
    - 在有限的 256 字节内压缩存储众多模具信息
    - 连上公司内部网路 (Intranet)，可以通过全球烧写, 比如上海的工程师可以远程对加拿大工厂的Tag 烧写。

该项目在我的领导下由上海开发完成，服务器部署于上海，由上海团队维护支持全球业务。

开发内容涉及 SharePoint, Silverlight, WinForm 以及 Socket 网络通讯。

[#RUBEE]: http://www.baike.com/wiki/rubee

## <a id="header-2-42" class="md-header-anchor"></a>HyMET HeatLogger<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006.12- 2007.04**
**桌面软件设计, 硬件信息实时采集**
一位资深工程师特意写信给我
> 这是我在 Husky 见过的最漂亮的软件工具

![](./pics/heatlogger-main.png) 
监测, 记录 HyMET 合金注塑机上各热电偶监测到的热流道( Hot Runner )温度, 供

- 设计工程师验证设计, 
- 服务工程师解决客户现场故障。

技术要求包括：

- 同时采集多达100通道数, 采集周期在 1s-180s 范围内可调。
- 因为程序安装于全球客户生产现场, 由 Husky 服务工程师每 3 个月至 6 个月取回数据供设计工程师分析, 
    - 程序必须占用尽可能少的系统资源 
    - 程序不能被客户意外终止 
    - 程序能自动监测注塑机运行/停止状态, 智能停止或重新启动记录, 并保证记录数据不能因为意外掉电而丢失。
    - 预测记录数据大小, 以保证在无人监控时导致记录空间不足。
 
该项目全部以 C++ 编写, 

- 利用 TWinCAT ADS API 实时采集硬件信息, 
- 利用开源库 [The POCO C++ Libraries][#POCO] 管理配置信息和数据持久化, 
- 利用 [WTL][#WTL]/ATL 架构主体程序及用户界面, 呈现实时信息, 用户权限认证管理。

设计领导解决方案及架构, 并亲自编写除 TWinCAT ADS API 之外全部代码, 作为范例讲授 Windows/C++ GUI, 高性能（低CPU占有率，低内存使用率）后台监控应用程序设计相关知识。
并在 [POCO C++ Libraries][#POCO] 的使用过程中, 改进其 XML 处理模块, 回馈社区, 位列 [Contributers][#CONTRIB]。

该应用部署于运行 Windows Embedded XP 的工控机( Industrial PC )。



[#WTL]: https://en.wikipedia.org/wiki/Windows_Template_Library
[#CONTRIB]: https://pocoproject.org/community/contributors.html
[#POCO]: https://pocoproject.org/

## <a id="header-2-43" class="md-header-anchor"></a>多语言翻译管理数据库(HkResource)<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006.05 - 2006.07**
**软件设计, COM/ATL**
设计新数据库文件格式，为 HMI 提供 22+ 种语言支持服务。采用 ATL/C++ 实现 COM 服务器。

当 Polaris HMI 支持的界面语言从一种语言（英文）增长到 22 种以上，原有语言字符串数据库设计不再适应

- 基于单个文件的设计，典型大小 70+ MB
    - 文件经常损坏， 尤其当 HMI 意外崩溃（比如系统掉电）
    - 读写性能太差
    - 不能裁剪选择只用到的语言，大部分客户只用到 2 种语言（英语及本国语言）

保持其接口不变的情况下重新

- 设计新数据库文件格式
- 实现其 COM 服务器，采用 ATL/C++
- 从用内存映射文件(Memory Mapped File)方式操作文件

克服了以上所有缺点， 而 Polaris HMI 不需要为此更改一行代码。


## <a id="header-2-44" class="md-header-anchor"></a>HyperSync-Altanium 工业4.0集成<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016.02 - 2016.10**
**软件设计, 系统集成,硬件采集**

HyperSync™为同步化系统，模具、机器、热流道以及辅助设备协同工作。除具备突出工业4.0级别的智能性及连通性外，机器及模具流程同步化的增强可以较低的总产品成本实现较快的注塑周期，同时不会影响产品的质量


Husky 的注塑机（Injection Molding Machin）与热流道控制器 (Hotrunner Controller)作为两个独立运营的部门，虽然各自居于市场领先， 当客户同时购买两者产品时，彼此视对方为普通第三方设备，并不能启用对方独有的高阶功能/性能。

二者紧密高效的集成，成为年度新机型 Horizon 的核心功能之一， 意在

-  把各种功能的分别连接方式（如 Digital I/O, RS422 等），简化整合为一根 以太连接线， 从而
    - **在硬件上**，简化电气连接， 及生产组装车间工人，客户现场服务工程师接线的复杂性和调试难度
    - **在软件上**，具有向前扩展性， 便于增加新功能

此项目涉及面繁复庞大，**需要在数百万，行数十个工程的 C# 和 PLC 代码中，阅读，理解并修改添加相关功能**。设计内容包括

- 整理所有拟支持的 Altanium 产品线包括Delta 3, Matrix 2 等的全部功能，重新设计基于 Ethernet 的命令集，
    - 温度控制
    - 伺服电机控制
    - 版本控制
    - 对端识别，及专属功能锁定与开放
- 在 Altanium Controller 端添加支持
- 在 Polaris HMI/PLC 端添加支持
- 验证 Beckhoff/TwinCAT RT-Ethernet 实时
- 对部分重要客户提供兼容性升级，在原有硬件连接(RS232/USB)基础上实现新功能


此项目方案及软件实现完全由上海团队调研设计完成， 付出众多， 比如 

- 方案的初期一直争议怀疑意见中进行
    - TwinCAT RT-Ethernet 传输，尤其对于伺服电机控制指令传输未经实际验证
    - 在同一电缆线种，实时信号与非实时信号相互干扰程度未经实际验证


- 12小时时差下，持续数月密集的电话，视频英文设计讨论会议
- 部分重要功能在没有硬件情况下完全靠软件模拟开发测试
- 多次通宵达旦，远程连接到加拿大车间的原型注塑机上调试
- 多次往返于上海与加拿大，调试验证
- 亲赴墨西哥原型机客户（利乐包装）现场组装调试机器




在此核心功能的支撑下，该机型如期在 2016 年秋天德国举行 [K-Show][#KSHOW] 首秀成功，并在 2017 获得 [Ringier 2017 Innovation Award][#RINGIER] 创新奖。


[#RINGIER]: http://www.husky.co/News.aspx?id=6442451127
[#KSHOW]: http://www.husky.co/News.aspx?id=6442451075


## <a id="header-2-45" class="md-header-anchor"></a>Altanium 远程桌面<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016.04 - 2016.08**
**软件设计, 系统集成,网络通讯**
基于 RDP 协议的嵌入式远程桌面, 实现对　Altanium　温度控制器的远程控制

当前注塑机（IMM）与 Altanium 控制器是两套相互独立运行的工控机（IPC）系统，各自拥有自己的采集与控制硬件以及 HMI 系统，仅在重要信号上抄送对方一份。操作工必须在两套设备的 HMI 显示屏 上来回操作。

此项目是 HyperSync/Altanium 以太网 One-Cable 集成的互补项目。意在直接从 IMM 的显示屏上直接复制 Altanium 的 HMI 控制界面，实现 One-Display 操作。

- 前者重点在于**实时硬件信号**的相互通讯，实现运动控制的有序性同步， 以**工业以太网( Industrial Ethernet )**协议传输
- 后者的重点在于**非实时软件偏好设置**，以标准 TCP/IP 协议传输

技术内容包括

- VNC 与 RDP 远程协议的方案选择与原型设计
- 设计/验证 RDP 通讯对带宽的占用，及对同电缆内实时信号的影响
- Polaris HMI 系统架构下集成 RDP 协议及界面
- Altanium 的 [APIPA][#APIPA] 自动接入检测，识别
- Altanium 控制器自动解锁屏处理
    - Altanium 采用无键盘纯触摸屏设计
        - 其在 被 RDP 协议远程连接后，会锁屏， 导致操作员不能进入


- 在工业强度下性能的应急响应实时性与可靠性设计与检验
[#APIPA]: http://baike.baidu.com/item/APIPA?sefr=cr


## <a id="header-2-46" class="md-header-anchor"></a>扁鹊（Bianque）云生产管理系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2013.04 - 2016.12**
**软件设计, 系统集成,网络通讯, 云计算, 大数据，机器智能，IoT, 数据挖掘**
立足 Azure 云技术（比如 IoT Hub）， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术的新一代 DevOps 系统。

本项目由我以上古名医命名，借指其对现有 Polaris Production System 的“起死回生”的治疗。其凝聚了自加入Husky 以来，我对其开发流程，生产支持流程的观察和解决之道。

Polaris Production System 自 2000 左右开始使用以来，众多的功能添加使其及庞大复杂，经常错误，以致在加拿大有工作 20 年经验的工程师几乎以每天解释各种错误及其绕过之道专职。在上海，其学习难度和莫名其妙的错误模式阻碍了团队的迅速扩大。

其开发环境与发布环境杂揉，多年增量式添加更改配置，延续到目前 VMWare 虚拟机已达已经 70GB 以上的，极少数人能从头配置其环境。

- 其中的完全依赖于加拿大，上海，卢森堡的本地网络映射盘数百Ｇ数据文件，时常因为文件不及同步，丢失，重命名等导致崩溃，网络速度。

- 众多的虚拟机既导致工作站导致性能下降， 共享盘空间浪费，其不停拷贝，也导致网络拥挤，引起 IT 部门的不满。

- 其复杂的工具集，界面，和莫名错误让需求相对单一的车间生产支持工程师不知所措。


扁鹊精分开发和发布的环境需求，立足当前虚拟化技术的前沿与 DevOps 的相关理念，改变其本地应用程序架构，基于网络服务（Web Services）的架构，

- 针对开发环境需求
  
  利用 Vagrant 管理虚拟机使
  
  - 开发轻量化

    基于公共 Vagrant Box， 不必囤积大量虚拟机镜像

  - 共享简单化 

    不必通过拷贝 70+GB 的文件实现开发/调试环境共享

  - 版本化

    容易回退或前进到某特定版本，或比较其差异

- 针对软件发布环境需求

  采用后端前端 Single Page Application (SPA)　网站应用 后端 REST API Service
  - 可以从新浏览器窗口， 发起新的编译请求
  - 进度状态消息实时 email 通知
  - 可以在 Intranet 覆盖的任何地方发起请求和下载结果，而不必守在电脑前监控过程等待结果 
  - 当部署到云端 （Microsoft Azure）上后，服务工程师可以全球远程下载补丁


扁鹊由若干子项目组成，主要

- Lancet（柳叶刀）

    主要以 Python 语言实现的 REST Service 总称， 包括重新实现部分原 Polaris Production System 的功能。

- Acupuncture（针灸）

    以 Django/Dojo Toolkit/Web Socket 为框架实现的单页网站应用(Single Page Application), 作为生产支持和现场服务工程师生成软件包的主要 GUI 门户。

- Transformer（变形金刚）

   管理预配置好的 VMWare 虚拟机群， 

   - 包装， 并行化执行 Polaris Production System 命令， 使编译生成机器时间从原先 典型 40 分钟， 缩减至 5-10 分钟
   - 合理分配请求，提高虚拟机资源的利用率

- HySearch

   基于 MongoDB / Django 的搜索网站， 通过关键字或主要类别，搜索 软件模块或现有机器，以参考比较。

- **部分截图(来自早期 PPT 准备素材)**
    
- **Bianque 早期概念图**
![](./pics/bq-arch.png)

- **HySearch 搜索门户视图**
![](./pics/bq-search.png)

- **Acupuncture 早期视图**
![](./pics/bq-acupuncture-1.jpg)
![](./pics/bq-acupuncture-2.png)

- **Acupuncture 早期视图（移动设备）**
![](./pics/bq-mobile.jpg)

- **Transformers 早期调试窗口**
![](./pics/bq-transformer.jpg)


<hr class="page_break" id="profile-zh-2" />






# <a id="header-1-47" class="md-header-anchor"></a>意法半导体<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-48" class="md-header-anchor"></a>BIOS Flash 闪存芯片功能验证及演示平台<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2003.12- 2004.03**
**嵌入式软件设计, 电子电路设计**
 
为新推出的 M50 系列 PC BIOS 专用 Firmware Hub/Low Pin Count Flash 总线存储芯片, 提供功能验证平台, 及用户开发演示代码。包括: 

- 功能验证和代码演示的硬件平台; 
- 分成 2 层的驱动代码（C语言）. 
    - 顶层（协议层）: 提供 Firmware Hub/Low Pin Count 协议的封装, 以标准 C 编写, 独立于硬件设计. 
    - 底层（物理层）: 提供硬件平台相关的地址, 数据, 控制相关基本操作代码, 与框架.
 
与芯片设计工程师合作, 全部负责软、硬件设计

## <a id="header-2-49" class="md-header-anchor"></a>嵌入式系统 SPI FLASH 闪存芯片应用包<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2004.05 - 2004.10**
**嵌入式软件设计, 电子电路设计**

该应用包源于日常工作需求

- 验证 IC 样片
- 演示应用场景
- 提供免费可下载 C 语言通用驱动代码库, 用于读写寄存器, 传输数据

本人负责方案设计以及全部硬件与软件设计

- 采用 [Protel/Altium][#ALTIUM] 设计了采用 uPSD3300 MCU 的嵌入式应用开发板硬件
    - 该应用也因此同时作为为即将推出的SoC(System On Chip)产品 uPSD3300提供开发包,
- 编写调试了通过 SPI 接口访问 M25/M45P(E) 系列程序/数据存储芯片的通用驱动库
- 编写了典型应用
    - 演示字符集存储, 可用于公共场所公告牌显示
    - 音频存储等应用, 可用于 MP3 播放器设计。 
- 编写英文应用指南( Application Notes ), 详细解释全部设计细节

全部软代码, 及应用指南( Application Notes )发布在 ST 官网上共供用户免费下载。

[#ALTIUM]: http://www.altium.com/

## <a id="header-2-50" class="md-header-anchor"></a>SPI Flash 闪存芯片编程器<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2003.09 - 2005.11**
**嵌入式软件设计, 桌面软件设计, 电子电路设计, 应用架构**

基于以前原有 VB6 的半成品原型，在理解需求和现有技术架构后， 完全重构重新设计。 包括

- 上位机 PC 端界面
    - GUI 图形界面，便于直观操作。
    - CLI 命令行界面，便于批处理与第 3 方集成
- USB 通讯
    - Windows 端 USB 驱动程序
    - MCU 端 USB 驱动程序
- SPI/I&sup2;C 通讯协议及 Flash 芯片命令的组织和解析

该套件 PC 端以 C++/Python 编写, MCU 端采用 ST7 以 C 及汇编语言编写。
该套件被 ST 时期的法国同事商业化，成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列。在其创业初期，以代理 CTO 身份帮助维护开发维护产品。 **在后来的闲聊中，得知  [Dediprog][#DP] 后续维护开发的工程师对其软件架构高度称赞**， 从 2005 年最初版本至今 10 多年来，支持的芯片种类大幅增加 ， 该架构一直维持无大改动。


## <a id="header-2-51" class="md-header-anchor"></a>嵌入式实时操作系统　uCLinux　移植<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005.04 - 2005.09**
**嵌入式软件设计, 电子电路设计**

该项目用于展示如何从 M25Pxx code Flash芯片启动操作系统(Embedded OS), 采用了基于 ARM 内核的 ST MCU 处理器。

采用 C 语言及汇编语言（Assembly）。

## <a id="header-2-52" class="md-header-anchor"></a>嵌入式实时操作系统　uCOS/II　移植<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005.04 - 2005.09**
**嵌入式软件设计, 电子电路设计**

该项目用于展示如何从 M25Pxx code Flash芯片启动操作系统(Embedded OS), 采用了基于 ARM 内核的 ST MCU 处理器。

采用 C 语言及汇编语言（Assembly）。

## <a id="header-2-53" class="md-header-anchor"></a>Anatidae　项目管理系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2004.02 - 2004.06**
**桌面软件设计**
 
日常项目跟踪软件, 用以同步上海　MMCC　内部, 以及与法国 Serial Flash Division/意大利 NAND Flash Division 之间的项目进度。
采用C++语言编写, 用到　MFC, STL, BOOST。采用 ADO + Access 构建本地数据库, 以 XML 同步异地数据。
 
负责软件架构及代码编写。


<hr class="page_break" id="profile-zh-3" />






# <a id="header-1-54" class="md-header-anchor"></a>施耐德电气<a class="back-to-top-link" href="#top">&#x27B6;</a>

## <a id="header-2-55" class="md-header-anchor"></a>紧急备用电源自动切换系统(ATS)<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2002.11- 2003.08**
**电气设计, 产品架构, PLC 应用**
 
开发新一代auto-transfer system, 以提供更智能, 更友好的人机界面, 更高可靠性和更低廉的成本.此系统基于 NEZA PLC, 配合NS系列塑壳式断路器使用, 为医院等场所提供可靠的主备用电源管理, 以持续电源供应. 

本项目由研发中心，合资工厂，NEZA PLC 事业部合作，意在

- 填补自动电源切换装置空白
- 提供新型 哪吒(NEZA)系列 PLC 的应用案例

本人领导并推动此项目, 

- 负责全部原型设计与改进，先后设计出 3 台样品
- 拟定并完成了各项 IEC/GB 规定相关试验
- 完成了投资/收益分析, 技术可行性分析及设计方案。

此项设计节省了 1/3 的成本。 

## <a id="header-2-56" class="md-header-anchor"></a>空气断路器国产化设计<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2002.11- 2003.08**
**电子电路设计**

为降低成本，缩小与本地生产商的价格差距，受合资工厂委托，对引进产品 Vigi 系列小型空气断路器进行本地化设计更改， 包括

- 分析国外原 PCB/ASIC 的设计原理图， 针对国内标准及供应商的差异修改设计，并设计实验验证设计
- 逆向工程国内市场竞争产品，研究其原理， 成本， 可靠性等， 并通过设计实验获取相关参数

此项目与另一同部门工程师合作完成， 责任不分主次。


## <a id="header-2-57" class="md-header-anchor"></a>工业 LED 指示报警灯<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2002.08 - 2002.11**
**电气设计, 产品设计前期调研**

为可能进入工业 LED 指示/报警灯市场，受合资工厂委托进行调研， 包括

- LED 光源技术原理
- LED 光源技术指标及检测手段，拜访包括复旦大学电光源研究所， 美能达光学仪器等研究所和企业交流潜在合作机会与意向
- 收集 LED 生产厂家样品， 在研发中心实验室根据国标（GB）和 IEC 相关标准，设计实验，获取原始数据。

此项目与另一同部门工程师合作完成， 责任不分主次。


## <a id="header-2-58" class="md-header-anchor"></a>恒温加热箱<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2002.11- 2003.01**
**电气设计, 产品设计, 单片机嵌入式应用**

此项目意在提供高达70 &#8451;的恒温实验环境, 节省昂贵的外购设备费用。 包括

- 单片机 PID 控制调试
- 封闭箱体机械设计，组装

此项目与另一同部门工程师合作完成， 责任不分主次。

## <a id="header-2-59" class="md-header-anchor"></a>开关电源国产化<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2003.05 - 2003.08**
**电气设计, 产品设计前期调研**

此项目研究引进原产波兰工厂开关直流电源的可行性， 包括

- 设计实验验证原产波兰工厂样品国家标准合规性，及收集各项性能指标。
- 收集调研国内市场同类产品的性能参数
- 出具可行性报告及建议 

此项目独立完成。


<hr class="page_break" id="profile-zh-4" />






# <a id="header-1-60" class="md-header-anchor"></a>哈尔滨工业大学<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-61" class="md-header-anchor"></a>铁道车辆用电器综合测试系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2000.09 - 2001.04**
**软件设计,电子设计,电气设计,产品设计前期调研**
此系统作为硕士学位论文课题验证设计，基于工业控制计算机,  为铁道车辆用电器（断路器, 接触器, 热继电器）的自动化常规检修平台.

- 经黑龙江省科委专家组技术评审鉴定“__居于国内领先__”。
- 相关论文发表于学术会议__第八届国际电器可靠性会议__。
- 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
- 2002.12 获黑龙江省教育厅__科技进步一等奖__
- 2003.05 获黑龙江省科技厅__科技进步二等奖__
 

此课题应哈尔滨铁路局要求设计, 检修内容包括接触电阻, 闭合/断开时间, 过压/欠压/失压, 过流, 短路等电气性能试验和正弦振动等机械性能试验.

硬件设计包括: 

- 工业控制计算机ISA I/O 板卡设计
- 工业控制计算机PCI A/D 板卡设计
- 计数器/定时器电路设计
- 大电流采样与切换电路
- 数字编码可变电源设计, 提供 1A to 1,300A 电流 及440VAC/220VDC电压
- 高精度阻性负载设计 (分辨率: 1/512 ohm)
- 变频器/电机控制

软件设计（采用Visual C++）包括:

- 精确电压/电流闭环控制算法
- 友好、全面的运行参数显示
- 故障报告与寿命预测(FMEA)
- 测试数据管理

此项目也作为硕士学位论文课题验证设计, 承担了全部的项目前期调研, 软/硬件设计及少量机械设计。

此项目深受用户好评, 被广泛推广至齐齐哈尔等7个车辆段销售累计逾百万人民币。
 

## <a id="header-2-62" class="md-header-anchor"></a>电子镇流器综合测试系统<a class="back-to-top-link" href="#top">&#x27B6;</a>

**1999.04 - 1999.10**
**软件设计,电子设计,电气设计,产品设计前期调研**
此课题应哈尔滨铁路局要求设计, 作为学士学位论文课题。负责了的调研, 全部硬件设计, 软件设计文档和调试。在国家级学术期刊发表相关论文一篇.
 
此系统能同时测试多达八只电子各种型号镇流器/逆变器 (15W, 20W, 30W, 40W), 提供各种详细性能参数信息, 包括灯电压, 灯电流, 功率因数, 谐波畸变, 镇流器温升等. 实时显示电压, 电流波形。一旦发现故障, 还将提供故障定位与诊断帮助. 建立试品维修历史数据库, 并自动生成报表. 
硬件设计包括: 

- ISA I/O 板, A/D 板 
- 交/直流电源 (DC 47V-57V, AC 200V – 240V) 
- 传感器板 (电压, 电流 和 温度检测 ) 
- 试品切换板 

软件设计包括: 

- 硬件驱动 
- 实时数据采样、分析 (快速傅立叶变换) 
- 波形显示 
- 测试数据管理该系统以其高效准确的性能深受好评, 并被推广至齐齐哈尔等铁路分局.


<hr class="page_break" id="profile-zh-5" />






# <a id="header-1-63" class="md-header-anchor"></a>业余时间<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-64" class="md-header-anchor"></a>Markdown CV<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2017.01 - 2013.03**
**HTML5 设计, 脚本编写, 效率工具** 

**本简历由此工具辅助完成**

在整理编写自己简历时，决定采用奥卡姆剃刀原则， 以 Markdown 格式纯文本书写

- 不依赖于 Window/Word或其他系统和工具环境依赖, 便于随时随地更新
- 分离内容与呈现形式，便于输出多种格式
  - PDF 以便于纸质打印输出
  - HTML 以便于电脑屏幕上链接跳转，浏览
  - TXT/Markdown 便于版本控制

采用了 Python, HTML, CSS, JavaScript, Bootstrap 4.0 alpha 及 [Bootstrap][#BOOTSTRAP] 主题。

相关模板及脚本开源于 [GitHub][#MDCV] 供免费下载。

- [link][#MDCV]

[#MDCV]: https://github.com/rockonedege/markdown-cv
[#BOOTSTRAP]: http://getbootstrap.com/

## <a id="header-2-65" class="md-header-anchor"></a>2D Water Effect in WTL<a class="back-to-top-link" href="#top">&#x27B6;</a>

2011
图像处理

该代码源于工作中**图像识别处理**, 以及 Windows 图形编程(GDI+, Direct2D ) 学习研究时，兴趣所致。

一段时间需要频繁处理从示波器中导出的芯片通信信号, 并需要于是写了此小工具用于数值的二进制，十进制，十六进制之间相互转换，

- 采用 C++/WTL, 并用英文解释了代码的设计和使用方法。 
- 发表于开源网站 [The Codeproject][#2DWTL]

- [link][#2DWTL]

[#2DWTL]: https://www.codeproject.com/Articles/188236/D-Water-Effect-in-WTL


## <a id="header-2-66" class="md-header-anchor"></a>A Literal Converter for Integers<a class="back-to-top-link" href="#top">&#x27B6;</a>

2005
C++

该代码源于工作中一段时间需要频繁处理从示波器中导出的芯片通信信号, 并需要于是写了此小工具用于数值的二进制，十进制，十六进制之间相互转换，

- 采用 C++, 并用英文解释了代码的设计和使用方法。 
- 发表于开源网站 [The Codeproject][#LITERAL]

- [link][#LITERAL]

[#LITERAL]: https://www.codeproject.com/Articles/10379/A-Literal-Converter-for-Integers


## <a id="header-2-67" class="md-header-anchor"></a>TwinCAT TSM 查看器<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016.06 - 2016.12**
**桌面/服务器/云服务器/软件设计, 网络通讯, IoT** 

TSM 文件是 倍福公司( Beckhoff ) 自动化设备管理与 PLC 运行环境软件 TwinCAT 2 的配置文件，该文件采用不透明的二进制格式，包含

- 所有**硬件**比如板卡及其配置信息
- 所有**软件**比如 PLC， 数控（NC） 等程序及相关**实时任务**配置信息
- 硬件采集与控制端口与对应软件变量的**映射**信息

一台典型的 Husky 注塑机系统拥有 6000+ 的节点映射信息，导致无论

- 开发阶段，
- 车间组装测试阶段， 
- 还是客户现场服务

调试，排错，定位都非常低效。

此工具采用最新 IT 技术, 实现以下功能

- 把所有二进制信息提取成文本格式，比如 JSON, XML 等
- 以 HTML5/JavaScript/CSS 的单页应用(SPA - Single Page Application) 实现了
  - 可视化节点信息
  - 快速搜索查询节点
  - 关系图（Dependency Graph）可视化
- REST 服务，供远程读取该注塑机的所有配置信息
  - ** 此功能使得设计工程师可以从办公室协助车间生产测试工程师调试机器， 而不用来回跑，节省大量时间 **
- Microsoft Azure IoT Hub 支持
  - ** 此功能使得设计工程师可以从 Husky 办公室协助全球客户现场工程师调试，节省大量时间金钱 **

此项目主要采用 C++/CMake, Python, JavaScript，采用开源库 [Vis.js][#VIS]等。

[#VIS]: http://visjs.org/

## <a id="header-2-68" class="md-header-anchor"></a>HostLink TCP 注塑机通讯协议<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2012.08 - 2013.04**
**桌面软件设计, 网络通讯,Windows, Linux**

![](./pics/hostlinkpp-ubuntu.png)

Hostlink 是原定义于 90 年代中期， Husky 专有的数据通讯协议， 基于原始 Socket（Raw Sockets） 定义众多命令，用于远程与 Husky 注塑机交互，包括

- 实时读取注塑周期各种参数(injection cycle data)
- 设置各种注塑参数

客户通过购买此协议模块，可以把 Husky 注塑机集成到其 生产执行系统( MES )与 ERP 系统中。

在为某大型客户修复该模块程序错误（Bugs）时， 我发现从长远计，原代码架构与系统依赖不久将淘汰，遂决定

- 在业余时间面向未来重新实现，
- 并作为小组相关技术培训的范例。 

开发内容包括

- 以标准 C++ (Standard C++) 重新实现该协议
    - 摒弃了以前依赖于 MFC Sockets 相关类的命令实现 
    - 在 Windows 之外，支持 Linux
    - 支持 64 bit 操作系统， 修复微妙的错误(Bugs)
- 通过 C++/CLI 支持 .NET 绑定， 供 C# / PowerShell / IronPython 等语言调用
- 通过 Boost.Python 支持 Python 调用 
- 增加新的命令字及数据格式, 以传递新的 HPP 机型拥有更多注塑周期实时信息

本代码大量使用使用 [Boost][#BOOST] 库，如 [Boost.Asio][#ASIO], [Boost.Spirit][#SPIRIT]， [Boost.PP][#PP]，[Boost.Python][#PYTHON] 等，以及模板元编程（metaprogramming）等高级现代 C++ 技术及库。

构建工具采用 CMake.
因为 Hostlink 协议不是开放协议，此代码未能开源。

[#BOOST]: http://www.boost.org/
[#ASIO]: http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html
[#SPIRIT]: http://boost-spirit.com/home/
[#PP]: http://www.boost.org/doc/libs/1_63_0/libs/preprocessor/doc/index.html
[#PYTHON]: http://www.boost.org/doc/libs/1_63_0/libs/python/doc/html/index.html


## <a id="header-2-69" class="md-header-anchor"></a>Console3<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2012-2016**

该项目基于开源工具[Console 2][#CONSOLE2]定制

- 修复众多错误
- 重新以 [Direct2D][#]/[DirectWrite][#DW] 替代原基于 GDI 的 UI 层，以提高性能和视觉效果
- 添加便利性功能，比如 集成常用 Husky 开发工具与系统变量设置等

该工具在 Husky 上海研发团队广泛长期使用。


[#CONSOLE2]: http://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx
[#D2D]: https://msdn.microsoft.com/zh-cn/library/dd370990(v=vs.85).aspx
[#DW]: https://msdn.microsoft.com/zh-cn/library/windows/desktop/dd368038(v=vs.85).aspx


## <a id="header-2-70" class="md-header-anchor"></a>Beckhoff TwinCAT ADS 通讯协议<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016.07 - 2016.12**
**软件设计** 

基于开源的 Beckhoff ADS 协议跨平台实现， 加入了 TwinCAT 2 的支持。

- [link][#ADS]
[#ADS]: https://github.com/rockonedege/ADS

## <a id="header-2-71" class="md-header-anchor"></a>Multi-Ping<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016.07**
**软件设计, 网络程序设计** 

在集成 Altanium 热流道控制器的项目中，一项设计目标是控制器被接入注塑机的瞬间，

- 自动探测检测到， 并获迅速或得其 APIPA 地址

使用 ping 应用程序 和 .NET库不能达到实时性要求（耗时 **10s - 40+s**），并且导致大量系统资源（比如内存 **700MB**）消耗, 以致程序崩溃。

我分析其原因可能是过多的 .NET 库封装开销所致， 以 C++/Boost.Asio 从 socket 层异步实现相似功能, 达到 **2s** 完成检测，内存消耗 **5MB** 左右。

随后以此算法封装成
- .NET 组件供 Polaris HMI 集成调用
- 独立应用程序，供车间测试工程师，现场服务工程师调试( Troubleshooting )用

-----
<small>Generated from <a href="http://daringfireball.net/projects/markdown/">Markdown</a> text using my open source tool <a href="https://github.com/rockonedege/markdown-cv">markdown-cv</a>. Last updated at 2018-01-11. </small>