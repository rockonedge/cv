---
banner:
  brand: '<a class=''navbar-brand'' href=''#top''>中文简历</a> <a class=''navbar-brand''
    href=''#full-cv-zh-en-1''>CV</a> '
  lastupdate: <a class="btn-link" href="https://tslides.netlify.com/cv">SLIDES</a>
    &middot; <a class="btn-link" href="./pdf/full-cv-zh-en.pdf">PDF</a> &middot; 2018-01-11
  nav: ''
  time: 2018-01-11 17:56:04.952248
  title: '中文简历 CV '

---







<small>**(+86)135-248-26926 &middot; [tom.tan@live.com](mailto:tom.tan@live.com) &middot; (+86) 135-248-26926 &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **研发管理 &middot; 工业软件 &middot; 固件 &middot; 硬件 &middot; 云计算 &middot; 数据科学 &middot; IoT**</small>

# **谭 智**

>- 创建全新全球研发团队，并领导 11+ 年
    - 成功完成众多全球协作开发项目，成绩备受赞誉
    - 善于促进团队合作与成长，富于责任感，感染力


>- 专业，高效的职业素质
    - 工作自觉性高与责任感强，崇尚以身作则
    - 善于多文化环境下沟通合作
    - 富有远见, 敏锐的技术敏感性, 渴望挑战


>- 18+ 年电气工程工业自动化，计算机软件/固件/硬件项目经验
    - 15+ 年供职于跨国公司研发部门
    - 自动化测量，工业实时驱动控制，半导体电子软/硬件系统研发
    - 熟悉电气/电子设计( Schematic & PCB ) 调试,仿真
    - 熟悉 Windows/Linux, 包括桌面，服务器，嵌入式以及 Azure 云计算软件开发
    - 熟练使用 C++，Python，C#，JavaScript, Go, Matlab 等语言
    - 长期紧密支持生产，销售，服务部门


>- 跨国高科技公司成功创业经验
>- [联合国儿童基金( UNICEF )](http://www.unicef.cn)定期捐赠者

## **求职目标**

- 研发总监／高级经理
    - 团队创建，管理
    - 工业4.0/工业互联网（IIoT）/物联网 （IoT）研发
    - 云端，桌面，嵌入式/移动设备软件研发


## **职业经历**

### 部门主管/创立者，全球研发工程部 &middot; <small>2005.11 - 2016.12</small>
**Team Leader/Founder，Global Development Engineering Department**

**汇报对象：全球研发工程部总监， 加拿大总部**

**[赫斯基注塑系统（上海）有限公司][#HUSKY]**


帮助建立了上海注塑机工厂， 在一年左右的时间里从零开始成功组建，生产， 装配， 测试所需的软件基础设施和工程师团队， 保证了在上海工厂的制造能力。随后，建立了赫斯基唯一的海外研发工程团队。通过电子邮件，电话视频会议以及人员互访与加拿大总部的研发团队同步协作，对全球提供技术支持达 10 余年。

#### 主要职责

作为创始人和本地区最高部门领导， 直接向总部研发总监和VP负责，受到充分信任授权，享有一切本地团队事务最终决定权， 日常职责包括

-  组建上海研发团队，承担全球合作开发。
-  管理上海研发团队日常运作，编制与控制预算开支，团队建设与人才培养等。
-  与全球其他各工程开发团队主管共同编制，协调开发计划与进度，设计原则，工程标准与质量。
-  领导团队开发与维护注塑机控制系统(MES)，工厂监控（SCADA）系统等
-  为生产制造部门开发，维护生产系统软件（Production System）以维持加拿大，卢森堡，美国，上海各地工厂的日常生产运行。
-  与市场，销售，客服部门合作为其提供技术支持，并根据反馈和建议研发或改进工业控制软件。
-  评估和引进新软件开发技术理念和方法，优化自动化控制系统。
-  探索实践跨国团队协作（Global Engineering）的高效管理方式和方法。

**因团队在项目质量，团队协作上表现杰出，多次应邀分享跨国工程团队组织和领导经验。**

#### 主要技术内容

过去11年里， 率领上海团队启动或参与各类系统研发。 

- 从 2014年左右，开始结合亚马逊 AWS 和微软 Windows Azure 研究实施

    - 云计算 / 工业4.0 / 物联网（IoT）解决方案， 比如 AWS IoT 和 Azure IoT Hub。
    - 数据挖掘 与机器学习(基于 Python / R)
    - 虚拟化

- 主要产品包括

    - 过去的十年推出的所有**注塑机**，
    - 部分热流道**控制器**，
    - **系统整合**解决方案等

- 主要技术包括

    - 人机交互（HMI）

        包括图形界面，实时/历史数据库，权限管理，SPC/SQC算法等。

    - 机器设备监控，与运动控制(含机器人)

        基于**工业互联网协议 EtherCAT** 和 Beckhoff/TWinCAT 控制器的温度采集，压力感应， 伺服电机驱动，实时运动控制，生产监控（SCADA）开发相关控制驱动软件等。

    - 企业生产监控系统(SCADA/MES)

        专注于注塑行业，监控采集企业单车间或跨地域工厂的所有机器（赫斯基和非赫斯基）运行，实时采集与控制周期参数，以及工班人员，能源消耗，物料分配安排， 报表生成等, 通过各种标准软件协议和硬件设备, 如 OPC(OLE for Process Control) 等，架起机器*生产车间* 与企业上层 IT 系统 如 ERP 等第三方系统的桥梁。

    - 第三方系统集成

        通过各种工业接口， 如Digital I/O, RS422/RS232， Ethernet 等集成辅助设备， 比如， ABB 机器人（如 ABB），Plastic Systems 树脂干燥机， 除湿器等。

    - 也视情形涉及电气系统调试与机械设计辅助任务。


- 目标平台包括
    Linux 和 Windows 平台的传统应用， 如
    - 嵌入式(Embedded Systems)
    - 移动设备(Mobile Platforms)
    - 桌面(Desktop Workstations)
    - 服务器(Web Servers)
    - Amazon AWS 和 Microsoft Azure 云平台 


- 主要使用语言包括
    - C++
    - C#
    - Visual Basic
    - Python
    - Ruby
    - JavaScript
    - PLC/IEC61131 programming languages 等。

 
- 工业总线接口/协议包括
    - EtherCAT/Realtime Ethernet
    - Profibus
    - Sercos
    - Euromap
    - CAN
    - SPI
    - USB
    - RS232/RS422/RS485 等。


#### 部分项目

此处仅简单列举。[**详情参阅在线描述**](https://ttan.netlify.com/projects-zh)

- 上海工厂启动
    - 引进了拿大总部工厂的生产支持系统 （Polaris）及流程
    - 招聘，培训了首批生产支持 （装配和测试）工程团队
- 建立了第一个全球研发工程团队， 全部由硕士研究生以上学历组成。
- 领导开发用于所有赫斯基注塑机的 Polaris 软件平台， 适用于 Hylectric， HyPET， HyPET HPP， HyCAP， HyperSync， Barrier等机型， 包括
    - Polaris HMI（人机界面）， 
        - 维护性开发基于 COM/VB6 平台HMI
        - 为新一代的注塑机重新架构和实现了基于 .NET/C# 平台的HMI
        - 十多年来，保持上述平台按季度/半年定期发布
    - Polaris PLC，基于 TwinCAT/CODESYS 技术的 PLC 编程以实现注射成型的实时控制相关算法，例如
        - 伺服驱动(Servo)
        - 机器人控制等。
    - Polaris Production System， 此系统是内部各部门 （研发， 生产，售后支持服务等）都协同公作的公共 SDLC（Software Development Life Cycle） 平台
- 领导 HyperSync 注塑机系列工业 4.0 相关特性，获得 [Ringier Technology Innovation Award（荣格技术创新大奖）](https://www.plasticstoday.com/injection-molding/husky-s-hypersync-specialty-closures-has-global-debut-k-2016/19859153725759)。        
- 领导开发统计过程控制（SPC）及 统计质量控制 （SQC） 模块
- 领导开发多语言翻译管理系统 （Translation Website） 的开发，简化软件开发中的全球化支持（22+语言）
- 领导开发基于 RuBee（IEEE 1902.1） 无线通讯技术的嵌入式模具无线自动识别(Mold ID)工业 IoT 解决方案
- 发起扁鹊（Bianque）项目，融合 Azure IoT Hub， REST service, 微服务（Micro service），移动终端 app 等云计算时代前沿技术。
- 领导[收购整合 Moldflow 制造部门][#BUYMF]的法国与美国团队资产， 并后续支持既有客户
    - 基于 Windows Server 采用 C++/Delphi/Java 开发的工厂过程监控系统系统Celltrack，Shotscope
    - 基于 Linux/Fedora 采用 C++/QT/MySQL 开发的热流道温度控制器 Matrix
- 领导开发 Shotscope NX， 一套基于 web 技术的过程和生产监控自动化实时解决方案。
- 引入 SCRUM/Test-Driven Development（TDD） 敏捷（Agile）开发流程，把源代码控制从 VSS 迁移到 TFS。        
- 领导上海团队成员参与了中国区，越南，墨西哥等地的全球 PLT 支持。 
    - PLT 是一个临时由研发部门设计人员或技术专家组建（因为此时生产和售前/后部门还不具备相关产品知识）， 为尚未正式推出的新机型早期客户提供现场安装调试服务的团队。是 Husky 推出新机型的传统程序。    
- 业余时间开发了看板网站应用 Whiteboard，基于 Python/Django， C#/ASP.NET/RESTful Service/WebAPI 和 JavaScript/Dojo Toolkit，Apache Cordova， 综合 Team Foundation Server, Visual SourceSafe, SharePoint, ERP 以及 UNC 映射盘等数据源信息
    - 便利了团队领导随时随地在线查看任何项目和状态
    - 在诸如
        - 每日站立会议（daily standup meeting）
        - 部门周会（Weekly team meeting）
        - 项目里程碑会议（milestone meeting）
    等日常或临时会议中简化了与会成员背景介绍时间，迅速进入主题。   

#### 奖励

- **杰出贡献奖**， 2007 年
    - 由时任全球机器事业部副总裁（VP of Machines）签署， 以奖励对上海工厂成立作出的杰出贡献
- **优秀团队领导（排名全球名列前茅）**，  2011 年
    - 知名人力资源机构肯耐珂萨（Kenexa）受聘对 Husky 全球员工满意度调查匿名调查报告
- **5 年服务奖**， 2010 年
- **10 年服务奖**， 2015 年




### 高级应用工程师/代理主管，存储器创新应用中心 &middot; <small>2003.9 - 2005.11</small>
**Senior Application Engineer/Interim Team Leader，Memory Competence Center**

**汇报对象：部门经理（上海）/研发经理（法国）**

**[意法半导体（上海）有限公司][#ST]**


作为公司 R&D 力量在亚太地区的延伸，代表 Serial Non-Volatile Memory Division 负责整个亚太地区应用开发，为本地现场应用工程师 （Field Application Engineer）提供技术和方案支持。

作为此新成立部门最早期员工之一，代理主管职责，协助成立并领导本地 Serial Non-Volatile Memory & Embedded System  Solutions Team，

- 负责建立与维持与法国 R&D 部门的日常联系，培训安排等，
- 开发重心包括 
    - **Software/Firmware 开发**
    - **原理图与 PCB 电路板设计**

    具体而言

    - 协助 IC 设计师进行样片功能验证，包括信号质量，时序图等。
    - 基于MCU，DSP，ARM7/9 等 CPU 开发嵌入式应用参考设计 （包括软件代码编写与硬件验证平台），演示 EEPROM/FLASH 等的功能和应用场景。
        - 电路原理图设计与 PCB 布线制作，调试，演示主要功能和应用领域。
        - 编写，发布可移植，高性能 C 语言芯片 Firmware 驱动代码到公司全球网站
    - 用英文撰写，发布应用指南（Application Notes）到公司全球网站
    - 各类部门自用工具软件，脚本编写，采用 C++, C#, Python 等

#### 部分项目

- 基于 uPSD （Intel8032 内核）开发维护 3 个系列串行 Flash 存储芯片的参考设计
    - M50xx FWH/LPC 接口 BIOS 存储芯片
    - M25Pxx  SPI 接口代码存储芯片系列
    - M45PExx SPI 接口数据存储芯片系列


- 基于 PC 和 ST7 （ Motorola 内核）， USB 接口的 Flash 编程烧录器（Serial Flash Programmer）
    - 基于以前 VB6 的未完成原型，完全以C++/汇编从 PC 端到 MCU 端重新设计
    - 被 ST 时期的法国同事商业化，成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列，并邀请我协助维护开发至SF300。 
    - 根据[其官网上的用户手册][#DEDIMANUAL], 时至今日其架构和功能几乎没有什么大的变动。



- 移植嵌入式操作系统到 STR71x
    - uC/OS II
    - uCLinux


- 开发了 Anatidae 项目管理软件跟踪同步上海，意大利，法国，捷克之间的项目进度。
    - 基于 MFC/STL/BOOST，XML 与 ADO 
    - 约5万行 C++ 代码 

[#DEDIMANUAL]: http://www.dediprog.com/save/84.pdf/to/dp_SF%20User%20Manual_EN_V6.7.pdf
        
### 电子设计工程师，中国研发中心 &middot; <small>2002.4 - 2003.8</small>
**Electronic Design Engineer，China Design Center**

**汇报对象：中国研发中心经理（法国人，由总部研发中心委派）**

**[施耐德电气 （中国）投资有限公司][#SECI]**


作为独资公司的研发力量，依托研发中心实验室和本地区10多家合资工厂，进行
- 本地化产品研发
- 投产产品故障分析等高级技术支持
- 着重于工控自动化（ICA）产品的电子设计改进和创新，包括

    - 从全球其他市场引进产品和国产化设计
    - 中国市场特有的新产品原型设计
    - 为合资工厂提供故障分析高级技术支持
    - 根据 IEC/GB 相关标准对新产品 / 样品进行预试验，并起草新产品的检验/验收试验方案及标准
    - 提交各类所需实验数据， 协助合资工厂向主管机构（比如上海电科所）申请各类本地化准入认证
    - 为实验室设计开发，选购软/硬件设备/工具
    - 对实验室操作人员技术培训

#### 部分项目

- 设计了自动应急电源切换系统的前后 3 套原型，填补市场上只有组合方案，没有集成产品的空白。 此系统用于医院手术室，重要的政府设施在紧急情况下主，备用电源之间自动切换
- 协同设计了基于 MCU 和 PID 算法的恒温试验控制箱，用于电路断路器/接触器/PLC 高温老化测试，提供恒定温度高达 75&deg;
- 为实验室开发了一套基于关系数据库的国产化试验管理软件，用于实验室数据分析，采用了 VB6/C++ 以 及 MATLAB
- 改进小型空气断路器 ASIC 电路设计
    - VC65
    - DPN Vigi
- 从波兰工厂引进开关电源的前期研究报告，包括
    - 设计了各类性能，可靠性实验，用以与本地品牌对比
    - 逆向工程本地品牌，完成成本分析
- Compact NS 断路器认证实验
- 工业用 LED 光报警灯设计

### 科研人员，哈尔滨工业大学车辆电器研究所 &middot; <small>1998.4 - 2002.3</small>
**Researcher，HIT Research Institute of Electrical Apparatus  **

**汇报对象：研究所所长/研究生导师**

**[哈尔滨工业大学][#HIT]**

这是由我的研究生导师成立并领导的校级研究所（实验室），与哈尔滨铁路局合作，依托全国“铁路电器现代化”战略，承接各类铁路车辆电器自动化研发项目。

我从大三自荐进入实验室开始，以及研究生院学习期间，一直到取得硕士学位毕业离校，大约 4 年间，一直参与实验室各类大小项目，承担各类职责。

**此段经历奠定了我迄今为止的专注于研究与开发（R&D）技术基础和不懈追求。**

我的本科及研究生学位论文均取材于期间负责的有关项目。

参与的研究方向和开发包括

- 电机与电器
    - 交直流电动机，发电机，变压器以及马达驱动 （Rockwell自动化旗下品牌）
    - 继电器，接触器和断路器
- 计算机控制系统
    - 直接数字控制系统 
    - 离散控制系统 
    - 现场总线控制系统 
- 实时控制及测量方法和电器可靠性(FMEA)
- 实时嵌入式的系统（RTOS）和嵌入式应用程序开发
- 各类电气电子接口电路与总线协议的研究
- 工业控制与自动化设备研究与开发
    - 基于工控机（IPC）和单片机（英特尔 MCS-51 系列 MCU）
    - 人机界面编写及数据实时采集，处理和管理软件开发，采用Visual Basic/C++
    - 驱动程序开发，采用C/C++/汇编
    - 硬件系统设的电气原理图及 PCB 设计，采用 Protel
    - 电气/电路仿真，采用 pSPICE, MATLAB/SIMULINK等
    - 机械设计（操作平台，凸轮设计），采用AutoCAD


#### 部分项目

受哈尔滨铁路局委托，先后**独立**成功开发

- **电子镇流器自动化测试系统**
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 《低压电器》发表相关论文*《铁路客车电子镇流器逆变器综合试验台的研制》*
    

- **低压电器自动化测试系统**
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 经黑龙江省科委专家组技术评审鉴定“居于国内领先”。
    - 相关论文发表于*第八届国际电器可靠性会议*。
    - 并被广泛推广于齐齐哈尔等 7 下辖个车辆段。
    - 黑龙江省教育厅 科技进步一等奖， 2002 年 12 月
    - 黑龙江省科技厅 科技进步二等奖， 2003 年 5 月

**此两系统分别销售 10+ 套, 累计销售逾百万元人民币。**

从前期立项到后期交付，以及人员培训，一力承担了除前一项目的软件开发以外的全部系统架构及硬件设计。

软件设计

- 基于 Windows, 运行于工控机（IPC） 

硬件设计涉及

- ISA I/O，PCI A/D 等计算机板卡设计
- 电源及负载的二进制编码设计与控制
- EMI，谐波干扰处理等以及
- 试验台的机械设计。

## **教育**

- **硕士**
    - 毕业答辩成绩: **优异**
    - 1999 年 9 月 - 2002 年 3 月
    - 电气工程系，计算机与电气工程学院
    - [哈尔滨工业大学][#HIT]


- **学士**
    - 毕业答辩成绩: **优异**
    - 1995 年 9 月 - 1999 年 7 月
    - 电气工程系，计算机与电气工程学院
    - [哈尔滨工业大学][#HIT]


- **著作/论文**

    研究生攻读期间在国家级期刊与国际会议发表2 篇学术论文。

    - 翟国富, 谭智, 陈 伟. 铁路客车电子镇流器逆变器综合试验台的研制. 低压电器
    - 翟国富, 谭智等. 铁路客车断路器接触器性能及可靠性测试系统的研究. 第8届国际电器可靠性会议论文集
    - 翻译导师论文供外刊发表


- **荣誉**
    - 获奖者 （多次），人民奖学金，         1995 年 - 1999 年
    - 成员，电气工程和计算机学院 学生会，    1996 年 - 1997 年
    - 成员，哈尔滨工业大学 学生会，          1995 年 - 1996 年
    - 志愿者，第八届国际北方城市会议，哈尔滨，1998 年 1 月
    
    -  大学英语四级（CET4），  1996 年 7 月（大学一年级 下学期）
    -  大学英语六级（CET6），  1997 年 1 月（大学二年级 上学期）

    -  全国计算机等级二级，    1998 年 4 月

## **语言能力**

- 汉语︰ 母语
- 英语︰ 流利

## **培训**

- **The 7 Habits of Highly Effective People Training Program**

    - 3 天，2008 年 3 月
    - 上海
    - 赫斯基注塑系统赞助
    - 由 [Franklin Covey Co.][#FRANKLIN] 培训


- **The 4 Disciplines of Execution: Manager Certification**

    - 3 天，2007 年 3 月
    - 上海
    - 赫斯基注塑系统赞助
    - 由 [Franklin Covey Co.][#FRANKLIN] 培训


- **Team Leadership development Training Program**

    - 一周，2006 年 1 月
    - 博尔顿 （Bolton），加拿大
    - 赫斯基注塑系统赞助
    - 由 [Workplace Competence International Limited](https://www.linkedin.com/company/674972) 培训


-  **Product training on Serial Non-volatile Memory Chips**
    - 2 周， 2004 年 5 月
    - Rousset，法国
    - 意法半导体公司赞助
    - 由[意法半导体公司][#ST]培训
    - 覆盖串行 EEPROM，SPI 串行闪存，BIOS Flash，非接触式存储器


-  **剑桥商务英语（BEC）︰ 中级 和 高级**
    - 每周六，2002 年 - 2003 年
    - 上海
    - 施耐德电气 （中国）投资有限公司赞助
    - 由 [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan) 培训


- **Creative Thinking and Problem Solving**，
    - 2 天，2002 年 7 月
    - 上海
    - 施耐德电气中国投资有限公司赞助
    - 由 [Key Consulting][#KEYCONSULTING] 培训


- **知识产权专利培训**，
    - 1 天，2002 年 6 月
    - 上海
    - 施耐德电气中国投资有限公司赞助
    - 由上海知识产权局某退休主任培训

[#FRANKLIN]: http://www.franklincovey.com
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/

## **其它**

- 开源代码与社区参与

    - [Boost C++ Libraries](http://www.boost.org/)

      审阅 [Boost.Chrono][#BOOST_REVIEWER]，此库被收入 最新 C++ 标准库

    - [POCO C++ Libraries](http://pocoproject.org/)

      贡献[代码][#POCO_CONTRIBUTOR]

    - [The CodeProject](http://www.codeproject.com/)

      发表部分[文章及代码][#CP_AUTHOR]

    - [Github](https://github.com/rockonedege) 

      维护部分[开源代码][#GITHUB]

[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#GITHUB]: https://github.com/rockonedege

- **闲暇**
    - 家庭活动
    - 阅读
    - 跑步/半程马拉松
    - 骑行
    - 旅行


- **个人主页**

    -  [中文简历](https://ttan.netlify.com/cv-zh)(**本文档**)
    -  [中文项目经历](https://ttan.netlify.com/projects-zh)
    -  [英文简历](https://ttan.netlify.com/cv-en)
    -  [英文项目经历](https://ttan.netlify.com/projects-en)




[#HIT]: http://www.hit.edu.cn/
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#DP]: http://www.dediprog.com/


<hr class="page_break" id="full-cv-zh-1" />

<hr class="page_break" />

## **附录**

- ![](./pics/husky-contribution.jpg)
- ![](./pics/husky-leadership.jpg)
- ![](./pics/kenexa-1.jpg)
- ![](./pics/kenexa-2.jpg)
- ![](./pics/mbti.istj-2.jpg)
- ![](./pics/unicef.jpg)
- ![](./pics/shanghai-half-marathon-2008.jpg)


<hr class="page_break" id="full-cv-zh-en-1" />









**[tom.tan@live.com](mailto:tom.tan@live.com) &middot; (+86) 135-248-26926 &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **R&D Managemnt**

# **Tom Tan**

>- 11+ years of Acknowledged Global Projects&People Leadership
>- 15+ years of Multi-sector R&D Career in World's top companies across Automation, Electronics and Machinery
>- 19+ years of Hands-on Software/Firmware/Hardware Development


>- Started up A Global Venture([Dedirpog Inc.](https://www.dediprog.com/US)) 
>- Started up Global Development & Production Engineering Teams
>- [Acknowledged contribution](./pics/husky-contribution.jpg) to launch&profit of a global manufacturing factory  



>- Challenges Pursuer, Visionary and Open-minded
>- Deeply rooted in Science, Technology, Engineering and Math(STEM) Related
>- A Regular [UNICEF](http://www.unicef.cn) Donator


## **Objective**

A Senior Manager/Director position, preferably

- Engineering teams startup&leadership
- Industrial, Scientific, Medical(ISM) or Automotive sectors
- IoT/IIoT/Industry 4.0 innovations 
- A global company Based in Shanghai




## **Summary**

**Personalities**

- Great self-motivation
- Being visionary and open-minded
- Enthusiastic pursuer of challenges
- Lead-by-example leadership style


**General Interests**

- any system/device involving electrical/electronic/computer engineering, 
- the industrial trend towards an ever more digital and software-defined future.


**Leadership Experience**

- successful startup of a hi-tech venture 
- **11+ years of global engineering team management**
  - quick startup of a high-profile team
  - high talent retention
  - high quality projects & accomplishments
  - widely acknowledged leadership from within and outside of the team


**Technical Experience**

- **18+ R&D-centric years**
  - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
  - programming with C++/Python/C#/JavaScript/Go/Matlab 
  - schematic&PCB design and simulation with Protel/Altium/Matlab/PSpice
  - Industrial PC, PLC and MCU


- **15+ professional years of service in World's top companies**
  - industrial automation, electronics& machinery manufacturing
  - cross-team collaboration with production, pre/after sales&service teams




## **Experience**
### **Team Leader/Founder, Global Development Engineering Department**

- **Reported to Director of Global Development Engineering(HQ in Canada)**
- **[Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016**
  
  Helped to start up from the scratch the manufacturing capacity of injection molding machines at Shanghai campus, and made [**Significant Contribution** acknowledged by the then Global Vice President of Machines](./pics/husky-contribution.jpg).

  Thereafter, started up the one-and-only offshore development engineering team, working hand-in-hand with Dev Eng teams in the HQ daily, through email, audio/video conference calls and mutual visits, in support of Husky's business around the world.

  The headcount grew from 5 to 12 in 2 years due to exceptional team performance.

  **Starting from 2014, focused on advocating Virtualization/DevOps/Cloud/Big Data/Mobile technologies inside the company, towards Industry 4.0/IIoT solutions, leveraging Iaas/Paas/SaaS offerings from Amazon AWS/ Microsoft Azure, e.g. AWS IoT, Azure IoT Hub etc.**

  **As the founder&top regional leader of the team, ultimately responsible for Shanghai team with direct reports to the headquarter**.

  - people management
    - talent acquisition&retention
    - team motivation/training etc.

  - project management
    - collaboration/prioritization of milestones with global/local engineering teams
    - engineering discipline/standards enforcement
    - regional R&D interface to production/service teams et al. 

  - evaluation and evangelism of new technologies and methodologies
  - budgets forecast&control

  **Being widely acknowledged as a model team, I was invited to share experience on global collaboration by other teams, such as Global Production.**
  

  Various levels of software development from real-time motion control to enterprise solutions, along with electronic/electrical/mechanical tasks had been accomplished under my leadership, covering

  - Human-Machine interfaces(HMI)
  - TWinCAT based motion control(PLC)
  - integration of 3rd auxiliary systems, e.g. ABB robot, Plastic Systems resin dryers, Altanium temperature/servo controllers
  - Manufacturing execution systems (MES) and manufacturing monitoring/management(SCADA) 


 for platforms including

  - Windows Embedded/Server
  - Linux(Fedora, Ubuntu)
  - Mobile(iPhone) 
  - Atmel MCUs
  - Microsoft Azure / Amazon AWS 


  with languages such as

  - C++
  - C#
  - Visual Basic
  - Python
  - Ruby
  - Java
  - JavaScript
  - IEC61131 PLC programming languages etc.

  and Industrial standards/protocols such as 
  
  - OPC(OLE for Process Control)
  - EtherCAT/Realtime Ethernet
  - Sercos
  - Profibus
  - Euromap
  - CAN
  - SPI etc.


  **Major Achievements**

  - led the merge of [Moldflow][#BUYMF]
  - led [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx)
  - led Polaris(HMI, PLC and Production System)

  - **Significant Contribution Award**, 2007
    - Signed by the then Global Vice President of Machines
    - In recognition of contributions during the startup and first profitable year of Husky's Shanghai Machines plant


  - **Top-Ranked Team Leader**, 2011
    - Husky's global employee survey conducted by [Kenexa][#KENEXA]


  - **10-Year Service Award**, 2015


[**See Feature Projects >>**](https://ttan.netlify.com/projects-en)




### **Senior Application Engineer/Interim Team Leader, Memory Competence Center**

- **Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France)**
- **[STMicroelectronics][#ST], 9/2003 - 11/2005**

  A technical liaison connecting Central R&D IC designers and A/P Field Application Engineering, involving 
  
  - electronic Schematic/PCB layout, 
  - software/firmware coding  
  - application notes authoring 
  
  for embedded/desktop systems as requested/required.

  Being a startup member and interim leader for the Serial Non-Volatile Memory & Embedded System Solutions Team, assumed the responsibility to **explore and build** the connection with the R&D Division back in France, and maintained the effective communication through trustworthy relationship.

  More specifically,

  - Assisted IC designers in France
    - IC chip verification of prototyping samples to ensure all specifications are met according to datasheets, e.g Signal-to-noise ratio(SNR), signal glitches etc.
    - authoring in English application notes for each product family

  - Software/hardware reference designs showcasing application scenarios
    - PCB design with Altium
    - Firmware driver code(in Assembly/C) for
      - M50 Firmware Hub/LPC BIOS Memory chips
      - M25PE/M25P/M45PE SPI code/data flash&EEPROM memory chips
    - Porting popular OSes to STR71x(uCLinux, µC/OS-II)
    - USB based memory chips programming kit(in C/C++/Assembly) for PC/Windows

  - Development of Anatidae, an in-house tracking system to synchronize projects between Shanghai, Czech, Italy and France, using MFC/STL/BOOST.

  Being the best tool in class, the aforementioned memory programmer kit was later licensed by my then-colleagues for a startup company named [Dediprog Inc.][#DP], branded as SF100/SF200/SF300 series. I was the _interim CTO_ during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards.




### **Electronic Design Engineer, China Design Center**

- **Reported to Manager of China Design Center(French Expat)**
- **[Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003**
  
  As one of the regional corporate R&D staff, working closely with the lab technicians, with a focus on improving and innovating industrial control&automation(ICA) products&solutions, including

  - electrical/electronic products design
    - localization of global products
    - new products targeting Chinese market only
  - premium technical support for joint ventures(JVs)
    - Homologation tests for JVs
  - hardware/software development of lab equipments/utilities

 Achievements included

  - Auto-Transfer System(ATS), an automatic switches on/off between two independent power supplies in no time in case of emergency for sectors like hospitals
    - based on NS Compact circuit breakers and NEZA PLC.
    - from the scratch to 3 full-functional prototypes
    - 1/3 cost saving from existing market solutions
    - submission to Chinese Management Committee(CMC) for final production investment evaluation 
  - Co-design of a thermostat for IEC regulated high-temperature(up to 75&#8451;) aging tests
  - Developed software data analytics utilities for experiments in VB6/C++/MATLAB
  - Localization design of ASICs for circuit breakers
    - VC65
    - DPN Vigi
  - preliminary investigation of local manufacturing of switching power supplies from Poland, including reports on performance/reliability/cost comparison with local brands.
  - Compact NS circuit breakers homologation tests for China Certification.
  - Development of industrial-strength LED alarms




### **Researcher, HIT Research Institute of Electrical Apparatus **

- **Reported to Head of Institute / Mentor Professor of My Postgraduate Study**
- **[Harbin Institute of Technology][#HIT], 4/1998 - 3/2002**

  This was a technical center/lab, founded&led by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.

  My period in the lab started when I was a junior and volunteered to clean up desk(soldering iron etc.) and ended when I graduated, with a Master Degree of Electrical Engineering, from the postgraduate School and left for Shanghai.

  **It could never be emphasized enough what a firm technical foundation as well as long lasting interests in science and technology this 4 years laid for my later career till today.**

  Both my theses for the Bachelor&Master degrees and several published academic papers were inspired from my research and development activities in the lab.

  My interests and yields included

  - Research on industrial control and automation system
    - Electrical machines and Apparatus
      - transformers, generators and (servo) motors(Rockwell Automation brands)
      - relays, contactors and circuit breakers
    - Computer-based control&automation systems
      - Direct Digital Control System
      - Distributed Control System (DCS)
      - Fieldbus Control system (FCS)
    - real-time control & measurement methodology and reliability, e.g. FMEA
    - real-time embedded system (RTOS) and embedded application development
    - Research on electrical/electronic interface/bus protocols(ISA, PCI etc.)
  - Development of control & automation solutions
    - based on industrial PCs(IPC) and/or MCUs(Intel MCS-51 family)
    - HMI development in C++/Visual Basic
    - device driver development in Assembly/C/C++

    - Electrical schematic&PCB design with Protel, and simulation with pSPICE/MATLAB/SIMULINK
    - mechanical design(cam wheel based vibration platforms) with AutoCAD


  **Most notably, 10+ sets of the following sytems I developed were sold, having generated sales of over 1 million RMB.**

  - The Electronic Ballast Test Platform 
  - The Low voltage Apparatus Automatic Maintenance Platform


  **Awards**

  - Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
  - Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
  - An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*.




## **Education**

- **Master**, _summa cum laude_
  - **Electrical Engineering**, 09/1999 - 03/2002
  - [Harbin Institute of Technology][#HIT], Harbin

- **Bachelor**, _summa cum laude_
  - **Electrical Engineering**, 09/1995 - 07/1999
  - [Harbin Institute of Technology][#HIT], Harbin

- **Paper**

  - 2 papers published in pursuit of the Master degree
    - Zhai Guofu, Tan Zhi, et al. The development of integral testing equipment for electronic ballasts and inverters used in trains. *Low Voltage Apparatus* 
    - Zhai Guofu, Tan Zhi, et al. Research on a test system of performance&reliability of rolling-stock circuit breakers&contactors. *The 8th International Low Voltage Apparatus Reliability Conference*


- **Awards & Social Activities**
  - Awardee(multiple times), Award of People's Scholarship, 1995 - 1999
  - Member, Students' Union, College of Electrical Engineering and Computer Science, 1996 - 1997
  - Member, Students' Union, Harbin Institute of Technology, 1995 - 1996
  - Volunteer, the 8th International Conference for Northern Cities, Harbin, 1/1998




## **Languages**

- Chinese: Native
- English: Fluent

## **Skills**

- Management
  - successful startup of a hi-tech venture in Shanghai/Taiwan 
  - successful startup of global engineering teams in Shanghai
  - team & project management with high retention rate
  - Top-ranked team leader in a global employee survey by [Kenexa][#KENEXA]


- Technologies

  - Industrial Standards

    - OPC(OLE for Process Control)
    - EtherCAT/Realtime Ethernet
    - Sercos
    - Profibus
    - Euromap
    - CAN
    - SPI etc.

  - Hardware
    - Schematic & PCB design with Protel/Altium, 
      - A/D, D/A and I/O and other peripheral circuits
      - MCU(MCS51, ARM, ST7)
      - PC(ISA/PCI) architectures.
    - knowledge of Verilog HDL.
    - Simulation with Matlab, pSPICE
    - Industrial PC, PLC, softPLC(CodeSys/TwinCAT)
    - wide-range current power supply(1A -1200A) design


  - Software
    - Open Source software(OSS) contributor to 
        - [BOOST][#BOOST_REVIEWER], 
        - [POCO][#POCO_CONTRIBUTOR] 
        - [The Code Project][#CP_AUTHOR]
    - Programming Languages
        - frequent use of C++(esp. C++ 11/14/17), Python, C#, JavaScript/HTML/CSS
        - occasional Golang, R, Matlab, ruby, TypeScript etc.
    - Application
        - IC chip drivers
        - realtime motion control(e.g servo motor, robotics)
        - desktop GUI/CLI applications
        - server/web based enterprise LOB(line of business)
        - cloud based mobile/IoT solutions
    - Platforms
        - Windows desktop, Windows Embedded/Windows Mobile, Windows Server
        - Windows Azure, AWS
        - Linux(Ubuntu/Fedora)
        - uC/OS, eCos and uCLinux
    - Tools&Library
        - Microsoft Team Foundation System(TFS)/Microsoft Visual Studio
        - Keil, ARM RealView 
        - SQL Server, MongoDB, SQLite 
        - .NET, ASP.NET, WinForm, WPF, UWP
        - MFC/WTL, QT, STL/BOOST, ACE
        - Dojo Toolkit, AngularJS
        - NodeJS, Django, Flask 
        - Cordova, Xamarian
        - Git, Mercurial(HG), SVN
        - Vagrant, Ansible
    - Methodology
        - Object Oriented Programming(OOP), generic programming/metaprogramming(GP), functional programming 
        - Agile/SCRUM/TDD




## **Trainings**

- **The 7 Habits of Highly Effective People Training Program**
  - 3 full days, 3/2008
  - At Shanghai, China
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Franklin Covey Co.](http://www.franklincovey.com)


- **The 4 Disciplines of Execution: Manager Certification**
  - 3 full days, 3/2007
  - At Shanghai, China
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Franklin Covey Co.](http://www.franklincovey.com)


- **Team Leadership development Training Program**
  - 1 full week, 01/2006
  - At Bolton, Canada
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Workplace Competence International Limited](https://www.linkedin.com/company/674972)


- **Product training on Serial Non-volatile Memory Chips**
  - 2 full weeks, 05/2004
  - At Rousset, France
  - Sponsored by STMicroelectronics
  - Conducted by Serial Flash Division, [STMicroelectronics][#ST]
  - Covering Serial EEPROM, Serial Flash, BIOS Flash, Contactless Memories


- **Cambridge English: Business (BEC)**, _Vantage_ through _Higher_
  - Every weekend throughout 1 year,  2002-2003
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Conducted by [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan)


- **Creative Thinking and Problem Solving**,
  - 1 weekend, 07/2002
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Conducted by [Key Consulting][#KEYCONSULTING]


- **Intellectual Property & Patents**，
  - 1 day, 06/2002
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Retired official of [Shanghai Intellectual Property Administration (SIPA)][#SIPA]

[#SIPA]: http://www.sipa.gov.cn/gb/zscq/zscqjel/index.html




## **Additional Information**

- **Contributions**
  - [Boost C++ Libraries](http://www.boost.org/)

    Reviewer of [Boost.Chrono][#BOOST_REVIEWER], a reference design of std::chrono in the standard C++ library.

  - [POCO C++ Libraries](http://pocoproject.org/)

    Contributed [Code][#POCO_CONTRIBUTOR] 

  - [The CodeProject](http://www.codeproject.com/)

    Authored [a few articals with code][#CP_AUTHOR]
  - [Github](https://github.com/rockonedege)
    Maintained [a few open source code](https://github.com/rockonedege)


- **When Not At Work**

  - **Family**
  - Reading
  - Running/Half-Marathon/Cycling
  - Travel


- **Web Pages**

  - [CV in English](https://ttan.netlify.com/cv-en)(**this document**)
  - [CV in Chinese](https://ttan.netlify.com/cv-zh)
  - [Feature Projects in English](https://ttan.netlify.com/projects-en)
  - [Feature Projects in Chinese](https://ttan.netlify.com/projects-zh)
  - [Linkedin Profile](www.linkedin.com/in/zhitan)


[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/




<hr class="page_break" />

## **Appendix**


- ![](./pics/husky-contribution.jpg)
- ![](./pics/husky-leadership.jpg)
- ![](./pics/kenexa-1.jpg)
- ![](./pics/kenexa-2.jpg)
- ![](./pics/mbti.istj-2.jpg)
- ![](./pics/unicef.jpg)
- ![](./pics/shanghai-half-marathon-2008.jpg)

-----
<small>Generated from <a href="http://daringfireball.net/projects/markdown/">Markdown</a> text using my open source tool <a href="https://github.com/rockonedege/markdown-cv">markdown-cv</a>. Last updated at 2018-01-11. </small>