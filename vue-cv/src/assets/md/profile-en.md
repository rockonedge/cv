---
banner:
  brand: '<a class=''navbar-brand'' href=''#top''>CV</a> <a class=''navbar-brand''
    href=''#profile-en-1''>Projects</a> '
  lastupdate: <a class="btn-link" href="https://tslides.netlify.com/cv">SLIDES</a>
    &middot; <a class="btn-link" href="./pdf/profile-en.pdf">PDF</a> &middot; 2018-01-11
  nav: <li class='nav-item active'><a class='nav-link' href='#profile-en-1'>Husky</a></li>
    <li class='nav-item'><a class='nav-link' href='#profile-en-2'>ST</a></li> <li
    class='nav-item'><a class='nav-link' href='#profile-en-3'>Schneider Electric</a></li>
    <li class='nav-item'><a class='nav-link' href='#profile-en-4'>HIT</a></li> <li
    class='nav-item'><a class='nav-link' href='#profile-en-5'>Spare Time</a></li>
  time: 2018-01-11 17:56:05.074172
  title: 'CV Projects '

---
<div id="md-toc">
- [**Tom Tan**](#header-1-1)
  - [**Objective**](#header-2-2)
  - [**Summary**](#header-2-3)
  - [**Experience**](#header-2-4)
    - [**Team Leader/Founder, Global Development Engineering Department**](#header-3-5)
    - [**Senior Application Engineer/Interim Team Leader, Memory Competence Center**](#header-3-6)
    - [**Electronic Design Engineer, China Design Center**](#header-3-7)
    - [**Researcher, HIT Research Institute of Electrical Apparatus **](#header-3-8)
  - [**Education**](#header-2-9)
  - [**Languages**](#header-2-10)
  - [**Skills**](#header-2-11)
  - [**Trainings**](#header-2-12)
  - [**Additional Information**](#header-2-13)
  - [**Appendix**](#header-2-14)
- [Husky](#header-1-15)
  - [List of Achievements](#header-2-16)
  - [Polaris Injection Control System](#header-2-17)
    - [**Overview**](#header-3-18)
    - [Polaris HMI](#header-3-19)
    - [Polaris PLC](#header-3-20)
    - [Integration of 3rd auxiliaries](#header-3-21)
  - [Polaris HkLabel](#header-2-22)
    - [Polaris Production System](#header-3-23)
    - [Polaris Mold ID Server](#header-3-24)
  - [Polaris HkResource Refactor](#header-2-25)
    - [Polaris Translation System](#header-3-26)
  - [Bianque](#header-2-27)
    - [Peeks of some screenshots](#header-3-28)
  - [Hypersync-Altanium Industrie 4.0 Features](#header-2-29)
  - [HyMET Heatlogger](#header-2-30)
  - [Shotscope, Celltrack & Matrix](#header-2-31)
  - [Shotscope NX](#header-2-32)
  - [Shanghai Factory Startup](#header-2-33)
  - [Whiteboard](#header-2-34)
  - [Product Launch Team(PLT) Trips](#header-2-35)
- [STMicroelectronics](#header-1-36)
  - [Porting Embedded Operating Systems,](#header-2-37)
  - [Driver and Application Demos for Flash Memories](#header-2-38)
  - [Memory Competence Center(MMCC) Project Tracking System](#header-2-39)
  - [Serial Flash Programmer Tool Kit](#header-2-40)
- [Schneider Electric](#header-1-41)
  - [Automatic Power Transfer System](#header-2-42)
  - [Development of industrial-strength LED alarms](#header-2-43)
- [Harbin Institute of Technology](#header-1-44)
  - [Automatic Test Platform for Rolling Stock Apparatus](#header-2-45)
  - [Auto Test Platform for Electronic Ballast](#header-2-46)
- [Spare-time Projects](#header-1-47)
  - [Markdown-CV](#header-2-48)
  - [2D Water Effect in WTL](#header-2-49)
  - [A Literal Converter for Integers](#header-2-50)
  - [TwinCAT TSM File Viewer](#header-2-51)
  - [Multi-Ping](#header-2-52)
  - [HostLinkpp - An Implementation of HostLink inter-machine Protocol](#header-2-53)
  - [Console3](#header-2-54)
  - [Beckhoff TwinCAT ADS Protocol](#header-2-55)
</div>
<hr id="end-of-toc" />







**[tom.tan@live.com](mailto:tom.tan@live.com) &middot; (+86) 135-248-26926 &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **R&D Managemnt**

# <a id="header-1-1" class="md-header-anchor"></a>**Tom Tan**<a class="back-to-top-link" href="#top">&#x27B6;</a>


>- 11+ years of Acknowledged Global Projects&People Leadership
>- 15+ years of Multi-sector R&D Career in World's top companies across Automation, Electronics and Machinery
>- 19+ years of Hands-on Software/Firmware/Hardware Development


>- Started up A Global Venture([Dedirpog Inc.](https://www.dediprog.com/US)) 
>- Started up Global Development & Production Engineering Teams
>- [Acknowledged contribution](./pics/husky-contribution.jpg) to launch&profit of a global manufacturing factory  



>- Challenges Pursuer, Visionary and Open-minded
>- Deeply rooted in Science, Technology, Engineering and Math(STEM) Related
>- A Regular [UNICEF](http://www.unicef.cn) Donator


## <a id="header-2-2" class="md-header-anchor"></a>**Objective**<a class="back-to-top-link" href="#top">&#x27B6;</a>


A Senior Manager/Director position, preferably

- Engineering teams startup&leadership
- Industrial, Scientific, Medical(ISM) or Automotive sectors
- IoT/IIoT/Industry 4.0 innovations 
- A global company Based in Shanghai




## <a id="header-2-3" class="md-header-anchor"></a>**Summary**<a class="back-to-top-link" href="#top">&#x27B6;</a>


**Personalities**

- Great self-motivation
- Being visionary and open-minded
- Enthusiastic pursuer of challenges
- Lead-by-example leadership style


**General Interests**

- any system/device involving electrical/electronic/computer engineering, 
- the industrial trend towards an ever more digital and software-defined future.


**Leadership Experience**

- successful startup of a hi-tech venture 
- **11+ years of global engineering team management**
  - quick startup of a high-profile team
  - high talent retention
  - high quality projects & accomplishments
  - widely acknowledged leadership from within and outside of the team


**Technical Experience**

- **18+ R&D-centric years**
  - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
  - programming with C++/Python/C#/JavaScript/Go/Matlab 
  - schematic&PCB design and simulation with Protel/Altium/Matlab/PSpice
  - Industrial PC, PLC and MCU


- **15+ professional years of service in World's top companies**
  - industrial automation, electronics& machinery manufacturing
  - cross-team collaboration with production, pre/after sales&service teams




## <a id="header-2-4" class="md-header-anchor"></a>**Experience**<a class="back-to-top-link" href="#top">&#x27B6;</a>

### <a id="header-3-5" class="md-header-anchor"></a>**Team Leader/Founder, Global Development Engineering Department**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **Reported to Director of Global Development Engineering(HQ in Canada)**
- **[Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016**
  
  Helped to start up from the scratch the manufacturing capacity of injection molding machines at Shanghai campus, and made [**Significant Contribution** acknowledged by the then Global Vice President of Machines](./pics/husky-contribution.jpg).

  Thereafter, started up the one-and-only offshore development engineering team, working hand-in-hand with Dev Eng teams in the HQ daily, through email, audio/video conference calls and mutual visits, in support of Husky's business around the world.

  The headcount grew from 5 to 12 in 2 years due to exceptional team performance.

  **Starting from 2014, focused on advocating Virtualization/DevOps/Cloud/Big Data/Mobile technologies inside the company, towards Industry 4.0/IIoT solutions, leveraging Iaas/Paas/SaaS offerings from Amazon AWS/ Microsoft Azure, e.g. AWS IoT, Azure IoT Hub etc.**

  **As the founder&top regional leader of the team, ultimately responsible for Shanghai team with direct reports to the headquarter**.

  - people management
    - talent acquisition&retention
    - team motivation/training etc.

  - project management
    - collaboration/prioritization of milestones with global/local engineering teams
    - engineering discipline/standards enforcement
    - regional R&D interface to production/service teams et al. 

  - evaluation and evangelism of new technologies and methodologies
  - budgets forecast&control

  **Being widely acknowledged as a model team, I was invited to share experience on global collaboration by other teams, such as Global Production.**
  

  Various levels of software development from real-time motion control to enterprise solutions, along with electronic/electrical/mechanical tasks had been accomplished under my leadership, covering

  - Human-Machine interfaces(HMI)
  - TWinCAT based motion control(PLC)
  - integration of 3rd auxiliary systems, e.g. ABB robot, Plastic Systems resin dryers, Altanium temperature/servo controllers
  - Manufacturing execution systems (MES) and manufacturing monitoring/management(SCADA) 


 for platforms including

  - Windows Embedded/Server
  - Linux(Fedora, Ubuntu)
  - Mobile(iPhone) 
  - Atmel MCUs
  - Microsoft Azure / Amazon AWS 


  with languages such as

  - C++
  - C#
  - Visual Basic
  - Python
  - Ruby
  - Java
  - JavaScript
  - IEC61131 PLC programming languages etc.

  and Industrial standards/protocols such as 
  
  - OPC(OLE for Process Control)
  - EtherCAT/Realtime Ethernet
  - Sercos
  - Profibus
  - Euromap
  - CAN
  - SPI etc.


  **Major Achievements**

  - led the merge of [Moldflow][#BUYMF]
  - led [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx)
  - led Polaris(HMI, PLC and Production System)

  - **Significant Contribution Award**, 2007
    - Signed by the then Global Vice President of Machines
    - In recognition of contributions during the startup and first profitable year of Husky's Shanghai Machines plant


  - **Top-Ranked Team Leader**, 2011
    - Husky's global employee survey conducted by [Kenexa][#KENEXA]


  - **10-Year Service Award**, 2015


[**See Feature Projects >>**](https://ttan.netlify.com/projects-en)




### <a id="header-3-6" class="md-header-anchor"></a>**Senior Application Engineer/Interim Team Leader, Memory Competence Center**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France)**
- **[STMicroelectronics][#ST], 9/2003 - 11/2005**

  A technical liaison connecting Central R&D IC designers and A/P Field Application Engineering, involving 
  
  - electronic Schematic/PCB layout, 
  - software/firmware coding  
  - application notes authoring 
  
  for embedded/desktop systems as requested/required.

  Being a startup member and interim leader for the Serial Non-Volatile Memory & Embedded System Solutions Team, assumed the responsibility to **explore and build** the connection with the R&D Division back in France, and maintained the effective communication through trustworthy relationship.

  More specifically,

  - Assisted IC designers in France
    - IC chip verification of prototyping samples to ensure all specifications are met according to datasheets, e.g Signal-to-noise ratio(SNR), signal glitches etc.
    - authoring in English application notes for each product family

  - Software/hardware reference designs showcasing application scenarios
    - PCB design with Altium
    - Firmware driver code(in Assembly/C) for
      - M50 Firmware Hub/LPC BIOS Memory chips
      - M25PE/M25P/M45PE SPI code/data flash&EEPROM memory chips
    - Porting popular OSes to STR71x(uCLinux, µC/OS-II)
    - USB based memory chips programming kit(in C/C++/Assembly) for PC/Windows

  - Development of Anatidae, an in-house tracking system to synchronize projects between Shanghai, Czech, Italy and France, using MFC/STL/BOOST.

  Being the best tool in class, the aforementioned memory programmer kit was later licensed by my then-colleagues for a startup company named [Dediprog Inc.][#DP], branded as SF100/SF200/SF300 series. I was the _interim CTO_ during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards.




### <a id="header-3-7" class="md-header-anchor"></a>**Electronic Design Engineer, China Design Center**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **Reported to Manager of China Design Center(French Expat)**
- **[Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003**
  
  As one of the regional corporate R&D staff, working closely with the lab technicians, with a focus on improving and innovating industrial control&automation(ICA) products&solutions, including

  - electrical/electronic products design
    - localization of global products
    - new products targeting Chinese market only
  - premium technical support for joint ventures(JVs)
    - Homologation tests for JVs
  - hardware/software development of lab equipments/utilities

 Achievements included

  - Auto-Transfer System(ATS), an automatic switches on/off between two independent power supplies in no time in case of emergency for sectors like hospitals
    - based on NS Compact circuit breakers and NEZA PLC.
    - from the scratch to 3 full-functional prototypes
    - 1/3 cost saving from existing market solutions
    - submission to Chinese Management Committee(CMC) for final production investment evaluation 
  - Co-design of a thermostat for IEC regulated high-temperature(up to 75&#8451;) aging tests
  - Developed software data analytics utilities for experiments in VB6/C++/MATLAB
  - Localization design of ASICs for circuit breakers
    - VC65
    - DPN Vigi
  - preliminary investigation of local manufacturing of switching power supplies from Poland, including reports on performance/reliability/cost comparison with local brands.
  - Compact NS circuit breakers homologation tests for China Certification.
  - Development of industrial-strength LED alarms




### <a id="header-3-8" class="md-header-anchor"></a>**Researcher, HIT Research Institute of Electrical Apparatus **<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **Reported to Head of Institute / Mentor Professor of My Postgraduate Study**
- **[Harbin Institute of Technology][#HIT], 4/1998 - 3/2002**

  This was a technical center/lab, founded&led by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.

  My period in the lab started when I was a junior and volunteered to clean up desk(soldering iron etc.) and ended when I graduated, with a Master Degree of Electrical Engineering, from the postgraduate School and left for Shanghai.

  **It could never be emphasized enough what a firm technical foundation as well as long lasting interests in science and technology this 4 years laid for my later career till today.**

  Both my theses for the Bachelor&Master degrees and several published academic papers were inspired from my research and development activities in the lab.

  My interests and yields included

  - Research on industrial control and automation system
    - Electrical machines and Apparatus
      - transformers, generators and (servo) motors(Rockwell Automation brands)
      - relays, contactors and circuit breakers
    - Computer-based control&automation systems
      - Direct Digital Control System
      - Distributed Control System (DCS)
      - Fieldbus Control system (FCS)
    - real-time control & measurement methodology and reliability, e.g. FMEA
    - real-time embedded system (RTOS) and embedded application development
    - Research on electrical/electronic interface/bus protocols(ISA, PCI etc.)
  - Development of control & automation solutions
    - based on industrial PCs(IPC) and/or MCUs(Intel MCS-51 family)
    - HMI development in C++/Visual Basic
    - device driver development in Assembly/C/C++

    - Electrical schematic&PCB design with Protel, and simulation with pSPICE/MATLAB/SIMULINK
    - mechanical design(cam wheel based vibration platforms) with AutoCAD


  **Most notably, 10+ sets of the following sytems I developed were sold, having generated sales of over 1 million RMB.**

  - The Electronic Ballast Test Platform 
  - The Low voltage Apparatus Automatic Maintenance Platform


  **Awards**

  - Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
  - Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
  - An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*.




## <a id="header-2-9" class="md-header-anchor"></a>**Education**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **Master**, _summa cum laude_
  - **Electrical Engineering**, 09/1999 - 03/2002
  - [Harbin Institute of Technology][#HIT], Harbin

- **Bachelor**, _summa cum laude_
  - **Electrical Engineering**, 09/1995 - 07/1999
  - [Harbin Institute of Technology][#HIT], Harbin

- **Paper**

  - 2 papers published in pursuit of the Master degree
    - Zhai Guofu, Tan Zhi, et al. The development of integral testing equipment for electronic ballasts and inverters used in trains. *Low Voltage Apparatus* 
    - Zhai Guofu, Tan Zhi, et al. Research on a test system of performance&reliability of rolling-stock circuit breakers&contactors. *The 8th International Low Voltage Apparatus Reliability Conference*


- **Awards & Social Activities**
  - Awardee(multiple times), Award of People's Scholarship, 1995 - 1999
  - Member, Students' Union, College of Electrical Engineering and Computer Science, 1996 - 1997
  - Member, Students' Union, Harbin Institute of Technology, 1995 - 1996
  - Volunteer, the 8th International Conference for Northern Cities, Harbin, 1/1998




## <a id="header-2-10" class="md-header-anchor"></a>**Languages**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- Chinese: Native
- English: Fluent

## <a id="header-2-11" class="md-header-anchor"></a>**Skills**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- Management
  - successful startup of a hi-tech venture in Shanghai/Taiwan 
  - successful startup of global engineering teams in Shanghai
  - team & project management with high retention rate
  - Top-ranked team leader in a global employee survey by [Kenexa][#KENEXA]


- Technologies

  - Industrial Standards

    - OPC(OLE for Process Control)
    - EtherCAT/Realtime Ethernet
    - Sercos
    - Profibus
    - Euromap
    - CAN
    - SPI etc.

  - Hardware
    - Schematic & PCB design with Protel/Altium, 
      - A/D, D/A and I/O and other peripheral circuits
      - MCU(MCS51, ARM, ST7)
      - PC(ISA/PCI) architectures.
    - knowledge of Verilog HDL.
    - Simulation with Matlab, pSPICE
    - Industrial PC, PLC, softPLC(CodeSys/TwinCAT)
    - wide-range current power supply(1A -1200A) design


  - Software
    - Open Source software(OSS) contributor to 
        - [BOOST][#BOOST_REVIEWER], 
        - [POCO][#POCO_CONTRIBUTOR] 
        - [The Code Project][#CP_AUTHOR]
    - Programming Languages
        - frequent use of C++(esp. C++ 11/14/17), Python, C#, JavaScript/HTML/CSS
        - occasional Golang, R, Matlab, ruby, TypeScript etc.
    - Application
        - IC chip drivers
        - realtime motion control(e.g servo motor, robotics)
        - desktop GUI/CLI applications
        - server/web based enterprise LOB(line of business)
        - cloud based mobile/IoT solutions
    - Platforms
        - Windows desktop, Windows Embedded/Windows Mobile, Windows Server
        - Windows Azure, AWS
        - Linux(Ubuntu/Fedora)
        - uC/OS, eCos and uCLinux
    - Tools&Library
        - Microsoft Team Foundation System(TFS)/Microsoft Visual Studio
        - Keil, ARM RealView 
        - SQL Server, MongoDB, SQLite 
        - .NET, ASP.NET, WinForm, WPF, UWP
        - MFC/WTL, QT, STL/BOOST, ACE
        - Dojo Toolkit, AngularJS
        - NodeJS, Django, Flask 
        - Cordova, Xamarian
        - Git, Mercurial(HG), SVN
        - Vagrant, Ansible
    - Methodology
        - Object Oriented Programming(OOP), generic programming/metaprogramming(GP), functional programming 
        - Agile/SCRUM/TDD




## <a id="header-2-12" class="md-header-anchor"></a>**Trainings**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **The 7 Habits of Highly Effective People Training Program**
  - 3 full days, 3/2008
  - At Shanghai, China
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Franklin Covey Co.](http://www.franklincovey.com)


- **The 4 Disciplines of Execution: Manager Certification**
  - 3 full days, 3/2007
  - At Shanghai, China
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Franklin Covey Co.](http://www.franklincovey.com)


- **Team Leadership development Training Program**
  - 1 full week, 01/2006
  - At Bolton, Canada
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Workplace Competence International Limited](https://www.linkedin.com/company/674972)


- **Product training on Serial Non-volatile Memory Chips**
  - 2 full weeks, 05/2004
  - At Rousset, France
  - Sponsored by STMicroelectronics
  - Conducted by Serial Flash Division, [STMicroelectronics][#ST]
  - Covering Serial EEPROM, Serial Flash, BIOS Flash, Contactless Memories


- **Cambridge English: Business (BEC)**, _Vantage_ through _Higher_
  - Every weekend throughout 1 year,  2002-2003
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Conducted by [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan)


- **Creative Thinking and Problem Solving**,
  - 1 weekend, 07/2002
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Conducted by [Key Consulting][#KEYCONSULTING]


- **Intellectual Property & Patents**，
  - 1 day, 06/2002
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Retired official of [Shanghai Intellectual Property Administration (SIPA)][#SIPA]

[#SIPA]: http://www.sipa.gov.cn/gb/zscq/zscqjel/index.html




## <a id="header-2-13" class="md-header-anchor"></a>**Additional Information**<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **Contributions**
  - [Boost C++ Libraries](http://www.boost.org/)

    Reviewer of [Boost.Chrono][#BOOST_REVIEWER], a reference design of std::chrono in the standard C++ library.

  - [POCO C++ Libraries](http://pocoproject.org/)

    Contributed [Code][#POCO_CONTRIBUTOR] 

  - [The CodeProject](http://www.codeproject.com/)

    Authored [a few articals with code][#CP_AUTHOR]
  - [Github](https://github.com/rockonedege)
    Maintained [a few open source code](https://github.com/rockonedege)


- **When Not At Work**

  - **Family**
  - Reading
  - Running/Half-Marathon/Cycling
  - Travel


- **Web Pages**

  - [CV in English](https://ttan.netlify.com/cv-en)(**this document**)
  - [CV in Chinese](https://ttan.netlify.com/cv-zh)
  - [Feature Projects in English](https://ttan.netlify.com/projects-en)
  - [Feature Projects in Chinese](https://ttan.netlify.com/projects-zh)
  - [Linkedin Profile](www.linkedin.com/in/zhitan)


[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/




<hr class="page_break" />

## <a id="header-2-14" class="md-header-anchor"></a>**Appendix**<a class="back-to-top-link" href="#top">&#x27B6;</a>



- ![](./pics/husky-contribution.jpg)
- ![](./pics/husky-leadership.jpg)
- ![](./pics/kenexa-1.jpg)
- ![](./pics/kenexa-2.jpg)
- ![](./pics/mbti.istj-2.jpg)
- ![](./pics/unicef.jpg)
- ![](./pics/shanghai-half-marathon-2008.jpg)


<hr class="page_break" id="profile-en-1" />






# <a id="header-1-15" class="md-header-anchor"></a>Husky<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-16" class="md-header-anchor"></a>List of Achievements<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005 - 2016**

  - Instrumental during the startup of Shanghai Machines plant, from the scratch

    - Replicated **Polaris Production System** from Bolton to Shanghai 
    - Helped starting up the **production engineering team** 

  - Started up a first offshore **global development engineering team**, all with Master of Science/Engineering degrees, and had successfully managed it for 11+ years.
  - Led the development on controls platform for all Husky injection machines, consisting of 
    -  2 Polaris HMI platforms
      - COM/VB6 HMI platform for legacy generations of injection machines
      - re-Architecture and re-implementation of a .NET/C# HMI platform for later generations of injection machines
      - value-added modules, e.g.
        - development of Statistical Quality Control (SQC) and Statistical Process Control (SPC) module
      - hostlink, a proprietary communication protocol to talk to Husky machines remotely 
    - Polaris PLC 
      - **PLC programming for real-time control** with TWinCAT/CODESYS technologies, such as
        - servo drives
        - robotics etc.
    - Polaris production system.
      - daily used by engineers across departments(DevEng, Production and Service etc.) , such as
        - Translation System, supporting 22+ languages for Husky software
        - Mold ID System, managing RuBee(IEEE 1902.1) based tags mounted onto Husky made molds
        - ...
    - **regular releases**(quarterly/semi-annually) of the aforementioned platforms for over a decade
  - Initiated **Bianque Production System**, a web-based replacement for Polaris Production System, leveraging Cloud-era technologies such as Microsoft Azure IoT Hub, REST, microservice, and mobile apps.
  - Led the development of Matrix, a Hot Runner Temperature Controller based on Linux/Fedora using QT/C++ and MySQL.
  - Instrumental in the takeover of software&hardware assets from French&US teams during [Husky buying MoldFlow manufacturing division](http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit) and subsequent customer retention.
  - Led the implementation of [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), an automated, real-time, web-based software&hardware hybrid solution for process and production monitoring.[
  - Led Industry 4.0 features introduced in the HyperSync machines, and won _Ringier Technology Innovation Award_](https://omnexus.specialchem.com/news/industry-news/husky-ringier-technology-innovation-award-000186265) .

  - Led the participation in the worldwide Product Launch Team(PLT), an ad-hoc top technical expert team to set up to-be-launched machines in the field for early adopter.

  - Fostered Agile methodologies such as SCRUM/Test-Driven Development(TDD) 
  - Helped the migration source control process from VSS to TFS.

  - Innovated on effective global teams collaboration&management process
    - developed the **Whiteboard**  website in spare time to synthesize info from various data source including 
      - Team Foundation Server
      - Visual SourceSafe
      - SharePoint
      - ERP/Oracle and 
      - UNC/mapped network drives

      leveraging 
      - MongoDB
      - Python/Django
      - Javascript/Dojo Toolkit
      - C#/ASP.NET/REST API
      - TFS/ ShairePoint SDKs etc.
      - Apache Cordova
      
      having saved time in routine meetings such as
        - daily standup meetings
        - weekly team meetings
        - milestone meetings etc. by quickly putting everyone on the same page
      - enabled team leaders to query project statuses within clicks.

## <a id="header-2-17" class="md-header-anchor"></a>Polaris Injection Control System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005 - 2016**

### <a id="header-3-18" class="md-header-anchor"></a>**Overview**<a class="back-to-top-link" href="#top">&#x27B6;</a>


Polaris is an umbrella name covering all development activities surrounding the general software platform. All software coming out of DevEng, unless specifically named(e.g. Shotscope NX), is regarded as part of Polaris by default.

Polaris to Husky is Windows to Microsoft, in that it's being developed, thus evolving, all the time and released regularly.  Any mechanical, electrical and software design change, or technology advances in the industry would pretty much likely result in changes in Polaris one way or another.

Roughly, Polaris consists of 

- Polaris HMI
- Polaris PLC 
- Integration of 3rd auxiliaries
- Polaris Production System

Polaris is an IPC-based software solution for controlling all Husky-made injection molding machines, written in C++, C#, VB6, PLC (IEC 61131) and Ruby among others.

All Polaris HMI&PLC features are wrapped into reusable and composable _modules_ like LEGOs, Polaris Production System 

- manages the 6K+ and growing modules and 
- generateds deployable packages(11K+), 

one for each machine, be it physically real or simulation for development purposes), so that each machine is equipped with a unique software package of HMI and PLC combined.  

**Suffice it to say, a big part of my time, as well as my team's in Husky, was spent directly or directly on this platform.**

It was Shanghai team's daily routine, under my leadership, to 

- pick tasks from the same pool and 
- follow the same schedules 
 
as peer development teams back in the HQ, with contributions to to the same code base with
- new features
- feature improvements
- bug fixes etc.

### <a id="header-3-19" class="md-header-anchor"></a>Polaris HMI<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006 - 2016**

A GUI/touch-screens enabled application running on Windows XP/7 Embedded with rich features enabling operators with an ultimate control 

- from cycle breakdowns to production jobs schedules. 

The UI is **configured** during package generation with the Production System per customer orders.

2 versions are maintained:

-  Legacy version

  - started around 2000
  - default for all product lines roughly between circa 2000 and 2007
    - being slow and corrupt-prone for being resource demanding as a result of accumulative abuse of system resources over the years.
  - coded in VB6 using COM technology, and source controlled by SourceSafe
  - running on Windows XP Embedded 


- Current version

  - started around 2007
  - default for all product lines starting from around 2008
  - coded in C# using .NET technology and modern Tools
    - A new architecture leveraging C#/.NET and with lessons(a lot of them!) from the previous version
      - fully OOP
      - decoupled layers for devices, domains and UI logics, with message-based communication
      - less dependent on TwinCAT opaque file formats
    - using new Application lifecycle management (ALM) tool and methodology
      - Microsoft Team Foundation System(TFS)
      - TDD/SCRUM process
  - running on Windows 7 Embedded

  I was wholly involved since the inception of the architecture of the .NET version, such as 

  - early concepts selection
    - e.g. meeting at Canada the representatives from Microsoft, IBM and McCabe and voting for the final choice of TFS
  - leading the year-long re-implementation of every VB6 screen in C#/.NET in Shanghai

### <a id="header-3-20" class="md-header-anchor"></a>Polaris PLC<a class="back-to-top-link" href="#top">&#x27B6;</a>


A TwinCAT based real-time program for 

- servo motors 
- sensors 
- actuators 

across the 

- injection
- clamping and 
- robotics systems, sending status to and receiving commands from the HMI.

Algorithms were written in IEC61131/ST Language and stored as modules per functionality, merged with the help of the Production system during machine packages generation.


### <a id="header-3-21" class="md-header-anchor"></a>Integration of 3rd auxiliaries<a class="back-to-top-link" href="#top">&#x27B6;</a>


It's more often than not that 3rd-party auxiliary equipments were requested to be integrated into Husky's HMI to 

- complete a whole production cycle/process
- facilitate the operators with a single one screen.

The task usually involved 

- data transfer through standard or proprietary(e.g. RS-485, digital I/O etc.) protocols 
- embedding screens into the Polaris HMI UI

examples included:

- ABB robots, 2006
- Resin Dryers from [Plastic Systems](http://plasticsystems.com/) 
- dehumidifier
- conveyers etc.

## <a id="header-2-22" class="md-header-anchor"></a>Polaris HkLabel<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006**

**This was the my first code check-in check-in at Husky.**

This project was to develop a replacement of the built-in Visual Basic Label control to 

- fix the truncation of text display due to extended text length caused by translation(e.g. from English to German) 
  - should show trailing ellipsis and pop up the full text when finger-clicked
  - should change the display when it's too long
- fix the overuse of GDI handles [hitting the 16K limit][#GDI] allowed by Windows XP.

Developed in C++ as a COM component. 

A companion utility was developed too to replace 1K+ occurrences of usages across the whole source code tree (hundreds of Kloc) at one go.

[#GDI]: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724291(v=vs.85).aspx


### <a id="header-3-23" class="md-header-anchor"></a>Polaris Production System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006 - 2016**

The Production System assembles 

- from 6k+ modules a package of the Platform System 
- specific to the machine configuration ordered by customers, and 
- delivers it to a target machine on the shopfloor for testing and shipping
- through network .

 A loosely-coupled collection of utilities, including CLI, GUI and web sites, for

- **creation and versioning of tens of thousands of modules** of HMI and PLC features
   - source code fragments
   - pre-built binaries
   - customizable templates etc.
  - assembly of deployable **platform installation packages and deviation/retrofit patches** per injection molding machine serial number
  - bridging development, production and service engineers for service issues
  - improving engineering time performance

For its nature, dealing with the Product System is a part of everyone daily work, and fighting with it to make it work or workaround it stayed a main theme.


### <a id="header-3-24" class="md-header-anchor"></a>Polaris Mold ID Server<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2009**

Latest Husky injection molding systems are equipped with the mold identification(Mold ID) feature, enabled by [Rubee](https://en.wikipedia.org/wiki/RuBee) tags attached to mold tooling plates.

When a mold is installed, parameters are read from the tags over the air, 
- enabling mold identification and quick alignment if matches, or
- reject installation if not

to avoid accidental damages.

My team built 

- a web portal 
  - global data center for all mold toolings, sold or under development
  - for daily cross-function collaboration.
    - mold designers to specify parameters
    - production engineers to program tags
    - service engineers to refurbish tags
- Integration into Polaris HMI
  - wireless detection of Rubee tags and parameter fetching
- Tag Programming/refurbishing Tool   
  - writing of mold tooling parameters through RuBee(IEEE 1902.1)
  - compress data into the 256-byte memory space
  - enabling engineers to program over the intranet, e.g. writing a Shanghai tag from Bolton 

SharePoint, Silverlight, WinForm and  Socket programming were involved. 

I oversaw the whole project as it was completely developed in Shanghai and deployed in Shanghai's IT servers for global access. The maintenance and response to requests raised through IT Help Desk are handle solely by the Shanghai team.


## <a id="header-2-25" class="md-header-anchor"></a>Polaris HkResource Refactor<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006.05**

With the growing number of supported languages from a mere 1, i.e. Engish, to over 22, the original single-file based solution suffered from

- too large file size up to 70+ MB
    - prone to corruption due to HMI crash or power loss
    - poor read/write performance
    - all-or-nothing rigidity, while only 2 languages(English and local) are requested in most cases.

By keeping its client interfaces intact,  I personally

- re-designed the storage schema
- re-implemented the COM server with ATL/C++
- leveraged memory-mapped file to access the files

thus getting rid of all the aforementioned shortcomings, without a single line of change in the HMI code.

### <a id="header-3-26" class="md-header-anchor"></a>Polaris Translation System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2006**

The Polaris HMI supports 22+ languages. For any phrase to be added to the screens, it's required to determine whether new translation is needed early on, and if so, among others

- applications should be filed for approval from team leaders for payment to a 3rd-part translation company 
- contexts of phrase usage, e.g. screenshots, should be provided to assist translation
- only selected languages should be packed and deployed onto the machine

A fresh college graduate, under my coach, developed a website using ASP.NET and SQL Server to have the goal achieved.

## <a id="header-2-27" class="md-header-anchor"></a>Bianque<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2013 - 2016**

The Polaris Production System dated back to circa 2000 and evolved into a daunting complexity with a very steep learning curve over the years, and a daily drain of engineering efficiency, To name a few,

- fail frequently
  
  unmanaged and undocumented heavy dependency on assumed fixed paths which are not

- poor errors 
  
  pop-up dialogues in the middle of automated processes, which invalidated the automation itself.

- convoluted system configurations

  70-100+GB decade-old VMWare image one can hardly create from the scratch.

- overuse of VMWare image file, a.k.a under use of virtualization technologies

  every engineer has **a copy for each task**. It's normal to have 2-3 such images running at the same time to get work done.

Named after the ancient Chinese doctor, Bianque was meant to fix it with a it-just-works solution with modern DevOps and virtualization technologies by distinguishin the requirements between

- Development Engineering, which creates software packages

  introduced  Vagrant for virtual eenvironment management  

- Production Engineering, which consumes software packages 

  replaced the legacy VB6 desktop application with a single page application(SPA) site backed up with REST web services

  - cloud(Microsoft Azure) based
    - removing the waste of gigantic duplicate VMWare image files(which IT complains all the time) with efficient cloud solutions
    - accessible from multiple platform
    - accessible from anywhere to benefit field service
    - easy upgrades
  - service-oriented with APIs
    - extensible for specific needs

Bianque largely consisted of

- Lancet

    A collection of REST Service mostly code in Python, including re-implementation of part of Polaris Production System functionalites。

 - Acupuncture

    A single page application leveraging Django/Dojo Toolkit/Web Socket, the front portal for 
    - production engineers on the shopfloor
    - service engineers in the field

- Transformer

    A manager of pre-configured VMware images
    
    - dispatch requests among images to maximized resource ultilization
    - concurrently execution of the Polaris Production System commands to have reduce from 40m to 5m-10m typically
    - exposing Polaris Production System commands as REST services 

-   HySearch

    A MongoDB/Django based portal as a convenience tool for search modules and machine packages through keywords.

### <a id="header-3-28" class="md-header-anchor"></a>Peeks of some screenshots<a class="back-to-top-link" href="#top">&#x27B6;</a>


- **early concept of Bianque**
  ![](./pics/bq-arch.png)

- **HySearch Engine**
  ![](./pics/bq-search.png)

- **Acupuncture**
  ![](./pics/bq-acupuncture-1.jpg)
  ![](./pics/bq-acupuncture-2.png)

- **Acupuncture on mobile devices**
  ![](./pics/bq-mobile.jpg)

- **early debug view of Transformers**
  ![](./pics/bq-transformer.jpg)

## <a id="header-2-29" class="md-header-anchor"></a>Hypersync-Altanium Industrie 4.0 Features<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016**

[Altanium controller](http://www.husky.co/EN-US/Altanium-Controllers-Overview.aspx) has stayed an independent business from the machine systems since its purchase from Moldflow, based in US.

It was decided from the Horizon(a.k.a HyperSync) product line, the Altanium controller would be an integrated part of the injection system for branding promotion and cost benefits:

- _one single touch-screen panel_ and integrated HMI instead of one for each.
- _premium Altanium feature/performance_ when ordered together, starting from HyperSync.

which means a design of a new unified & proprietary interface

- hardware

  one ethernet cable instead of various digital I/O, RS422 and other industrial interfaces

- software
  - design of a protocol 
    - unifying all in-use commands for temperature and servo control commands
    - extensibility of future functionalities
    - identification of Husky equipments from either side
      - protection of Husky IP
      - protection of malice penetration from the network
  - remote desktop viewing
    - an VNC/RDP-like solution for an operator to control the Altanium controller from the machine

This was completely done by my team in shanghai

- started with a few slides of ppt from the Product Manager, mentioning _Altanium Integration_,
- collected requirements and consensus from all teams at stake and 
- Concepting
- Design execution and validation

It was an unforgettable experience for all the challenges we had to cope with. 

- The one and only Prototype machine built in Bolton
  - late nights of remote connection&debugging on Bolton's shopfloor till 4:00AM
  - several tight validation trips to and from Bolton
    - compete with other design teams for the availability of the prototype machine
- all teams except for software design are in North American
- had to borrow servo and motors from [Baumueller](http://www.baumueller.de/en) at the [ChinaPlas](http://www.chinaplasonline.com/CPS15/Home/lang-eng/Information.aspx)
- had to figure the Altanium side technologies by ourselves through code reading

Due to departmental priorities conflicts between US and Canada teams, we stayed in the middle of one party too demanding and one party too uninterested.

The features were successfully shipped to the first customer TetraPak at Mexicali, Mexico. One designer from my team was part of the PLT team to startup the system .

**It was quite an experience and I was proud that my team made it.**

## <a id="header-2-30" class="md-header-anchor"></a>HyMET Heatlogger<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2007**

![](./pics/heatlogger-main.png)

Lack of experience alloys injection, designers wished to monitor the heat parameters at the barrels and sprues in the long term. It was then decided to develop this tool to be deployed onto every sold HyMET machine, logging all interesting parameters. A husky service guy would visit the customer to copy the data out every 6 month or longer.

The project is to read from up to about 100 temperatures values from thermal couplers and save them into logs.

The critical requirements include:

- no threat to normal machines operations
  - no system resource contention with the Polaris HMI/PLC, i.e. minimum CPU cycles and memory footprints
  - no disk space running out
- non-stoppable by the factory operators
- up to 100 channels
- records at an interval from 1 sec to 180 seconds.
- flexible scheduling based on calendars and shifts
- intelligent start/stop of logging with on/off statuses of injection cycles
- data must be power-failure safe, i.e., no corrupt file due to accidental power loss 
- tight schedule imposed by machine shipping date


I coached an intern to have developed the code to read temperatures through TwinCAT API, and personally  wrote the UI in [WTL](https://en.wikipedia.org/wiki/Windows_Template_Library) and the logging in [POCO](https://pocoproject.org/), I also contributed back the enhancement I made since it's an open source project.

A colleague from the HyMET team wrote to me saying:
> this is the most beautiful software I've ever seen in Husky.

## <a id="header-2-31" class="md-header-anchor"></a>Shotscope, Celltrack & Matrix<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2008**

When it bought Moldflow manufacturing division, Husky dismissed the original French&US teams and had all the assets(software&hardware) sent to Shanghai.

Some of the code dated backed to early 1990s, and the technologies varied from Delphi, Borland C++, Sybase database, MySQL, Linux

- Shotscope, a production monitoring software
- Celltrack, another production monitoring software
- **Matrix 1**, a linux-based Altanium HotRunner Controller software

My team successfully supported existing European/American customers with sustainment development until the next .net versions of Shotscope and Matrix, i.e. SSNX,  came out.

## <a id="header-2-32" class="md-header-anchor"></a>Shotscope NX<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2008 - 2015**

The successor to Shotscope and Celltrack, with modern IT technologies. To quote its [homepage](http://www.husky.co/EN-US/Shotscope-NX.aspx):
  >The industry’s most advanced plant-wide process and production monitoring system

A first ever Husky made manufacturing monitoring system aimed to be an integration of 3 SCADA/ERP software products: SmartLink, Shotscope, CellTrack, embracing modern technologies at the time to achieve:

- Friendly and intuitive UI and better data presentation ability
- More distributed supervisory ability
- More data storage and processing power
- Built-in communication with other Husky software such as Polaris IMS Control system and Altanium Hot Runner Controller

The first release was delivered on time and received immediate orders at its debut at NPE show 2009, Chicago.

The majority of implementation was done in Shanghai under my supervision with a 3rd-party test team hired from Toronto for feedbacks and bugs reporting.

My team was a core party from the start and kept improving it until it was feature stable and transfered to After Market Service(AMS) engineering team for customer-specific changes in 2015.

Major Technologies include WCF, Silverlight, SQL Server, written in C#.

## <a id="header-2-33" class="md-header-anchor"></a>Shanghai Factory Startup<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005 - 2006**

In the context of Husky's rapid growth in China, it was decided to build **a machines&hot runners manufacturing factory**, hence the need for strong engineering teams to

- **build infrastructures**
- **build up teams** of assembly, testing, production engineering
- **transfer engineering knowledge, disciplines&experience** from Canada to local teams

3 Global Engineering Execution(GEE) teams, including 1 controls focused(which I led) and 2 mechanical focused, were founded to support the mission.

With no prior domain knowledge of injection&molding, and by splitting my time in half between Bolton and Shanghai in the first year, I successfully replicated the production infrastructures concerning controls(Polaris Production System) from the ground up.

This 1+ year long efforts earned me a Special Contribution Award, signed by the then Global Vice President of Machines.

## <a id="header-2-34" class="md-header-anchor"></a>Whiteboard<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2011-2016**

One of the typical challenges for global teams is the time differences and geographical distances making it hard to put everyone on the same page in meetings. While local teams could debrief at a coffee break or stop by the desk, global teams do not have the luxury. It could be only worse if different projects are interwoven and information is scattered among multiple sources

From expereince of talking to various teams from mechanical, electrical to software and all levels of leaders up to VP, I developed an application to pull information from various sources, including

- Visual Sourcesafe database
- Team Foundation Server
- SharePoint portals
- SQL Server database from IT's attendance&vacation system
- Shared files on mapped network drives

and synthesize them into a dashboard site with links back to the information sources.

An orchestration of technologies/languages are used:

- [Django](https://www.djangoproject.com)
- [Dojo](https://dojotoolkit.org)
- [ASP.NET Web API](https://www.asp.net/web-api)
- [MongoDB](https://www.mongodb.com/)
- Python
- C#
- JavaScript

## <a id="header-2-35" class="md-header-anchor"></a>Product Launch Team(PLT) Trips<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2011 - 2016**

It's been Husky's tradition to set up a highly knowledgeable and experienced team directly from the Dev Eng, instead of regular service teams, to start up machines for early adopter before the formal launch of a product line.

My team had been backing up these teams since 2011 and sending people directly since 2013 to destinations like Vietnam, Mexico and multiple cities in China.


<hr class="page_break" id="profile-en-2" />






# <a id="header-1-36" class="md-header-anchor"></a>STMicroelectronics<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-37" class="md-header-anchor"></a>Porting Embedded Operating Systems,<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2005.04 - 2005.09**

Based on ARM7 ST7 CPU, meant to showcase the usages, read/write performance specifically, of ST's M45/M25 SPI flashes in embedded apparatuses.

Ported uCLinux and uc/OS-II.

Wrote code in Assembly and C with RealView Development Suite/Keil.

## <a id="header-2-38" class="md-header-anchor"></a>Driver and Application Demos for Flash Memories<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2003.12 - 2004.10**

Designed an embedded system for demo & verification of SPI interfaced Flash/EEPROM chips.

- used ST's own MCU chip
- designed PCBs with Altium
- wrote code in C language
  - portable and reusable driver code in C language
    - published on ST's website for download
    - SPI flash & EEPROM chips, e.g. M25Pxx code flash memories/ M45PE data flash memories
    - Firmware Hub(FWH) and Low Pin Count(LPC) chips, e.g M50xx flash memory 

  - showcased driver code with an application demos rotating quotes from _Albert Camus_ on a LCD display, of which my favorite one was

  > Don't walk in front of me;
  > I may not follow.
  > Don't walk behind me;
  > I may not lead.
  > Just walk beside me and be my friend.

- authored&published corresponding application notes, in English, explaining the rationale and usage of above application


## <a id="header-2-39" class="md-header-anchor"></a>Memory Competence Center(MMCC) Project Tracking System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2004.02 - 2004.06**

A Project tracking tool to synchronize projects

- between MMCCs at Shanghai and Prague, Czech Republic.
- between MMCC Shanghai and Divisions in France/Italy.

- written in C++, MFC/STL/BOOST.
- ADO + Access as for local database
- XML import/export remote synchronization


## <a id="header-2-40" class="md-header-anchor"></a>Serial Flash Programmer Tool Kit<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2003.09 - 2003.12**

In line with Company’s effort to provide more accessible e-tools for the mass market, this kit was to facilitate the  usage of ST memory products, supporting SPI, I2C Initially. Specifically,

- testing chips with all supported page/sector read, program and erase commands
- free in-production usage of chip programming in small volumes(e.g. R&D designers at Intel Israel)

It consists of two parts:

- PC-based GUI software
  - Windows 9x, 2K, XP
- a PCB board running a ST7 based embedded system
  - connection to PC through the parallel port (LTP) or USB.

I took over from and re-designed the whole system reference a legacy half-done tool written in VB6, including

- a full functional GUI in MFC/C++
- firmware code on the ST7 board
  - enabled the chip-specific USB buffering techniques
    - coded against the Data Transfer Co-processor(DTC), a 2nd processor handling USB data transfer in parallel to the main CPU
      - with its specific Assembly instruction
      - within 128-byte code space

Being the best tool in class, it was later licensed by my then-colleagues from ST to have started up a company named [Dediprog Inc.]( http://www.dediprog.com/), branded as SF100/SF200/SF300 series. I was the interim CTO during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards. 
According to [this manual](http://www.dediprog.com/save/84.pdf/to/dp_SF%20User%20Manual_EN_V6.7.pdf), this features stays largely the same as what I left.


<hr class="page_break" id="profile-en-3" />






# <a id="header-1-41" class="md-header-anchor"></a>Schneider Electric<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-42" class="md-header-anchor"></a>Automatic Power Transfer System<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2002.11 - 2003.06**

 a set of critical switching unit for dual power supplies ensuring reliably & continual power supply where power faults may lead to critical results (e.g. surgical rooms in hospitals).

 There was no out-of-box product on the market, except for ad-hoc solutions built up with components from vendors of customers choice.

The goal was to develop a complete auto-transfer system aimed for

- more intelligence
- friendlier HMI
- higher reliability
- lower cost

All objectives achieved using all Schneider products

- NEZA PLC for intelligence and user-friendliness
- popular NS circuit breakers for reliability
- safety achieved with interlocks
  - software/electrical interlock by PLC
  - built-in mechanical interlock by NS

Led a team from multiple departments (Design center, Marketing, Field Application) and pushed from concepts to fully-functional prototype, includes analysis reports on
- cost (only 1/3 of solution on the market)
- technical risks
- qualification plan

It was submitted for Chinese Management Committee(CMC) for final review of production investment by the time I left.

## <a id="header-2-43" class="md-header-anchor"></a>Development of industrial-strength LED alarms<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2002.11 - 2003.06**

research to in preparation for launch Schneider branded alarms for factory shopfloors.
- investiagate LED technologies.
- collect LED samples from select vendors  
- devise tests for IEC conformance tests regarding lumen, product life etc.


<hr class="page_break" id="profile-en-4" />






# <a id="header-1-44" class="md-header-anchor"></a>Harbin Institute of Technology<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-45" class="md-header-anchor"></a>Automatic Test Platform for Rolling Stock Apparatus<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2000.09 - 2001.04**


An Industrial PC-based (IPC) automation system bid for Harbin Railway Bureau, a regional bureau affiliated to the Ministry of Railways (China).

which I took full charge of and design from scratch, including hardware, software and some mechanical design. 

The objective of the project was to provide an automatic and all-in-one platform to cover the routine maintenance check of all low voltage apparatuses used on the train: 

- circuit breakers
- contactors, and
- thermal relays

The checklist included

- electrical performance, e.g. 
  - contact resistance
  - make/break time
  - over-voltage
  - under-voltage
  - over-current 
  - short circuit etc. 
- mechanical performance 
  - sinusoidal vibration.

- Hardware design 

  - ISA board for digital I/Os
  - PCI board for data sampling and A/D conversion
  - Timer/counter circuit design
  - digital-coded power supply ranging between 1A-1300A or 0-440VAC/220VDC
  - Precise resistant load (resolution: 1/512 ohm)
  - High current sample and switch circuit.


- Software development
  - Motor control &　Hardware driving
  - Precise voltage/current control algorithm
  - Complete and user-friendly performance parameter display
  - Failure report and Lifetime estimation(FMEA)
  - Test data management

Awards&Achievements:

- Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
- Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
- An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*

## <a id="header-2-46" class="md-header-anchor"></a>Auto Test Platform for Electronic Ballast<a class="back-to-top-link" href="#top">&#x27B6;</a>

**1999.04 - 1999.10**

Another Industrial PC-based (IPC) system designed for Harbin Railway Bureau. 

Ojective

- up to 8 electronic ballasts (15W, 20W, 30W, 40W) concurrently, 
- detailed performance parameters such as 
  - voltage, 
  - current, 
  - power factor, 
  - harmonic distortions(THD), 
  - temperature rise etc. 
  - real-time voltage and current waveform 
  - FMEA reports
  - historic events database
  - pretty print

- hardware
  - ISA I/O board, A/D board
  - Power supply board (DC 47V-57V, AC 200V – 240V)
  - Sensor board (voltage, current and temperature)
  - Target-switching board

- software
  - Hardware driving
  - Real-time sample and analysis (FFT algorithm)
  - Rendering visual waveform
  - Test data management

This platform greatly sped up the routine check efforts of technicians who were dealing with tens of electronic ballasts daily. The usages were spreaded to another 5 railway bureaus soon after the completion for its satisfactory performance.

A paper was published on a national academic periodical explain the design.

It was all of my work from initial research, hardware design, software design spec and debug, except for the software UI design.


<hr class="page_break" id="profile-en-5" />






# <a id="header-1-47" class="md-header-anchor"></a>Spare-time Projects<a class="back-to-top-link" href="#top">&#x27B6;</a>


## <a id="header-2-48" class="md-header-anchor"></a>Markdown-CV<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2017**
**Python, HTML, Boostrap, JQuery**

A set of scripts and templates to generate html and PDF files from resume written in pure text, i.e. [Markdown][#MD], leveraging [Bootstrap][#BOOTSTRAP] themes and other open source code such as [marked js][#MARKEDJS].

- open sourced on [GitHub][#MDCV]
- Written in Python, HTML, CSS, JavaScript, [Bootswatch][#BOOTSWATCH]/[Bootstrap 4.0 alpha][#BOOTSTRAP]

- [link][#MDCV]

**This very resume is generated with this tool**

[#MDCV]: https://github.com/rockonedege/markdown-cv
[#BOOTSTRAP]: http://v4-alpha.getbootstrap.com/
[#BOOTSWATCH]: http://bootswatch.com/
[#MD]: http://daringfireball.net/projects/markdown/
[#MARKEDJS]: https://github.com/chjj/marked

## <a id="header-2-49" class="md-header-anchor"></a>2D Water Effect in WTL<a class="back-to-top-link" href="#top">&#x27B6;</a>

2011
C++, Image, Graphics

A hobby project while learning about image processing and graphics rendering.  
- Open sourced with an article on [The Codeproject][#2DWTL].
- Written in C++/WTL

- [#2DWTL]

[#2DWTL]: https://www.codeproject.com/Articles/188236/D-Water-Effect-in-WTL


## <a id="header-2-50" class="md-header-anchor"></a>A Literal Converter for Integers<a class="back-to-top-link" href="#top">&#x27B6;</a>

2004

A small utility for myself to help process the data file from the oscilloscope, which I used to record data going through Serial Peripheral Interface(SPI) ports while verifying the flash memory chips.

- Open sourced with an article on [The Codeproject][#LITERAL]
- Written in C++

- [link][#LITERAL]

[#LITERAL]: https://www.codeproject.com/Articles/10379/A-Literal-Converter-for-Integers


## <a id="header-2-51" class="md-header-anchor"></a>TwinCAT TSM File Viewer<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016**

A binary opaque format, named tsm, is used in TwinCAT 2 for configurations, which is where hardware *meets* software in the TwinCAT-based control system, specifically it maps software *variables* with Hardware I/O *signals*

- loads variables from software logic written in PLC/CNC/C++ etc.
- scans hardware I/O devices such as cards, boxes, terminals
- scheduling: exchanges values to-and-from in a timely manner(i.e. as tasks).

Reading directly from the tsm file is the most reliable way at runtime to extract the information for extension.

Husky's HMI has been structured in a way that's closely coupled to a local TwinCAT runtime,  which has hindered its evolution in two ways
- engineering efficiency
  - even simple non-PLC HMI change requires to fire up the whole simulation environment in complicated VMWare image, which, in turn, makes it hard to bring in new bloods to the team, as the learning curve for such simple tasks is is very unusually steep and daunting for new engineers

- innovations for industry 4.0
  To Quote Microsoft, this world is heading toward **cloud first** and **mobile first**. A universal set of UI that enables people to monitor, or operate with properly configured access rights is a must to be industry 4.0 compatible.

- product roadmap
  while it's been long desired to unify the platforms for the Machines and HotRunner controllers business to strengthen the one-Husky brand, duplicate development and features have to be paid and the customers have to operator on two separate HMI/Touch screens to get their job done.

I figured decyphering the tsm file and hosting it through a web service would be an ideal decoupling solution
- minimal risk
 - leaving the PLC algorithms intact largely.
- maximum extensibility of HMI
   - with modern UI frameworks, the HMI can be develop only once to fit all client platforms. declarative style UI can be developed any platform, mobile or desktop, deployable to local, on-premises servers or on cloud.
   - dependency on the PLC can be easily simulated with a simple JSON file in most cases.

I personally completed the proof of concept with some open source tool
- [websocketd](http://websocketd.com/)
- [ADS, Beckhoff protocol to communicate with TwinCAT devices](https://github.com/rockonedege/ADS)
 and in-house coding based on TwinCAT APIs.

C++, Python, Javascript and some Go were used in the prototyping.

I did not have a chance to present the concepts and the work I've done before leaving Husky, Though planned to.

## <a id="header-2-52" class="md-header-anchor"></a>Multi-Ping<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016**

While integrating Altanium controllers into Horizon machines, one of the features was to detect the connection of a controller, and fetch its APIPA address, which became a challenge due to a maximum of delay was allowed.  

The built-in .NET classes did not cut as the it resulted in **10s at best and 40s on average**, and cost about 700MB memory or higher till the host HMI apllication crashes.

Guessing that it might be the wrapping of .NET over the raw system sockets, I implemented the functionality with C++/Boost.Asio and leveraged asynchrous techniques, and was able to complete the detection within **2s** using around **5MB** memory.

The algorithm was then wrapped 
- through C++/CLI as a .Net Assembly for consumption of Polaris HMI
- as a trouble-shooting  utility apllication for testers on the shopfloor and service people in the field.  

## <a id="header-2-53" class="md-header-anchor"></a>HostLinkpp - An Implementation of HostLink inter-machine Protocol<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2012**
**C++, TCP, socket?Boost.ASIO, Boost.spirit**

![](./pics/hostlinkpp-ubuntu.png)

A C++ Implementation of Husky Hostlink, a proprietary raw sockets based binary protocol accessing Husky injection machines, e.g.

- issuing commands  
- reading statuses and cycle data

It has been adopted by some major customers to integrate Husky systems into their OPC/ERP systems, who requested more commands(e.g reading injection cycle breakdown data) to be supported, hence the project.

I personally wrote the whole code, based on the original MFC based code written in around 2000, as an illustration and teaching material of standard C++ for efficient, cross-platform system related code.

- added more advanced commands to fetch extended cycle data available in new generations of machines  
- fixed bugs specific to 64-bit, which was not considered when the original code was written
- replaced ad-hoc command  construction&parsing with Boost.Spirit and Boost.Qi
- replaced MFC based sockets communicated with Boost.ASIO
- supported platforms beyond Windows to Linux(tested Ubuntu)
- supported Python binding with Boost.Python
- supported .NET binding with C++/CLI

This project leverage quite a few  [Boost][#BOOST] libraries such as [Boost.Asio][#ASIO], [Boost.Spirit][#SPIRIT]， [Boost.PP][#PP]，[Boost.Python][#PYTHON] and advanced modern C++ techniques such as metaprogramming.

Built with CMake.

The code was not open sourced because the Hostlink protocol is proprietary.

[#BOOST]: http://www.boost.org/
[#ASIO]: http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html
[#SPIRIT]: http://boost-spirit.com/home/
[#PP]: http://www.boost.org/doc/libs/1_63_0/libs/preprocessor/doc/index.html
[#PYTHON]: http://www.boost.org/doc/libs/1_63_0/libs/python/doc/html/index.html


## <a id="header-2-54" class="md-header-anchor"></a>Console3<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2012 - 2016**

A fork of [Console 2][#CONSOLE2]

- fixed bugs
- replaced rendering GDI from to [Direct2D][#]/[DirectWrite][#DW] 
- added Husky-specific feature with integration to Husky tools

This tool was widely used by the Shanghai team.

[#CONSOLE2]: http://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx
[#D2D]: https://msdn.microsoft.com/zh-cn/library/dd370990(v=vs.85).aspx
[#DW]: https://msdn.microsoft.com/zh-cn/library/windows/desktop/dd368038(v=vs.85).aspx


## <a id="header-2-55" class="md-header-anchor"></a>Beckhoff TwinCAT ADS Protocol<a class="back-to-top-link" href="#top">&#x27B6;</a>

**2016**

A fork of the open source implementation of Beckhoff ADS protocol

- added support of TwinCAT 2

- [link]][#ADS]

[#ADS]: https://github.com/rockonedege/ADS

-----
<small>Generated from <a href="http://daringfireball.net/projects/markdown/">Markdown</a> text using my open source tool <a href="https://github.com/rockonedege/markdown-cv">markdown-cv</a>. Last updated at 2018-01-11. </small>