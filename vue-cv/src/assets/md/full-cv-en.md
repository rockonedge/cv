---
banner:
  brand: Curriculum Vitae
  lastupdate: <a class="btn-link" href="https://tslides.netlify.com/cv">SLIDES</a>
    &middot; <a class="btn-link" href="./pdf/full-cv-en.pdf">PDF</a> &middot; 2018-01-11
  nav: ''
  time: 2018-01-11 17:56:04.805340
  title: Curriculum Vitae

---




**[tom.tan@live.com](mailto:tom.tan@live.com) &middot; (+86) 135-248-26926 &middot; [LinkedIn](https://www.linkedin.com/in/zhitan/) &middot; [Website](https://ttan.netlify.com)** | **R&D Managemnt**

# **Tom Tan**

>- 11+ years of Acknowledged Global Projects&People Leadership
>- 15+ years of Multi-sector R&D Career in World's top companies across Automation, Electronics and Machinery
>- 19+ years of Hands-on Software/Firmware/Hardware Development


>- Started up A Global Venture([Dedirpog Inc.](https://www.dediprog.com/US)) 
>- Started up Global Development & Production Engineering Teams
>- [Acknowledged contribution](./pics/husky-contribution.jpg) to launch&profit of a global manufacturing factory  



>- Challenges Pursuer, Visionary and Open-minded
>- Deeply rooted in Science, Technology, Engineering and Math(STEM) Related
>- A Regular [UNICEF](http://www.unicef.cn) Donator


## **Objective**

A Senior Manager/Director position, preferably

- Engineering teams startup&leadership
- Industrial, Scientific, Medical(ISM) or Automotive sectors
- IoT/IIoT/Industry 4.0 innovations 
- A global company Based in Shanghai




## **Summary**

**Personalities**

- Great self-motivation
- Being visionary and open-minded
- Enthusiastic pursuer of challenges
- Lead-by-example leadership style


**General Interests**

- any system/device involving electrical/electronic/computer engineering, 
- the industrial trend towards an ever more digital and software-defined future.


**Leadership Experience**

- successful startup of a hi-tech venture 
- **11+ years of global engineering team management**
  - quick startup of a high-profile team
  - high talent retention
  - high quality projects & accomplishments
  - widely acknowledged leadership from within and outside of the team


**Technical Experience**

- **18+ R&D-centric years**
  - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
  - programming with C++/Python/C#/JavaScript/Go/Matlab 
  - schematic&PCB design and simulation with Protel/Altium/Matlab/PSpice
  - Industrial PC, PLC and MCU


- **15+ professional years of service in World's top companies**
  - industrial automation, electronics& machinery manufacturing
  - cross-team collaboration with production, pre/after sales&service teams




## **Experience**
### **Team Leader/Founder, Global Development Engineering Department**

- **Reported to Director of Global Development Engineering(HQ in Canada)**
- **[Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016**
  
  Helped to start up from the scratch the manufacturing capacity of injection molding machines at Shanghai campus, and made [**Significant Contribution** acknowledged by the then Global Vice President of Machines](./pics/husky-contribution.jpg).

  Thereafter, started up the one-and-only offshore development engineering team, working hand-in-hand with Dev Eng teams in the HQ daily, through email, audio/video conference calls and mutual visits, in support of Husky's business around the world.

  The headcount grew from 5 to 12 in 2 years due to exceptional team performance.

  **Starting from 2014, focused on advocating Virtualization/DevOps/Cloud/Big Data/Mobile technologies inside the company, towards Industry 4.0/IIoT solutions, leveraging Iaas/Paas/SaaS offerings from Amazon AWS/ Microsoft Azure, e.g. AWS IoT, Azure IoT Hub etc.**

  **As the founder&top regional leader of the team, ultimately responsible for Shanghai team with direct reports to the headquarter**.

  - people management
    - talent acquisition&retention
    - team motivation/training etc.

  - project management
    - collaboration/prioritization of milestones with global/local engineering teams
    - engineering discipline/standards enforcement
    - regional R&D interface to production/service teams et al. 

  - evaluation and evangelism of new technologies and methodologies
  - budgets forecast&control

  **Being widely acknowledged as a model team, I was invited to share experience on global collaboration by other teams, such as Global Production.**
  

  Various levels of software development from real-time motion control to enterprise solutions, along with electronic/electrical/mechanical tasks had been accomplished under my leadership, covering

  - Human-Machine interfaces(HMI)
  - TWinCAT based motion control(PLC)
  - integration of 3rd auxiliary systems, e.g. ABB robot, Plastic Systems resin dryers, Altanium temperature/servo controllers
  - Manufacturing execution systems (MES) and manufacturing monitoring/management(SCADA) 


 for platforms including

  - Windows Embedded/Server
  - Linux(Fedora, Ubuntu)
  - Mobile(iPhone) 
  - Atmel MCUs
  - Microsoft Azure / Amazon AWS 


  with languages such as

  - C++
  - C#
  - Visual Basic
  - Python
  - Ruby
  - Java
  - JavaScript
  - IEC61131 PLC programming languages etc.

  and Industrial standards/protocols such as 
  
  - OPC(OLE for Process Control)
  - EtherCAT/Realtime Ethernet
  - Sercos
  - Profibus
  - Euromap
  - CAN
  - SPI etc.


  **Major Achievements**

  - led the merge of [Moldflow][#BUYMF]
  - led [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx)
  - led Polaris(HMI, PLC and Production System)

  - **Significant Contribution Award**, 2007
    - Signed by the then Global Vice President of Machines
    - In recognition of contributions during the startup and first profitable year of Husky's Shanghai Machines plant


  - **Top-Ranked Team Leader**, 2011
    - Husky's global employee survey conducted by [Kenexa][#KENEXA]


  - **10-Year Service Award**, 2015


[**See Feature Projects >>**](https://ttan.netlify.com/projects-en)




### **Senior Application Engineer/Interim Team Leader, Memory Competence Center**

- **Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France)**
- **[STMicroelectronics][#ST], 9/2003 - 11/2005**

  A technical liaison connecting Central R&D IC designers and A/P Field Application Engineering, involving 
  
  - electronic Schematic/PCB layout, 
  - software/firmware coding  
  - application notes authoring 
  
  for embedded/desktop systems as requested/required.

  Being a startup member and interim leader for the Serial Non-Volatile Memory & Embedded System Solutions Team, assumed the responsibility to **explore and build** the connection with the R&D Division back in France, and maintained the effective communication through trustworthy relationship.

  More specifically,

  - Assisted IC designers in France
    - IC chip verification of prototyping samples to ensure all specifications are met according to datasheets, e.g Signal-to-noise ratio(SNR), signal glitches etc.
    - authoring in English application notes for each product family

  - Software/hardware reference designs showcasing application scenarios
    - PCB design with Altium
    - Firmware driver code(in Assembly/C) for
      - M50 Firmware Hub/LPC BIOS Memory chips
      - M25PE/M25P/M45PE SPI code/data flash&EEPROM memory chips
    - Porting popular OSes to STR71x(uCLinux, µC/OS-II)
    - USB based memory chips programming kit(in C/C++/Assembly) for PC/Windows

  - Development of Anatidae, an in-house tracking system to synchronize projects between Shanghai, Czech, Italy and France, using MFC/STL/BOOST.

  Being the best tool in class, the aforementioned memory programmer kit was later licensed by my then-colleagues for a startup company named [Dediprog Inc.][#DP], branded as SF100/SF200/SF300 series. I was the _interim CTO_ during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards.




### **Electronic Design Engineer, China Design Center**

- **Reported to Manager of China Design Center(French Expat)**
- **[Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003**
  
  As one of the regional corporate R&D staff, working closely with the lab technicians, with a focus on improving and innovating industrial control&automation(ICA) products&solutions, including

  - electrical/electronic products design
    - localization of global products
    - new products targeting Chinese market only
  - premium technical support for joint ventures(JVs)
    - Homologation tests for JVs
  - hardware/software development of lab equipments/utilities

 Achievements included

  - Auto-Transfer System(ATS), an automatic switches on/off between two independent power supplies in no time in case of emergency for sectors like hospitals
    - based on NS Compact circuit breakers and NEZA PLC.
    - from the scratch to 3 full-functional prototypes
    - 1/3 cost saving from existing market solutions
    - submission to Chinese Management Committee(CMC) for final production investment evaluation 
  - Co-design of a thermostat for IEC regulated high-temperature(up to 75&#8451;) aging tests
  - Developed software data analytics utilities for experiments in VB6/C++/MATLAB
  - Localization design of ASICs for circuit breakers
    - VC65
    - DPN Vigi
  - preliminary investigation of local manufacturing of switching power supplies from Poland, including reports on performance/reliability/cost comparison with local brands.
  - Compact NS circuit breakers homologation tests for China Certification.
  - Development of industrial-strength LED alarms




### **Researcher, HIT Research Institute of Electrical Apparatus **

- **Reported to Head of Institute / Mentor Professor of My Postgraduate Study**
- **[Harbin Institute of Technology][#HIT], 4/1998 - 3/2002**

  This was a technical center/lab, founded&led by my mentor professor, focusing on automation technologies for the railway industry, sponsored by Harbin Railway Bureau with many R&D projects within the then *Modernize-the-Railway* budgets.

  My period in the lab started when I was a junior and volunteered to clean up desk(soldering iron etc.) and ended when I graduated, with a Master Degree of Electrical Engineering, from the postgraduate School and left for Shanghai.

  **It could never be emphasized enough what a firm technical foundation as well as long lasting interests in science and technology this 4 years laid for my later career till today.**

  Both my theses for the Bachelor&Master degrees and several published academic papers were inspired from my research and development activities in the lab.

  My interests and yields included

  - Research on industrial control and automation system
    - Electrical machines and Apparatus
      - transformers, generators and (servo) motors(Rockwell Automation brands)
      - relays, contactors and circuit breakers
    - Computer-based control&automation systems
      - Direct Digital Control System
      - Distributed Control System (DCS)
      - Fieldbus Control system (FCS)
    - real-time control & measurement methodology and reliability, e.g. FMEA
    - real-time embedded system (RTOS) and embedded application development
    - Research on electrical/electronic interface/bus protocols(ISA, PCI etc.)
  - Development of control & automation solutions
    - based on industrial PCs(IPC) and/or MCUs(Intel MCS-51 family)
    - HMI development in C++/Visual Basic
    - device driver development in Assembly/C/C++

    - Electrical schematic&PCB design with Protel, and simulation with pSPICE/MATLAB/SIMULINK
    - mechanical design(cam wheel based vibration platforms) with AutoCAD


  **Most notably, 10+ sets of the following sytems I developed were sold, having generated sales of over 1 million RMB.**

  - The Electronic Ballast Test Platform 
  - The Low voltage Apparatus Automatic Maintenance Platform


  **Awards**

  - Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
  - Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
  - An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*.




## **Education**

- **Master**, _summa cum laude_
  - **Electrical Engineering**, 09/1999 - 03/2002
  - [Harbin Institute of Technology][#HIT], Harbin

- **Bachelor**, _summa cum laude_
  - **Electrical Engineering**, 09/1995 - 07/1999
  - [Harbin Institute of Technology][#HIT], Harbin

- **Paper**

  - 2 papers published in pursuit of the Master degree
    - Zhai Guofu, Tan Zhi, et al. The development of integral testing equipment for electronic ballasts and inverters used in trains. *Low Voltage Apparatus* 
    - Zhai Guofu, Tan Zhi, et al. Research on a test system of performance&reliability of rolling-stock circuit breakers&contactors. *The 8th International Low Voltage Apparatus Reliability Conference*


- **Awards & Social Activities**
  - Awardee(multiple times), Award of People's Scholarship, 1995 - 1999
  - Member, Students' Union, College of Electrical Engineering and Computer Science, 1996 - 1997
  - Member, Students' Union, Harbin Institute of Technology, 1995 - 1996
  - Volunteer, the 8th International Conference for Northern Cities, Harbin, 1/1998




## **Languages**

- Chinese: Native
- English: Fluent

## **Skills**

- Management
  - successful startup of a hi-tech venture in Shanghai/Taiwan 
  - successful startup of global engineering teams in Shanghai
  - team & project management with high retention rate
  - Top-ranked team leader in a global employee survey by [Kenexa][#KENEXA]


- Technologies

  - Industrial Standards

    - OPC(OLE for Process Control)
    - EtherCAT/Realtime Ethernet
    - Sercos
    - Profibus
    - Euromap
    - CAN
    - SPI etc.

  - Hardware
    - Schematic & PCB design with Protel/Altium, 
      - A/D, D/A and I/O and other peripheral circuits
      - MCU(MCS51, ARM, ST7)
      - PC(ISA/PCI) architectures.
    - knowledge of Verilog HDL.
    - Simulation with Matlab, pSPICE
    - Industrial PC, PLC, softPLC(CodeSys/TwinCAT)
    - wide-range current power supply(1A -1200A) design


  - Software
    - Open Source software(OSS) contributor to 
        - [BOOST][#BOOST_REVIEWER], 
        - [POCO][#POCO_CONTRIBUTOR] 
        - [The Code Project][#CP_AUTHOR]
    - Programming Languages
        - frequent use of C++(esp. C++ 11/14/17), Python, C#, JavaScript/HTML/CSS
        - occasional Golang, R, Matlab, ruby, TypeScript etc.
    - Application
        - IC chip drivers
        - realtime motion control(e.g servo motor, robotics)
        - desktop GUI/CLI applications
        - server/web based enterprise LOB(line of business)
        - cloud based mobile/IoT solutions
    - Platforms
        - Windows desktop, Windows Embedded/Windows Mobile, Windows Server
        - Windows Azure, AWS
        - Linux(Ubuntu/Fedora)
        - uC/OS, eCos and uCLinux
    - Tools&Library
        - Microsoft Team Foundation System(TFS)/Microsoft Visual Studio
        - Keil, ARM RealView 
        - SQL Server, MongoDB, SQLite 
        - .NET, ASP.NET, WinForm, WPF, UWP
        - MFC/WTL, QT, STL/BOOST, ACE
        - Dojo Toolkit, AngularJS
        - NodeJS, Django, Flask 
        - Cordova, Xamarian
        - Git, Mercurial(HG), SVN
        - Vagrant, Ansible
    - Methodology
        - Object Oriented Programming(OOP), generic programming/metaprogramming(GP), functional programming 
        - Agile/SCRUM/TDD




## **Trainings**

- **The 7 Habits of Highly Effective People Training Program**
  - 3 full days, 3/2008
  - At Shanghai, China
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Franklin Covey Co.](http://www.franklincovey.com)


- **The 4 Disciplines of Execution: Manager Certification**
  - 3 full days, 3/2007
  - At Shanghai, China
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Franklin Covey Co.](http://www.franklincovey.com)


- **Team Leadership development Training Program**
  - 1 full week, 01/2006
  - At Bolton, Canada
  - Sponsored by Husky Injection Molding Systems
  - Conducted by [Workplace Competence International Limited](https://www.linkedin.com/company/674972)


- **Product training on Serial Non-volatile Memory Chips**
  - 2 full weeks, 05/2004
  - At Rousset, France
  - Sponsored by STMicroelectronics
  - Conducted by Serial Flash Division, [STMicroelectronics][#ST]
  - Covering Serial EEPROM, Serial Flash, BIOS Flash, Contactless Memories


- **Cambridge English: Business (BEC)**, _Vantage_ through _Higher_
  - Every weekend throughout 1 year,  2002-2003
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Conducted by [ClarkMorgan Ltd.](https://www.linkedin.com/company/clarkmorgan)


- **Creative Thinking and Problem Solving**,
  - 1 weekend, 07/2002
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Conducted by [Key Consulting][#KEYCONSULTING]


- **Intellectual Property & Patents**，
  - 1 day, 06/2002
  - At Shanghai, China
  - Sponsored by Schneider Electric China Investment Co.
  - Retired official of [Shanghai Intellectual Property Administration (SIPA)][#SIPA]

[#SIPA]: http://www.sipa.gov.cn/gb/zscq/zscqjel/index.html




## **Additional Information**

- **Contributions**
  - [Boost C++ Libraries](http://www.boost.org/)

    Reviewer of [Boost.Chrono][#BOOST_REVIEWER], a reference design of std::chrono in the standard C++ library.

  - [POCO C++ Libraries](http://pocoproject.org/)

    Contributed [Code][#POCO_CONTRIBUTOR] 

  - [The CodeProject](http://www.codeproject.com/)

    Authored [a few articals with code][#CP_AUTHOR]
  - [Github](https://github.com/rockonedege)
    Maintained [a few open source code](https://github.com/rockonedege)


- **When Not At Work**

  - **Family**
  - Reading
  - Running/Half-Marathon/Cycling
  - Travel


- **Web Pages**

  - [CV in English](https://ttan.netlify.com/cv-en)(**this document**)
  - [CV in Chinese](https://ttan.netlify.com/cv-zh)
  - [Feature Projects in English](https://ttan.netlify.com/projects-en)
  - [Feature Projects in Chinese](https://ttan.netlify.com/projects-zh)
  - [Linkedin Profile](www.linkedin.com/in/zhitan)


[#HIT]: http://en.hit.edu.cn
[#SECI]:http://www.schneider-electric.cn/zh/
[#ST]: http://www.st.com
[#HUSKY]:http:www.husky.co
[#BUYMF]: http://www.manufacturing.net/news/2007/06/husky-buying-moldflows-manufacturing-solutions-unit
[#POCO_CONTRIBUTOR]:  https://pocoproject.org/community/contributors.html
[#BOOST_REVIEWER]: http://www.boost.org/doc/libs/1_62_0/doc/html/chrono/appendices.html#chrono.appendices.acknowledgements
[#CP_AUTHOR]: http://www.codeproject.com/script/Articles/MemberArticles.aspx?amid=1960969
[#KENEXA]: http://www-01.ibm.com/software/smarterworkforce/
[#KEYCONSULTING]: http://www.keyconsulting.com.cn/en/
[#DP]: http://www.dediprog.com/




<hr class="page_break" />

## **Appendix**


- ![](./pics/husky-contribution.jpg)
- ![](./pics/husky-leadership.jpg)
- ![](./pics/kenexa-1.jpg)
- ![](./pics/kenexa-2.jpg)
- ![](./pics/mbti.istj-2.jpg)
- ![](./pics/unicef.jpg)
- ![](./pics/shanghai-half-marathon-2008.jpg)

-----
<small>Generated from <a href="http://daringfireball.net/projects/markdown/">Markdown</a> text using my open source tool <a href="https://github.com/rockonedege/markdown-cv">markdown-cv</a>. Last updated at 2018-01-11. </small>