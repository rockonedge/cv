import 'jquery'
import 'popper.js'
import './assets/js/bootstrap.min.js'

//css
import "./assets/css/blockquote.css";
import "./assets/css/markcv.css";
import "./assets/css/numbered_heading.css";

import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
