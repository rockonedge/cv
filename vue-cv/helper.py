from pathlib import Path
from os import listdir


assets = Path(__file__).parent/'src/assets'

lines = []
for root in [
  'bootswatch-4', 
  'md', 
  'pics',
  'css'
  ]:
  lines.append(f'//{root}')
  for f in  listdir(str(assets /root)):

    lines.append(f'import "../assets/{root}/{f}"')


lines = '\n'.join(lines)

vue = f"""<script>
{lines}

export default {{

  data() {{
    return {{}};
  }}
}};
</script>"""

(assets.parent / 'components/statics.vue').write_text(vue)

