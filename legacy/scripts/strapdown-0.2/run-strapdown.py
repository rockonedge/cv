import os
from os import path
from datetime import date


def this_folder():
    import os
    return os.path.dirname(os.path.abspath(__file__))

def par_folder():
    return path.join(this_folder(), '..')

def make_if_none(directory):
    import os
    if not os.path.exists(directory):
        os.makedirs(directory)

def run(name, theme):
    """ Generate Strapdownjs based html files.
    - https://github.com/arturadib/strapdown
    - http://strapdownjs.com/
    """
    

    def is_cv():
        return name.lower().startswith('cv')

    # markdown input
    def input(name):
        where = path.join(par_folder(), '..','markdown', name + '.md')

        with open(where, encoding='utf-8') as f:
            lines = f.readlines()

            if is_cv():                
                title = 'Resume &middot; Last updated at {date}'.format(
                        date = date.today())
                return title, ''.join(lines)

            else:                
                for line in lines:
                    if line.startswith('# '):
                        brand = line.partition(' ')[-1]
                        break

                title = '{brand} &middot; Zhi(Tom) Tan &middot; Last updated at {date}'.format(
                    brand = ' '.join(name.split('-')[:-1]).upper(),
                    date = date.today())

                lines = [ line # + '\n' if line.lstrip().startswith('* ') else line
                            for line in lines if not line.startswith('# ')]

                return title, '\n'.join(lines)       
                        
    def css(theme):

        css = []
        root = path.join(this_folder())
        for p in [
                path.join(root, 'themes', theme + '.min.css'),
                # path.join(root, 'bootstrap-responsive.min.css'),
                path.join(root, 'strapdown.css')
            ]:
            with open(p) as f:
                css.append('<style>{css}</style>'.format(css = f.read()))

        return '\n'.join(css)
    
    def js():

        js = []
        root = path.join(this_folder())
        for p in [
                path.join(root, 'marked.min.js'),
                # path.join(root, 'jquery.min.js'),
                path.join(root, 'prettify.min.js'),
                path.join(root, 'strapdown.js')
            ]:
            with open(p, encoding='utf-8') as f:
                js.append('<script>{js}</script>'.format(js = f.read()))

        return '\n'.join(js)

    def html(title, markdown, theme, flavor):

        with open(path.join(this_folder(), 'template.html')) as f:
            t = f.read()

        return t.format(
                title=title,
                flavor=flavor, #'primary', #inverse, faded
                markdown=markdown, 
                css=css(theme),
                js=js()
        )


    # html output
    def output(name, theme):

        #out
        for flavor in ['primary', 'inverse', 'faded']:

            out_folder = path.join(this_folder(), 'out-strapdown')
            out_file =  path.join(out_folder, '.'.join([name, theme, flavor, 'html']))
            make_if_none(out_folder)

            title, markdown = input(name)

            with open(out_file, 'w+', encoding='utf-8') as f:
                f.write(html(title, markdown, theme, flavor))
                print('done generating {file}'.format(file=out_file))

    output(name, theme)

# themes = ['Amelia', 
#     'Cerulean', 
#     'Cyborg',
#     'Journal',
#     'Readable',
#     'Simplex',
#     'Slate',
#     'Spacelab',
#     'Spruce',
#     'Superhero',
#     'United']


# ['amelia',
#  'bootstrap',
#  'cerulean',
#  'cosmo',
#  'custom',
#  'cyborg',
#  'darkly',
#  'flatly',
#  'journal',
#  'litera',
#  'lumen',
#  'lux',
#  'materia',
#  'minty',
#  'paper',
#  'pulse',
#  'readable',
#  'sandstone',
#  'simplex',
#  'slate',
#  'spacelab',
#  'spruce',
#  'superhero',
#  'united',
#  'yeti']

themes =[theme.partition('.')[0] for theme in os.listdir(path.join(this_folder(), 'themes')) if theme.endswith('css')]
    
for theme in themes:

    run('cv-en', theme)

    for f in [
            'projects-at-husky-en',
            'projects-prior-to-husky-en'
            ]:
            run(f, theme)

print(len(themes))
print([themes])