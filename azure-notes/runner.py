#!/usr/bin/python3

import os
import subprocess
import sys
from datetime import datetime
import threading


def worker(timeout):


  def bye():
    print("Bye, time's up.")
    os._exit(0)

  t = threading.Timer(timeout, bye)
  t.start() 

  cmd = input('Waiting for command(timeout after %ds):\n' %timeout)
  t.cancel()
  
  cmd = cmd.split(' ')
  if cmd:
    subprocess.call(cmd)
  else:
    print('No....Say something')


def main():
  while(True):
    worker(120)

if __name__ == '__main__':

    main()