
# Get Started


## for openvpn vm
- `sshfs tomgee@52.229.166.127:/home/tomgee /home/ttan/azr-vm/openvpn/`
- `ssh tomgee@52.229.166.127`

## for tdocker
- `sshfs tomgee@52.168.137.237:/home/tomgee /home/ttan/azr-vm/tdocker/`
- `ssh tomgee@52.168.137.237`

- use `scp` to copy file. e.g. `scp tomgee@52.229.166.127:~/refreship ./`
  - [link]https://unix.stackexchange.com/questions/106480/how-to-copy-files-from-one-machine-to-another-using-ssh()

    - 3 ways to copy files
    - scp
    - sshfs
      on the local machine
      - `sudo apt-get install sshfs`
      - `mkdir azr-vm-openvpn`
      - `sshfs tomgee@52.229.166.127:/home/tomgee /home/ttan/azr-vm-openvpn/`
        - N.B. `tomgee@52.229.166.127:~` or `tomgee@52.229.166.127:~/` does not work
        - "unlink" the dirs
          `fusermount -u /home/ttan/azr-vm/openvpn`
    - rsync


# wkhtmltopdf
- install from std ppa
- overwrite with the latest statble from the official site
  - `wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz`


    tar -xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

    ​

    ​

    tomgee@OpenVPN-VM:~$cd wkhtmltox/
    tomgee@OpenVPN-VM:~/wkhtmltox$ sudo cp -r ./ /usr/
    tomgee@OpenVPN-VM:~/wkhtmltox$ wkhtmltopdf -V
    wkhtmltopdf 0.12.4 (with patched qt)
    tomgee@OpenVPN-VM:~/wkhtmltox$ wkhtmltopdf --print-media-type --encoding utf-8 --no-background --quiet --footer-font-size 6 --footer-line --footer-left "(+86) 135-248-26926   tom.tan@live.com" --footer-right " [page]/[toPage]" --header-font-size 6 --header-right "[section]" --header-line  https://ttan.netlify.com/full-cv-en-zh.html?theme=43 full-cv-en-zh.pdf


tar -xvf test123.tar.gz

  x -- extract the file
  v -- verbose
  f -- forcefully done
[^](https://stackoverflow.com/questions/12298368/how-to-extract-tar-xz-files-in-linux)

> not working
> `add-apt-repository ppa:ecometrica/servers && \
    apt-get -y update && \
    apt-get install -y wkhtmltopdf `
>wrong way: no qt
 - [QXcbConnection: Could not connect to display](https://unix.stackexchange.com/questions/192642/wkhtmltopdf-qxcbconnection-could-not-connect-to-display)
  - fix 1:
  - install xvfb
  - wkhtmltopdf -> xvfb-run wkhtmltopdf

    xvfb-run wkhtmltopdf --print-media-type --encoding utf-8 --no-background --quiet --footer-font-size 6 --footer-line --footer-left "(+86) 135-248-26926   tom.tan@live.com" --footer-right " [page]/[toPage]" --header-font-size 6 --header-right "[section]" --header-line  https://ttan.netlify.com/full-cv-en-zh.html?theme=43 full-cv-en-zh.pdf

# websocketd
  - wget https://github.com/joewalnes/websocketd/releases/download/v0.2.11/websocketd-0.2.11-linux_amd64.zip



## Docker

`docker run` to start a __container__ from an image

`docker execute` to execute a command in a running container: e.g. to run as root

```sh
docker exec -u 0 -it mycontainer bash
```