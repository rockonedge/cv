@echo off

set theme=yeti
set key=full-cv-en-zh
set name=%key%.%theme%.faded
REM set src=.\build\output\%key%.%theme%.faded.html
set src=file:///F:/cv-static/mini-cv-zh.html?theme=4

set dest=.\%key%.%theme%.faded.pdf

if exist %dest% del %dest%

REM --footer-left "[date]" 
rem --quiet 

set misc_opt=--print-media-type --encoding utf-8 --no-background --enable-toc-back-links 
::--image-quality 5 --lowquality
set footer_opt=--footer-right "[page]/[toPage]" --footer-left "(+86) 135-248-26926  tom.tan@live.com" --footer-font-size 6 --footer-line
set header_opt=--header-font-size 6 --header-right "[section]" --header-line

set option= %misc_opt% %footer_opt% %header_opt%
wkhtmltopdf %option% %src% %dest%