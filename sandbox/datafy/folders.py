from pathlib import Path
from os.path import abspath

def this_folder():
    return Path(abspath(__file__)).parent

def src_md_folder():
    return this_folder().parent.parent / 'src'

def temp_folder():
    return this_folder()/ 'temp'

def by_filter_folder():
    return temp_folder() / 'by_filter'

def yaml_folder():
    return temp_folder() / 'with_yaml'

def target_md_folder():
    return temp_folder() / 'md'

def table_folder():
    return temp_folder() / 'table'

def make_if_none(directory):
    Path.mkdir(directory, exist_ok=True)

temp_folder().mkdir(exist_ok = True, parents=True)
yaml_folder().mkdir(exist_ok = True, parents=True)
target_md_folder().mkdir(exist_ok = True, parents=True)
table_folder().mkdir(exist_ok = True, parents=True)
by_filter_folder().mkdir(exist_ok = True, parents=True)
