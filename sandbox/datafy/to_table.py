from  pathlib import Path
import yaml
import os


from folders import this_folder, target_md_folder, yaml_folder,table_folder, make_if_none

def yaml_dump(y):
    return yaml.dump(y, default_flow_style=False, allow_unicode=True)

make_if_none(target_md_folder())
make_if_none(table_folder())


def split_file(name):

    import re

    content = name.read_text(encoding='utf-8')
    content = re.split(r'^---$', content, 2, re.MULTILINE)

    yml = content[-2] if len(content)  == 3 else ''
    md = content[-1].lstrip()

    return yaml.load(yml), md

def collect(root):
    r = []    
    for f in root.glob('**/*' + '.md'):
        # discard, header, body = f.read_text(encoding = 'utf-8').split('---', 2)
        # header = header.replace('---', '')
        # header = yaml.load(header)

        header, body = split_file(f)

        r.append({'header': header, 'body': body})

    # Path(this_folder() /'all.yml').write_text(yaml_dump(r), encoding='utf-8')
    return r


def get_formated_body(p):
    h = p['header']
    body = p['body'].strip()

    return body

    title = h['name']
    duration = h['duration']
    duration = ' - '.join([duration['start'], duration['end']])
    keywords = h.get('keywords', [])
    subtitle =  ' &middot; '.join([duration] + keywords)

    return f'''
## {title}
**{subtitle}**

{body}
'''

def stringfy(p):
    prj = p.copy()
    prj['keywords']  = ', '.join(prj['keywords'])
    duration = prj['duration']  
    print(duration, p['name'])
    prj['duration'] = ' - '.join([duration['start'], duration['end']])
    return prj.copy()

def to_table(prjs):
    tables = []
    header ='|No.|' 
    sep = '|---|'
    for col in prjs[0]:
        header += col + '|'
        sep += '---|'

    header = [header, sep]

    lines = []
    for i, prj in enumerate(prjs):
        fields = '|'.join(stringfy(prj).values())
        line = f'|{i}|{fields}|'
        # for v in : 
        #     print(v)
        #     line += v + '|'
        lines.append(line)
    return ['\n'] + header + lines  


def to_tables():
    prjs = [y['header'] for y in collect(yaml_folder())]
    prjs = sorted(prjs, key = lambda x: x['at'])
    prjs_en = [h for h in prjs if h['language'] == 'en']
    prjs_zh = [h for h in prjs if h['language'] == 'zh']


    prjs = to_table(prjs) + to_table(prjs_en) + to_table(prjs_zh)

    with (table_folder() /'table.md').open(mode='w', encoding='utf-8') as m:
        m.write('\n'.join(prjs))    

def by_project():

    for y in collect(yaml_folder()):

        name = y['header']['name']

        to = target_md_folder() / (name + '.md')
        to.write_text(get_formated_body(y), encoding='utf-8')    

def by_company():
    from itertools import groupby

    key = lambda y: y['header']['at']

    md = sorted( collect(yaml_folder()), key = key)

    for k, group in groupby(md, key = key):
        md = [get_formated_body(i) for i in group]


        # by_at = [get_formated_body(y for y in collect(yaml_folder()) if y['at']]
        to = target_md_folder() / (k + '.md')
        to.write_text('\n\n'.join(md), encoding='utf-8')

by_company()
by_project()
to_tables()
